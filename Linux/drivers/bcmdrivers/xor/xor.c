/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * This file implements xor offload (through the PAE)
 *
 */

#include <linux/err.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/scatterlist.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/semaphore.h>
#include <linux/rtnetlink.h>
#include <linux/raid/xor.h>
#include <linux/random.h>

#include <linux/dmaengine.h>
#include <linux/async_tx.h>
#include <drivers/dma/dmaengine.h>

#include "xor_util.h"
#include "../pae/paemsg/pae_xor.h"

#include "xor.h"


#define PAE_REG_BASE                                  0x18049000
#define PAE_INTERRUPT                                 PAE_REG_BASE
#define PAE_INTERRUPT_OFFSET                          0x0
#define PAE_INTERRUPT__PAE_MESSAGE_AVAIL_TO_HOST      (1 << 12)
#define PAE_INTERRUPT_MASK                            0x18049004
#define PAE_INTERRUPT_MASK_OFFSET                     0x4
#define PAE_INTERRUPT_MASK__PAE_MESSAGE_AVAIL_TO_HOST (1 << 12)
#define PAE_MESSAGE_OUT_MAILBOX                       0x18049008
#define PAE_MESSAGE_OUT_MAILBOX_OFFSET                0x8
#define PAE_MESSAGE_OUT_MAILBOX__MESSAGE_READY        (1 << 31)
#define PAE_MESSAGE_OUT_MAILBOX__MESSAGE              (1 << 0)
#define PAE_MESSAGE_OUT_MAILBOX__MESSAGE_WIDTH        30
#define PAE_MESSAGE_IN_MAILBOX                         0x1804900C
#define PAE_MESSAGE_IN_MAILBOX_OFFSET                  0xC
#define PAE_MESSAGE_IN_MAILBOX__MESSAGE_READY         (1 << 31)
#define PAE_MESSAGE_IN_MAILBOX__MESSAGE               (1 << 0)
#define PAE_MESSAGE_IN_MAILBOX__MESSAGE_WIDTH         30


/* ================= Device Structure ================== */

struct xor_device_private_s xor_priv;

static dma_cookie_t xor_async_tx_submit(struct dma_async_tx_descriptor *tx);
static int xor_xor_self_test(void);

int xor_enable_device(bool full);
void xor_disable_device(void);


/* ==================== Parameters ===================== */

struct xor_attr_s {
    struct attribute attr;
    int value;
};

static struct xor_attr_s xor_enable = {
    .attr.name="enable",
    .attr.mode = 0644,
    .value = 0,
};

static struct attribute * xor_attrs[] = {
    &xor_enable.attr,
    NULL
};


static ssize_t
xor_show(struct kobject *kobj, struct attribute *attr, char *buf)
{
    struct xor_attr_s *a = container_of(attr, struct xor_attr_s, attr);

    return scnprintf(buf, PAGE_SIZE, "%d\n", a->value);
}


/**
 * 0 - disable
 * 1 - enable
 * 2 - enable all DMA functionality (slower)
 **/
static ssize_t
xor_store(struct kobject *kobj, struct attribute *attr, const char *buf, size_t len)
{
    struct xor_attr_s *a = container_of(attr, struct xor_attr_s, attr);

    sscanf(buf, "%d", &a->value);

    if (a->value) {
        if (!xor_priv.enabled)
            xor_enable_device(a->value == 2);
        else
            pr_warn("iproc_xor: already enabled\n");
    } else {
        if (!xor_priv.enabled)
            pr_warn("iproc_xor: already disabled\n");
        else
            pr_warn("iproc_xor: cannot disable, must remove the module.\n");
    }

    return len;
}


int xor_debug_logging = 0;
int xor_logging_sleep = 0;

module_param(xor_debug_logging, int, 0644);
MODULE_PARM_DESC(xor_debug_logging, "Enable XOR Debug Logging");

module_param(xor_logging_sleep, int, 0644);
MODULE_PARM_DESC(xor_logging_sleep, "XOR Debug Logging Sleep");

unsigned xor_op_counts[6] = {0, 0, 0, 0, 0, 0};
unsigned long xor_bytes_through = 0;

module_param_array(xor_op_counts, uint, NULL, 0644);
MODULE_PARM_DESC(xor_op_counts, "XOR Operation Counts: xor");

module_param(xor_bytes_through, ulong, 0644);
MODULE_PARM_DESC(xor_bytes_through, "XOR Bytes Processed");


/* ==================== Queue Tasks  ==================== */

static
irqreturn_t
xor_irq_handler(int irq, void *dev_id)
{
    struct xor_chan_s *xor_chan = &xor_priv.xor_chan;
    struct xor_desc_s *desc = xor_chan->in_progress;
    struct dma_async_tx_descriptor *tx = &desc->async_tx;
    unsigned long flags;

    /* reset the message and notify flag _then_ reset the irq reg*/
    xor_reg_set32(xor_priv.base_ptr, PAE_MESSAGE_OUT_MAILBOX_OFFSET, 0x0);
    barrier();
    xor_reg_set32(xor_priv.base_ptr, PAE_INTERRUPT_OFFSET, 0x1000);

    if (!desc) {
        pr_err("iproc_xor: handler called without active descriptor\n");
        return IRQ_HANDLED;
    }

    /* Handle this processed desc if there is one */
    xor_log("%s done tx:%p cb:%p cb_p:%p\n",
            __func__, tx, desc->async_tx.callback, desc->async_tx.callback_param);

    dma_cookie_complete(&desc->async_tx);

    if (desc->async_tx.callback) {
        desc->async_tx.callback(desc->async_tx.callback_param);
    }

    dma_run_dependencies(&desc->async_tx);

    /* Unmap the buffers, if requested */
    if (!(tx->flags & DMA_COMPL_SKIP_DEST_UNMAP)) {
        if (tx->flags & DMA_COMPL_DEST_UNMAP_SINGLE) {
            dma_unmap_single(&xor_priv.pdev->dev, desc->dest, desc->len,
                             (desc->src_cnt > 1) ? DMA_BIDIRECTIONAL : DMA_FROM_DEVICE);
        } else {
            dma_unmap_page(&xor_priv.pdev->dev, desc->dest, desc->len,
                           (desc->src_cnt > 1) ? DMA_BIDIRECTIONAL : DMA_FROM_DEVICE);
        }
    }
    if (!(tx->flags & DMA_COMPL_SKIP_SRC_UNMAP)) {
        while (desc->src_cnt--) {
            if (desc->srcs[desc->src_cnt] == desc->dest) {
                continue;
            }

            if (tx->flags & DMA_COMPL_SRC_UNMAP_SINGLE) {
                dma_unmap_single(&xor_priv.pdev->dev, desc->srcs[desc->src_cnt], desc->len, DMA_TO_DEVICE);
            } else {
                dma_unmap_page(&xor_priv.pdev->dev, desc->srcs[desc->src_cnt], desc->len, DMA_TO_DEVICE);
            }
        }
    }

    spin_lock_irqsave(&xor_chan->desc_lock, flags);
    {
        /* move desc to the free pool*/
        list_add_tail(&desc->node, &xor_chan->free_q);
        xor_bytes_through += desc->len;

        /* if we have an element to process then get ready */
        if (!list_empty(&xor_chan->pending_q)) {
            desc = list_first_entry(&xor_chan->pending_q, struct xor_desc_s, node);
            list_del_init(&desc->node);
        } else {
            desc = NULL;
        }

        xor_chan->in_progress = desc;
    }
    spin_unlock_irqrestore(&xor_chan->desc_lock, flags);

    /* if we are ready to process something then kick it off */
    if (desc) {
        xor_log("%s run tx:%p\n", __func__, &desc->async_tx);
        pae_do_xor(desc->src_cnt, desc->len, desc->dest, desc->srcs);
    }

    return IRQ_HANDLED;
}


/* ==================== Helper Functions ===================== */

static
struct xor_desc_s *
xor_alloc_descriptor(struct xor_chan_s *xor_chan, gfp_t flags)
{   
    struct xor_desc_s *desc;

    desc = kmalloc(sizeof(*desc), flags);
    if (desc) {
        xor_chan->total_desc++;
        desc->async_tx.tx_submit = xor_async_tx_submit;

        INIT_LIST_HEAD(&desc->node);
    }

    xor_log("%s made tx:%p\n", __func__, &desc->async_tx);

    return desc;
}


/**
 * @device_alloc_chan_resources: allocate resources and return the
 *  number of allocated descriptors
 */
static
int
xor_alloc_chan_resources(struct dma_chan *chan)
{
    struct xor_chan_s *xor_chan = container_of(chan, struct xor_chan_s, common);
    struct xor_desc_s *desc;
    LIST_HEAD(tmp_list);
    int i, added = 0;
    unsigned long flags;

    xor_log("%s total:%u\n", __func__, xor_chan->total_desc);

    if (!list_empty(&xor_chan->free_q))
        return xor_chan->total_desc;

    for (i = 0; i < XOR_MAX_DESCRIPTORS; i++) {
        desc = xor_alloc_descriptor(xor_chan, GFP_ATOMIC);
        if (!desc) {
            dev_err(xor_chan->common.device->dev, "Only %d initial descriptors\n", i);
            break;
        }
        added++;
        list_add_tail(&desc->node, &tmp_list);
    }

    if (!i)
        return -ENOMEM;

    /* At least one desc is allocated */
    if (added) {
        spin_lock_irqsave(&xor_chan->desc_lock, flags);
        {
            list_splice_init(&tmp_list, &xor_chan->free_q);
        }
        spin_unlock_irqrestore(&xor_chan->desc_lock, flags);
    }

    xor_log("%s returning total:%u\n", __func__, xor_chan->total_desc);

    return xor_chan->total_desc;
}


/**
 * @device_free_chan_resources: release DMA channel's resources       
 */
static
void
xor_free_chan_resources(struct dma_chan *chan)
{
    struct xor_chan_s *xor_chan = container_of(chan, struct xor_chan_s, common);
    struct xor_desc_s *desc, *_desc;
    unsigned long flags;

    xor_log("%s chan:%p\n", __func__, chan);

    spin_lock_irqsave(&xor_chan->desc_lock, flags);
    {
        list_for_each_entry_safe(desc, _desc, &xor_chan->free_q, node) {
            list_del(&desc->node);
            xor_chan->total_desc--;
            kfree(desc);
        }
        list_for_each_entry_safe(desc, _desc, &xor_chan->submit_q, node) {
            list_del(&desc->node);
            xor_chan->total_desc--;
            kfree(desc);
        }
        list_for_each_entry_safe(desc, _desc, &xor_chan->pending_q, node) {
            list_del(&desc->node);
            xor_chan->total_desc--;
            kfree(desc);
        }
        if (xor_chan->in_progress) {
            xor_chan->total_desc--;
            kfree(xor_chan->in_progress);
            xor_chan->in_progress = NULL;
        }
    }
    spin_unlock_irqrestore(&xor_chan->desc_lock, flags);

    WARN_ON(xor_chan->total_desc != 0);

    /* Some descriptor not freed? */
    if (unlikely(xor_chan->total_desc))
        pr_warn("iproc_xor: Failed to free xor channel resource\n");
}


/**
 * xor_status: poll for transaction completion, the optional
 *  txstate parameter can be supplied with a pointer to get a
 *  struct with auxiliary transfer status information, otherwise the call
 *  will just return a simple status code
 * @chan: XOR channel handle
 * @cookie: XOR transaction identifier
 * @txstate: XOR transactions state holder (or NULL)
 */
static
enum dma_status
xor_status(struct dma_chan *chan, dma_cookie_t cookie, struct dma_tx_state *txstate)
{
    enum dma_status ret;

    ret = dma_cookie_status(chan, cookie, txstate);

    xor_log("%s cookie:%u txstate:%p c-cookie:%u c-done:%u ret:%d\n",
            __func__, cookie, txstate, chan->cookie, chan->completed_cookie, ret);

    return ret;
}


/**
 * xor_issue_pending: push the submited transactions to "hardware".  Moves
 * descriptors in submit_q to pending_q and starts the processing
 * @chan: DMA channel                           
 */
static
void
xor_issue_pending(struct dma_chan *chan)
{
    struct xor_chan_s *xor_chan = container_of(chan, struct xor_chan_s, common);
    struct xor_desc_s *desc = NULL;
    unsigned long flags;

    spin_lock_irqsave(&xor_chan->desc_lock, flags);
    {
        /* add the submit_q to the pending_q */
        list_splice_tail_init(&xor_chan->submit_q, &xor_chan->pending_q);

        /* if we have an element to process and are not already processing then do it */
        if (!xor_chan->in_progress && !list_empty(&xor_chan->pending_q)) {
            desc = list_first_entry(&xor_chan->pending_q, struct xor_desc_s, node);
            list_del_init(&desc->node);

            /* set in progress (we will kick-off in a sec) */
            xor_chan->in_progress = desc;
        }
    }
    spin_unlock_irqrestore(&xor_chan->desc_lock, flags);

    xor_log("%s run tx:%p\n", __func__, &desc->async_tx);

    if (desc) {
        pae_do_xor(desc->src_cnt, desc->len, desc->dest, desc->srcs);
    }
}


/**
 * xor_async_tx_submit: set the prepared descriptor(s) to be executed
 * by the engine.
 * @tx: descriptor to move to submit_q
 */
static
dma_cookie_t
xor_async_tx_submit(struct dma_async_tx_descriptor *tx)
{   
    struct xor_desc_s *desc = container_of(tx, struct xor_desc_s, async_tx);
    struct xor_chan_s *xor_chan = container_of(tx->chan, struct xor_chan_s, common);
    dma_cookie_t cookie;
    unsigned long flags;

    xor_log("%s tx:%p\n", __func__, tx);

    spin_lock_irqsave(&xor_chan->desc_lock, flags);
    {
        xor_op_counts[0] += 1;

        cookie = dma_cookie_assign(tx);

        list_add_tail(&desc->node, &xor_chan->submit_q);
    }
    spin_unlock_irqrestore(&xor_chan->desc_lock, flags);

    return cookie;
}


/**
 * xor_prep_dma_xor: prepares a xor operation
 */
static
struct dma_async_tx_descriptor *
xor_prep_dma_xor(struct dma_chan *chan, dma_addr_t dest, dma_addr_t *src,
                 unsigned int src_cnt, size_t len, unsigned long dma_flags)
{
    struct xor_chan_s *xor_chan = container_of(chan, struct xor_chan_s, common);
    struct xor_desc_s *desc = NULL;
    int i;
    unsigned long flags;

    BUG_ON(len < XOR_MIN_BYTE_COUNT);
    BUG_ON(len > XOR_MAX_BYTE_COUNT);

    spin_lock_irqsave(&xor_chan->desc_lock, flags);
    {
         if (!list_empty(&xor_chan->free_q)) {
            desc = list_first_entry(&xor_chan->free_q, struct xor_desc_s, node);
            list_del_init(&desc->node);
        }
    }
    spin_unlock_irqrestore(&xor_chan->desc_lock, flags);

    if (!desc) {
        desc = xor_alloc_descriptor(xor_chan, GFP_ATOMIC);
    }
    if (!desc) {
        pr_err("iproc_xor: failed to allocate new tx_desc\n");
        return NULL;
    }

    xor_log("%s tx:%p dest:%08x (virt:%p) src_cnt:%d len:%u dma_flags:%08lx\n",
            __func__, &desc->async_tx, dest, bus_to_virt(dest), src_cnt, len, dma_flags);
    for (i = 0; i < src_cnt; i++)
        xor_log("  src[%d]: phy:%08x (virt:%p)\n", i, src[i], bus_to_virt(src[i]));

    dma_async_tx_descriptor_init(&desc->async_tx, &xor_chan->common);

    desc->src_cnt = src_cnt;
    desc->len = len;
    desc->dest = dest;
    for (i = 0; i < src_cnt; i++) {
        desc->srcs[i] = src[i];
    }

    desc->async_tx.parent = NULL;
    desc->async_tx.next = NULL;
    desc->async_tx.callback = NULL;
    desc->async_tx.callback_param = NULL;

    desc->async_tx.flags = dma_flags;

    return &desc->async_tx;
}


static struct dma_async_tx_descriptor *
xor_prep_dma_interrupt(struct dma_chan *chan, unsigned long flags)
{
    xor_log("%s\n", __func__);
    return NULL;
}
static struct dma_async_tx_descriptor *
xor_prep_dma_memcpy(struct dma_chan *chan, dma_addr_t dest, dma_addr_t src,
                    size_t len, unsigned long flags)
{
    xor_log("%s\n", __func__);
    return NULL;
}
static struct dma_async_tx_descriptor *
xor_prep_dma_memset(struct dma_chan *chan, dma_addr_t dest, int value,
                       size_t len, unsigned long flags)
{
    xor_log("%s\n", __func__);
    return NULL;
}
static struct dma_async_tx_descriptor *
xor_prep_dma_xor_val(struct dma_chan *chan, dma_addr_t *src, unsigned int src_cnt,
                     size_t len, enum sum_check_flags *result, unsigned long flags)
{
    xor_log("%s\n", __func__);
    return NULL;
}
static struct dma_async_tx_descriptor *
xor_prep_dma_pq(struct dma_chan *chan, dma_addr_t *dst, dma_addr_t *src,
                unsigned int src_cnt, const unsigned char *scf,
                size_t len, unsigned long flags)
{
    xor_log("%s\n", __func__);
    return NULL;
}
static struct dma_async_tx_descriptor *
xor_prep_dma_pq_val(struct dma_chan *chan, dma_addr_t *pq, dma_addr_t *src,
                        unsigned int src_cnt, const unsigned char *scf, size_t len,
                        enum sum_check_flags *pqres, unsigned long flags)
{
    xor_log("%s\n", __func__);
    return NULL;
}


/* ==================== Self Tests ==================== */

static void
do_sync_xor(struct page *dest, struct page **src_list, unsigned src_cnt, size_t len)
{
    void *dest_buf;
    void *srcs[XOR_SELF_TEST_NUM_SRCS];
    int xor_src_cnt = 0;
    int src_off = 0;
    unsigned i;

    BUG_ON(src_cnt > XOR_SELF_TEST_NUM_SRCS);
    BUG_ON(src_cnt < 1);

    /* convert to buffer pointers */
    for (i = 0; i < src_cnt; i++)
        srcs[i] = page_address(src_list[i]);
    dest_buf = page_address(dest);

    xor_log("%s src_cnt:%d src_off:%d dest[0]:%08x\n", __func__, src_cnt, src_off, ((u32*)dest_buf)[0]);

    while (src_cnt > 0) {
        /* process up to 'MAX_XOR_BLOCKS' sources */
        xor_src_cnt = (src_cnt > MAX_XOR_BLOCKS) ? MAX_XOR_BLOCKS : src_cnt;
        xor_blocks(xor_src_cnt, len, dest_buf, &srcs[src_off]);

        /* drop completed sources */
        src_cnt -= xor_src_cnt;
        src_off += xor_src_cnt;
    }
}


static
int xor_do_test(unsigned src_cnt, struct page *dest, struct page** srcs)
{
    int err = 0;
    int i;
    int src_idx;
    struct page *test_dest, *saved_dest;
    struct dma_async_tx_descriptor *tx;
    addr_conv_t addr_conv[src_cnt];
    struct async_submit_ctl submit;

    xor_log("%s src_cnt:%d dest virt:%p\n", __func__, src_cnt, page_address(dest));
    for (i = 0; i < src_cnt; i++)
        xor_log("  src[%d]: virt:%p\n", i, page_address(srcs[i]));

    /* create the necessary scratch buffers for testing */
    test_dest = alloc_page(GFP_KERNEL);
    if (!test_dest) {
        pr_err("iproc_xor: failed test_dest allocation\n");
        return 1;
    }
    saved_dest = alloc_page(GFP_KERNEL);
    if (!saved_dest) {
        pr_err("iproc_xor: failed saved_dest allocation\n");
        __free_page(test_dest);
        return 1;
    }

    /* set srcs */
    for (src_idx = 0; src_idx < src_cnt; src_idx++)
        for (i = 0; i < PAGE_SIZE/sizeof(u32); i++) {
            u32 val = random32();
            u32 *ptr = page_address(srcs[src_idx]);
            ptr[i] = val;
        }

    /* generate a test result using the kernel sw sync xor alg */
    if (srcs[0] == dest) {
        /* for sync XOR with dest in srcs */
        memcpy(page_address(saved_dest), page_address(srcs[0]), PAGE_SIZE); /* save the src/dest */
        do_sync_xor(dest, &srcs[1], src_cnt - 1, PAGE_SIZE);                /* generate result */
        memcpy(page_address(test_dest), page_address(dest), PAGE_SIZE);     /* save result */
        memcpy(page_address(srcs[0]), page_address(saved_dest), PAGE_SIZE); /* restore saved src/dest */
    } else {
        /* for sync XOR with separate dest */
        memset(page_address(dest), 0, PAGE_SIZE);                       /* zero the dest for sync xor */
        do_sync_xor(dest, srcs, src_cnt, PAGE_SIZE);                    /* generate result */
        memcpy(page_address(test_dest), page_address(dest), PAGE_SIZE); /* save result */
    }

    /* use the driver and run the test */
    init_async_submit(&submit, 0, NULL, NULL, NULL, addr_conv);
    tx = async_xor(dest, srcs, 0, src_cnt, PAGE_SIZE, &submit);
    async_tx_quiesce(&tx);

    /* check results */
    {
        u32 *ptr1 = page_address(dest);
        u32 *ptr2 = page_address(test_dest);
        unsigned j;

        for (i = 0; i < (PAGE_SIZE / sizeof(u32)); i++) {
            if (ptr1[i] != ptr2[i]) {
                pr_err("iproc_xor: Self-test failed src_cnt:%d dest:%p idx:%u dest:%08x expected:%08x\n",
                       src_cnt, ptr1, i, ptr1[i], ptr2[i]);
                for (j = 0; j < src_cnt; j++) {
                    pr_err("  src[%u]:%p val[%u]:0x%08x\n", j, page_address(srcs[j]), i, ((u32*)page_address(srcs[j]))[i]);
                }
                pr_err("  saved val[%u]:0x%08x\n", i, ((u32*)page_address(saved_dest))[i]);
                err++;
                goto do_test_free_resources;
            }
        }
    }

do_test_free_resources:

    xor_log("%s done srcs:%d dest:%p\n\n", __func__, src_cnt, page_address(dest));

    __free_page(saved_dest);
    __free_page(test_dest);

    return err;
}


static
int
xor_xor_self_test(void)
{
    int src_cnt, iter, src_idx;
    int err = 0;
    struct page *pages[XOR_SELF_TEST_NUM_SRCS + 1];

    xor_log("%s\n", __func__);

    /* The zeroth xor_src will be the dest.  Normal tests start from src[1] to src[N] */
    /* If you want to test an overlapping scenarios then start from src[0] */
    for (src_idx = 0; src_idx < XOR_SELF_TEST_NUM_SRCS + 1; src_idx++) {
        pages[src_idx] = alloc_page(GFP_KERNEL);
        if (!pages[src_idx]) {
            while (src_idx--)
                __free_page(pages[src_idx]);
            return -ENOMEM;
        }
    }

    for (iter = 0; iter < 5; iter++) {
        for (src_cnt = 2; src_cnt <= XOR_SELF_TEST_NUM_SRCS; src_cnt++) {
            err += xor_do_test(src_cnt, pages[0], &pages[1]); /* non-overlap case */
            err += xor_do_test(src_cnt, pages[0], &pages[0]); /* overlap case */
        }
    }

    xor_log("%s self-test completed %u failures.\n", __func__, err);

    while (src_idx--) {
        __free_page(pages[src_idx]);
    }
    return err;
}


/* ==================== Kernel Platform API ==================== */

int
xor_enable_device(bool full)
{
    struct dma_device *dma_dev = &xor_priv.dma_dev;
    const char *name           = xor_priv.pdev->name;
    unsigned irq = 204;
    int rc = 0;
    uint32_t irq_mask = PAE_INTERRUPT_MASK__PAE_MESSAGE_AVAIL_TO_HOST;

    xor_log("%s\n", __func__);

    xor_priv.base_ptr = ioremap(PAE_REG_BASE, 32);
    if (!xor_priv.base_ptr) {
        pr_err("iproc_xor: ioremap of register space failed\n");
        rc = -ENOMEM;
        goto xor_enable_unmap;
    }
    xor_log("%s got base addr:%p\n", __func__, xor_priv.base_ptr);    

    if (full) {
        xor_log("%s adding full (slow) callbacks.\n", __func__);

        dma_cap_set(DMA_INTERRUPT, dma_dev->cap_mask);
        dma_dev->device_prep_dma_interrupt = xor_prep_dma_interrupt;
        dma_cap_set(DMA_MEMCPY, dma_dev->cap_mask);
        dma_dev->device_prep_dma_memcpy = xor_prep_dma_memcpy;
        dma_cap_set(DMA_MEMSET, dma_dev->cap_mask);
        dma_dev->device_prep_dma_memset = xor_prep_dma_memset;
        dma_cap_set(DMA_XOR_VAL, dma_dev->cap_mask);
        dma_dev->device_prep_dma_xor_val = xor_prep_dma_xor_val;
        dma_cap_set(DMA_PQ, dma_dev->cap_mask);
        dma_dev->device_prep_dma_pq = xor_prep_dma_pq;
        dma_cap_set(DMA_PQ_VAL, dma_dev->cap_mask);
        dma_dev->device_prep_dma_pq_val = xor_prep_dma_pq_val;
    }

    xor_log("%s enabling dma registration.\n", __func__);
    rc = dma_async_device_register(&xor_priv.dma_dev);
    if (rc) {
        pr_err("iproc_xor: failed to register slave DMA engine device: %d\n", rc);
        goto xor_enable_unmap;
    }

    /* clear the message & irq regs, then set mask */
    xor_reg_set32(xor_priv.base_ptr, PAE_MESSAGE_OUT_MAILBOX_OFFSET, 0x0);
    xor_reg_set32(xor_priv.base_ptr, PAE_MESSAGE_IN_MAILBOX_OFFSET, 0x0);
    xor_reg_set32(xor_priv.base_ptr, PAE_INTERRUPT_OFFSET, 0x0);
    barrier();
    xor_reg_set32(xor_priv.base_ptr, PAE_INTERRUPT_MASK_OFFSET, irq_mask);
    barrier();

    /* enable interrupt handler for XOR communications */
    rc = request_irq(irq, xor_irq_handler, 0, name, NULL);
    if (rc) {
        pr_err("iproc_xor: Failed to register %s irq %d\n", name, irq);
        goto xor_enable_unreg;
    }

    xor_priv.enabled = true;

    return xor_xor_self_test();

xor_enable_unmap:
    iounmap(xor_priv.base_ptr);
xor_enable_unreg:
    xor_priv.dma_dev.chancnt = 0;
    dma_async_device_unregister(&xor_priv.dma_dev);
    return rc;
}


static
int
__devinit iproc_xor_probe(struct platform_device *pdev)
{
    struct dma_device *dma_dev  = &xor_priv.dma_dev;
    struct xor_chan_s *xor_chan = &xor_priv.xor_chan;
    int rc = 0;

    pr_debug("iproc_xor: %s() pdev:%p\n", __func__, pdev);

    xor_priv.enabled = false;

    INIT_LIST_HEAD(&dma_dev->channels);

    /* set base routines and capabilities*/
    dma_dev->dev = &pdev->dev;
    dma_dev->device_alloc_chan_resources = xor_alloc_chan_resources;
    dma_dev->device_free_chan_resources = xor_free_chan_resources;
    dma_dev->device_tx_status = xor_status;
    dma_dev->device_issue_pending = xor_issue_pending;

    dma_cap_set(DMA_XOR, dma_dev->cap_mask);
    dma_dev->device_prep_dma_xor = xor_prep_dma_xor;
    dma_dev->max_xor = XOR_MAX_SOURCES;

    xor_chan->common.device = dma_dev;

    INIT_LIST_HEAD(&xor_chan->submit_q);
    INIT_LIST_HEAD(&xor_chan->pending_q);
    INIT_LIST_HEAD(&xor_chan->free_q);

    spin_lock_init(&xor_chan->desc_lock);
    
    dma_cookie_init(&xor_chan->common);

    list_add_tail(&xor_chan->common.device_node, &dma_dev->channels);

    return rc;
}


static
int
__devexit iproc_xor_remove(struct platform_device *pdev)
{
    struct xor_chan_s *xor_chan;
    struct dma_chan *chan, *_chan;

    pr_debug("iproc_xor: remove() pdev:%p\n", pdev);

    if (xor_priv.enabled) {
        xor_priv.enabled = false;

        free_irq(204, NULL);

        iounmap(xor_priv.base_ptr);

        list_for_each_entry_safe(chan, _chan, &xor_priv.dma_dev.channels, device_node) {
            xor_chan = container_of(chan, struct xor_chan_s, common);
            list_del(&chan->device_node);

            xor_log("  found chan:%p with %d descs\n", chan, xor_chan->total_desc);

            xor_free_chan_resources(chan);
            dma_release_channel(chan);
            kfree(xor_chan);
        }
        xor_priv.dma_dev.chancnt = 0;

        dma_async_device_unregister(&xor_priv.dma_dev);
    }

    pr_debug("iproc_xor: remove() done\n");

    return 0;
}


/* ===== Kernel Module API ===== */

static struct platform_driver xor_pdriver = { 
    .driver = {
        .name   = "iproc_xor",
        .owner  = THIS_MODULE,
    },
    .probe  = iproc_xor_probe,
    .remove = iproc_xor_remove,
    .suspend = NULL,
    .resume  = NULL,
};

static struct sysfs_ops xor_ops = {
    .show = xor_show,
    .store = xor_store,
};

static struct kobj_type xor_type = {
    .sysfs_ops = &xor_ops,
    .default_attrs = xor_attrs,
};

struct kobject *xor_kobj;


static
int
__init iproc_xor_init(void)
{
    int rc;

    pr_debug("iproc_xor: loading driver\n");
 
    xor_kobj = kzalloc(sizeof(*xor_kobj), GFP_KERNEL);
    if (!xor_kobj) {
        pr_err("%s: Sysfs alloc failed\n", __func__);
        rc = -ENOMEM;
        goto alloc_failed;
    }
            
    kobject_init(xor_kobj, &xor_type);
    if (kobject_add(xor_kobj, NULL, "%s", "iproc_xor")) {
        pr_err("%s: Sysfs creation failed\n", __func__);
        rc = -1;
        goto sysfs_failed;
    }   

    rc = platform_driver_register(&xor_pdriver);

    if (rc < 0) {
        pr_err("%s: Driver registration failed, error %d\n", __func__, rc);
        goto driver_reg_failed;
    }

    xor_priv.pdev = platform_device_register_simple("iproc_xor", -1, NULL, 0);

    if (!xor_priv.pdev) {
        rc = -EINVAL;
        goto device_reg_failed;
    }
 
    platform_set_drvdata(xor_priv.pdev, &xor_priv);

    return 0;

device_reg_failed:
    platform_driver_unregister(&xor_pdriver);
driver_reg_failed:
sysfs_failed:
    kobject_put(xor_kobj);
    xor_kobj = NULL;
alloc_failed:
    return rc;
}


static
void
__exit iproc_xor_exit(void)
{
    platform_device_unregister(xor_priv.pdev);
    platform_driver_unregister(&xor_pdriver);

    if (xor_kobj) {
        kobject_put(xor_kobj);
        kfree(xor_kobj);
    }
}


module_init(iproc_xor_init);
module_exit(iproc_xor_exit);

MODULE_DESCRIPTION("iProc XOR offload support.");
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Broadcom Corporation");
