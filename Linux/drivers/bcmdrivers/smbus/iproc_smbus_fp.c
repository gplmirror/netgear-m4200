/*
 * Copyright (C) 2015, Broadcom Corporation. All Rights Reserved.
 * Dante Su <dantesu@broadcom.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/io.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include <linux/platform_device.h>
#include <linux/i2c-algo-bit.h>

#define DRV_NAME "iproc-smb"

#ifndef setbits_le32
#define setbits_le32(reg, val)  writel(readl(reg) | (val), reg)
#endif

#ifndef clrbits_le32
#define clrbits_le32(reg, val)  writel(readl(reg) & ~(val), reg)
#endif

struct iproc_i2c_regs {
	uint32_t cfg;     /* configuration register */
	uint32_t tcfg;    /* timing configuration register */
	uint32_t sacr;    /* slave address control register */
	uint32_t mfcr;    /* master fifo control register */

	uint32_t sfcr;    /* slave fifo control register */
	uint32_t bbcr;    /* bit-bang control register */
	uint32_t rsvd[6];

	uint32_t mcr;     /* master command register */
	uint32_t scr;     /* slave command register */
	uint32_t ier;     /* interrupt enable register */
	uint32_t isr;     /* interrupt status register */

	uint32_t mdwr;    /* master data write register */
	uint32_t mdrr;    /* master data read register */
	uint32_t sdwr;    /* slave data write register */
	uint32_t sdrr;    /* slave data read register */
};

/* Configuration register */
#define CFG_RESET               0x80000000
#define CFG_ENABLE              0x40000000
#define CFG_BITBANG             0x20000000

/* Timing configuration register */
#define TCFG_400KHZ             0x80000000

/* Master fifo control register */
#define MFCR_RXCLR              0x80000000
#define MFCR_TXCLR              0x40000000
#define MFCR_RXPKT(x)           (((x) >> 16) & 0x7f)
#define MFCR_RXTHR(x)           (((x) & 0x3f) << 8)

/* Bit-Bang control register */
#define BBCR_SCLIN              0x80000000
#define BBCR_SCLOUT             0x40000000
#define BBCR_SDAIN              0x20000000
#define BBCR_SDAOUT             0x10000000

/* Master command register */
#define MCR_START               0x80000000 /* Send START (busy) */
#define MCR_ABORT               0x40000000
#define MCR_PROTO(x)            (((x) & 0x0f) << 9) /* SMBus protocol */
#define MCR_PEC                 0x00000100 /* Packet Error Check */
#define MCR_RXLEN(x)            ((x) & 0xff)

/* Master command status */
#define MCR_STATUS(x)           (((x) >> 25) & 7)
#define MCR_STATUS_SUCCESS      0
#define MCR_STATUS_LOST_ARB     1
#define MCR_STATUS_NACK_FB      2 /* NACK on the 1st byte */
#define MCR_STATUS_NACK_NFB     3 /* NACK on the non-1st byte */
#define MCR_STATUS_TIMEOUT      4
#define MCR_STATUS_TX_TLOW_MEXT 5
#define MCR_STATUS_RX_TLOW_MEXT 6

/* Master SMBus protocol type */
#define MCR_PROT_QUICK_CMD               0
#define MCR_PROT_SEND_BYTE               1
#define MCR_PROT_RECV_BYTE               2
#define MCR_PROT_WR_BYTE                 3
#define MCR_PROT_RD_BYTE                 4
#define MCR_PROT_WR_WORD                 5
#define MCR_PROT_RD_WORD                 6
#define MCR_PROT_BLK_WR                  7
#define MCR_PROT_BLK_RD                  8
#define MCR_PROT_PROC_CALL               9
#define MCR_PROT_BLK_WR_BLK_RD_PROC_CALL 10

/* Interrupt status (Master only) */
#define IRQ_RXFF      0x80000000 /* rx fifo full */
#define IRQ_RXFT      0x40000000 /* rx fifo threshold */
#define IRQ_RX        0x20000000
#define IRQ_BUSY      0x10000000
#define IRQ_TXUR      0x08000000 /* tx underrun */
#define IRQ_MASK      0xf8000000

/* Tx/Rx data control */
#define MDWR_END      0x80000000 /* end of transfer */
#define MDRR_END      0x80000000 /* end of transfer */
#define MDRR_NACK     0x40000000

struct iproc_i2c_chip {
	struct i2c_algo_bit_data bit_data;

	struct device           *dev;
	void __iomem            *base;
	struct resource         *res;
	int                      irq;
	struct i2c_adapter       adap;
	spinlock_t               lock;
	wait_queue_head_t        wait;

	struct {
		int timeout;
		int bus_error;
		int sda_failure;
		int sda_success;
	} recovery;
};

#ifdef CONFIG_LVL7_IPROC_I2C_MIXED
static int iproc_i2c_bus_reset(struct iproc_i2c_chip *chip)
{
	struct iproc_i2c_regs *regs = chip->base;
	int ret, i;

	/* enable bit-bang */
	if (!(readl(&regs->cfg) & CFG_BITBANG)) {
		setbits_le32(&regs->cfg, CFG_BITBANG);
		udelay(60);
	}

	/* set SDA=H, SCL=H */
	setbits_le32(&regs->bbcr, BBCR_SCLOUT | BBCR_SDAOUT);
	udelay(5);

	/* generate 9 dummy clocks (8-bits data + 1-bit NACK) */
	for (i = 0; i < 18; ++i) {
		if (i & 1)
			setbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: high */
		else
			clrbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: low */
		udelay(5);
	}

	/* generate a STOP signal */
	clrbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: low */
	udelay(2);
	clrbits_le32(&regs->bbcr, BBCR_SDAOUT); /* SDA: low */
	udelay(3);
	setbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: high */
	udelay(2);
	setbits_le32(&regs->bbcr, BBCR_SDAOUT); /* SDA: high */
	udelay(5);

	/* disable bit-bang control */
	clrbits_le32(&regs->cfg, CFG_BITBANG);
	udelay(60);

	/* Now SDA should be at level high */
	if (readl(&regs->bbcr) & BBCR_SDAIN) {
		ret = 0;
		++chip->recovery.sda_success;
	} else {
		ret = -1;
		++chip->recovery.sda_failure;
		dev_info(chip->dev, "bus reset failed\n");
	}

	return ret;
}
#endif /* CONFIG_LVL7_IPROC_I2C_MIXED */

#ifdef CONFIG_LVL7_IPROC_I2C_BUSTST
/*
 * Sanity check for the adapter hardware - check the reaction of
 * the bus lines only if it seems to be idle.
 */
static void iproc_i2c_bus_test_reset(struct iproc_i2c_chip *chip)
{
	struct iproc_i2c_regs *regs = chip->base;
	int scl, sda, i;

	/* enable bit-bang */
	setbits_le32(&regs->cfg, CFG_BITBANG);
	udelay(60);

	sda = (readl(&regs->bbcr) & BBCR_SDAIN) ? 1 : 0;
	scl = (readl(&regs->bbcr) & BBCR_SCLIN) ? 1 : 0;
	if (!scl || !sda) {
		dev_warn(chip->dev, "bus seems to be busy (scl=%d, sda=%d)\n",
		         scl, sda);
		goto bailout;
	}

	clrbits_le32(&regs->bbcr, BBCR_SDAOUT);
	udelay(5);
	sda = (readl(&regs->bbcr) & BBCR_SDAIN) ? 1 : 0;
	scl = (readl(&regs->bbcr) & BBCR_SCLIN) ? 1 : 0;
	if (sda) {
		dev_warn(chip->dev, "SDA stuck high!\n");
		goto bailout;
	}
	if (!scl) {
		dev_warn(chip->dev, "SCL unexpected low while pulling SDA low!\n");
		goto bailout;
	}

	setbits_le32(&regs->bbcr, BBCR_SDAOUT);
	udelay(5);
	sda = (readl(&regs->bbcr) & BBCR_SDAIN) ? 1 : 0;
	scl = (readl(&regs->bbcr) & BBCR_SCLIN) ? 1 : 0;
	if (!sda) {
		dev_warn(chip->dev, "SDA stuck low!\n");
		goto bailout;
	}
	if (!scl) {
		dev_warn(chip->dev, "SCL unexpected low while pulling SDA high!\n");
		goto bailout;
	}

	/* disable bit-bang control */
	clrbits_le32(&regs->cfg, CFG_BITBANG);
	udelay(60);
	return;

bailout:
	iproc_i2c_bus_reset(chip);
}
#endif /* #ifdef CONFIG_LVL7_IPROC_I2C_BUSTST */

static int iproc_i2c_chip_reset(struct iproc_i2c_chip *chip)
{
	struct iproc_i2c_regs *regs = chip->base;
	unsigned long val;

	/* chip reset */
	writel(CFG_RESET, &regs->cfg);
	udelay(50);

	/* clock setup */
	val = readl(&regs->tcfg);
#ifdef CONFIG_LVL7_IPROC_I2C_400KHZ
	val |= TCFG_400KHZ;
#else  /* 100k Hz */
	val &= ~TCFG_400KHZ;
#endif
	writel(val, &regs->tcfg);

	/* chip enable + 15 times of hardware retries */
	writel(CFG_ENABLE | (15 << 16), &regs->cfg);

	/* 50us delay as per HW spec. */
	udelay(50);

	return 0;
}

#ifdef CONFIG_LVL7_IPROC_I2C_MIXED
static int iproc_i2c_poll(struct iproc_i2c_chip *chip)
{
	struct iproc_i2c_regs *regs = chip->base;
	uint32_t val;
	int ret = 0;

	val = readl(&regs->mcr);
	if (val & MCR_START)
		ret = -EAGAIN;
	else if (MCR_STATUS(val) != MCR_STATUS_SUCCESS)
		ret = -EREMOTEIO;

	return ret;
}

static irqreturn_t iproc_i2c_isr(int irq, void *dev_id)
{
	struct iproc_i2c_chip *chip = dev_id;
	struct iproc_i2c_regs *regs = chip->base;
	uint32_t st;

	spin_lock(&chip->lock);

	st = readl(&regs->isr);
	writel(st, &regs->isr);

	if (iproc_i2c_poll(chip) != -EAGAIN)
		wake_up(&chip->wait);

	spin_unlock(&chip->lock);
	return IRQ_HANDLED;
}

static int iproc_i2c_wait(struct iproc_i2c_chip *chip)
{
	unsigned long timeout = msecs_to_jiffies(20);
	struct iproc_i2c_regs *regs = chip->base;
	int err = 0, ret = 0;

	if (chip->irq < 0) {
		for (timeout += jiffies; time_is_after_jiffies(timeout); ) {
			ret = iproc_i2c_poll(chip);
			if (ret != -EAGAIN)
				break;
			schedule();
		}
		if (!time_is_after_jiffies(timeout))
			ret = -ETIMEDOUT;
	} else {
		uint32_t mcr;

		wait_event_timeout(chip->wait,
		                   !((mcr = readl(&regs->mcr)) & MCR_START),
		                   timeout);
		if (mcr & MCR_START)
			ret = -ETIMEDOUT;
		else if (MCR_STATUS(mcr) != MCR_STATUS_SUCCESS)
			ret = -EREMOTEIO;
	}

	err = ret ? MCR_STATUS(readl(&regs->mcr)) : 0;
	switch(ret) {
	case -ETIMEDOUT:
		dev_dbg(chip->dev, "timed out\n");
		++chip->recovery.timeout;
		break;
	case -EREMOTEIO:
		dev_dbg(chip->dev, "bus error, err=%d\n", err);
		if (err == MCR_STATUS_TIMEOUT)
			++chip->recovery.timeout;
		else
			++chip->recovery.bus_error;
		break;
	default:
		break;
	}

#ifdef CONFIG_LVL7_IPROC_I2C_BUSTST
	if (ret == -ETIMEDOUT) {
		if (readl(&regs->mcr) & MCR_START)
			iproc_i2c_chip_reset(chip);
		iproc_i2c_bus_test_reset(chip);
	}
#else
	if (ret == -ETIMEDOUT) {
		/* only activate bus reset upon SDA low */
		if (!(readl(&regs->bbcr) & BBCR_SDAIN))
			iproc_i2c_bus_reset(chip);
		/* only activate chip reset upon busy/start=1 */
		if (readl(&regs->mcr) & MCR_START)
			iproc_i2c_chip_reset(chip);
	}
#endif

	if (ret) {
		dev_dbg(chip->dev, "recovery statistics:\n");
		dev_dbg(chip->dev, "    timeout      = %d\n", 
			chip->recovery.timeout);
		dev_dbg(chip->dev, "    bus error    = %d\n", 
			chip->recovery.bus_error);
		dev_dbg(chip->dev, "    sda recovery = %d (%d failed)\n",
			chip->recovery.sda_success + chip->recovery.sda_failure,
			chip->recovery.sda_failure);
	}

	return ret;
}

static int iproc_i2c_smbus_xfer(struct i2c_adapter *adap, u16 addr,
			unsigned short flags, char read_write,
			u8 command, int size, union i2c_smbus_data *data)
{
	struct iproc_i2c_chip *chip = adap->algo_data;
	struct iproc_i2c_regs *regs = chip->base;
	int len, ret, prot = 0;
	struct i2c_msg msg;
	u8 *buf;

	/*
	 * In Hurricane2 platforms, the I2C module is inside CMIC and
	 * it would be reset upon SDK initialization in user mode.
	 * So we'll have to re-enable the I2C controller.
	 */
	if (!(readl(&regs->cfg) & CFG_ENABLE))
		iproc_i2c_chip_reset(chip);

	ret = 0;
	len = 0;
	buf = &data->byte;

	if (readl(&regs->mcr) & MCR_START)
		BUG();

	/* fifo clear */
	writel(MFCR_TXCLR | MFCR_RXCLR, &regs->mfcr);
	while (readl(&regs->mfcr) & (MFCR_TXCLR | MFCR_RXCLR))
		schedule();

	switch (size) {
	case I2C_SMBUS_QUICK:
		msg.addr  = addr;
		/* Special case: The read/write field is used as data */
		msg.flags = flags | (read_write == I2C_SMBUS_READ ? I2C_M_RD : 0);
		msg.len   = 0;
		msg.buf   = NULL;
		return adap->algo->master_xfer(adap, &msg, 1);

	case I2C_SMBUS_BYTE:
		if (read_write == I2C_SMBUS_READ) {
			prot = MCR_PROT_RECV_BYTE;
			writel((addr << 1) | 1, &regs->mdwr);
			len = 1;
		} else {
			prot = MCR_PROT_SEND_BYTE;
			writel(addr << 1, &regs->mdwr);
			writel(command | MDWR_END, &regs->mdwr);
		}
		break;

	case I2C_SMBUS_BYTE_DATA:
		writel(addr << 1, &regs->mdwr);
		writel(command, &regs->mdwr);
		if (read_write == I2C_SMBUS_READ) {
			prot = MCR_PROT_RD_BYTE;
			writel((addr << 1) | 1, &regs->mdwr);
			len = 1;
		} else {
			prot = MCR_PROT_WR_BYTE;
			writel(buf[0] | MDWR_END, &regs->mdwr);
		}
		break;

	case I2C_SMBUS_WORD_DATA:
		writel(addr << 1, &regs->mdwr);
		writel(command, &regs->mdwr);
		if (read_write == I2C_SMBUS_READ) {
			prot = MCR_PROT_RD_WORD;
			writel((addr << 1) | 1, &regs->mdwr);
			len = 2;
		} else {
			prot = MCR_PROT_WR_WORD;
			writel(buf[0], &regs->mdwr);
			writel(buf[1] | MDWR_END, &regs->mdwr);
		}
		break;

	case I2C_SMBUS_BLOCK_DATA:
		writel(addr << 1, &regs->mdwr);
		writel(command, &regs->mdwr);
		if (read_write == I2C_SMBUS_READ) {
			prot = MCR_PROT_BLK_RD;
			writel((addr << 1) | 1, &regs->mdwr);
			len = I2C_SMBUS_BLOCK_MAX + 1;
		} else {
			prot = MCR_PROT_BLK_WR;
			len = *buf + 1;
			++buf;
			if (len > I2C_SMBUS_BLOCK_MAX)
				len = I2C_SMBUS_BLOCK_MAX;
			writel(len, &regs->mdwr);
			while (len-- > 0) {
				if (!len)
					writel(*buf | MDWR_END, &regs->mdwr);
				else
					writel(*buf, &regs->mdwr);
				++buf;
			}
		}
		break;

	default:
		dev_err(chip->dev,
			"SMBus protocol %d is not yet implemented\n", size);
		return -EOPNOTSUPP;
	}

	if ((flags & I2C_CLIENT_PEC) && (size != I2C_SMBUS_QUICK) &&
	    (size != I2C_SMBUS_I2C_BLOCK_DATA))
		prot |= MCR_PEC;

	/* clear master interrupt status */
	writel(IRQ_MASK, &regs->isr);
	/* start I2C transactions */
	writel(MCR_START | MCR_PROTO(prot) | MCR_RXLEN(len), &regs->mcr);

	ret = iproc_i2c_wait(chip);
	if (!ret) {
		while (len-- > 0) {
			*buf = (u8)(readl(&regs->mdrr) & 0xff);
			++buf;
		}
	}

	return ret ? -EAGAIN : 0;
}
#endif /* CONFIG_LVL7_IPROC_I2C_MIXED */

static void iproc_i2c_setsda(void *data, int state)
{
	struct iproc_i2c_chip *chip = data;
	struct iproc_i2c_regs *regs = chip->base;

	if (state)
		setbits_le32(&regs->bbcr, BBCR_SDAOUT);
	else
		clrbits_le32(&regs->bbcr, BBCR_SDAOUT);
}

static void iproc_i2c_setscl(void *data, int state)
{
	struct iproc_i2c_chip *chip = data;
	struct iproc_i2c_regs *regs = chip->base;

	if (state)
		setbits_le32(&regs->bbcr, BBCR_SCLOUT);
	else
		clrbits_le32(&regs->bbcr, BBCR_SCLOUT);
}

static int iproc_i2c_getsda(void *data)
{
	struct iproc_i2c_chip *chip = data;
	struct iproc_i2c_regs *regs = chip->base;

	return (readl(&regs->bbcr) & BBCR_SDAIN) ? 1 : 0;
}

static int iproc_i2c_getscl(void *data)
{
	struct iproc_i2c_chip *chip = data;
	struct iproc_i2c_regs *regs = chip->base;

	return (readl(&regs->bbcr) & BBCR_SCLIN) ? 1 : 0;
}

static int iproc_i2c_bit_pre_xfer(struct i2c_adapter *adap)
{
	struct i2c_algo_bit_data *algo = adap->algo_data;
	struct iproc_i2c_chip *chip = algo->data;
	struct iproc_i2c_regs *regs = chip->base;

	/* enable bit-bang */
	setbits_le32(&regs->cfg, CFG_BITBANG);
	udelay(60);

	return 0;
}

static void iproc_i2c_bit_post_xfer(struct i2c_adapter *adap)
{
	struct i2c_algo_bit_data *algo = adap->algo_data;
	struct iproc_i2c_chip *chip = algo->data;
	struct iproc_i2c_regs *regs = chip->base;

	/* disable bit-bang control */
	clrbits_le32(&regs->cfg, CFG_BITBANG);
}

static int iproc_i2c_master_xfer(struct i2c_adapter *adap,
                                 struct i2c_msg msgs[], int num)
{
	return i2c_bit_algo.master_xfer(adap, msgs, num);
}

static u32 iproc_i2c_functionality(struct i2c_adapter *adap)
{
	return i2c_bit_algo.functionality(adap);
}

static struct i2c_algorithm iproc_i2c_algorithm = {
#ifdef CONFIG_LVL7_IPROC_I2C_MIXED
	.smbus_xfer    = iproc_i2c_smbus_xfer,
#endif
	.master_xfer   = iproc_i2c_master_xfer,
	.functionality = iproc_i2c_functionality,
};

static int __devinit iproc_i2c_probe(struct platform_device *pdev)
{
	struct iproc_i2c_chip *chip;
	struct iproc_i2c_regs *regs;
	struct i2c_adapter    *adap;
	struct i2c_algorithm  *algo = &iproc_i2c_algorithm;
	struct resource       *res;
	int ret = 0;

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		ret = -EINVAL;
		goto err_out;
	}

	if (!request_mem_region(res->start, resource_size(res), DRV_NAME)) {
		ret = -EBUSY;
		goto err_out;
	}

	chip = kzalloc(sizeof(*chip), GFP_KERNEL);
	if (!chip) {
		ret = -ENOMEM;
		goto err_release;
	}

	spin_lock_init(&chip->lock);
	init_waitqueue_head(&chip->wait);

	regs = ioremap(res->start, resource_size(res));
	if (!regs) {
		ret = -ENOMEM;
		goto err_free;
	}

	chip->bit_data.data       = chip;
	chip->bit_data.setsda     = iproc_i2c_setsda;
	chip->bit_data.setscl     = iproc_i2c_setscl;
	chip->bit_data.getsda     = iproc_i2c_getsda;
	chip->bit_data.getscl     = iproc_i2c_getscl;
	chip->bit_data.pre_xfer   = iproc_i2c_bit_pre_xfer;
	chip->bit_data.post_xfer  = iproc_i2c_bit_post_xfer;
	chip->bit_data.udelay     = 5; /* 5us, 100kHz */
	chip->bit_data.timeout    = msecs_to_jiffies(200);

	chip->dev                 = &pdev->dev;
	chip->irq                 = platform_get_irq(pdev, 0);
	chip->res                 = res;
	chip->base                = regs;

	adap                      = &chip->adap;
	snprintf(adap->name, sizeof(adap->name), "iproc-i2c-mixed");
	adap->algo                = algo;
	adap->algo_data           = chip;
	adap->retries             = 10;
	adap->timeout             = msecs_to_jiffies(200);
	adap->nr                  = pdev->id;
	adap->class               = 0;
	adap->dev.parent          = &pdev->dev;

	/* chip reset */
	iproc_i2c_chip_reset(chip);

#ifdef CONFIG_LVL7_IPROC_I2C_MIXED
	if (chip->irq > 0) {
		writel(IRQ_BUSY, &regs->ier);
		writel(IRQ_BUSY, &regs->isr);
		ret = request_irq(chip->irq, iproc_i2c_isr, IRQF_SHARED, pdev->name, chip);
		if (ret) {
			dev_err(&pdev->dev, "unable to request IRQ %d\n", chip->irq);
			goto err_unmap;
		}
	}
#endif

	platform_set_drvdata(pdev, chip);
	ret = i2c_add_numbered_adapter(adap);
	if (ret < 0) {
		dev_err(&pdev->dev, "unable to register I2C adapter\n");
		goto err_free_irq;
	}

	return 0;

err_free_irq:
	if (chip->irq > 0)
		free_irq(chip->irq, chip);
#ifdef CONFIG_LVL7_IPROC_I2C_MIXED
err_unmap:
#endif
	iounmap(chip->base);
err_free:
	kfree(chip);
err_release:
	release_mem_region(res->start, resource_size(res));
err_out:
	return ret;
}

static int __devexit iproc_i2c_remove(struct platform_device *pdev)
{
	struct iproc_i2c_chip *chip = platform_get_drvdata(pdev);
	struct iproc_i2c_regs *regs = chip->base;

	writel(0, &regs->cfg);

	if (chip->irq > 0)
		free_irq(chip->irq, chip);

	if (chip->base) {
		iounmap(chip->base);
		release_mem_region(chip->res->start, resource_size(chip->res));
	}

	platform_set_drvdata(pdev, NULL);
	i2c_del_adapter(&chip->adap);

	kfree(chip);

	return 0;
}

static struct platform_driver iproc_i2c_driver = {
	.probe		= iproc_i2c_probe,
	.remove		= __devexit_p(iproc_i2c_remove),
	.driver		= {
		.name	= DRV_NAME,
		.owner	= THIS_MODULE,
	},
};

static int __init iproc_i2c_init(void)
{
	pr_info("I2C: Broadcom I2C driver\n");
	return platform_driver_register(&iproc_i2c_driver);
}

static void __exit iproc_i2c_exit(void)
{
	platform_driver_unregister(&iproc_i2c_driver);
}

MODULE_DESCRIPTION("I2C-Bus adapter routines for Broadcom I2C");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:" DRV_NAME);

subsys_initcall(iproc_i2c_init);
module_exit(iproc_i2c_exit);
