/*****************************************************************************
* Copyright 2001 - 2011 Broadcom Corporation.  All rights reserved.
*
* Unless you and Broadcom execute a separate written software license
* agreement governing use of this software, this software is licensed to you
* under the terms of the GNU General Public License version 2, available at
* http://www.broadcom.com/licenses/GPLv2.php (the "GPL").
*
* Notwithstanding the above, under no circumstances may you combine this
* software in any way with any other Broadcom software provided under a
* license other than the GPL, without Broadcom's express prior written
* consent.
*****************************************************************************/




#ifndef __IPROC_LCD_H
#define __IPROC_LCD_H

#include "mach/socregs-cygnus.h"

#define IRQ_OLCDC 183
#define START_LCD_CFG LCDTiming0 /* defined in include/asm-arm/arch/iproc_reg.h */
#define END_LCD_CFG  (LCDTiming0+ 0x1000 - 1)

/* ---------------------------------------------------------------------------------------- */
#define LCD_B_REGWIDTH                                                                    32
#define LCD_B_INDEXWIDTH                                                                  16
#define LCD_B_RABUSWIDTH                                                                  32
#define LCD_B_RABUSTYPE                                                                  APB
#define LCD_B_RAADDRTYPE                                                                BYTE
#define LCD_B_OFSTINDX                                                               0x89000
#define LCD_B_NUMREG                                                                    1024
#define LCD_B_SUBDIV                                                                       0
#define LCD_B_NUMSHADOW                                                                    0
#define LCD_B_DIV_RANGE                                                                 0xff
/* ---------------------------------------------------------------------------------------- */

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_ClcdCrsrCtrl                                                             */
/* Description:  Cursor Control Register                                                    */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89c00                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_ClcdCrsrCtrl_MEMADDR                                                   0x89c00
#define LCD_R_ClcdCrsrCtrl_SEL                                                        0x9c00
#define LCD_R_ClcdCrsrCtrl_SIZE                                                           32
#define LCD_R_ClcdCrsrCtrl_MASK                                                   0xffffffff
#define LCD_R_ClcdCrsrCtrl_INIT                                                          0x0
#define LCD_R_ClcdCrsrCtrl_RWMODE                                                         RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrOn                                                                      */
/* Description: 0 = Cursor not displayed                                                                                               1 = Cursor is displayed. */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [0:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrOn_R                                                                     0
#define LCD_F_CrsrOn_L                                                                     0
#define LCD_F_CrsrOn_SIZE                                                                  1
#define LCD_F_CrsrOn_MASK                                                         0x00000001
#define LCD_F_CrsrOn_RANGE                                                               0:0
#define LCD_F_CrsrOn_INIT                                                         0x00000000
#define LCD_F_CrsrOn_RWMODE                                                               RW

#define LCD_F_CrsrOn_GET(x) \
( \
    (((x) & LCD_F_CrsrOn_MASK) >> \
            LCD_F_CrsrOn_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrNumber                                                                  */
/* Description: " Cursor Image number. This field provides an offset into the cursor image buffer, to you to address enable one of four 32x32 cursors. The images each occupy one quarter of the image memory, with Cursor0 from location 0, followed by Cursor1 from address 0x" */
/* Width: 2 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [5:4]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrNumber_R                                                                 4
#define LCD_F_CrsrNumber_L                                                                 5
#define LCD_F_CrsrNumber_SIZE                                                              2
#define LCD_F_CrsrNumber_MASK                                                     0x00000030
#define LCD_F_CrsrNumber_RANGE                                                           5:4
#define LCD_F_CrsrNumber_INIT                                                     0x00000000
#define LCD_F_CrsrNumber_RWMODE                                                           RW

#define LCD_F_CrsrNumber_GET(x) \
( \
    (((x) & LCD_F_CrsrNumber_MASK) >> \
            LCD_F_CrsrNumber_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDTiming2                                                               */
/* Description:  Clock and Signal Polarity Control Register                                 */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89008                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDTiming2_MEMADDR                                                     0x89008
#define LCD_R_LCDTiming2_SEL                                                          0x9008
#define LCD_R_LCDTiming2_SIZE                                                             32
#define LCD_R_LCDTiming2_MASK                                                     0xffffffff
#define LCD_R_LCDTiming2_INIT                                                            0x0
#define LCD_R_LCDTiming2_RWMODE                                                           RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_IHS                                                                         */
/* Description:  Invert horizontal synchronization:                                                                                    0 = CLLP pin is active HIGH and inactive LOW                                                                1 = CLLP pin is active LOW a */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [12:12]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_IHS_R                                                                       12
#define LCD_F_IHS_L                                                                       12
#define LCD_F_IHS_SIZE                                                                     1
#define LCD_F_IHS_MASK                                                            0x00001000
#define LCD_F_IHS_RANGE                                                                12:12
#define LCD_F_IHS_INIT                                                            0x00000000
#define LCD_F_IHS_RWMODE                                                                  RW

#define LCD_F_IHS_GET(x) \
( \
    (((x) & LCD_F_IHS_MASK) >> \
            LCD_F_IHS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_IOE                                                                         */
/* Description:  Invert output enable:                                                                                                    0 = CLAC output pin is active HIGH in TFT mode                                                             1 = CLAC output pin is act */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [14:14]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_IOE_R                                                                       14
#define LCD_F_IOE_L                                                                       14
#define LCD_F_IOE_SIZE                                                                     1
#define LCD_F_IOE_MASK                                                            0x00004000
#define LCD_F_IOE_RANGE                                                                14:14
#define LCD_F_IOE_INIT                                                            0x00000000
#define LCD_F_IOE_RWMODE                                                                  RW

#define LCD_F_IOE_GET(x) \
( \
    (((x) & LCD_F_IOE_MASK) >> \
            LCD_F_IOE_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_IPC                                                                         */
/* Description:  Invert panel clock:                                                                                                         0 = Data is driven on the LCD data lines on the rising edge of CLCP                                  1 = Data is driven on the LCD */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [13:13]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_IPC_R                                                                       13
#define LCD_F_IPC_L                                                                       13
#define LCD_F_IPC_SIZE                                                                     1
#define LCD_F_IPC_MASK                                                            0x00002000
#define LCD_F_IPC_RANGE                                                                13:13
#define LCD_F_IPC_INIT                                                            0x00000000
#define LCD_F_IPC_RWMODE                                                                  RW

#define LCD_F_IPC_GET(x) \
( \
    (((x) & LCD_F_IPC_MASK) >> \
            LCD_F_IPC_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLKSEL                                                                      */
/* Description:  This bit drives the CLCDCLKSEL signal. It is the select signal for the external LCD clock multiplexor. */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [5:5]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLKSEL_R                                                                     5
#define LCD_F_CLKSEL_L                                                                     5
#define LCD_F_CLKSEL_SIZE                                                                  1
#define LCD_F_CLKSEL_MASK                                                         0x00000020
#define LCD_F_CLKSEL_RANGE                                                               5:5
#define LCD_F_CLKSEL_INIT                                                         0x00000000
#define LCD_F_CLKSEL_RWMODE                                                               RW

#define LCD_F_CLKSEL_GET(x) \
( \
    (((x) & LCD_F_CLKSEL_MASK) >> \
            LCD_F_CLKSEL_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_PCD_LO                                                                      */
/* Description: " Lower five bits of panel clock divisor.a The ten-bit PCD field derives the LCD panel clock frequency CLCP from the CLCDCLK frequency,                                                                 CLCP = CLCDCLK/(PCD+2).                                 " */
/* Width: 5 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [4:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_PCD_LO_R                                                                     0
#define LCD_F_PCD_LO_L                                                                     4
#define LCD_F_PCD_LO_SIZE                                                                  5
#define LCD_F_PCD_LO_MASK                                                         0x0000001f
#define LCD_F_PCD_LO_RANGE                                                               4:0
#define LCD_F_PCD_LO_INIT                                                         0x00000000
#define LCD_F_PCD_LO_RWMODE                                                               RW

#define LCD_F_PCD_LO_GET(x) \
( \
    (((x) & LCD_F_PCD_LO_MASK) >> \
            LCD_F_PCD_LO_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_BCD                                                                         */
/* Description:  Bypass pixel clock divider. Setting this to 1 bypasses the pixel clock divider logic. You use this mainly for TFT displays. */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [26:26]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_BCD_R                                                                       26
#define LCD_F_BCD_L                                                                       26
#define LCD_F_BCD_SIZE                                                                     1
#define LCD_F_BCD_MASK                                                            0x04000000
#define LCD_F_BCD_RANGE                                                                26:26
#define LCD_F_BCD_INIT                                                            0x00000000
#define LCD_F_BCD_RWMODE                                                                  RW

#define LCD_F_BCD_GET(x) \
( \
    (((x) & LCD_F_BCD_MASK) >> \
            LCD_F_BCD_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_IVS                                                                         */
/* Description:  Invert vertical synchronization:                                                                                        0 = CLFP pin is active HIGH and inactive LOW                                                                1 = CLFP pin is active LOW */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [11:11]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_IVS_R                                                                       11
#define LCD_F_IVS_L                                                                       11
#define LCD_F_IVS_SIZE                                                                     1
#define LCD_F_IVS_MASK                                                            0x00000800
#define LCD_F_IVS_RANGE                                                                11:11
#define LCD_F_IVS_INIT                                                            0x00000000
#define LCD_F_IVS_RWMODE                                                                  RW

#define LCD_F_IVS_GET(x) \
( \
    (((x) & LCD_F_IVS_MASK) >> \
            LCD_F_IVS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_PCD_HI                                                                      */
/* Description: " Upper five bits of panel clock divisor. The ten-bit PCD field, comprising PCD_HI and PCD_LO (bits [4:0]), derives the LCD panel clock frequency CLCP from the CLCDCLK frequency, CLCP = CLCDCLK/(PCD+2). For mono STN displays with a 4 or 8-bit interface, th" */
/* Width: 5 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [31:27]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_PCD_HI_R                                                                    27
#define LCD_F_PCD_HI_L                                                                    31
#define LCD_F_PCD_HI_SIZE                                                                  5
#define LCD_F_PCD_HI_MASK                                                         0xf8000000
#define LCD_F_PCD_HI_RANGE                                                             31:27
#define LCD_F_PCD_HI_INIT                                                         0x00000000
#define LCD_F_PCD_HI_RWMODE                                                               RW

#define LCD_F_PCD_HI_GET(x) \
( \
    (((x) & LCD_F_PCD_HI_MASK) >> \
            LCD_F_PCD_HI_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CPL                                                                         */
/* Description: " Clocks per line. This field specifies the number of actual CLCP clocks to the LCD panel on each line. For each display type, this is:                                                                        TFT (actual pixels-per-line/1) - 1.              " */
/* Width: 10 bits                                                                           */
/* Type: RW                                                                                 */
/* Bit Mappings: [25:16]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CPL_R                                                                       16
#define LCD_F_CPL_L                                                                       25
#define LCD_F_CPL_SIZE                                                                    10
#define LCD_F_CPL_MASK                                                            0x03ff0000
#define LCD_F_CPL_RANGE                                                                25:16
#define LCD_F_CPL_INIT                                                            0x00000000
#define LCD_F_CPL_RWMODE                                                                  RW

#define LCD_F_CPL_GET(x) \
( \
    (((x) & LCD_F_CPL_MASK) >> \
            LCD_F_CPL_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_ACB                                                                         */
/* Description: " AC bias pin frequency. The AC bias pin frequency is only applicable to STN displays. These require the pixel voltage polarity to periodically reverse to prevent damage caused by DC charge accumulation. Program this field with the required value, minus on" */
/* Width: 5 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [10:6]                                                                     */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_ACB_R                                                                        6
#define LCD_F_ACB_L                                                                       10
#define LCD_F_ACB_SIZE                                                                     5
#define LCD_F_ACB_MASK                                                            0x000007c0
#define LCD_F_ACB_RANGE                                                                 10:6
#define LCD_F_ACB_INIT                                                            0x00000000
#define LCD_F_ACB_RWMODE                                                                  RW

#define LCD_F_ACB_GET(x) \
( \
    (((x) & LCD_F_ACB_MASK) >> \
            LCD_F_ACB_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDTiming1                                                               */
/* Description:  Vertical Axis Panel Control Register                                       */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89004                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDTiming1_MEMADDR                                                     0x89004
#define LCD_R_LCDTiming1_SEL                                                          0x9004
#define LCD_R_LCDTiming1_SIZE                                                             32
#define LCD_R_LCDTiming1_MASK                                                     0xffffffff
#define LCD_R_LCDTiming1_INIT                                                            0x0
#define LCD_R_LCDTiming1_RWMODE                                                           RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_VFP                                                                         */
/* Description: " Vertical front porch. This is the number of inactive lines at the end of frame, before the vertical synchronization period. Program to zero on passive displays or reduced contrast results. The 8-bit VFP field specifies the number of line clocks to insert" */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [23:16]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_VFP_R                                                                       16
#define LCD_F_VFP_L                                                                       23
#define LCD_F_VFP_SIZE                                                                     8
#define LCD_F_VFP_MASK                                                            0x00ff0000
#define LCD_F_VFP_RANGE                                                                23:16
#define LCD_F_VFP_INIT                                                            0x00000000
#define LCD_F_VFP_RWMODE                                                                  RW

#define LCD_F_VFP_GET(x) \
( \
    (((x) & LCD_F_VFP_MASK) >> \
            LCD_F_VFP_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_VSW                                                                         */
/* Description: " Vertical synchronization pulse width. This is the number of horizontal synchronization lines. This value must be small (for example, program to zero) for passive STN LCDs. Program to the number of lines required, minus one. The higher the value, the wors" */
/* Width: 6 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [15:10]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_VSW_R                                                                       10
#define LCD_F_VSW_L                                                                       15
#define LCD_F_VSW_SIZE                                                                     6
#define LCD_F_VSW_MASK                                                            0x0000fc00
#define LCD_F_VSW_RANGE                                                                15:10
#define LCD_F_VSW_INIT                                                            0x00000000
#define LCD_F_VSW_RWMODE                                                                  RW

#define LCD_F_VSW_GET(x) \
( \
    (((x) & LCD_F_VSW_MASK) >> \
            LCD_F_VSW_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_VBP                                                                         */
/* Description: " Vertical back porch. This is the number of inactive lines at the start of a frame, after the vertical synchronization period. Program to zero on passive displays or reduced contrast results. The 8-bit VBP field specifies the number of line clocks inserte" */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [31:24]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_VBP_R                                                                       24
#define LCD_F_VBP_L                                                                       31
#define LCD_F_VBP_SIZE                                                                     8
#define LCD_F_VBP_MASK                                                            0xff000000
#define LCD_F_VBP_RANGE                                                                31:24
#define LCD_F_VBP_INIT                                                            0x00000000
#define LCD_F_VBP_RWMODE                                                                  RW

#define LCD_F_VBP_GET(x) \
( \
    (((x) & LCD_F_VBP_MASK) >> \
            LCD_F_VBP_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LPP                                                                         */
/* Description: " Lines per panel. This is the number of active lines per screen. Program to number of lines required, minus 1. The LPP field specifies the total number of lines or rows on the LCD panel being controlled. LPP is a 10-bit value enabling between 1 and 1024 l" */
/* Width: 10 bits                                                                           */
/* Type: RW                                                                                 */
/* Bit Mappings: [9:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LPP_R                                                                        0
#define LCD_F_LPP_L                                                                        9
#define LCD_F_LPP_SIZE                                                                    10
#define LCD_F_LPP_MASK                                                            0x000003ff
#define LCD_F_LPP_RANGE                                                                  9:0
#define LCD_F_LPP_INIT                                                            0x00000000
#define LCD_F_LPP_RWMODE                                                                  RW

#define LCD_F_LPP_GET(x) \
( \
    (((x) & LCD_F_LPP_MASK) >> \
            LCD_F_LPP_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_CLCDPeriphID1                                                            */
/* Description:  Peripheral Identification Register 1                                       */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89fe4                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_CLCDPeriphID1_MEMADDR                                                  0x89fe4
#define LCD_R_CLCDPeriphID1_SEL                                                       0x9fe4
#define LCD_R_CLCDPeriphID1_SIZE                                                          32
#define LCD_R_CLCDPeriphID1_MASK                                                  0xffffffff
#define LCD_R_CLCDPeriphID1_INIT                                                        0x11
#define LCD_R_CLCDPeriphID1_RWMODE                                                        RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_PartNumber1                                                                 */
/* Description:  These bits read back as 0x1                                                */
/* Width: 4 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [3:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_PartNumber1_R                                                                0
#define LCD_F_PartNumber1_L                                                                3
#define LCD_F_PartNumber1_SIZE                                                             4
#define LCD_F_PartNumber1_MASK                                                    0x0000000f
#define LCD_F_PartNumber1_RANGE                                                          3:0
#define LCD_F_PartNumber1_INIT                                                    0x00000001
#define LCD_F_PartNumber1_RWMODE                                                          RO

#define LCD_F_PartNumber1_GET(x) \
( \
    (((x) & LCD_F_PartNumber1_MASK) >> \
            LCD_F_PartNumber1_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_Designer0                                                                   */
/* Description:  These bits read back as 0x1                                                */
/* Width: 4 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [7:4]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_Designer0_R                                                                  4
#define LCD_F_Designer0_L                                                                  7
#define LCD_F_Designer0_SIZE                                                               4
#define LCD_F_Designer0_MASK                                                      0x000000f0
#define LCD_F_Designer0_RANGE                                                            7:4
#define LCD_F_Designer0_INIT                                                      0x00000001
#define LCD_F_Designer0_RWMODE                                                            RO

#define LCD_F_Designer0_GET(x) \
( \
    (((x) & LCD_F_Designer0_MASK) >> \
            LCD_F_Designer0_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDRIS                                                                   */
/* Description:  Raw Interrupt Status Register                                              */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89020                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDRIS_MEMADDR                                                         0x89020
#define LCD_R_LCDRIS_SEL                                                              0x9020
#define LCD_R_LCDRIS_SIZE                                                                 32
#define LCD_R_LCDRIS_MASK                                                         0xffffffff
#define LCD_R_LCDRIS_INIT                                                                0x0
#define LCD_R_LCDRIS_RWMODE                                                               RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_MBERRORRIS                                                                  */
/* Description: " AHB master bus error status, set when the AHB master encounters a bus error response from a slave." */
/* Width: 1 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [4:4]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_MBERRORRIS_R                                                                 4
#define LCD_F_MBERRORRIS_L                                                                 4
#define LCD_F_MBERRORRIS_SIZE                                                              1
#define LCD_F_MBERRORRIS_MASK                                                     0x00000010
#define LCD_F_MBERRORRIS_RANGE                                                           4:4
#define LCD_F_MBERRORRIS_INIT                                                     0x00000000
#define LCD_F_MBERRORRIS_RWMODE                                                           RO

#define LCD_F_MBERRORRIS_GET(x) \
( \
    (((x) & LCD_F_MBERRORRIS_MASK) >> \
            LCD_F_MBERRORRIS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_FUFRIS                                                                      */
/* Description: " FIFO underflow, set when either the upper or lower DMA FIFOs have been read accessed when empty, causing an underflow condition to occur." */
/* Width: 1 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [1:1]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_FUFRIS_R                                                                     1
#define LCD_F_FUFRIS_L                                                                     1
#define LCD_F_FUFRIS_SIZE                                                                  1
#define LCD_F_FUFRIS_MASK                                                         0x00000002
#define LCD_F_FUFRIS_RANGE                                                               1:1
#define LCD_F_FUFRIS_INIT                                                         0x00000000
#define LCD_F_FUFRIS_RWMODE                                                               RO

#define LCD_F_FUFRIS_GET(x) \
( \
    (((x) & LCD_F_FUFRIS_MASK) >> \
            LCD_F_FUFRIS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LNBURIS                                                                     */
/* Description: " LCD next address base update, mode dependent, set when the current base address registers have been successfully updated by the next address registers. Signifies that a new next address can be loaded if you use double buffering." */
/* Width: 1 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [2:2]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LNBURIS_R                                                                    2
#define LCD_F_LNBURIS_L                                                                    2
#define LCD_F_LNBURIS_SIZE                                                                 1
#define LCD_F_LNBURIS_MASK                                                        0x00000004
#define LCD_F_LNBURIS_RANGE                                                              2:2
#define LCD_F_LNBURIS_INIT                                                        0x00000000
#define LCD_F_LNBURIS_RWMODE                                                              RO

#define LCD_F_LNBURIS_GET(x) \
( \
    (((x) & LCD_F_LNBURIS_MASK) >> \
            LCD_F_LNBURIS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_VcompRIS                                                                    */
/* Description: " Vertical compare, set when one of the four vertical regions, selected through the LCDControl Register, is reached." */
/* Width: 1 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [3:3]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_VcompRIS_R                                                                   3
#define LCD_F_VcompRIS_L                                                                   3
#define LCD_F_VcompRIS_SIZE                                                                1
#define LCD_F_VcompRIS_MASK                                                       0x00000008
#define LCD_F_VcompRIS_RANGE                                                             3:3
#define LCD_F_VcompRIS_INIT                                                       0x00000000
#define LCD_F_VcompRIS_RWMODE                                                             RO

#define LCD_F_VcompRIS_GET(x) \
( \
    (((x) & LCD_F_VcompRIS_MASK) >> \
            LCD_F_VcompRIS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_ClcdCrsrMIS                                                              */
/* Description:  Cursor Masked Interrupt Status Register                                    */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89c2c                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_ClcdCrsrMIS_MEMADDR                                                    0x89c2c
#define LCD_R_ClcdCrsrMIS_SEL                                                         0x9c2c
#define LCD_R_ClcdCrsrMIS_SIZE                                                            32
#define LCD_R_ClcdCrsrMIS_MASK                                                    0xffffffff
#define LCD_R_ClcdCrsrMIS_INIT                                                           0x0
#define LCD_R_ClcdCrsrMIS_RWMODE                                                          RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrMIS                                                                     */
/* Description: " The cursor interrupt status is set immediately after the last data read from the cursor image for the current frame, providing that you set the corresponding bit in the ClcdCrsrIMSC Register. The bit remains clear if the ClcdCrsrIMSC Register is clear. Y" */
/* Width: 1 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [0:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrMIS_R                                                                    0
#define LCD_F_CrsrMIS_L                                                                    0
#define LCD_F_CrsrMIS_SIZE                                                                 1
#define LCD_F_CrsrMIS_MASK                                                        0x00000001
#define LCD_F_CrsrMIS_RANGE                                                              0:0
#define LCD_F_CrsrMIS_INIT                                                        0x00000000
#define LCD_F_CrsrMIS_RWMODE                                                              RO

#define LCD_F_CrsrMIS_GET(x) \
( \
    (((x) & LCD_F_CrsrMIS_MASK) >> \
            LCD_F_CrsrMIS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDUPBASE                                                                */
/* Description:  Upper and Lower Panel Frame Base Address Registers                         */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89010                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDUPBASE_MEMADDR                                                      0x89010
#define LCD_R_LCDUPBASE_SEL                                                           0x9010
#define LCD_R_LCDUPBASE_SIZE                                                              32
#define LCD_R_LCDUPBASE_MASK                                                      0xffffffff
#define LCD_R_LCDUPBASE_INIT                                                             0x0
#define LCD_R_LCDUPBASE_RWMODE                                                            RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LCDUPBASE                                                                   */
/* Description:  LCD upper panel base address. This is the start address of the upper panel frame data in memory and is doubleword-aligned. */
/* Width: 29 bits                                                                           */
/* Type: RW                                                                                 */
/* Bit Mappings: [31:3]                                                                     */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LCDUPBASE_R                                                                  3
#define LCD_F_LCDUPBASE_L                                                                 31
#define LCD_F_LCDUPBASE_SIZE                                                              29
#define LCD_F_LCDUPBASE_MASK                                                      0xfffffff8
#define LCD_F_LCDUPBASE_RANGE                                                           31:3
#define LCD_F_LCDUPBASE_INIT                                                      0x00000000
#define LCD_F_LCDUPBASE_RWMODE                                                            RW

#define LCD_F_LCDUPBASE_GET(x) \
( \
    (((x) & LCD_F_LCDUPBASE_MASK) >> \
            LCD_F_LCDUPBASE_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_ClcdCrsrRIS                                                              */
/* Description:  Cursor Raw Interrupt Status Register                                       */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89c28                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_ClcdCrsrRIS_MEMADDR                                                    0x89c28
#define LCD_R_ClcdCrsrRIS_SEL                                                         0x9c28
#define LCD_R_ClcdCrsrRIS_SIZE                                                            32
#define LCD_R_ClcdCrsrRIS_MASK                                                    0xffffffff
#define LCD_R_ClcdCrsrRIS_INIT                                                           0x0
#define LCD_R_ClcdCrsrRIS_RWMODE                                                          RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrRIS                                                                     */
/* Description:  The cursor interrupt status is set immediately after the last data read from the cursor image for the current frame. You clear this bit by writing to the CrsrIC bit in the ClcdCrsrICR Register. */
/* Width: 1 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [0:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrRIS_R                                                                    0
#define LCD_F_CrsrRIS_L                                                                    0
#define LCD_F_CrsrRIS_SIZE                                                                 1
#define LCD_F_CrsrRIS_MASK                                                        0x00000001
#define LCD_F_CrsrRIS_RANGE                                                              0:0
#define LCD_F_CrsrRIS_INIT                                                        0x00000000
#define LCD_F_CrsrRIS_RWMODE                                                              RO

#define LCD_F_CrsrRIS_GET(x) \
( \
    (((x) & LCD_F_CrsrRIS_MASK) >> \
            LCD_F_CrsrRIS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDLPCURR                                                                */
/* Description:  LCD Upper and Lower Panel Current Address Value Registers                  */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89030                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDLPCURR_MEMADDR                                                      0x89030
#define LCD_R_LCDLPCURR_SEL                                                           0x9030
#define LCD_R_LCDLPCURR_SIZE                                                              32
#define LCD_R_LCDLPCURR_MASK                                                      0xffffffff
#define LCD_R_LCDLPCURR_INIT                                                             0x0
#define LCD_R_LCDLPCURR_RWMODE                                                            RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LCDLPCURR                                                                   */
/* Description:  LCD Upper and Lower Panel Current Address Value Registers                  */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Bit Mappings: [31:0]                                                                     */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LCDLPCURR_R                                                                  0
#define LCD_F_LCDLPCURR_L                                                                 31
#define LCD_F_LCDLPCURR_SIZE                                                              32
#define LCD_F_LCDLPCURR_MASK                                                      0xffffffff
#define LCD_F_LCDLPCURR_RANGE                                                           31:0
#define LCD_F_LCDLPCURR_INIT                                                      0x00000000
#define LCD_F_LCDLPCURR_RWMODE                                                            RO

#define LCD_F_LCDLPCURR_GET(x) \
( \
    (((x) & LCD_F_LCDLPCURR_MASK) >> \
            LCD_F_LCDLPCURR_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDITOP1                                                                 */
/* Description:  Integration Test Output Register 1                                         */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89f04                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDITOP1_MEMADDR                                                       0x89f04
#define LCD_R_LCDITOP1_SEL                                                            0x9f04
#define LCD_R_LCDITOP1_SIZE                                                               32
#define LCD_R_LCDITOP1_MASK                                                       0xffffffff
#define LCD_R_LCDITOP1_INIT                                                              0x0
#define LCD_R_LCDITOP1_RWMODE                                                             RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCDMBEINTR                                                                 */
/* Description: " Writes specify the value to drive on CLCDMBEINTR, when in integration test mode. A read returns the value of CLCDMBEINTR at the output of the test multiplexor." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [4:4]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCDMBEINTR_R                                                                4
#define LCD_F_CLCDMBEINTR_L                                                                4
#define LCD_F_CLCDMBEINTR_SIZE                                                             1
#define LCD_F_CLCDMBEINTR_MASK                                                    0x00000010
#define LCD_F_CLCDMBEINTR_RANGE                                                          4:4
#define LCD_F_CLCDMBEINTR_INIT                                                    0x00000000
#define LCD_F_CLCDMBEINTR_RWMODE                                                          RW

#define LCD_F_CLCDMBEINTR_GET(x) \
( \
    (((x) & LCD_F_CLCDMBEINTR_MASK) >> \
            LCD_F_CLCDMBEINTR_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCDLNBUINTR                                                                */
/* Description: " Writes specify the value to drive on CLCDLNBUINTR, when in integration test mode. A read returns the value of CLCDLNBUINTR at the output of the test multiplexor." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [2:2]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCDLNBUINTR_R                                                               2
#define LCD_F_CLCDLNBUINTR_L                                                               2
#define LCD_F_CLCDLNBUINTR_SIZE                                                            1
#define LCD_F_CLCDLNBUINTR_MASK                                                   0x00000004
#define LCD_F_CLCDLNBUINTR_RANGE                                                         2:2
#define LCD_F_CLCDLNBUINTR_INIT                                                   0x00000000
#define LCD_F_CLCDLNBUINTR_RWMODE                                                         RW

#define LCD_F_CLCDLNBUINTR_GET(x) \
( \
    (((x) & LCD_F_CLCDLNBUINTR_MASK) >> \
            LCD_F_CLCDLNBUINTR_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCDVCOMPINTR                                                               */
/* Description: " Writes specify the value to drive on CLCDVCOMPINTR, when in integration test mode. A read returns the value of CLCDVCOMPINTR at the output of the test multiplexor." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [1:1]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCDVCOMPINTR_R                                                              1
#define LCD_F_CLCDVCOMPINTR_L                                                              1
#define LCD_F_CLCDVCOMPINTR_SIZE                                                           1
#define LCD_F_CLCDVCOMPINTR_MASK                                                  0x00000002
#define LCD_F_CLCDVCOMPINTR_RANGE                                                        1:1
#define LCD_F_CLCDVCOMPINTR_INIT                                                  0x00000000
#define LCD_F_CLCDVCOMPINTR_RWMODE                                                        RW

#define LCD_F_CLCDVCOMPINTR_GET(x) \
( \
    (((x) & LCD_F_CLCDVCOMPINTR_MASK) >> \
            LCD_F_CLCDVCOMPINTR_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCDCLKSEL                                                                  */
/* Description: " Writes specify the value to drive on CLCDCLKSEL, when in integration test mode. A read returns the value of the CLCDCLKSEL signal at the output of the test multiplexor." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [5:5]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCDCLKSEL_R                                                                 5
#define LCD_F_CLCDCLKSEL_L                                                                 5
#define LCD_F_CLCDCLKSEL_SIZE                                                              1
#define LCD_F_CLCDCLKSEL_MASK                                                     0x00000020
#define LCD_F_CLCDCLKSEL_RANGE                                                           5:5
#define LCD_F_CLCDCLKSEL_INIT                                                     0x00000000
#define LCD_F_CLCDCLKSEL_RWMODE                                                           RW

#define LCD_F_CLCDCLKSEL_GET(x) \
( \
    (((x) & LCD_F_CLCDCLKSEL_MASK) >> \
            LCD_F_CLCDCLKSEL_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCDINTR                                                                    */
/* Description: " Writes specify the value to drive on CLCDINTR, when in integration test mode. A read returns the value of CLCDINTR at the output of the test multiplexor." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [0:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCDINTR_R                                                                   0
#define LCD_F_CLCDINTR_L                                                                   0
#define LCD_F_CLCDINTR_SIZE                                                                1
#define LCD_F_CLCDINTR_MASK                                                       0x00000001
#define LCD_F_CLCDINTR_RANGE                                                             0:0
#define LCD_F_CLCDINTR_INIT                                                       0x00000000
#define LCD_F_CLCDINTR_RWMODE                                                             RW

#define LCD_F_CLCDINTR_GET(x) \
( \
    (((x) & LCD_F_CLCDINTR_MASK) >> \
            LCD_F_CLCDINTR_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCDFUFINTR                                                                 */
/* Description: " Writes specify the value to drive on CLCDFUFINTR, when in integration test mode. A read returns the value of CLCDFUFINTR at the output of the test multiplexor." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [3:3]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCDFUFINTR_R                                                                3
#define LCD_F_CLCDFUFINTR_L                                                                3
#define LCD_F_CLCDFUFINTR_SIZE                                                             1
#define LCD_F_CLCDFUFINTR_MASK                                                    0x00000008
#define LCD_F_CLCDFUFINTR_RANGE                                                          3:3
#define LCD_F_CLCDFUFINTR_INIT                                                    0x00000000
#define LCD_F_CLCDFUFINTR_RWMODE                                                          RW

#define LCD_F_CLCDFUFINTR_GET(x) \
( \
    (((x) & LCD_F_CLCDFUFINTR_MASK) >> \
            LCD_F_CLCDFUFINTR_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_CLCDPCellID0                                                             */
/* Description:  PrimeCell Identification Register 0                                        */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89ff0                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_CLCDPCellID0_MEMADDR                                                   0x89ff0
#define LCD_R_CLCDPCellID0_SEL                                                        0x9ff0
#define LCD_R_CLCDPCellID0_SIZE                                                           32
#define LCD_R_CLCDPCellID0_MASK                                                   0xffffffff
#define LCD_R_CLCDPCellID0_INIT                                                          0xd
#define LCD_R_CLCDPCellID0_RWMODE                                                         RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCDPCellID0                                                                */
/* Description:  These bits read back as 0x0D                                               */
/* Width: 8 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [7:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCDPCellID0_R                                                               0
#define LCD_F_CLCDPCellID0_L                                                               7
#define LCD_F_CLCDPCellID0_SIZE                                                            8
#define LCD_F_CLCDPCellID0_MASK                                                   0x000000ff
#define LCD_F_CLCDPCellID0_RANGE                                                         7:0
#define LCD_F_CLCDPCellID0_INIT                                                   0x0000000d
#define LCD_F_CLCDPCellID0_RWMODE                                                         RO

#define LCD_F_CLCDPCellID0_GET(x) \
( \
    (((x) & LCD_F_CLCDPCellID0_MASK) >> \
            LCD_F_CLCDPCellID0_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDControl                                                               */
/* Description:  LCD Control Register                                                       */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89018                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDControl_MEMADDR                                                     0x89018
#define LCD_R_LCDControl_SEL                                                          0x9018
#define LCD_R_LCDControl_SIZE                                                             32
#define LCD_R_LCDControl_MASK                                                     0xffffffff
#define LCD_R_LCDControl_INIT                                                            0x0
#define LCD_R_LCDControl_RWMODE                                                           RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_BEBO                                                                        */
/* Description:  Big-endian byte order:                                                                                                   0 = little-endian byte order                                                                                             1 = big-endi */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [9:9]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_BEBO_R                                                                       9
#define LCD_F_BEBO_L                                                                       9
#define LCD_F_BEBO_SIZE                                                                    1
#define LCD_F_BEBO_MASK                                                           0x00000200
#define LCD_F_BEBO_RANGE                                                                 9:9
#define LCD_F_BEBO_INIT                                                           0x00000000
#define LCD_F_BEBO_RWMODE                                                                 RW

#define LCD_F_BEBO_GET(x) \
( \
    (((x) & LCD_F_BEBO_MASK) >> \
            LCD_F_BEBO_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_BEPO                                                                        */
/* Description:  Big-endian pixel ordering within a byte:                                                                          0 = little-endian ordering within a byte                                                                             1 = big-endian pixel or */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [10:10]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_BEPO_R                                                                      10
#define LCD_F_BEPO_L                                                                      10
#define LCD_F_BEPO_SIZE                                                                    1
#define LCD_F_BEPO_MASK                                                           0x00000400
#define LCD_F_BEPO_RANGE                                                               10:10
#define LCD_F_BEPO_INIT                                                           0x00000000
#define LCD_F_BEPO_RWMODE                                                                 RW

#define LCD_F_BEPO_GET(x) \
( \
    (((x) & LCD_F_BEPO_MASK) >> \
            LCD_F_BEPO_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LcdVComp                                                                    */
/* Description:  Generate interrupt at:                                                                                                     b00 = start of vertical synchronization                                                                             b01 = start of */
/* Width: 2 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [13:12]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LcdVComp_R                                                                  12
#define LCD_F_LcdVComp_L                                                                  13
#define LCD_F_LcdVComp_SIZE                                                                2
#define LCD_F_LcdVComp_MASK                                                       0x00003000
#define LCD_F_LcdVComp_RANGE                                                           13:12
#define LCD_F_LcdVComp_INIT                                                       0x00000000
#define LCD_F_LcdVComp_RWMODE                                                             RW

#define LCD_F_LcdVComp_GET(x) \
( \
    (((x) & LCD_F_LcdVComp_MASK) >> \
            LCD_F_LcdVComp_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_BGR                                                                         */
/* Description:  RGB or BGR format selection:                                                                                     0 = RGB normal output                                                                                                 1 = BGR red and blue sw */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [8:8]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_BGR_R                                                                        8
#define LCD_F_BGR_L                                                                        8
#define LCD_F_BGR_SIZE                                                                     1
#define LCD_F_BGR_MASK                                                            0x00000100
#define LCD_F_BGR_RANGE                                                                  8:8
#define LCD_F_BGR_INIT                                                            0x00000000
#define LCD_F_BGR_RWMODE                                                                  RW

#define LCD_F_BGR_GET(x) \
( \
    (((x) & LCD_F_BGR_MASK) >> \
            LCD_F_BGR_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LcdEn                                                                       */
/* Description: " CLCDC enable:                                                                                                           0 = LCD signals CLLP, CLCP, CLFP, CLAC, and CLLE disabled (LOW)                           1 = LCD signals CLLP, CLCP, CLFP, CLAC, and " */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [0:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LcdEn_R                                                                      0
#define LCD_F_LcdEn_L                                                                      0
#define LCD_F_LcdEn_SIZE                                                                   1
#define LCD_F_LcdEn_MASK                                                          0x00000001
#define LCD_F_LcdEn_RANGE                                                                0:0
#define LCD_F_LcdEn_INIT                                                          0x00000000
#define LCD_F_LcdEn_RWMODE                                                                RW

#define LCD_F_LcdEn_GET(x) \
( \
    (((x) & LCD_F_LcdEn_MASK) >> \
            LCD_F_LcdEn_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LcdBpp                                                                      */
/* Description: LCD bits per pixel:                                                                                                       b000 = 1bpp                                                                                                                 b001 = 2b */
/* Width: 3 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [3:1]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LcdBpp_R                                                                     1
#define LCD_F_LcdBpp_L                                                                     3
#define LCD_F_LcdBpp_SIZE                                                                  3
#define LCD_F_LcdBpp_MASK                                                         0x0000000e
#define LCD_F_LcdBpp_RANGE                                                               3:1
#define LCD_F_LcdBpp_INIT                                                         0x00000000
#define LCD_F_LcdBpp_RWMODE                                                               RW

#define LCD_F_LcdBpp_GET(x) \
( \
    (((x) & LCD_F_LcdBpp_MASK) >> \
            LCD_F_LcdBpp_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LcdPwr                                                                      */
/* Description: " LCD power enable:                                                                                                        0 = power not gated through to LCD panel and CLD[23:0] signals disabled, (held LOW)      1 = power gated through to LCD panel and CLD" */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [11:11]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LcdPwr_R                                                                    11
#define LCD_F_LcdPwr_L                                                                    11
#define LCD_F_LcdPwr_SIZE                                                                  1
#define LCD_F_LcdPwr_MASK                                                         0x00000800
#define LCD_F_LcdPwr_RANGE                                                             11:11
#define LCD_F_LcdPwr_INIT                                                         0x00000000
#define LCD_F_LcdPwr_RWMODE                                                               RW

#define LCD_F_LcdPwr_GET(x) \
( \
    (((x) & LCD_F_LcdPwr_MASK) >> \
            LCD_F_LcdPwr_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LcdBW                                                                       */
/* Description:  STN LCD is monochrome (black and white):                                                                   0 = STN LCD is color                                                                                                    1 = STN LCD is monochrome. */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [4:4]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LcdBW_R                                                                      4
#define LCD_F_LcdBW_L                                                                      4
#define LCD_F_LcdBW_SIZE                                                                   1
#define LCD_F_LcdBW_MASK                                                          0x00000010
#define LCD_F_LcdBW_RANGE                                                                4:4
#define LCD_F_LcdBW_INIT                                                          0x00000000
#define LCD_F_LcdBW_RWMODE                                                                RW

#define LCD_F_LcdBW_GET(x) \
( \
    (((x) & LCD_F_LcdBW_MASK) >> \
            LCD_F_LcdBW_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LcdTFT                                                                      */
/* Description:  LCD is TFT:                                                                                                                   0 = LCD is an STN display. Use gray scaler                                                                    1 = LCD is a TFT d */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [5:5]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LcdTFT_R                                                                     5
#define LCD_F_LcdTFT_L                                                                     5
#define LCD_F_LcdTFT_SIZE                                                                  1
#define LCD_F_LcdTFT_MASK                                                         0x00000020
#define LCD_F_LcdTFT_RANGE                                                               5:5
#define LCD_F_LcdTFT_INIT                                                         0x00000000
#define LCD_F_LcdTFT_RWMODE                                                               RW

#define LCD_F_LcdTFT_GET(x) \
( \
    (((x) & LCD_F_LcdTFT_MASK) >> \
            LCD_F_LcdTFT_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_WATERMARK                                                                   */
/* Description:  LCD DMA FIFO watermark level:                                                                                    0 = asserts HBUSREQM when either of the DMA FIFOs have four or more empty locations                                                          */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [16:16]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_WATERMARK_R                                                                 16
#define LCD_F_WATERMARK_L                                                                 16
#define LCD_F_WATERMARK_SIZE                                                               1
#define LCD_F_WATERMARK_MASK                                                      0x00010000
#define LCD_F_WATERMARK_RANGE                                                          16:16
#define LCD_F_WATERMARK_INIT                                                      0x00000000
#define LCD_F_WATERMARK_RWMODE                                                            RW

#define LCD_F_WATERMARK_GET(x) \
( \
    (((x) & LCD_F_WATERMARK_MASK) >> \
            LCD_F_WATERMARK_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LcdDual                                                                     */
/* Description:  LCD interface is dual-panel STN:                                                                                    0 = single-panel LCD is in use                                                                                      1 = dual-panel LCD is */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [7:7]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LcdDual_R                                                                    7
#define LCD_F_LcdDual_L                                                                    7
#define LCD_F_LcdDual_SIZE                                                                 1
#define LCD_F_LcdDual_MASK                                                        0x00000080
#define LCD_F_LcdDual_RANGE                                                              7:7
#define LCD_F_LcdDual_INIT                                                        0x00000000
#define LCD_F_LcdDual_RWMODE                                                              RW

#define LCD_F_LcdDual_GET(x) \
( \
    (((x) & LCD_F_LcdDual_MASK) >> \
            LCD_F_LcdDual_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LcdMono8                                                                    */
/* Description: " Monochrome LCD. This has an 8-bit interface. This bit controls whether monochrome STN LCD uses a 4 or 8-bit parallel interface. It has no meaning in other modes, and you must    program it to zero.                                                         " */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [6:6]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LcdMono8_R                                                                   6
#define LCD_F_LcdMono8_L                                                                   6
#define LCD_F_LcdMono8_SIZE                                                                1
#define LCD_F_LcdMono8_MASK                                                       0x00000040
#define LCD_F_LcdMono8_RANGE                                                             6:6
#define LCD_F_LcdMono8_INIT                                                       0x00000000
#define LCD_F_LcdMono8_RWMODE                                                             RW

#define LCD_F_LcdMono8_GET(x) \
( \
    (((x) & LCD_F_LcdMono8_MASK) >> \
            LCD_F_LcdMono8_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_CLCDPCellID3                                                             */
/* Description:  PrimeCell Identification Register 3                                        */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89ffc                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_CLCDPCellID3_MEMADDR                                                   0x89ffc
#define LCD_R_CLCDPCellID3_SEL                                                        0x9ffc
#define LCD_R_CLCDPCellID3_SIZE                                                           32
#define LCD_R_CLCDPCellID3_MASK                                                   0xffffffff
#define LCD_R_CLCDPCellID3_INIT                                                         0xb1
#define LCD_R_CLCDPCellID3_RWMODE                                                         RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCDPCellID3                                                                */
/* Description:  These bits read back as 0xB1                                               */
/* Width: 8 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [7:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCDPCellID3_R                                                               0
#define LCD_F_CLCDPCellID3_L                                                               7
#define LCD_F_CLCDPCellID3_SIZE                                                            8
#define LCD_F_CLCDPCellID3_MASK                                                   0x000000ff
#define LCD_F_CLCDPCellID3_RANGE                                                         7:0
#define LCD_F_CLCDPCellID3_INIT                                                   0x000000b1
#define LCD_F_CLCDPCellID3_RWMODE                                                         RO

#define LCD_F_CLCDPCellID3_GET(x) \
( \
    (((x) & LCD_F_CLCDPCellID3_MASK) >> \
            LCD_F_CLCDPCellID3_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_CLCDPeriphID3                                                            */
/* Description:  Peripheral Identification Register 3                                       */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89fec                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_CLCDPeriphID3_MEMADDR                                                  0x89fec
#define LCD_R_CLCDPeriphID3_SEL                                                       0x9fec
#define LCD_R_CLCDPeriphID3_SIZE                                                          32
#define LCD_R_CLCDPeriphID3_MASK                                                  0xffffffff
#define LCD_R_CLCDPeriphID3_INIT                                                         0x0
#define LCD_R_CLCDPeriphID3_RWMODE                                                        RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_Configuration                                                               */
/* Description:  These bits read back as 0x0                                                */
/* Width: 8 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [7:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_Configuration_R                                                              0
#define LCD_F_Configuration_L                                                              7
#define LCD_F_Configuration_SIZE                                                           8
#define LCD_F_Configuration_MASK                                                  0x000000ff
#define LCD_F_Configuration_RANGE                                                        7:0
#define LCD_F_Configuration_INIT                                                  0x00000000
#define LCD_F_Configuration_RWMODE                                                        RO

#define LCD_F_Configuration_GET(x) \
( \
    (((x) & LCD_F_Configuration_MASK) >> \
            LCD_F_Configuration_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDICR                                                                   */
/* Description:  LCD Interrupt Clear Register                                               */
/* Width: 32 bits                                                                           */
/* Type: WO                                                                                 */
/* Reg Index: 0x89028                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDICR_MEMADDR                                                         0x89028
#define LCD_R_LCDICR_SEL                                                              0x9028
#define LCD_R_LCDICR_SIZE                                                                 32
#define LCD_R_LCDICR_MASK                                                         0xffffffff
#define LCD_R_LCDICR_INIT                                                                0x0
#define LCD_R_LCDICR_RWMODE                                                               WO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_VcompIC                                                                     */
/* Description:  Clear vertical compare interrupt                                           */
/* Width: 1 bits                                                                            */
/* Type: WO                                                                                 */
/* Bit Mappings: [3:3]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_VcompIC_R                                                                    3
#define LCD_F_VcompIC_L                                                                    3
#define LCD_F_VcompIC_SIZE                                                                 1
#define LCD_F_VcompIC_MASK                                                        0x00000008
#define LCD_F_VcompIC_RANGE                                                              3:3
#define LCD_F_VcompIC_INIT                                                        0x00000000
#define LCD_F_VcompIC_RWMODE                                                              WO

#define LCD_F_VcompIC_GET(x) \
( \
    (((x) & LCD_F_VcompIC_MASK) >> \
            LCD_F_VcompIC_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_FUFIC                                                                       */
/* Description:  Clear FIFO underflow interrupt                                             */
/* Width: 1 bits                                                                            */
/* Type: WO                                                                                 */
/* Bit Mappings: [1:1]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_FUFIC_R                                                                      1
#define LCD_F_FUFIC_L                                                                      1
#define LCD_F_FUFIC_SIZE                                                                   1
#define LCD_F_FUFIC_MASK                                                          0x00000002
#define LCD_F_FUFIC_RANGE                                                                1:1
#define LCD_F_FUFIC_INIT                                                          0x00000000
#define LCD_F_FUFIC_RWMODE                                                                WO

#define LCD_F_FUFIC_GET(x) \
( \
    (((x) & LCD_F_FUFIC_MASK) >> \
            LCD_F_FUFIC_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_MBERRORIC                                                                   */
/* Description:  Clear AHB master error interrupt                                           */
/* Width: 1 bits                                                                            */
/* Type: WO                                                                                 */
/* Bit Mappings: [4:4]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_MBERRORIC_R                                                                  4
#define LCD_F_MBERRORIC_L                                                                  4
#define LCD_F_MBERRORIC_SIZE                                                               1
#define LCD_F_MBERRORIC_MASK                                                      0x00000010
#define LCD_F_MBERRORIC_RANGE                                                            4:4
#define LCD_F_MBERRORIC_INIT                                                      0x00000000
#define LCD_F_MBERRORIC_RWMODE                                                            WO

#define LCD_F_MBERRORIC_GET(x) \
( \
    (((x) & LCD_F_MBERRORIC_MASK) >> \
            LCD_F_MBERRORIC_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LNBUIC                                                                      */
/* Description:  Clear LCD next base address update interrupt                               */
/* Width: 1 bits                                                                            */
/* Type: WO                                                                                 */
/* Bit Mappings: [2:2]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LNBUIC_R                                                                     2
#define LCD_F_LNBUIC_L                                                                     2
#define LCD_F_LNBUIC_SIZE                                                                  1
#define LCD_F_LNBUIC_MASK                                                         0x00000004
#define LCD_F_LNBUIC_RANGE                                                               2:2
#define LCD_F_LNBUIC_INIT                                                         0x00000000
#define LCD_F_LNBUIC_RWMODE                                                               WO

#define LCD_F_LNBUIC_GET(x) \
( \
    (((x) & LCD_F_LNBUIC_MASK) >> \
            LCD_F_LNBUIC_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDMIS                                                                   */
/* Description:  Masked Interrupt Status Register                                           */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89024                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDMIS_MEMADDR                                                         0x89024
#define LCD_R_LCDMIS_SEL                                                              0x9024
#define LCD_R_LCDMIS_SIZE                                                                 32
#define LCD_R_LCDMIS_MASK                                                         0xffffffff
#define LCD_R_LCDMIS_INIT                                                                0x0
#define LCD_R_LCDMIS_RWMODE                                                               RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_VcompMIS                                                                    */
/* Description:  Vertical compare interrupt status bit                                      */
/* Width: 1 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [3:3]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_VcompMIS_R                                                                   3
#define LCD_F_VcompMIS_L                                                                   3
#define LCD_F_VcompMIS_SIZE                                                                1
#define LCD_F_VcompMIS_MASK                                                       0x00000008
#define LCD_F_VcompMIS_RANGE                                                             3:3
#define LCD_F_VcompMIS_INIT                                                       0x00000000
#define LCD_F_VcompMIS_RWMODE                                                             RO

#define LCD_F_VcompMIS_GET(x) \
( \
    (((x) & LCD_F_VcompMIS_MASK) >> \
            LCD_F_VcompMIS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_FUFMIS                                                                      */
/* Description:  FIFO underflow interrupt status bit                                        */
/* Width: 1 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [1:1]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_FUFMIS_R                                                                     1
#define LCD_F_FUFMIS_L                                                                     1
#define LCD_F_FUFMIS_SIZE                                                                  1
#define LCD_F_FUFMIS_MASK                                                         0x00000002
#define LCD_F_FUFMIS_RANGE                                                               1:1
#define LCD_F_FUFMIS_INIT                                                         0x00000000
#define LCD_F_FUFMIS_RWMODE                                                               RO

#define LCD_F_FUFMIS_GET(x) \
( \
    (((x) & LCD_F_FUFMIS_MASK) >> \
            LCD_F_FUFMIS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LNBUMIS                                                                     */
/* Description:  LCD next base address update interrupt status bit                          */
/* Width: 1 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [2:2]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LNBUMIS_R                                                                    2
#define LCD_F_LNBUMIS_L                                                                    2
#define LCD_F_LNBUMIS_SIZE                                                                 1
#define LCD_F_LNBUMIS_MASK                                                        0x00000004
#define LCD_F_LNBUMIS_RANGE                                                              2:2
#define LCD_F_LNBUMIS_INIT                                                        0x00000000
#define LCD_F_LNBUMIS_RWMODE                                                              RO

#define LCD_F_LNBUMIS_GET(x) \
( \
    (((x) & LCD_F_LNBUMIS_MASK) >> \
            LCD_F_LNBUMIS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_MBERRORMIS                                                                  */
/* Description:  AHB master error interrupt status bit                                      */
/* Width: 1 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [4:4]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_MBERRORMIS_R                                                                 4
#define LCD_F_MBERRORMIS_L                                                                 4
#define LCD_F_MBERRORMIS_SIZE                                                              1
#define LCD_F_MBERRORMIS_MASK                                                     0x00000010
#define LCD_F_MBERRORMIS_RANGE                                                           4:4
#define LCD_F_MBERRORMIS_INIT                                                     0x00000000
#define LCD_F_MBERRORMIS_RWMODE                                                           RO

#define LCD_F_MBERRORMIS_GET(x) \
( \
    (((x) & LCD_F_MBERRORMIS_MASK) >> \
            LCD_F_MBERRORMIS_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_ClcdCrsrIMSC                                                             */
/* Description:  Cursor Interrupt Mask Set/Clear Register                                   */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89c20                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_ClcdCrsrIMSC_MEMADDR                                                   0x89c20
#define LCD_R_ClcdCrsrIMSC_SEL                                                        0x9c20
#define LCD_R_ClcdCrsrIMSC_SIZE                                                           32
#define LCD_R_ClcdCrsrIMSC_MASK                                                   0xffffffff
#define LCD_R_ClcdCrsrIMSC_INIT                                                          0x0
#define LCD_R_ClcdCrsrIMSC_RWMODE                                                         RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrIM                                                                      */
/* Description: " When set, the cursor interrupts the processor immediately after reading of the last word of cursor image. When clear, the cursor never interrupts the processor." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [0:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrIM_R                                                                     0
#define LCD_F_CrsrIM_L                                                                     0
#define LCD_F_CrsrIM_SIZE                                                                  1
#define LCD_F_CrsrIM_MASK                                                         0x00000001
#define LCD_F_CrsrIM_RANGE                                                               0:0
#define LCD_F_CrsrIM_INIT                                                         0x00000000
#define LCD_F_CrsrIM_RWMODE                                                               RW

#define LCD_F_CrsrIM_GET(x) \
( \
    (((x) & LCD_F_CrsrIM_MASK) >> \
            LCD_F_CrsrIM_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_CLCDPeriphID2                                                            */
/* Description:  Peripheral Identification Register 2                                       */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89fe8                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_CLCDPeriphID2_MEMADDR                                                  0x89fe8
#define LCD_R_CLCDPeriphID2_SEL                                                       0x9fe8
#define LCD_R_CLCDPeriphID2_SIZE                                                          32
#define LCD_R_CLCDPeriphID2_MASK                                                  0xffffffff
#define LCD_R_CLCDPeriphID2_INIT                                                        0x14
#define LCD_R_CLCDPeriphID2_RWMODE                                                        RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_Designer1                                                                   */
/* Description:  These bits read back as 0x4                                                */
/* Width: 4 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [3:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_Designer1_R                                                                  0
#define LCD_F_Designer1_L                                                                  3
#define LCD_F_Designer1_SIZE                                                               4
#define LCD_F_Designer1_MASK                                                      0x0000000f
#define LCD_F_Designer1_RANGE                                                            3:0
#define LCD_F_Designer1_INIT                                                      0x00000004
#define LCD_F_Designer1_RWMODE                                                            RO

#define LCD_F_Designer1_GET(x) \
( \
    (((x) & LCD_F_Designer1_MASK) >> \
            LCD_F_Designer1_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_Revision                                                                    */
/* Description: These bits provide the revision number of the controller:                                                     0x0 = revision r0p0                                                                                                        0x1 = revision r0p1   */
/* Width: 4 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [7:4]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_Revision_R                                                                   4
#define LCD_F_Revision_L                                                                   7
#define LCD_F_Revision_SIZE                                                                4
#define LCD_F_Revision_MASK                                                       0x000000f0
#define LCD_F_Revision_RANGE                                                             7:4
#define LCD_F_Revision_INIT                                                       0x00000001
#define LCD_F_Revision_RWMODE                                                             RO

#define LCD_F_Revision_GET(x) \
( \
    (((x) & LCD_F_Revision_MASK) >> \
            LCD_F_Revision_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_ClcdCrsrClip                                                             */
/* Description:  Cursor Clip Position Register                                              */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89c14                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_ClcdCrsrClip_MEMADDR                                                   0x89c14
#define LCD_R_ClcdCrsrClip_SEL                                                        0x9c14
#define LCD_R_ClcdCrsrClip_SIZE                                                           32
#define LCD_R_ClcdCrsrClip_MASK                                                   0xffffffff
#define LCD_R_ClcdCrsrClip_INIT                                                          0x0
#define LCD_R_ClcdCrsrClip_RWMODE                                                         RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrClipX                                                                   */
/* Description: " Distance from left edge of cursor image to the first displayed pixel of cursor. When 0, the first pixel of the cursor line is displayed." */
/* Width: 6 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [5:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrClipX_R                                                                  0
#define LCD_F_CrsrClipX_L                                                                  5
#define LCD_F_CrsrClipX_SIZE                                                               6
#define LCD_F_CrsrClipX_MASK                                                      0x0000003f
#define LCD_F_CrsrClipX_RANGE                                                            5:0
#define LCD_F_CrsrClipX_INIT                                                      0x00000000
#define LCD_F_CrsrClipX_RWMODE                                                            RW

#define LCD_F_CrsrClipX_GET(x) \
( \
    (((x) & LCD_F_CrsrClipX_MASK) >> \
            LCD_F_CrsrClipX_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrClipY                                                                   */
/* Description: " Distance from top of cursor image to the first displayed pixel in cursor. When 0, the first displayed pixel is from the top line of the cursor image." */
/* Width: 6 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [13:8]                                                                     */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrClipY_R                                                                  8
#define LCD_F_CrsrClipY_L                                                                 13
#define LCD_F_CrsrClipY_SIZE                                                               6
#define LCD_F_CrsrClipY_MASK                                                      0x00003f00
#define LCD_F_CrsrClipY_RANGE                                                           13:8
#define LCD_F_CrsrClipY_INIT                                                      0x00000000
#define LCD_F_CrsrClipY_RWMODE                                                            RW

#define LCD_F_CrsrClipY_GET(x) \
( \
    (((x) & LCD_F_CrsrClipY_MASK) >> \
            LCD_F_CrsrClipY_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDITOP2                                                                 */
/* Description:  Integration Test Output Register 2                                         */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89f08                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDITOP2_MEMADDR                                                       0x89f08
#define LCD_R_LCDITOP2_SEL                                                            0x9f08
#define LCD_R_LCDITOP2_SIZE                                                               32
#define LCD_R_LCDITOP2_MASK                                                       0xffffffff
#define LCD_R_LCDITOP2_INIT                                                              0x0
#define LCD_R_LCDITOP2_RWMODE                                                             RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLD                                                                         */
/* Description: " Writes specify the values to drive on CLD, when in integration test mode." */
/* Width: 24 bits                                                                           */
/* Type: RW                                                                                 */
/* Bit Mappings: [23:0]                                                                     */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLD_R                                                                        0
#define LCD_F_CLD_L                                                                       23
#define LCD_F_CLD_SIZE                                                                    24
#define LCD_F_CLD_MASK                                                            0x00ffffff
#define LCD_F_CLD_RANGE                                                                 23:0
#define LCD_F_CLD_INIT                                                            0x00000000
#define LCD_F_CLD_RWMODE                                                                  RW

#define LCD_F_CLD_GET(x) \
( \
    (((x) & LCD_F_CLD_MASK) >> \
            LCD_F_CLD_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLFP                                                                        */
/* Description: " Writes specify the value to drive on CLFP, when in integration test mode." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [26:26]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLFP_R                                                                      26
#define LCD_F_CLFP_L                                                                      26
#define LCD_F_CLFP_SIZE                                                                    1
#define LCD_F_CLFP_MASK                                                           0x04000000
#define LCD_F_CLFP_RANGE                                                               26:26
#define LCD_F_CLFP_INIT                                                           0x00000000
#define LCD_F_CLFP_RWMODE                                                                 RW

#define LCD_F_CLFP_GET(x) \
( \
    (((x) & LCD_F_CLFP_MASK) >> \
            LCD_F_CLFP_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCP                                                                        */
/* Description: " Writes specify the value to drive on CLCP, when in integration test mode." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [27:27]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCP_R                                                                      27
#define LCD_F_CLCP_L                                                                      27
#define LCD_F_CLCP_SIZE                                                                    1
#define LCD_F_CLCP_MASK                                                           0x08000000
#define LCD_F_CLCP_RANGE                                                               27:27
#define LCD_F_CLCP_INIT                                                           0x00000000
#define LCD_F_CLCP_RWMODE                                                                 RW

#define LCD_F_CLCP_GET(x) \
( \
    (((x) & LCD_F_CLCP_MASK) >> \
            LCD_F_CLCP_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLLE                                                                        */
/* Description: " Writes specify the value to drive on CLLE, when in integration test mode." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [24:24]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLLE_R                                                                      24
#define LCD_F_CLLE_L                                                                      24
#define LCD_F_CLLE_SIZE                                                                    1
#define LCD_F_CLLE_MASK                                                           0x01000000
#define LCD_F_CLLE_RANGE                                                               24:24
#define LCD_F_CLLE_INIT                                                           0x00000000
#define LCD_F_CLLE_RWMODE                                                                 RW

#define LCD_F_CLLE_GET(x) \
( \
    (((x) & LCD_F_CLLE_MASK) >> \
            LCD_F_CLLE_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLLP                                                                        */
/* Description: " Writes specify the value to drive on CLLP, when in integration test mode." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [28:28]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLLP_R                                                                      28
#define LCD_F_CLLP_L                                                                      28
#define LCD_F_CLLP_SIZE                                                                    1
#define LCD_F_CLLP_MASK                                                           0x10000000
#define LCD_F_CLLP_RANGE                                                               28:28
#define LCD_F_CLLP_INIT                                                           0x00000000
#define LCD_F_CLLP_RWMODE                                                                 RW

#define LCD_F_CLLP_GET(x) \
( \
    (((x) & LCD_F_CLLP_MASK) >> \
            LCD_F_CLLP_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLPOWER                                                                     */
/* Description: " Writes specify the value to drive on CLPOWER, when in integration test mode." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [29:29]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLPOWER_R                                                                   29
#define LCD_F_CLPOWER_L                                                                   29
#define LCD_F_CLPOWER_SIZE                                                                 1
#define LCD_F_CLPOWER_MASK                                                        0x20000000
#define LCD_F_CLPOWER_RANGE                                                            29:29
#define LCD_F_CLPOWER_INIT                                                        0x00000000
#define LCD_F_CLPOWER_RWMODE                                                              RW

#define LCD_F_CLPOWER_GET(x) \
( \
    (((x) & LCD_F_CLPOWER_MASK) >> \
            LCD_F_CLPOWER_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLAC                                                                        */
/* Description: "Writes specify the value to drive on CLAC, when in integration test mode." */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [25:25]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLAC_R                                                                      25
#define LCD_F_CLAC_L                                                                      25
#define LCD_F_CLAC_SIZE                                                                    1
#define LCD_F_CLAC_MASK                                                           0x02000000
#define LCD_F_CLAC_RANGE                                                               25:25
#define LCD_F_CLAC_INIT                                                           0x00000000
#define LCD_F_CLAC_RWMODE                                                                 RW

#define LCD_F_CLAC_GET(x) \
( \
    (((x) & LCD_F_CLAC_MASK) >> \
            LCD_F_CLAC_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_CLCDPeriphID0                                                            */
/* Description:  Peripheral Identification Register 0                                       */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89fe0                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_CLCDPeriphID0_MEMADDR                                                  0x89fe0
#define LCD_R_CLCDPeriphID0_SEL                                                       0x9fe0
#define LCD_R_CLCDPeriphID0_SIZE                                                          32
#define LCD_R_CLCDPeriphID0_MASK                                                  0xffffffff
#define LCD_R_CLCDPeriphID0_INIT                                                        0x11
#define LCD_R_CLCDPeriphID0_RWMODE                                                        RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_PartNumber0                                                                 */
/* Description:  These bits read back as 0x11                                               */
/* Width: 8 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [7:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_PartNumber0_R                                                                0
#define LCD_F_PartNumber0_L                                                                7
#define LCD_F_PartNumber0_SIZE                                                             8
#define LCD_F_PartNumber0_MASK                                                    0x000000ff
#define LCD_F_PartNumber0_RANGE                                                          7:0
#define LCD_F_PartNumber0_INIT                                                    0x00000011
#define LCD_F_PartNumber0_RWMODE                                                          RO

#define LCD_F_PartNumber0_GET(x) \
( \
    (((x) & LCD_F_PartNumber0_MASK) >> \
            LCD_F_PartNumber0_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_CLCDPCellID1                                                             */
/* Description:  PrimeCell Identification Register 1                                        */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89ff4                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_CLCDPCellID1_MEMADDR                                                   0x89ff4
#define LCD_R_CLCDPCellID1_SEL                                                        0x9ff4
#define LCD_R_CLCDPCellID1_SIZE                                                           32
#define LCD_R_CLCDPCellID1_MASK                                                   0xffffffff
#define LCD_R_CLCDPCellID1_INIT                                                         0xf0
#define LCD_R_CLCDPCellID1_RWMODE                                                         RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCDPCellID1                                                                */
/* Description:  These bits read back as 0xF0                                               */
/* Width: 8 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [7:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCDPCellID1_R                                                               0
#define LCD_F_CLCDPCellID1_L                                                               7
#define LCD_F_CLCDPCellID1_SIZE                                                            8
#define LCD_F_CLCDPCellID1_MASK                                                   0x000000ff
#define LCD_F_CLCDPCellID1_RANGE                                                         7:0
#define LCD_F_CLCDPCellID1_INIT                                                   0x000000f0
#define LCD_F_CLCDPCellID1_RWMODE                                                         RO

#define LCD_F_CLCDPCellID1_GET(x) \
( \
    (((x) & LCD_F_CLCDPCellID1_MASK) >> \
            LCD_F_CLCDPCellID1_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_ClcdCrsrXY                                                               */
/* Description:  Cursor XY Position Register                                                */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89c10                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_ClcdCrsrXY_MEMADDR                                                     0x89c10
#define LCD_R_ClcdCrsrXY_SEL                                                          0x9c10
#define LCD_R_ClcdCrsrXY_SIZE                                                             32
#define LCD_R_ClcdCrsrXY_MASK                                                     0xffffffff
#define LCD_R_ClcdCrsrXY_INIT                                                            0x0
#define LCD_R_ClcdCrsrXY_RWMODE                                                           RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrX_expansion                                                             */
/* Description:  Reserved for coordinate expansion. You must write this as zero.            */
/* Width: 2 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [11:10]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrX_expansion_R                                                           10
#define LCD_F_CrsrX_expansion_L                                                           11
#define LCD_F_CrsrX_expansion_SIZE                                                         2
#define LCD_F_CrsrX_expansion_MASK                                                0x00000c00
#define LCD_F_CrsrX_expansion_RANGE                                                    11:10
#define LCD_F_CrsrX_expansion_INIT                                                0x00000000
#define LCD_F_CrsrX_expansion_RWMODE                                                      RW

#define LCD_F_CrsrX_expansion_GET(x) \
( \
    (((x) & LCD_F_CrsrX_expansion_MASK) >> \
            LCD_F_CrsrX_expansion_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrX                                                                       */
/* Description: " X ordinate of the cursor origin measured in pixels. When 0, the left edge of the cursor is at the left edge of the display." */
/* Width: 10 bits                                                                           */
/* Type: RW                                                                                 */
/* Bit Mappings: [9:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrX_R                                                                      0
#define LCD_F_CrsrX_L                                                                      9
#define LCD_F_CrsrX_SIZE                                                                  10
#define LCD_F_CrsrX_MASK                                                          0x000003ff
#define LCD_F_CrsrX_RANGE                                                                9:0
#define LCD_F_CrsrX_INIT                                                          0x00000000
#define LCD_F_CrsrX_RWMODE                                                                RW

#define LCD_F_CrsrX_GET(x) \
( \
    (((x) & LCD_F_CrsrX_MASK) >> \
            LCD_F_CrsrX_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrY_expansion                                                             */
/* Description:  Reserved for coordinate expansion. You must write this as zero.            */
/* Width: 2 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [27:26]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrY_expansion_R                                                           26
#define LCD_F_CrsrY_expansion_L                                                           27
#define LCD_F_CrsrY_expansion_SIZE                                                         2
#define LCD_F_CrsrY_expansion_MASK                                                0x0c000000
#define LCD_F_CrsrY_expansion_RANGE                                                    27:26
#define LCD_F_CrsrY_expansion_INIT                                                0x00000000
#define LCD_F_CrsrY_expansion_RWMODE                                                      RW

#define LCD_F_CrsrY_expansion_GET(x) \
( \
    (((x) & LCD_F_CrsrY_expansion_MASK) >> \
            LCD_F_CrsrY_expansion_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrY                                                                       */
/* Description: " Y ordinate of the cursor origin measured in pixels. When 0, the top edge of the cursor is at the top of the display." */
/* Width: 10 bits                                                                           */
/* Type: RW                                                                                 */
/* Bit Mappings: [25:16]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrY_R                                                                     16
#define LCD_F_CrsrY_L                                                                     25
#define LCD_F_CrsrY_SIZE                                                                  10
#define LCD_F_CrsrY_MASK                                                          0x03ff0000
#define LCD_F_CrsrY_RANGE                                                              25:16
#define LCD_F_CrsrY_INIT                                                          0x00000000
#define LCD_F_CrsrY_RWMODE                                                                RW

#define LCD_F_CrsrY_GET(x) \
( \
    (((x) & LCD_F_CrsrY_MASK) >> \
            LCD_F_CrsrY_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDTiming3                                                               */
/* Description:  Line End Control Register                                                  */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x8900c                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDTiming3_MEMADDR                                                     0x8900c
#define LCD_R_LCDTiming3_SEL                                                          0x900c
#define LCD_R_LCDTiming3_SIZE                                                             32
#define LCD_R_LCDTiming3_MASK                                                     0xffffffff
#define LCD_R_LCDTiming3_INIT                                                            0x0
#define LCD_R_LCDTiming3_RWMODE                                                           RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LED                                                                         */
/* Description: " Line-end signal delay from the rising-edge of the last panel clock, CLCP. Program with number of CLCDCLK clock periods, minus 1." */
/* Width: 7 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [6:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LED_R                                                                        0
#define LCD_F_LED_L                                                                        6
#define LCD_F_LED_SIZE                                                                     7
#define LCD_F_LED_MASK                                                            0x0000007f
#define LCD_F_LED_RANGE                                                                  6:0
#define LCD_F_LED_INIT                                                            0x00000000
#define LCD_F_LED_RWMODE                                                                  RW

#define LCD_F_LED_GET(x) \
( \
    (((x) & LCD_F_LED_MASK) >> \
            LCD_F_LED_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LEE                                                                         */
/* Description:  LCD Line end enable:                                                                                                    0 = CLLE disabled (held LOW)                                                                                       1 = CLLE signal act */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [16:16]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LEE_R                                                                       16
#define LCD_F_LEE_L                                                                       16
#define LCD_F_LEE_SIZE                                                                     1
#define LCD_F_LEE_MASK                                                            0x00010000
#define LCD_F_LEE_RANGE                                                                16:16
#define LCD_F_LEE_INIT                                                            0x00000000
#define LCD_F_LEE_RWMODE                                                                  RW

#define LCD_F_LEE_GET(x) \
( \
    (((x) & LCD_F_LEE_MASK) >> \
            LCD_F_LEE_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_ClcdCrsrPalette1                                                         */
/* Description: Cursor Palette Registers                                                    */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89c0c                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_ClcdCrsrPalette1_MEMADDR                                               0x89c0c
#define LCD_R_ClcdCrsrPalette1_SEL                                                    0x9c0c
#define LCD_R_ClcdCrsrPalette1_SIZE                                                       32
#define LCD_R_ClcdCrsrPalette1_MASK                                               0xffffffff
#define LCD_R_ClcdCrsrPalette1_INIT                                                      0x0
#define LCD_R_ClcdCrsrPalette1_RWMODE                                                     RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_Red_palette1                                                                */
/* Description:  Red color component                                                        */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [7:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_Red_palette1_R                                                               0
#define LCD_F_Red_palette1_L                                                               7
#define LCD_F_Red_palette1_SIZE                                                            8
#define LCD_F_Red_palette1_MASK                                                   0x000000ff
#define LCD_F_Red_palette1_RANGE                                                         7:0
#define LCD_F_Red_palette1_INIT                                                   0x00000000
#define LCD_F_Red_palette1_RWMODE                                                         RW

#define LCD_F_Red_palette1_GET(x) \
( \
    (((x) & LCD_F_Red_palette1_MASK) >> \
            LCD_F_Red_palette1_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_Blue_palette1                                                               */
/* Description:  Blue color component                                                       */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [23:16]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_Blue_palette1_R                                                             16
#define LCD_F_Blue_palette1_L                                                             23
#define LCD_F_Blue_palette1_SIZE                                                           8
#define LCD_F_Blue_palette1_MASK                                                  0x00ff0000
#define LCD_F_Blue_palette1_RANGE                                                      23:16
#define LCD_F_Blue_palette1_INIT                                                  0x00000000
#define LCD_F_Blue_palette1_RWMODE                                                        RW

#define LCD_F_Blue_palette1_GET(x) \
( \
    (((x) & LCD_F_Blue_palette1_MASK) >> \
            LCD_F_Blue_palette1_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_Green_palette1                                                              */
/* Description:  Green color component                                                      */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [15:8]                                                                     */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_Green_palette1_R                                                             8
#define LCD_F_Green_palette1_L                                                            15
#define LCD_F_Green_palette1_SIZE                                                          8
#define LCD_F_Green_palette1_MASK                                                 0x0000ff00
#define LCD_F_Green_palette1_RANGE                                                      15:8
#define LCD_F_Green_palette1_INIT                                                 0x00000000
#define LCD_F_Green_palette1_RWMODE                                                       RW

#define LCD_F_Green_palette1_GET(x) \
( \
    (((x) & LCD_F_Green_palette1_MASK) >> \
            LCD_F_Green_palette1_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDLPBASE                                                                */
/* Description:  Upper and Lower Panel Frame Base Address Registers                         */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89014                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDLPBASE_MEMADDR                                                      0x89014
#define LCD_R_LCDLPBASE_SEL                                                           0x9014
#define LCD_R_LCDLPBASE_SIZE                                                              32
#define LCD_R_LCDLPBASE_MASK                                                      0xffffffff
#define LCD_R_LCDLPBASE_INIT                                                             0x0
#define LCD_R_LCDLPBASE_RWMODE                                                            RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LCDLPBASE                                                                   */
/* Description:  LCD lower panel base address. This is the start address of the lower panel frame data in memory and is doubleword-aligned. */
/* Width: 29 bits                                                                           */
/* Type: RW                                                                                 */
/* Bit Mappings: [31:3]                                                                     */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LCDLPBASE_R                                                                  3
#define LCD_F_LCDLPBASE_L                                                                 31
#define LCD_F_LCDLPBASE_SIZE                                                              29
#define LCD_F_LCDLPBASE_MASK                                                      0xfffffff8
#define LCD_F_LCDLPBASE_RANGE                                                           31:3
#define LCD_F_LCDLPBASE_INIT                                                      0x00000000
#define LCD_F_LCDLPBASE_RWMODE                                                            RW

#define LCD_F_LCDLPBASE_GET(x) \
( \
    (((x) & LCD_F_LCDLPBASE_MASK) >> \
            LCD_F_LCDLPBASE_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDTCR                                                                   */
/* Description:  Integration Test Control Register                                          */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89f00                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDTCR_MEMADDR                                                         0x89f00
#define LCD_R_LCDTCR_SEL                                                              0x9f00
#define LCD_R_LCDTCR_SIZE                                                                 32
#define LCD_R_LCDTCR_MASK                                                         0xffffffff
#define LCD_R_LCDTCR_INIT                                                                0x0
#define LCD_R_LCDTCR_RWMODE                                                               RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_ITEN                                                                        */
/* Description:  Integration test enable:                                                                                                0 = normal CLCDC operation                                                                                         1 = integration tes */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [0:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_ITEN_R                                                                       0
#define LCD_F_ITEN_L                                                                       0
#define LCD_F_ITEN_SIZE                                                                    1
#define LCD_F_ITEN_MASK                                                           0x00000001
#define LCD_F_ITEN_RANGE                                                                 0:0
#define LCD_F_ITEN_INIT                                                           0x00000000
#define LCD_F_ITEN_RWMODE                                                                 RW

#define LCD_F_ITEN_GET(x) \
( \
    (((x) & LCD_F_ITEN_MASK) >> \
            LCD_F_ITEN_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_ClcdCrsrConfig                                                           */
/* Description:  Cursor Configuration Register                                              */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89c04                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_ClcdCrsrConfig_MEMADDR                                                 0x89c04
#define LCD_R_ClcdCrsrConfig_SEL                                                      0x9c04
#define LCD_R_ClcdCrsrConfig_SIZE                                                         32
#define LCD_R_ClcdCrsrConfig_MASK                                                 0xffffffff
#define LCD_R_ClcdCrsrConfig_INIT                                                        0x0
#define LCD_R_ClcdCrsrConfig_RWMODE                                                       RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrFrameSync                                                               */
/* Description: 0 = Cursor coordinates asynchronous                                                                             1 = Cursor coordinates synchronized to frame synchronization pulse */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [1:1]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrFrameSync_R                                                              1
#define LCD_F_CrsrFrameSync_L                                                              1
#define LCD_F_CrsrFrameSync_SIZE                                                           1
#define LCD_F_CrsrFrameSync_MASK                                                  0x00000002
#define LCD_F_CrsrFrameSync_RANGE                                                        1:1
#define LCD_F_CrsrFrameSync_INIT                                                  0x00000000
#define LCD_F_CrsrFrameSync_RWMODE                                                        RW

#define LCD_F_CrsrFrameSync_GET(x) \
( \
    (((x) & LCD_F_CrsrFrameSync_MASK) >> \
            LCD_F_CrsrFrameSync_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrSize                                                                    */
/* Description: 0 = 32x32 pixel cursor                                                                                                   1 = 64x64 pixel cursor */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [0:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrSize_R                                                                   0
#define LCD_F_CrsrSize_L                                                                   0
#define LCD_F_CrsrSize_SIZE                                                                1
#define LCD_F_CrsrSize_MASK                                                       0x00000001
#define LCD_F_CrsrSize_RANGE                                                             0:0
#define LCD_F_CrsrSize_INIT                                                       0x00000000
#define LCD_F_CrsrSize_RWMODE                                                             RW

#define LCD_F_CrsrSize_GET(x) \
( \
    (((x) & LCD_F_CrsrSize_MASK) >> \
            LCD_F_CrsrSize_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDUPCURR                                                                */
/* Description:  LCD Upper and Lower Panel Current Address Value Registers                  */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x8902c                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDUPCURR_MEMADDR                                                      0x8902c
#define LCD_R_LCDUPCURR_SEL                                                           0x902c
#define LCD_R_LCDUPCURR_SIZE                                                              32
#define LCD_R_LCDUPCURR_MASK                                                      0xffffffff
#define LCD_R_LCDUPCURR_INIT                                                             0x0
#define LCD_R_LCDUPCURR_RWMODE                                                            RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LCDUPCURR                                                                   */
/* Description:  LCD Upper and Lower Panel Current Address Value Registers                  */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Bit Mappings: [31:0]                                                                     */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LCDUPCURR_R                                                                  0
#define LCD_F_LCDUPCURR_L                                                                 31
#define LCD_F_LCDUPCURR_SIZE                                                              32
#define LCD_F_LCDUPCURR_MASK                                                      0xffffffff
#define LCD_F_LCDUPCURR_RANGE                                                           31:0
#define LCD_F_LCDUPCURR_INIT                                                      0x00000000
#define LCD_F_LCDUPCURR_RWMODE                                                            RO

#define LCD_F_LCDUPCURR_GET(x) \
( \
    (((x) & LCD_F_LCDUPCURR_MASK) >> \
            LCD_F_LCDUPCURR_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDIMSC                                                                  */
/* Description:  Interrupt Mask Set/Clear Register                                          */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x8901c                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDIMSC_MEMADDR                                                        0x8901c
#define LCD_R_LCDIMSC_SEL                                                             0x901c
#define LCD_R_LCDIMSC_SIZE                                                                32
#define LCD_R_LCDIMSC_MASK                                                        0xffffffff
#define LCD_R_LCDIMSC_INIT                                                               0x0
#define LCD_R_LCDIMSC_RWMODE                                                              RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_MBERRORIM                                                                   */
/* Description:  AHB master error interrupt enable                                          */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [4:4]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_MBERRORIM_R                                                                  4
#define LCD_F_MBERRORIM_L                                                                  4
#define LCD_F_MBERRORIM_SIZE                                                               1
#define LCD_F_MBERRORIM_MASK                                                      0x00000010
#define LCD_F_MBERRORIM_RANGE                                                            4:4
#define LCD_F_MBERRORIM_INIT                                                      0x00000000
#define LCD_F_MBERRORIM_RWMODE                                                            RW

#define LCD_F_MBERRORIM_GET(x) \
( \
    (((x) & LCD_F_MBERRORIM_MASK) >> \
            LCD_F_MBERRORIM_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_LNBUIM                                                                      */
/* Description:  LCD next base address update interrupt enable                              */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [2:2]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_LNBUIM_R                                                                     2
#define LCD_F_LNBUIM_L                                                                     2
#define LCD_F_LNBUIM_SIZE                                                                  1
#define LCD_F_LNBUIM_MASK                                                         0x00000004
#define LCD_F_LNBUIM_RANGE                                                               2:2
#define LCD_F_LNBUIM_INIT                                                         0x00000000
#define LCD_F_LNBUIM_RWMODE                                                               RW

#define LCD_F_LNBUIM_GET(x) \
( \
    (((x) & LCD_F_LNBUIM_MASK) >> \
            LCD_F_LNBUIM_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_VcompIM                                                                     */
/* Description:  Vertical compare interrupt enable                                          */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [3:3]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_VcompIM_R                                                                    3
#define LCD_F_VcompIM_L                                                                    3
#define LCD_F_VcompIM_SIZE                                                                 1
#define LCD_F_VcompIM_MASK                                                        0x00000008
#define LCD_F_VcompIM_RANGE                                                              3:3
#define LCD_F_VcompIM_INIT                                                        0x00000000
#define LCD_F_VcompIM_RWMODE                                                              RW

#define LCD_F_VcompIM_GET(x) \
( \
    (((x) & LCD_F_VcompIM_MASK) >> \
            LCD_F_VcompIM_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_FUFIM                                                                       */
/* Description:  FIFO underflow interrupt enable                                            */
/* Width: 1 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [1:1]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_FUFIM_R                                                                      1
#define LCD_F_FUFIM_L                                                                      1
#define LCD_F_FUFIM_SIZE                                                                   1
#define LCD_F_FUFIM_MASK                                                          0x00000002
#define LCD_F_FUFIM_RANGE                                                                1:1
#define LCD_F_FUFIM_INIT                                                          0x00000000
#define LCD_F_FUFIM_RWMODE                                                                RW

#define LCD_F_FUFIM_GET(x) \
( \
    (((x) & LCD_F_FUFIM_MASK) >> \
            LCD_F_FUFIM_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_ClcdCrsrPalette0                                                         */
/* Description: Cursor Palette Registers                                                    */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89c08                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_ClcdCrsrPalette0_MEMADDR                                               0x89c08
#define LCD_R_ClcdCrsrPalette0_SEL                                                    0x9c08
#define LCD_R_ClcdCrsrPalette0_SIZE                                                       32
#define LCD_R_ClcdCrsrPalette0_MASK                                               0xffffffff
#define LCD_R_ClcdCrsrPalette0_INIT                                                      0x0
#define LCD_R_ClcdCrsrPalette0_RWMODE                                                     RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_Blue                                                                        */
/* Description:  Blue color component                                                       */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [23:16]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_Blue_R                                                                      16
#define LCD_F_Blue_L                                                                      23
#define LCD_F_Blue_SIZE                                                                    8
#define LCD_F_Blue_MASK                                                           0x00ff0000
#define LCD_F_Blue_RANGE                                                               23:16
#define LCD_F_Blue_INIT                                                           0x00000000
#define LCD_F_Blue_RWMODE                                                                 RW

#define LCD_F_Blue_GET(x) \
( \
    (((x) & LCD_F_Blue_MASK) >> \
            LCD_F_Blue_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_Red                                                                         */
/* Description:  Red color component                                                        */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [7:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_Red_R                                                                        0
#define LCD_F_Red_L                                                                        7
#define LCD_F_Red_SIZE                                                                     8
#define LCD_F_Red_MASK                                                            0x000000ff
#define LCD_F_Red_RANGE                                                                  7:0
#define LCD_F_Red_INIT                                                            0x00000000
#define LCD_F_Red_RWMODE                                                                  RW

#define LCD_F_Red_GET(x) \
( \
    (((x) & LCD_F_Red_MASK) >> \
            LCD_F_Red_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_Green                                                                       */
/* Description:  Green color component                                                      */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [15:8]                                                                     */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_Green_R                                                                      8
#define LCD_F_Green_L                                                                     15
#define LCD_F_Green_SIZE                                                                   8
#define LCD_F_Green_MASK                                                          0x0000ff00
#define LCD_F_Green_RANGE                                                               15:8
#define LCD_F_Green_INIT                                                          0x00000000
#define LCD_F_Green_RWMODE                                                                RW

#define LCD_F_Green_GET(x) \
( \
    (((x) & LCD_F_Green_MASK) >> \
            LCD_F_Green_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_ClcdCrsrICR                                                              */
/* Description:  Cursor Interrupt Clear Register                                            */
/* Width: 32 bits                                                                           */
/* Type: WO                                                                                 */
/* Reg Index: 0x89c24                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_ClcdCrsrICR_MEMADDR                                                    0x89c24
#define LCD_R_ClcdCrsrICR_SEL                                                         0x9c24
#define LCD_R_ClcdCrsrICR_SIZE                                                            32
#define LCD_R_ClcdCrsrICR_MASK                                                    0xffffffff
#define LCD_R_ClcdCrsrICR_INIT                                                           0x0
#define LCD_R_ClcdCrsrICR_RWMODE                                                          WO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CrsrIC                                                                      */
/* Description: " When set, the cursor interrupt status is cleared. Writing 0 to this bit has no effect." */
/* Width: 1 bits                                                                            */
/* Type: WO                                                                                 */
/* Bit Mappings: [0:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CrsrIC_R                                                                     0
#define LCD_F_CrsrIC_L                                                                     0
#define LCD_F_CrsrIC_SIZE                                                                  1
#define LCD_F_CrsrIC_MASK                                                         0x00000001
#define LCD_F_CrsrIC_RANGE                                                               0:0
#define LCD_F_CrsrIC_INIT                                                         0x00000000
#define LCD_F_CrsrIC_RWMODE                                                               WO

#define LCD_F_CrsrIC_GET(x) \
( \
    (((x) & LCD_F_CrsrIC_MASK) >> \
            LCD_F_CrsrIC_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_CLCDPCellID2                                                             */
/* Description:  PrimeCell Identification Register 2                                        */
/* Width: 32 bits                                                                           */
/* Type: RO                                                                                 */
/* Reg Index: 0x89ff8                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_CLCDPCellID2_MEMADDR                                                   0x89ff8
#define LCD_R_CLCDPCellID2_SEL                                                        0x9ff8
#define LCD_R_CLCDPCellID2_SIZE                                                           32
#define LCD_R_CLCDPCellID2_MASK                                                   0xffffffff
#define LCD_R_CLCDPCellID2_INIT                                                          0x5
#define LCD_R_CLCDPCellID2_RWMODE                                                         RO

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_CLCDPCellID2                                                                */
/* Description:  These bits read back as 0x05                                               */
/* Width: 8 bits                                                                            */
/* Type: RO                                                                                 */
/* Bit Mappings: [7:0]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_CLCDPCellID2_R                                                               0
#define LCD_F_CLCDPCellID2_L                                                               7
#define LCD_F_CLCDPCellID2_SIZE                                                            8
#define LCD_F_CLCDPCellID2_MASK                                                   0x000000ff
#define LCD_F_CLCDPCellID2_RANGE                                                         7:0
#define LCD_F_CLCDPCellID2_INIT                                                   0x00000005
#define LCD_F_CLCDPCellID2_RWMODE                                                         RO

#define LCD_F_CLCDPCellID2_GET(x) \
( \
    (((x) & LCD_F_CLCDPCellID2_MASK) >> \
            LCD_F_CLCDPCellID2_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Register: LCD_R_LCDTiming0                                                               */
/* Description:  Horizontal Axis Panel Control Register                                     */
/* Width: 32 bits                                                                           */
/* Type: RW                                                                                 */
/* Reg Index: 0x89000                                                                       */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_R_LCDTiming0_MEMADDR                                                     0x89000
#define LCD_R_LCDTiming0_SEL                                                          0x9000
#define LCD_R_LCDTiming0_SIZE                                                             32
#define LCD_R_LCDTiming0_MASK                                                     0xffffffff
#define LCD_R_LCDTiming0_INIT                                                            0x0
#define LCD_R_LCDTiming0_RWMODE                                                           RW

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_HFP                                                                         */
/* Description: " Horizontal front porch. This is the number of CLCP periods between the end of active data and the rising edge of CLLP. Program with value minus 1. The 8-bit HFP field sets the number of pixel clock intervals at the end of each line or row of pixels, befo" */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [23:16]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_HFP_R                                                                       16
#define LCD_F_HFP_L                                                                       23
#define LCD_F_HFP_SIZE                                                                     8
#define LCD_F_HFP_MASK                                                            0x00ff0000
#define LCD_F_HFP_RANGE                                                                23:16
#define LCD_F_HFP_INIT                                                            0x00000000
#define LCD_F_HFP_RWMODE                                                                  RW

#define LCD_F_HFP_GET(x) \
( \
    (((x) & LCD_F_HFP_MASK) >> \
            LCD_F_HFP_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_HBP                                                                         */
/* Description:  Horizontal back porch. This is the number of CLCP periods between the falling edge of CLLP and the start of active data. Program with value minus 1. Use the 8-bit HBP field to specify the number of pixel clock periods inserted at the beginning of each li */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [31:24]                                                                    */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_HBP_R                                                                       24
#define LCD_F_HBP_L                                                                       31
#define LCD_F_HBP_SIZE                                                                     8
#define LCD_F_HBP_MASK                                                            0xff000000
#define LCD_F_HBP_RANGE                                                                31:24
#define LCD_F_HBP_INIT                                                            0x00000000
#define LCD_F_HBP_RWMODE                                                                  RW

#define LCD_F_HBP_GET(x) \
( \
    (((x) & LCD_F_HBP_MASK) >> \
            LCD_F_HBP_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_PPL                                                                         */
/* Description:  Pixels-per-line. Actual pixels-per-line = 16 * (PPL + 1). This field specifies the number of pixels in each line of the screen. PPL is a 6-bit value that represents between 16 and 1024 pixels-per-line. PPL counts the number of pixel clocks that occur bef */
/* Width: 6 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [7:2]                                                                      */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_PPL_R                                                                        2
#define LCD_F_PPL_L                                                                        7
#define LCD_F_PPL_SIZE                                                                     6
#define LCD_F_PPL_MASK                                                            0x000000fc
#define LCD_F_PPL_RANGE                                                                  7:2
#define LCD_F_PPL_INIT                                                            0x00000000
#define LCD_F_PPL_RWMODE                                                                  RW

#define LCD_F_PPL_GET(x) \
( \
    (((x) & LCD_F_PPL_MASK) >> \
            LCD_F_PPL_R) \
)

/* ---------------------------------------------------------------------------------------- */
/*                                                                                          */
/* Field: LCD_F_HSW                                                                         */
/* Description: " Horizontal synchronization pulse width. This is the width of the CLLP signal in CLCP periods. Program with value minus 1. The 8-bit HSW field specifies the pulse width of the line clock in passive mode, or the horizontal synchronization pulse in active m" */
/* Width: 8 bits                                                                            */
/* Type: RW                                                                                 */
/* Bit Mappings: [15:8]                                                                     */
/*                                                                                          */
/* ---------------------------------------------------------------------------------------- */
#define LCD_F_HSW_R                                                                        8
#define LCD_F_HSW_L                                                                       15
#define LCD_F_HSW_SIZE                                                                     8
#define LCD_F_HSW_MASK                                                            0x0000ff00
#define LCD_F_HSW_RANGE                                                                 15:8
#define LCD_F_HSW_INIT                                                            0x00000000
#define LCD_F_HSW_RWMODE                                                                  RW

#define LCD_F_HSW_GET(x) \
( \
    (((x) & LCD_F_HSW_MASK) >> \
            LCD_F_HSW_R) \
)

#undef PDEBUG             /* undef it, just in case */
#ifdef IPROC_LCD_DEBUG
#  ifdef __KERNEL__
	/* This one if debugging is on, and kernel space */
#	define PDEBUG(fmt, args...) printk( KERN_CRIT "IPROC_LCD: " fmt, ## args)
#  else
	/* This one for user space */
#	define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#  endif
#else
#  define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif

/* currently we are still debugging */
#undef PDEBUG
#	define PDEBUG(fmt, args...) printk( KERN_CRIT "IPROC_LCD: " fmt, ## args)


/* IOCTL number for setting of LCD buffer address */
/* this is used for external allocation, like using VDEC buffer for LCD display */
#define FBIO_SET_BUFFER_ADDR	_IOW('F', 0x55, __u32)    /* 'F' is for frame buffer, 0x55 is a number >= 0x20 */



/**********************************************************************
 * LCD panel parameter values
 **********************************************************************/
#define IPROC_LCD_800x480_PPL     800
#define IPROC_LCD_800x480_HBP      88
#define IPROC_LCD_800x480_HFP      40
#define IPROC_LCD_800x480_HSW      48
#define IPROC_LCD_800x480_LPP     480
#define IPROC_LCD_800x480_VSW       3
#define IPROC_LCD_800x480_VBP      33
#define IPROC_LCD_800x480_VFP      14
#define IPROC_LCD_800x480_LED       3

/* PP max output size is 720x720, so this set can be used for 800x480 panels */
#define IPROC_LCD_720x480_PPL     720
#define IPROC_LCD_720x480_HBP      40
#define IPROC_LCD_720x480_HFP     187
#define IPROC_LCD_720x480_HSW       1
#define IPROC_LCD_720x480_LPP     480
#define IPROC_LCD_720x480_VSW       1
#define IPROC_LCD_720x480_VBP      33
#define IPROC_LCD_720x480_VFP      12
#define IPROC_LCD_720x480_LED       5

#define IPROC_LCD_640x480_PPL     640
#define IPROC_LCD_640x480_HBP       2
#define IPROC_LCD_640x480_HFP     143
#define IPROC_LCD_640x480_HSW      30
#define IPROC_LCD_640x480_LPP     480
#define IPROC_LCD_640x480_VSW       3
#define IPROC_LCD_640x480_VBP      33
#define IPROC_LCD_640x480_VFP      12
#define IPROC_LCD_640x480_LED       5

#define IPROC_LCD_480x272_PPL     480
#define IPROC_LCD_480x272_HBP       2
#define IPROC_LCD_480x272_HFP       2
#define IPROC_LCD_480x272_HSW      41
#define IPROC_LCD_480x272_LPP     272
#define IPROC_LCD_480x272_VSW      10
#define IPROC_LCD_480x272_VBP       2
#define IPROC_LCD_480x272_VFP       2
#define IPROC_LCD_480x272_LED       5


#define IPROC_LCD_RES_DEFAULT_X     IPROC_LCD_800x480_PPL
#define IPROC_LCD_RES_DEFAULT_Y     IPROC_LCD_800x480_LPP

#define IPROC_LCD_RES_BBP_MAX   32 /* MAX is 24bpp, but 24 actually uses 32bpp */
#define IPROC_LCD_RES_BBP_DEF   32
#define IPROC_LCD_RES_CTRL_LCDVCOMP   12

#define IPROC_LCD_DEF_CLKDIV	   	8
#define IPROC_LCD_INVALID_CLKDIV  0xFFFFFFFF

#define IPROC_LCD_FRAME_ALIGN(a) (a)

/**********************************************************************
 * LCD panel related info structure
 **********************************************************************/
struct iproc_lcd_panel {
	u16    bTFT;  /* if this is a TFT panel */

	u16    nPPL; /* Pixels Per Line */
	u16    nHBP; /* Horizontal Back Porch */
	u16    nHFP; /* Horizontal Front Porch */
	u16    nHSW; /* Horizontal Sync Width */
	u16    nLPP; /* Lines Per Panel */
	u16    nVSW; /* Vertical Sync Width */
	u16    nVBP; /* Vertical Back Porch */
	u16    nVFP; /* Vertical Front Porch */
	u16    nLED; /* Line End Delay */
};


/**********************************************************************
 * LCD driver software context
 **********************************************************************/
#define IPROC_LCD_HW_PALETTE_ENTRY 256
#define IPROC_LCD_PSEUDO_PALETTE_ENTRY 16

struct iproc_lcd_ctx {
	struct fb_info		*fb;
	struct device		*dev;

	struct iproc_lcd_panel panel;

	u32       *pFrameBuf_CPU;
	dma_addr_t pFrameBuf_DMA;
	u32        nFrameBufLen;

	u32       *pFrameBuf_CPU_2;
	dma_addr_t pFrameBuf_DMA_2;

    u32 lcdmode;
	wait_queue_head_t       done;
	struct pwm_device      *lpwm;

	struct resource *lcd_reg_mem;  /* pointer to requested register memory region */
	void __iomem    *lcd_reg_base; /* pointer to iomapped register memory region */

	u32        irq[5];

	spinlock_t palette_lock;			/* spin lock */
	u32	       palette_ready;
	u32	       palette_buffer[IPROC_LCD_HW_PALETTE_ENTRY]; /* soft copy of palette. Use 32 bit in order to store a flag */

	u32	       pseudo_pal[IPROC_LCD_PSEUDO_PALETTE_ENTRY];
};

#define PALETTE_BUFF_CLEAR (0x80000000)	/* entry is clear/invalid */

/**********************************************************************
 *  Palette (palette data is 1-5-5-5)
 **********************************************************************/
#define LCDPALETTE_R_MASK   0x001F /* [ 4: 0] */
#define LCDPALETTE_G_MASK   0x03E0 /* [ 9: 5] */
#define LCDPALETTE_B_MASK   0x7C00 /* [14:10] */
#define LCDPALETTE_I_MASK   0x8000 /* [15:15] */
#define LCDPALETTE_R_SHIFT  0
#define LCDPALETTE_G_SHIFT  5
#define LCDPALETTE_B_SHIFT  10
#define LCDPALETTE_I_SHIFT  15
#define LCDPALETTE_R_LEN    5
#define LCDPALETTE_G_LEN    5
#define LCDPALETTE_B_LEN    5

/* Each r/g/b from fb_cmap from user space is 16bits, need to change to 5 bits and put together
*/
#define LCDPALETTE_RGBI(r, g, b, i) \
                       ( (((r >> (16 - LCDPALETTE_R_LEN)) << LCDPALETTE_R_SHIFT) & LCDPALETTE_R_MASK) | \
                         (((g >> (16 - LCDPALETTE_G_LEN)) << LCDPALETTE_G_SHIFT) & LCDPALETTE_G_MASK) | \
                         (((b >> (16 - LCDPALETTE_B_LEN)) << LCDPALETTE_B_SHIFT) & LCDPALETTE_B_MASK) )


/**********************************************************************
 *  Register
 **********************************************************************/
#define LCDREG_TIMING0		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0000)
#define LCDREG_TIMING1		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0004)
#define LCDREG_TIMING2		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0008)
#define LCDREG_TIMING3		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x000C)
#define LCDREG_UPBASE		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0010)
#define LCDREG_LPBASE		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0014)
#define LCDREG_CONTROL		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0018)
#define LCDREG_INTMASK		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x001C)
#define LCDREG_INTRAW		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0020) /* RAW INTERRUPT STATUS REG */
#define LCDREG_INTSTATUS	(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0024) /* MASKED INTERRUPT STATUS REG */
#define LCDREG_INTCLEAR		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0028) /* INTERRUPT CLEAR REG */
#define LCDREG_UPCURR   	(volatile void __iomem *)(pCTX->lcd_reg_base + 0x002C) /* MASKED INTERRUPT STATUS REG */
#define LCDREG_LPCURR    	(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0030) /* INTERRUPT CLEAR REG */
#define LCDREG_PALETTE		(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0200)
#define LCDREG_PALETTE_i(i)	(volatile void __iomem *)(pCTX->lcd_reg_base + 0x0200 + i*4)


enum _lcd_color_mode
{
	/* Palettized */
	LCD_BPP_1 = 0,
	LCD_BPP_2,
	LCD_BPP_4,
	LCD_BPP_8,

	/* Non Palettized */
	LCD_BPP_16_1555, /* 16bpp in 1555 mode */
	LCD_BPP_24,      /* only for TFT */
	LCD_BPP_16_565,  /* 16bpp in 565 mode */
	LCD_BPP_12       /* 12bpp in 444 mode */
};

/* bits per pixel value -> register value */
__inline u32 LCD_BPP2REGVAL (u32 bpp)
        {
			switch (bpp) {
				case 1:
					return (LCD_BPP_1  << LCD_F_LcdBpp_R);
				case 2:
					return (LCD_BPP_2  << LCD_F_LcdBpp_R);
				case 4:
					return (LCD_BPP_4  << LCD_F_LcdBpp_R);
				case 8:
					return (LCD_BPP_8  << LCD_F_LcdBpp_R);
				case 12:
					return (LCD_BPP_12 << LCD_F_LcdBpp_R);
				case 15:
					return (LCD_BPP_16_1555 << LCD_F_LcdBpp_R);
				case 16:
					return (LCD_BPP_16_565  << LCD_F_LcdBpp_R);
				default: /* 24, 32, default */
					return (LCD_BPP_24 << LCD_F_LcdBpp_R); /* default to use 24bpp. This is the case with bpp=32 */
			}
		}


#endif
