Description:
============
This is the GMAC source to build the GMAC Driver and et application.

There are 2 componenets to the GMAC Driver build:

1. GMAC Driver: either part of kernel or et.ko (module)
2. et - utility to interact with GMAC driver.


Build instructions:
===================

A. Building GMAC driver:
========================
Building the linux image will build this driver
If a module is built it will be located in:
	iproc/bcmdrivers/gmac/et/et.ko

Note:
=====
I am currently having problems building the GMAC driver as a module (et.ko).
The driver will build as a module, however the module is unable to read the
U-Boot environment variables.  The module will then rely on the variables in
nvramstubs.c


B. Building et application:
===========================

1. cd iproc/bcmdrivers/gmac/src/et/linux
2. make et ARCH=arm CROSS_COMPILE=arm-unknown-linux-uclibcgnueabi-
3. et application will be located in the same directory.
