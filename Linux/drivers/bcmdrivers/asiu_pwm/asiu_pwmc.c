/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/err.h>
#include <linux/clk.h>
#include <linux/io.h>
#include <linux/delay.h>

#include <linux/platform_device.h>
#include <linux/pwm.h>

#ifdef CONFIG_DEBUG_FS
#include <linux/debugfs.h>
#endif

#include <mach/iproc_regs.h>
#include <mach/memory.h>

#define ASIU_PWM_PRESCALE_MASK	((1 << ASIU_PRESCALE_CONTROL__PWM0_PRESCALE_WIDTH) - 1)
#define ASIU_PWM_PEROID_WIDTH	ASIU_PWM0_PERIOD_COUNT__PWM0_CNT_WIDTH
#define ASIU_PWM_PERIOD_MASK	((1 << ASIU_PWM_PEROID_WIDTH) - 1)
#define ASIU_PWM_DUTYHI_WIDTH	ASIU_PWM0_DUTY_CYCLE_HIGH__PWM0_HIGH_WIDTH
#define ASIU_PWM_DUTYHI_MASK	((1 << ASIU_PWM_DUTYHI_WIDTH) - 1)

#define MAX_ASIU_PWM_NUMS	6

#define ASIU_PWMC_CLK	(1000000)	/* 1M Hz */

#define GET_REG(addr) readl((addr))
#define SET_REG(value, addr) writel((value), (addr))

struct asiu_pwm_cfg {
	unsigned long prescale;
	unsigned long period_cnt;
	unsigned long dutyhi_cnt;
	unsigned long polarity;
	unsigned long enabled;
#ifdef CONFIG_DEBUG_FS	
	struct dentry *dbgfs_entry;
	struct dentry *dbgfs_period;
	struct dentry *dbgfs_dutyhi;
	struct dentry *dbgfs_polarity;
	struct dentry *dbgfs_enable;
	u32 dbgfs_period_ns;
	u32 dbgfs_dutyhi_ns;
	unsigned long dbgfs_requested;
#endif
};

struct asiu_pwm_chip {
	struct pwm_chip chip;
	void __iomem *iobase;
	void __iomem *crmu_iobase;
	unsigned long tick_hz;
#ifdef CONFIG_DEBUG_FS	
	struct dentry *dbgfs_root;
#endif	
	struct asiu_pwm_cfg *cfgs;
	/* cfgs field should be placed in the end of the struct */	
};

#define to_asiu_pwm_chip(_chip) container_of(_chip, struct asiu_pwm_chip, chip)


static inline unsigned long reg_get_pwm_clk_enable(struct asiu_pwm_chip *apc) 
{
	unsigned long value, shft_bits;
	void __iomem * addr = apc->crmu_iobase + CRMU_ASIU_PWM_CLK_DIV_BASE;
	
	value = GET_REG(addr);
	shft_bits = CRMU_ASIU_PWM_CLK_DIV__CLK_DIV_ENABLE;
	return ((value >> shft_bits) & 1);
}

static inline void reg_set_pwm_clk_enable(struct asiu_pwm_chip *apc, 
						unsigned long enable)
{
	unsigned long value, shft_bits;
	void __iomem * addr = apc->iobase + CRMU_ASIU_PWM_CLK_DIV_BASE;
	
	value = GET_REG(addr);
	shft_bits = CRMU_ASIU_PWM_CLK_DIV__CLK_DIV_ENABLE;
	value &= ~(1 << shft_bits);
	value |= ((enable & 1) << shft_bits);
	SET_REG(value, addr);
}

static inline void reg_set_smooth_type(struct asiu_pwm_chip *apc, int pwm_idx, 
						unsigned long smooth_type)
{
	unsigned long value, shft_bits;
	void __iomem * addr = apc->iobase + ASIU_PWM_CONTROL_OFFSET;
	
	value = GET_REG(addr);
	shft_bits = ASIU_PWM_CONTROL__SMOOTH_TYPE_R + pwm_idx;
	value &= ~(1 << shft_bits);
	value |= ((smooth_type & 1) << shft_bits);
	SET_REG(value, addr);
}

static inline void reg_set_pwmout_type(struct asiu_pwm_chip *apc, int pwm_idx, 
						unsigned long pwmout_type)
{
	unsigned long value, shft_bits;
	void __iomem * addr = apc->iobase + ASIU_PWM_CONTROL_OFFSET;
	
	value = GET_REG(addr);
	shft_bits = ASIU_PWM_CONTROL__PWMOUT_TYPE_R + pwm_idx;
	value &= ~(1 << shft_bits);
	value |= ((pwmout_type & 1) << shft_bits);
	SET_REG(value, addr);
}

static inline unsigned long reg_get_pwmout_polarity(struct asiu_pwm_chip *apc, int pwm_idx) 
{
	unsigned long value, shft_bits;
	void __iomem * addr = apc->iobase + ASIU_PWM_CONTROL_OFFSET;
	
	value = GET_REG(addr);
	shft_bits = ASIU_PWM_CONTROL__PWMOUT_POLARITY_R + pwm_idx;
	return ((value >> shft_bits) & 1);
}

static inline void reg_set_pwmout_polarity(struct asiu_pwm_chip *apc, int pwm_idx, 
						unsigned long pwmout_polarity)
{
	unsigned long value, shft_bits;
	void __iomem * addr = apc->iobase + ASIU_PWM_CONTROL_OFFSET;
	
	value = GET_REG(addr);
	shft_bits = ASIU_PWM_CONTROL__PWMOUT_POLARITY_R + pwm_idx;
	value &= ~(1 << shft_bits);
	value |= ((pwmout_polarity & 1) << shft_bits);
	SET_REG(value, addr);
}

static inline void reg_set_pwmout_enable(struct asiu_pwm_chip *apc, int pwm_idx, 
						unsigned long pwmout_enable)
{
	unsigned long value, shft_bits;
	void __iomem * addr = apc->iobase + ASIU_PWM_CONTROL_OFFSET;
	
	value = GET_REG(addr);
	shft_bits = ASIU_PWM_CONTROL__PWMOUT_ENABLE_R + pwm_idx;
	value &= ~(1 << shft_bits);
	value |= ((pwmout_enable & 1) << shft_bits);
	SET_REG(value, addr);
}

static inline void reg_set_prescale(struct asiu_pwm_chip *apc, int pwm_idx, 
						unsigned long prescale)
{
	unsigned long value;
	void __iomem * addr = apc->iobase + ASIU_PRESCALE_CONTROL_OFFSET;

	value = GET_REG(addr) & ~(ASIU_PWM_PRESCALE_MASK << (pwm_idx * 4));
	value |= ((prescale & ASIU_PWM_PRESCALE_MASK) << (pwm_idx * 4));
	SET_REG(value, addr);
}

static inline void reg_set_period(struct asiu_pwm_chip *apc, int pwm_idx, 
						unsigned long period)
{
	unsigned long reg_offset;

	reg_offset = ASIU_PWM0_PERIOD_COUNT_OFFSET + pwm_idx * 8;
	SET_REG(period & ASIU_PWM_PERIOD_MASK, apc->iobase + reg_offset);
}

static inline void reg_set_dutyhi(struct asiu_pwm_chip *apc, int pwm_idx, 
						unsigned long dutyhi)
{
	unsigned long reg_offset;

	reg_offset = ASIU_PWM0_DUTY_CYCLE_HIGH_OFFSET + pwm_idx * 8;
	SET_REG(dutyhi & ASIU_PWM_DUTYHI_MASK, apc->iobase + reg_offset);
}

#ifdef CONFIG_DEBUG_FS
static int dbgfs_enable_get(void *data, u64 *value)
{
	struct pwm_device *pwm = data;
	struct asiu_pwm_cfg *pwm_cfg = pwm_get_chip_data(pwm);

	*value = pwm_cfg->enabled;

	return 0;
}

static int dbgfs_enable_set(void *data, u64 value)
{
	struct pwm_device *pwm = data;
	struct asiu_pwm_cfg *pwm_cfg = pwm_get_chip_data(pwm);

	if (value) {
		pwm_config(pwm, pwm_cfg->dbgfs_dutyhi_ns, pwm_cfg->dbgfs_period_ns);
		pwm_enable(pwm);
	}
	else {
		pwm_disable(pwm);
	}

	return 0;
}


DEFINE_SIMPLE_ATTRIBUTE(dbgfs_enable_fop, dbgfs_enable_get, 
				dbgfs_enable_set, "%llu\n");


static int dbgfs_request_get(void *data, u64 *value)
{
	struct pwm_device *pwm = data;
	struct asiu_pwm_cfg *pwm_cfg = pwm_get_chip_data(pwm);

	if(pwm_cfg && pwm_cfg->dbgfs_requested)
		*value = 1;
	else
		*value = 0;
	
	return 0;
}


static int dbgfs_request_set(void *data, u64 value)
{
	struct pwm_device *pwm = data;
	struct asiu_pwm_cfg *pwm_cfg = pwm_get_chip_data(pwm);
	char pwm_id[16];
	struct pwm_device *pwm_req;

	if (value) {
		sprintf(pwm_id, "pwm-%d", pwm->hwpwm);
		pwm_req = pwm_get(pwm->chip->dev, pwm_id);
		if (!IS_ERR_OR_NULL(pwm_req) && (pwm == pwm_req)) {
			pwm_cfg->dbgfs_period = debugfs_create_u32("period_ns",
				S_IRUGO | S_IWUSR, pwm_cfg->dbgfs_entry,
				&pwm_cfg->dbgfs_period_ns);
			pwm_cfg->dbgfs_dutyhi = debugfs_create_u32("dutyhi_ns",
				S_IRUGO | S_IWUSR, pwm_cfg->dbgfs_entry,
				&pwm_cfg->dbgfs_dutyhi_ns);
			pwm_cfg->dbgfs_polarity = debugfs_create_u32("polarity",
				S_IRUGO | S_IWUSR, pwm_cfg->dbgfs_entry,
				&pwm_cfg->polarity);
			pwm_cfg->dbgfs_enable = debugfs_create_file("enable",
				S_IRUGO | S_IWUSR, pwm_cfg->dbgfs_entry,
				data, &dbgfs_enable_fop);
			pwm_cfg->dbgfs_requested = 1;
		}
	}
	else {
		if (pwm_cfg->dbgfs_requested) {
			debugfs_remove(pwm_cfg->dbgfs_period);
			debugfs_remove(pwm_cfg->dbgfs_dutyhi);
			debugfs_remove(pwm_cfg->dbgfs_polarity);
			debugfs_remove(pwm_cfg->dbgfs_enable);
			pwm_disable(pwm);
			pwm_put(pwm);
			pwm_cfg->dbgfs_requested = 0;
		}
	}

	return 0;
}


DEFINE_SIMPLE_ATTRIBUTE(dbgfs_request_fop, dbgfs_request_get, 
					dbgfs_request_set, "%llu\n");

static int __init asiu_pwmc_debugfs_init(struct asiu_pwm_chip *apc)
{
	int i;
	char entry_name[16];
	struct pwm_device *pwm;
	struct asiu_pwm_cfg *cfg;

	apc->dbgfs_root = debugfs_create_dir("asiu_pwm", NULL);
	
	if (apc->dbgfs_root) {
		for (i=0; i<apc->chip.npwm; i++) {
			pwm = apc->chip.pwms + i;
			cfg = apc->cfgs + i;
			sprintf(entry_name, "asiu_pwm_%d", i);
			cfg->dbgfs_entry = debugfs_create_dir(entry_name, 
							apc->dbgfs_root);
			if(cfg->dbgfs_entry)
				debugfs_create_file("request", S_IRUGO | S_IWUSR,
							cfg->dbgfs_entry,
							pwm, &dbgfs_request_fop);
		}
	}

	return 0;
}
#endif /* CONFIG_DEBUG_FS */

static int asiu_pwmc_request(struct pwm_chip *chip, struct pwm_device *pwm)
{
	struct asiu_pwm_chip *asiu_pwm = to_asiu_pwm_chip(chip);
	unsigned long pwm_mux, shift;	
	void __iomem *ioaddr;

	if (pwm->hwpwm < 4)
	{
		/* Set IOMUX function to 0 for the requested pwm index for PWM IOMUX CTRL */
		ioaddr = asiu_pwm->crmu_iobase + CRMU_IOMUX_CTRL3_BASE;
		pwm_mux = __raw_readl(ioaddr);
		shift = CRMU_IOMUX_CTRL3__CORE_TO_IOMUX_PWM0_SEL_R + pwm->hwpwm * 4;
		pwm_mux &= ~(7 << shift);
		__raw_writel(pwm_mux, ioaddr);
	}
	else if (pwm->hwpwm == 4)
	{
		/*Set IOMUX function to 2 to the for the requested pwm index for GPIO5 IOMUX CTRL */
		ioaddr = asiu_pwm->crmu_iobase + CRMU_IOMUX_CTRL0_BASE;
		pwm_mux = __raw_readl(ioaddr);
		pwm_mux &= ~(7 << CRMU_IOMUX_CTRL0__CORE_TO_IOMUX_GPIO5_SEL_R);
		pwm_mux |= (2 << CRMU_IOMUX_CTRL0__CORE_TO_IOMUX_GPIO5_SEL_R);
		__raw_writel(pwm_mux, ioaddr);
	}
	else if (pwm->hwpwm == 5)
	{
		/*Set IOMUX function to 2 to the for the requested pwm index for GPIO6 IOMUX CTRL */
		ioaddr = asiu_pwm->crmu_iobase + CRMU_IOMUX_CTRL0_BASE;
		pwm_mux = __raw_readl(ioaddr);
		pwm_mux &= ~(7 << CRMU_IOMUX_CTRL0__CORE_TO_IOMUX_GPIO6_SEL_R);
		pwm_mux |= (2 << CRMU_IOMUX_CTRL0__CORE_TO_IOMUX_GPIO6_SEL_R);
		__raw_writel(pwm_mux, ioaddr);
	}

	printk(KERN_INFO "The mapping IO MUX function is being changed "
		"by the pwm-%d request.\nThis change might affect the modules "
		"mapping to the corresponding IO MUX function.\n", pwm->hwpwm);

	return 0;
}

static int asiu_pwmc_enable(struct pwm_chip *chip, struct pwm_device *pwm)
{
	struct asiu_pwm_chip *asiu_pwm = to_asiu_pwm_chip(chip);
	struct asiu_pwm_cfg *pwm_cfg = pwm_get_chip_data(pwm);
	unsigned long valid_jiffies;

	dev_info(chip->dev, "%s [pwm-%d]\n", __FUNCTION__, pwm->hwpwm);

	if (!asiu_pwm || !pwm_cfg) {
		dev_warn(chip->dev, "%s(%d) : asiu_pwm = %p, pwm_cfg = %p\n",
				__FUNCTION__, __LINE__, asiu_pwm, pwm_cfg);
		return -EINVAL;
	}

	reg_set_smooth_type(asiu_pwm, pwm->hwpwm, 1);
	reg_set_pwmout_enable(asiu_pwm, pwm->hwpwm, 0);

	valid_jiffies = jiffies + usecs_to_jiffies(400);

	reg_set_prescale(asiu_pwm, pwm->hwpwm, pwm_cfg->prescale);
	reg_set_period(asiu_pwm, pwm->hwpwm, pwm_cfg->period_cnt);
	reg_set_dutyhi(asiu_pwm, pwm->hwpwm, pwm_cfg->dutyhi_cnt);
	reg_set_pwmout_polarity(asiu_pwm, pwm->hwpwm, pwm_cfg->polarity);

	/* make sure the setting between pwmout disable and enable is longer 
	    than 400ns */
	while (time_is_before_jiffies(valid_jiffies)) {
		cpu_relax();
	}

	reg_set_pwmout_enable(asiu_pwm, pwm->hwpwm, 1);
	pwm_cfg->enabled = 1;

	return 0;	
}

static void asiu_pwmc_disable(struct pwm_chip *chip, struct pwm_device *pwm)
{
	struct asiu_pwm_chip *asiu_pwm = to_asiu_pwm_chip(chip);
	struct asiu_pwm_cfg *pwm_cfg = pwm_get_chip_data(pwm);
	
	reg_set_smooth_type(asiu_pwm, pwm->hwpwm, 0);
	reg_set_pwmout_enable(asiu_pwm, pwm->hwpwm, 0);
	
	reg_set_prescale(asiu_pwm, pwm->hwpwm, 0);
	reg_set_pwmout_polarity(asiu_pwm, pwm->hwpwm, 1);
	reg_set_dutyhi(asiu_pwm, pwm->hwpwm, 0);
	reg_set_period(asiu_pwm, pwm->hwpwm, 0);

	reg_set_pwmout_enable(asiu_pwm, pwm->hwpwm, 1);

	/* FIX ME. Check if PWM clk or PWM_SHUTDOWN is needed to be implemented. */


	pwm_cfg->enabled = 0;
}


static int asiu_pwmc_config(struct pwm_chip *chip, struct pwm_device *pwm,
				int duty_ns, int period_ns)
{
	struct asiu_pwm_chip *asiu_pwm = to_asiu_pwm_chip(chip);
	struct asiu_pwm_cfg *pwm_cfg;
	unsigned long period_counts, dutyhi_counts;
	unsigned long prescale = 0;
	unsigned long long ticks;

	dev_info(chip->dev, "%s : [pwm-%d] duty_ns = %d, period_ns = %d\n", 
				__FUNCTION__, pwm->hwpwm, duty_ns, period_ns);

	if (duty_ns == 0 && period_ns ==0) {
		/* PWM stay low, duty cycle = 0% */
		prescale = 0;
		period_counts = 0;
		dutyhi_counts = 0;
	}
	else if (duty_ns >= period_ns) {
		/* PWM stay high, duty cycle = 100% */
		prescale = 0;
		period_counts = ASIU_PWM_PERIOD_MASK;
		dutyhi_counts = ASIU_PWM_DUTYHI_MASK;
	}
	else {
		ticks = (unsigned long long)period_ns * asiu_pwm->tick_hz;
		do_div(ticks, NSEC_PER_SEC);
		period_counts = ticks;
		prescale = period_counts >> ASIU_PWM_PEROID_WIDTH;
		if(prescale & ~ASIU_PWM_PRESCALE_MASK) {
			dev_warn(chip->dev, "%s(%d) : period_counts = %d, prescale = 0x%x\n",
					__FUNCTION__, __LINE__, period_counts, prescale);
			return -EINVAL;
		}
		ticks = (unsigned long long)duty_ns * asiu_pwm->tick_hz;
		do_div(ticks, NSEC_PER_SEC);
		dutyhi_counts = ticks;
	}

	pwm_cfg = pwm_get_chip_data(pwm);
	if (!pwm_cfg) {
		dev_warn(chip->dev, "fail to get pwm config data\n");
		return -ENOMEM;
	}

	pwm_cfg->prescale = prescale;
	pwm_cfg->period_cnt = period_counts;
	pwm_cfg->dutyhi_cnt = dutyhi_counts;

	if (pwm_cfg->enabled) {
		/* Change the PWM output with the new config */
		asiu_pwmc_enable(chip, pwm);
	}

	dev_info(chip->dev, "%s : pwm_cfg->prescale = %d, period_cnt = %d, dutyhi_cnt = %d\n",
			__FUNCTION__, pwm_cfg->prescale, pwm_cfg->period_cnt, pwm_cfg->dutyhi_cnt);

	return 0;
}


static void asiu_pwmc_free(struct pwm_chip *chip, struct pwm_device *pwm)
{
	struct asiu_pwm_cfg *pwm_cfg = (struct asiu_pwm_cfg *)pwm_get_chip_data(pwm);

	if(pwm_cfg && pwm_cfg->enabled)
		asiu_pwmc_disable(chip, pwm);
}

static const struct pwm_ops asiu_pwm_ops = {
	.request = asiu_pwmc_request,
	.config = asiu_pwmc_config,
	.enable = asiu_pwmc_enable,
	.disable = asiu_pwmc_disable,
	.free = asiu_pwmc_free,
	.owner = THIS_MODULE,
};

static int __devinit asiu_pwmc_probe(struct platform_device *pdev)
{
	struct asiu_pwm_chip *asiu_pwm;
	struct resource *res;
	int i, alloc_size, ret = 0;
	int npwm = MAX_ASIU_PWM_NUMS; /* FIX ME. May be different per SKU */

	if (npwm > MAX_ASIU_PWM_NUMS) {
		dev_err(&pdev->dev, "Unsupport pwm numbers : %d\n", npwm);
		ret = -EINVAL;
		goto err;
	}

	alloc_size = sizeof(*asiu_pwm) + sizeof(struct asiu_pwm_cfg) * npwm;
	asiu_pwm = devm_kzalloc(&pdev->dev, alloc_size, GFP_KERNEL);
	if (!asiu_pwm) {
		dev_err(&pdev->dev, "Kzalloc failed\n");
		ret = -ENOMEM;
		goto err;
	}

	res = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!res) {
		dev_err(&pdev->dev, "Failed to get resource\n");
		ret = -EINVAL;
		goto err;
	}

	asiu_pwm->iobase = devm_request_and_ioremap(&pdev->dev, res);
	if (IS_ERR_OR_NULL(asiu_pwm->iobase)) {
		dev_err(&pdev->dev, "Failed to do ioremap\n");
		ret = -EADDRNOTAVAIL;
		goto err;
	}

	res = platform_get_resource(pdev, IORESOURCE_MEM, 1);
	if (!res) {
		dev_err(&pdev->dev, "Failed to get crmu resource\n");
		ret = -EINVAL;
		goto err;
	}

	asiu_pwm->crmu_iobase = devm_request_and_ioremap(&pdev->dev, res);
	if (IS_ERR_OR_NULL(asiu_pwm->crmu_iobase)) {
		dev_err(&pdev->dev, "Failed to do crmu ioremap\n");
		ret = -EADDRNOTAVAIL;
		goto err;
	}

	asiu_pwm->cfgs = (struct asiu_pwm_cfg *)((char *)asiu_pwm + offsetof(struct asiu_pwm_chip, cfgs));
	asiu_pwm->tick_hz = ASIU_PWMC_CLK;

	asiu_pwm->chip.dev = &pdev->dev;
	asiu_pwm->chip.ops = &asiu_pwm_ops;
	asiu_pwm->chip.base = -1;
	asiu_pwm->chip.npwm = npwm;

	ret = pwmchip_add(&asiu_pwm->chip);
	if (ret < 0) {
		dev_err(&pdev->dev, "failed to add PWM chip, error %d\n", ret);
		goto err;	
	}

	for(i=0; i<npwm; i++) {
		pwm_set_chip_data(&asiu_pwm->chip.pwms[i], &asiu_pwm->cfgs[i]);
	}

#ifdef CONFIG_DEBUG_FS
	asiu_pwmc_debugfs_init(asiu_pwm);
#endif

	if (!reg_get_pwm_clk_enable(asiu_pwm))
		reg_set_pwm_clk_enable(asiu_pwm, 1);

	platform_set_drvdata(pdev, asiu_pwm);

	return 0;
err:
	dev_err(&pdev->dev, "Probe Failed!!!\n");
	return ret;
}

static int __devexit asiu_pwmc_remove(struct platform_device *pdev)
{
	struct asiu_pwm_chip *asiu_pwm = platform_get_drvdata(pdev);
	int ret = 0;

	if(asiu_pwm) {
		 ret = pwmchip_remove(&asiu_pwm->chip);
		 if (ret == 0) {
		 	debugfs_remove_recursive(asiu_pwm->dbgfs_root);
		 }
	}

	return ret;
}

static struct platform_driver asiu_pwmc_driver = {
	.driver = {
		.name = "asiu_pwmc",
		.owner = THIS_MODULE,
	},
	.probe = asiu_pwmc_probe,
	.remove = __devexit_p(asiu_pwmc_remove),
};

static int __init asiu_pwmc_init(void)
{
	return platform_driver_register(&asiu_pwmc_driver);
}

static void __exit asiu_pwmc_exit(void)
{
	platform_driver_unregister(&asiu_pwmc_driver);
}

module_init(asiu_pwmc_init);
module_exit(asiu_pwmc_exit);

MODULE_AUTHOR("Broadcom Corporation");
MODULE_DESCRIPTION("Driver for ASIU PWMC");
MODULE_LICENSE("GPL");
