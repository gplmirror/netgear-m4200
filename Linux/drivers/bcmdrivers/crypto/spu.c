/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * This file generates and parses SPU request packets based on a given context
 *
 */


#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/random.h>
#include <linux/in.h>
#include <linux/delay.h>
#include <linux/if_ether.h>
#include <crypto/scatterwalk.h>

#include "util.h"
#include "spu.h"


void
dump_spu_message(uint8_t *buf, unsigned buf_len)
{
    uint8_t* ptr = buf;
    SPUHEADER *spuh = (SPUHEADER *) buf;
    unsigned hashKeyLength = 0;
    unsigned hashStateLength = 0;
    unsigned padding_length = 0;
    unsigned is_response = 0;

    packet_log("\n");
    packet_log("SPU Message %p len:%u\n", buf, buf_len);

    /* ========== Process the MH & EMH ========== */
    {
        packet_log("  MH 0x%08x\n", htonl(*((uint32_t*) ptr)));
        if (spuh->mh.flags.SCTX_PR)  packet_log("    SCTX  present\n");
        if (spuh->mh.flags.BDESC_PR) packet_log("    BDESC present\n");
        if (spuh->mh.flags.MFM_PR)   packet_log("    MFM   present\n");
        if (spuh->mh.flags.BD_PR)    packet_log("    BD    present\n");
        if (spuh->mh.flags.HASH_PR)  packet_log("    HASH  present\n");
        if (spuh->mh.flags.SUPDT_PR) packet_log("    SUPDT present\n");
        packet_log("    Opcode 0x%02x\n", spuh->mh.opCode);

        /* Process the EMH */
        packet_log("  EMH 0x%08x\n", htonl(spuh->emh));
        /* Byte 0: 0x80 is FA2_SPU_TRNS_ID_DRIVER_MASK bit showing this is one of our packets
         *       : 0x40 Load context bit
         *       : 0x3F Next 6 bits are for the index
         */
        is_response = (ptr[4] & 0x80) >> 7;
        packet_log("    Byte0 %02x FA2BIT:%u LoadContext:%u Index:%u\n",
                 ptr[4], is_response, (ptr[4] & 0x40) >> 6, ptr[4] & 0x3F);
        /* Byte 1: 0xF0 stream ID
         *         0x0F operation type
         *         0x00 None
         *         0x01 AES128     0x02 AES192     0x03 AES256    0x04 DES     0x05 3DES
         *         0x06 MD5        0x07 SHA1       0x08 SHA224    0x09 SHA256  0x0A ARC4
         */
        packet_log("    Byte1 %02x StreamID:%u OpType:%u\n",
                 ptr[5], (ptr[5] & 0xF0) >> 4, ptr[5] & 0x0F);
        /* Byte 2: 0x80 Final hash packet
         *         0x40 Unused bit
         *         0x3F Length of packet padding used to ensure pkt_len > 64
         */
        padding_length = ptr[6] & 0x3F;
        packet_log("    Byte2 %02x IsFinal:%u PaddingLength:%u\n",
                 ptr[6], (ptr[6] & 0x80) >> 7, padding_length);
        /* Byte 3: Response size delta from actual request size without padding
         */
        packet_log("    Byte3 %02x ResponseSizeDelta:%d\n",
                 ptr[7], ((int8_t*)ptr)[7]);

        ptr += 2 * 4;
    }

    /* ========== Process the SCTX ========== */
    if (spuh->mh.flags.SCTX_PR) {
        packet_log("  SCTX[0] 0x%08x\n", htonl(spuh->sa.protocol.bits));
        packet_log("    Size %u words\n", spuh->sa.protocol.flags.SCTX_size);

        packet_log("  SCTX[1] 0x%08x\n", htonl(spuh->sa.cipher.bits));
        packet_log("    Inbound:%u (1:decrypt/verify 0:encrypt/auth)\n",
                 spuh->sa.cipher.flags.inbound);
        packet_log("    Order:%u (1:AuthFirst 0:EncFirst)\n",
                 spuh->sa.cipher.flags.order);
        packet_log("    RSV:%x\n",
                 spuh->sa.cipher.flags.reserved);
        packet_log("    Crypto Alg:%u Mode:%u Type:%u\n",
                 spuh->sa.cipher.flags.cryptoAlg,
                 spuh->sa.cipher.flags.cryptoMode,
                 spuh->sa.cipher.flags.cryptoType);
        packet_log("    Hash   Alg:%x Mode:%x Type:%x\n",
                 spuh->sa.cipher.flags.hashAlg,
                 spuh->sa.cipher.flags.hashMode,
                 spuh->sa.cipher.flags.hashType);
        packet_log("    UPDT_Offset:%u\n", spuh->sa.cipher.flags.UPDT_ofst);

        packet_log("  SCTX[2] 0x%08x\n", htonl(spuh->sa.ecf.bits));
        packet_log("    WriteICV:%u CheckICV:%u ICV_SIZE:%u\n",
                 spuh->sa.ecf.flags.insert_icv,
                 spuh->sa.ecf.flags.check_icv,
                 spuh->sa.ecf.flags.ICV_size);
        packet_log("    SCTX_IV:%u ExplicitIV:%u GenIV:%u IV_OV_OFST:%u EXP_IV_SIZE:%u\n",
                 spuh->sa.ecf.flags.SCTX_IV,
                 spuh->sa.ecf.flags.explicit_IV,
                 spuh->sa.ecf.flags.gen_IV,
                 spuh->sa.ecf.flags.iv_offset,
                 spuh->sa.ecf.flags.explicit_IV_size);

        ptr += 3 * 4;

        /* if (spuh->sa.cipher.flags.hashAlg && !spuh->sa.ecf.flags.ICV_size) { */
        /*     packet_log("    HashAlg without ICV\n"); */
        /* } */
        /* if (spuh->sa.ecf.flags.ICV_size) { */
        /*     packet_log("    ICV Length:%u Words\n", spuh->sa.ecf.flags.ICV_size); */
        /*     packet_dump("    ICV: ", ptr, spuh->sa.ecf.flags.ICV_size * 4); */
        /*     ptr += (spuh->sa.ecf.flags.ICV_size * 4); */
        /* } */

        if (spuh->sa.cipher.flags.hashAlg && spuh->sa.cipher.flags.hashMode) {
            char* name = "NONE";

            switch (spuh->sa.cipher.flags.hashAlg) {
            case HASH_ALG_MD5:    hashKeyLength = 16;  name = "MD5";    break;
            case HASH_ALG_SHA1:   hashKeyLength = 20;  name = "SHA1";   break;
            case HASH_ALG_SHA224: hashKeyLength = 28;  name = "SHA224"; break;
            case HASH_ALG_SHA256: hashKeyLength = 32;  name = "SHA256"; break;
            case HASH_ALG_AES:    hashKeyLength = 0;   name = "AES";    break;
            case HASH_ALG_NONE:
                break;
            }

            packet_log("    Auth Key Type:%s Length:%u Bytes\n", name, hashKeyLength);
            packet_dump("    KEY: ", ptr, hashKeyLength);
            ptr += hashKeyLength;
        }

        if (spuh->sa.cipher.flags.hashAlg &&
            !spuh->sa.cipher.flags.hashMode &&
            (spuh->sa.cipher.flags.hashType == HASH_TYPE_UPDT)) {
            char* name = "NONE";

            switch (spuh->sa.cipher.flags.hashAlg) {
            case HASH_ALG_MD5:    hashStateLength = 16;  name = "MD5";    break;
            case HASH_ALG_SHA1:   hashStateLength = 20;  name = "SHA1";   break;
            case HASH_ALG_SHA224: hashStateLength = 28;  name = "SHA224"; break;
            case HASH_ALG_SHA256: hashStateLength = 32;  name = "SHA256"; break;
            case HASH_ALG_AES:    hashStateLength = 0;   name = "AES";    break;
            case HASH_ALG_NONE:
                break;
            }

            packet_log("    Auth State Type:%s Length:%u Bytes\n", name, hashStateLength);
            packet_dump("    State: ", ptr, hashStateLength);
            ptr += hashStateLength;
        }

        if (spuh->sa.cipher.flags.cryptoAlg) {
            unsigned length = 0;
            char* name = "NONE";

            switch (spuh->sa.cipher.flags.cryptoAlg) {
            case CIPHER_ALG_DES:  length = 8;   name = "DES";  break;
            case CIPHER_ALG_3DES: length = 24;  name = "3DES"; break;
            case CIPHER_ALG_RC4:  length = 260; name = "ARC4"; break;
            case CIPHER_ALG_AES:
                switch (spuh->sa.cipher.flags.cryptoType) {
                case CIPHER_TYPE_AES128: length = 16; name = "AES128"; break;
                case CIPHER_TYPE_AES192: length = 24; name = "AES192"; break;
                case CIPHER_TYPE_AES256: length = 32; name = "AES256"; break;
                }
                break;
            case CIPHER_ALG_NONE: break;
            }

            packet_log("    Cipher Key Type:%s Length:%u Bytes\n", name, length);
            packet_dump("    KEY: ", ptr, length);
            ptr += length;

            if (spuh->sa.ecf.flags.SCTX_IV) {
                unsigned iv_len =
                    (spuh->sa.protocol.flags.SCTX_size - 3) * 4 - (hashKeyLength + hashStateLength + length);
                packet_log("    IV Length:%u Bytes\n", iv_len);
                packet_dump("    IV: ", ptr, iv_len);
                ptr += iv_len;
            }
        }
    }

    /* ========== Process the BDESC ========== */
    if (spuh->mh.flags.BDESC_PR) {
        BDESC_HEADER *bdesc = (BDESC_HEADER*) ptr;

        packet_log("  BDESC[0] 0x%08x\n", htonl(*((uint32_t*) ptr)));
        packet_log("    OffsetMAC:%u LengthMAC:%u\n",
                 htons(bdesc->offsetMAC), htons(bdesc->lengthMAC));
        ptr += 4;

        packet_log("  BDESC[1] 0x%08x\n", htonl(*((uint32_t*) ptr)));
        packet_log("    OffsetCrypto:%u LengthCrypto:%u\n",
                 htons(bdesc->offsetCrypto), htons(bdesc->lengthCrypto));
        ptr += 4;

        packet_log("  BDESC[2] 0x%08x\n", htonl(*((uint32_t*) ptr)));
        packet_log("    OffsetICV:%u OffsetIV:%u\n",
                 htons(bdesc->offsetICV), htons(bdesc->offsetIV));
        ptr += 4;
    }

    /* ========== Process the MFM ========== */
    if (spuh->mh.flags.MFM_PR) {
        packet_log("    MFM currently unparsed!\n");
    }

    /* ========== Process the BD ========== */
    if (spuh->mh.flags.BD_PR) {
        BD_HEADER *bd = (BD_HEADER*) ptr;
        unsigned padding = ((htons(bd->size) + 3) & ~3) - htons(bd->size);

        packet_log("  BD[0] 0x%08x\n", htonl(*((uint32_t*) ptr)));
        packet_log("    Size:%ubytes PrevLength:%u\n",
                 htons(bd->size), htons(bd->PrevLength));
        ptr += 4;

        packet_dump("    BD: ", ptr, htons(bd->size));
        ptr += htons(bd->size);

        if (padding) {
            packet_log("    padding to 4byte alignment: %u bytes\n", padding);
            ptr += padding;
        }
    }

    /* ========== Process the HASH ========== */
    if (spuh->mh.flags.HASH_PR) {
        unsigned length = 0;

        switch (buf[5] & 0x0F) {
        case CTX_MD5:    packet_log("  HASH  Alg:MD5\n");         length = 16; break;
        case CTX_SHA1:   packet_log("  HASH  Alg:SHA1\n");        length = 20; break;
        case CTX_SHA224: packet_log("  HASH  Alg:SHA224\n");      length = 28; break;
        case CTX_SHA256: packet_log("  HASH  Alg:SHA256\n");      length = 32; break;
        default: packet_log("  HASH  Alg:UNKNOWN!!! BAD!\n");     length = 0; break;
        }

        packet_dump("    HASH: ", ptr, length);
        ptr += length;
    }

    /* ========== Process the SUPDT ========== */
    if (spuh->mh.flags.SUPDT_PR) {
        packet_dump("    SUPDT: ", ptr, 260);
        ptr += 260;
    }

    /* ========== Process the STAT ========== */
    {
        packet_log("  STAT 0x%08x\n", htonl(*((uint32_t*) ptr)));
        ptr += 4;
    }

    /* ========== Run out the paddding =========== */
    if (padding_length && !is_response) {
        packet_log("  PAD: %u\n", padding_length);
        packet_dump("    PAD: ", ptr, padding_length);
        ptr += padding_length;
    }

    /* Double check sanity */
    if (buf + buf_len != ptr) {
        packet_log(" Packet parsed incorrectly! buf:%p buf_len:%u buf+buf_len:%p ptr:%p\n",
                   buf, buf_len, buf+buf_len, ptr);
    }

    packet_log("\n");
}


struct sk_buff*
spu_create_request(bool pae_chaining, uint8_t pae_streamid,
                   unsigned isInbound, unsigned authFirst,
                   CIPHER_ALG cipher_alg, CIPHER_MODE cipher_mode, CIPHER_TYPE cipher_type,
                   uint8_t *cipher_key_buf, unsigned cipher_key_len,
                   uint8_t *cipher_iv_buf, unsigned cipher_iv_len,
                   HASH_ALG auth_alg, HASH_MODE auth_mode, HASH_TYPE auth_type,
                   uint8_t digestsize,
                   uint8_t *auth_key_buf, unsigned auth_key_len,
                   uint16_t prev_length_blocks,
                   struct scatterlist *assoc, unsigned assoc_start, unsigned assoc_size,
                   uint8_t *prebuf, unsigned prebuf_len,
                   struct scatterlist *data, unsigned data_start, unsigned data_size,
                   unsigned dtls_aead, unsigned hmac_offset,
                   unsigned hash_pad_len, unsigned total_sent,
                   uint8_t *aead_iv_buf, unsigned aead_iv_buf_len)
{
    SPUHEADER *spuh;
    BDESC_HEADER *bdesc;
    BD_HEADER *bd;

    struct sk_buff *skb;

    uint8_t *ptr;
    pae_ctx_type context_optype = CTX_NONE;
    uint32_t protocol_bits = 0;
    uint32_t cipher_bits = 0;
    uint32_t ecf_bits = 0;
    uint8_t  sctx_words = 0;
    int8_t delta_size = 0;
    unsigned buf_len = 0;

    /* size/offset of the cipher payload */
    unsigned cipher_len  =
        (dtls_aead ? aead_iv_buf_len : 0) + prebuf_len + data_size + hash_pad_len;

    unsigned cipher_offset = assoc_size + (dtls_aead ? 0 : aead_iv_buf_len);

    /* total size of the DB data (without word padding) */
    unsigned real_db_size = assoc_size + aead_iv_buf_len + prebuf_len + data_size + hash_pad_len;

    /* size/offset of the auth payload */
    unsigned auth_len = dtls_aead ? (assoc_size + aead_iv_buf_len + hmac_offset) : real_db_size;
    unsigned auth_offset = 0;

    /* pading for 4byte alignment for STAT */
    unsigned status_padding = ((real_db_size + 3) & ~3) - real_db_size;

    flow_log("%s()\n", __FUNCTION__);
    flow_log("  pae: chain:%u streamid:%u\n", pae_chaining, pae_streamid);
    flow_log("  in:%u authFirst:%u\n", isInbound, authFirst);
    flow_log("  cipher alg:%u mode:%u type %u\n", cipher_alg, cipher_mode, cipher_type);
    flow_log("    key: %d\n", cipher_key_len);
    flow_dump("    key: ", cipher_key_buf, cipher_key_len);
    flow_log("    iv: %d\n", cipher_iv_len);
    flow_dump("    iv: ", cipher_iv_buf, cipher_iv_len);
    flow_log("  auth alg:%u mode:%u type %u\n", auth_alg, auth_mode, auth_type);
    flow_log("  digestsize: %u\n", digestsize);
    flow_log("  authkey: %d\n", auth_key_len);
    flow_dump("  authkey: ", auth_key_buf, auth_key_len);
    flow_log("  prev_length_blocks: %u\n", prev_length_blocks);
    flow_log("  assoc:%p, assoc_start:%u, assoc_size:%u\n", assoc, assoc_start, assoc_size);
    flow_log("  prebuf:%p, prebuf_len:%u\n", prebuf, prebuf_len);
    flow_log("  data:%p, data_start:%u data_size:%u\n", data, data_start, data_size);
    flow_log("  dtls_aead:%u hash_pad_len:%u total_sent:%u real_db_size:%u\n",
             dtls_aead, hash_pad_len, total_sent, real_db_size);
    flow_log("  auth_offset:%u auth_len:%u cipher_offset:%u cipher_len:%u\n",
             auth_offset, auth_len, cipher_offset, cipher_len);
    flow_log("  hmac_offset:%u\n", hmac_offset);
    flow_log("  aead_iv: %u\n", aead_iv_buf_len);
    flow_dump("  aead_iv: ", aead_iv_buf, aead_iv_buf_len);

    skb = alloc_skb(MAX_SPU_PKT_SIZE, GFP_KERNEL);
    if (!skb) {
        printk(KERN_WARNING "make_spu_request(): skb memory squeeze, dropping.\n");
        return NULL;
    }

    /* reserve space for bcm header type 3 */
    skb_reserve(skb, 8);
    /* reserve space for SPU header + data + 0-3 padding + status + hash padding */
    skb_put(skb, sizeof(SPUHEADER));
    ptr = skb->data;
    
    /* starting out: zero the header (plus some) */
    memset(ptr, 0, sizeof(SPUHEADER));
    
    /* format master header word */
    /* Do not set the next bit even though the datasheet says to */
    spuh = (SPUHEADER *)ptr;
    ptr += sizeof(SPUHEADER); buf_len += sizeof(SPUHEADER);

    spuh->mh.opCode = SPU_CRYPTO_OPERATION_GENERIC;
    spuh->mh.flags.SCTX_PR = 1;
    spuh->mh.flags.BDESC_PR = 1;
    spuh->mh.flags.BD_PR = 1;

    /* TODO: optimize chaining, don't always set optype */
    /* if (pae_chaining) { */
    switch (cipher_alg) {
    case CIPHER_ALG_DES:  context_optype = CTX_DES;  break;
    case CIPHER_ALG_3DES: context_optype = CTX_3DES; break;
    case CIPHER_ALG_RC4:  context_optype = CTX_RC4;  delta_size += 260; break;
    case CIPHER_ALG_AES:
        switch (cipher_type) {
        case CIPHER_TYPE_AES128: context_optype = CTX_AES128; break;
        case CIPHER_TYPE_AES192: context_optype = CTX_AES192; break;
        case CIPHER_TYPE_AES256: context_optype = CTX_AES256; break;
        }
        break;
    case CIPHER_ALG_NONE:
        break;
    }
    
    switch (auth_alg) {
    case HASH_ALG_MD5:    context_optype = CTX_MD5;    break;
    case HASH_ALG_SHA1:   context_optype = CTX_SHA1;   break;
    case HASH_ALG_SHA224: context_optype = CTX_SHA224; break;
    case HASH_ALG_SHA256: context_optype = CTX_SHA256; break;
    case HASH_ALG_AES:
    case HASH_ALG_NONE:
        break;
    }
    /* } */

    skb->data[4] |= (pae_chaining << 6);
    skb->data[5] = (pae_streamid << 4) + context_optype;
    flow_log("  pae ctl: 0x%x 0x%x\n", skb->data[4], skb->data[5]);

    /* Format sctx word 0 (protocol_bits) */
    sctx_words = 3; /* size so far in words, update later */

    /* Format sctx word 1 (cipher_bits) */
    if (isInbound) {
        cipher_bits |= 1 << CIPHER_INBOUND_SHIFT;
    }
    if (authFirst) {
        cipher_bits |= 1 << CIPHER_ORDER_SHIFT;
    }

    /* Set the crypto parameters in the cipher.flags */
    cipher_bits |= cipher_alg << CIPHER_ALG_SHIFT;
    cipher_bits |= cipher_mode << CIPHER_MODE_SHIFT;
    cipher_bits |= cipher_type << CIPHER_TYPE_SHIFT;

    /* Set the auth parameters in the cipher.flags */
    cipher_bits |= auth_alg << HASH_ALG_SHIFT;
    cipher_bits |= auth_mode << HASH_MODE_SHIFT;
    cipher_bits |= auth_type << HASH_TYPE_SHIFT;

    /* Format sctx extensions if required, and update main fields if required) */
    if (auth_alg) {
        /* Write the authentication key material if present */
        if (auth_key_len) {
            skb_put(skb, auth_key_len);
            memcpy(ptr, auth_key_buf, auth_key_len);

            ptr += auth_key_len; buf_len += auth_key_len;
            sctx_words += auth_key_len/4;
        }

        /* in DTLS we need to write the ICV into the payload */
        if (dtls_aead) {
            if (!isInbound) {
                ecf_bits |= 1 << INSERT_ICV_SHIFT;
            } else {
                ecf_bits |= 1 << CHECK_ICV_SHIFT;
            }
        }

        /* Inform the SPU of the ICV size */
        ecf_bits |= (digestsize / 4) << ICV_SIZE_SHIFT;
    }        

    /* copy the encryption keys in the SAD entry */
    if (cipher_alg) {
        if (cipher_key_len) {
            skb_put(skb, cipher_key_len);
            memcpy(ptr, cipher_key_buf, cipher_key_len);

            ptr += cipher_key_len; buf_len += cipher_key_len;
            sctx_words += cipher_key_len/4;
        }

        /* if encrypting then set IV size, use SCTX IV unless no IV given here */
        if (cipher_iv_buf && cipher_iv_len) {
            /* Use SCTX IV */
            ecf_bits |= 1 << SCTX_IV_SHIFT;

            /* cipher iv provided so put it in here */
            skb_put(skb, cipher_iv_len);
            memcpy(ptr, cipher_iv_buf, cipher_iv_len);

            ptr += cipher_iv_len;  buf_len += cipher_iv_len;
            sctx_words += cipher_iv_len/4;
        }
    }

    /* write in the total sctx length now that we know it */
    protocol_bits |= sctx_words;

    /* Endian adjust the SCTX */
    spuh->sa.protocol.bits = htonl(protocol_bits);
    spuh->sa.cipher.bits = htonl(cipher_bits);
    spuh->sa.ecf.bits = htonl(ecf_bits);

    delta_size -= sctx_words * 4;  /* SCTX is not present in output */

    /* === create the BDESC section === */
    skb_put(skb, sizeof(BDESC_HEADER));
    bdesc = (BDESC_HEADER *)ptr;

    /* in the case of DTLS we'll fill in the lengthMAC later */
    bdesc->offsetMAC  = htons(auth_offset);
    bdesc->lengthMAC = htons(auth_len);
    bdesc->offsetCrypto = htons(cipher_offset);
    bdesc->lengthCrypto = htons(cipher_len);
    bdesc->offsetICV = htons(auth_len);
    bdesc->offsetIV = htons(0);
        
    ptr += sizeof(BDESC_HEADER); buf_len += sizeof(BDESC_HEADER);
    delta_size -= sizeof(BDESC_HEADER);  /* BDESC is not present in output */

    /* === no MFM section === */

    /* === create the BD section === */

    /* add the header */
    skb_put(skb, sizeof(BD_HEADER) + real_db_size);
    bd = (BD_HEADER*)ptr;

    bd->size = htons(real_db_size);
    bd->PrevLength = htons(prev_length_blocks);

    ptr += sizeof(BD_HEADER); buf_len += sizeof(BD_HEADER);

    /* copy in assoc data, if present */
    if (assoc_size) {
        sg_copy_part_to_buf(assoc, ptr, assoc_size, assoc_start);
        ptr += assoc_size; buf_len += assoc_size;
    }
    
    /* copy in IV, if present */
    if (aead_iv_buf_len) {
        memcpy(ptr, aead_iv_buf, aead_iv_buf_len);
        ptr += aead_iv_buf_len; buf_len += aead_iv_buf_len;
    }

    /* copy in the prepend buffer */
    memcpy(ptr, prebuf, prebuf_len);
    ptr += prebuf_len; buf_len += prebuf_len;

    /* copy in the data */
    sg_copy_part_to_buf(data, ptr, data_size, data_start);
    ptr += data_size; buf_len += data_size;

    /* fix data alignent for GCM */
    if (cipher_mode == CIPHER_MODE_GCM) {
        unsigned gcm_padding = ((data_size + 15) & ~15) - data_size;
        if (data_size == 0)
            gcm_padding = 16;
        flow_log("  GCM: padding to 16 byte alignment: %u bytes\n", gcm_padding);

        skb_put(skb, gcm_padding);
        memset(ptr, 0, gcm_padding);
        ptr += gcm_padding; buf_len += gcm_padding;

        auth_len += gcm_padding;
        bdesc->lengthMAC = htons(auth_len);

        real_db_size += gcm_padding;
        bd->size = htons(real_db_size);
    }

    /* add hash padding if requested */
    if (hash_pad_len) {
        /* clear the padding section */
        memset(ptr, 0, hash_pad_len);

        /* indicate that this is a final hash */
        skb->data[6] |= 0x80;

        /* terminate the data */
        *ptr = 0x80;
        ptr += (hash_pad_len - sizeof(uint64_t)); buf_len += (hash_pad_len - sizeof(uint64_t));

        /* add the size at the end as required per alg */
        if (auth_alg == HASH_ALG_MD5)
            *(uint64_t*)ptr = cpu_to_le64((u64)total_sent * 8);
        else  /* SHA1, SHA2-224, SHA2-256 */
            *(uint64_t*)ptr = cpu_to_be64((u64)total_sent * 8);
        ptr += sizeof(uint64_t); buf_len += sizeof(uint64_t);
    }

    /* pad to a 4byte alignment for STAT */
    if (status_padding) {
        unsigned status_padding = ((real_db_size + 3) & ~3) - real_db_size;
        flow_log("  STAT: padding to 4byte alignment: %u bytes\n", status_padding);

        skb_put(skb, status_padding);
        memset(ptr, 0, status_padding);
        ptr += status_padding; buf_len += status_padding;
    }

    /* === no HASH section from the host (only received) === */
    if (auth_alg && !dtls_aead) {
        delta_size += digestsize;  /* but HASH section will be added */
    }

    /* === add the STAT section === */
    skb_put(skb, 4);
    memset(ptr, 0, 4);
    ptr += 4; buf_len += 4;

    /* EMH: Use the emh to store the change in size due to SPU processing */
    skb->data[7] = delta_size;
    
    /* Enforce a minimum frame size */
    if (buf_len < MIN_SPU_PKT_SIZE) {
        if ((MIN_SPU_PKT_SIZE - buf_len) > 63) {
            printk("%s() Error: oversized packet padding.\n", __FUNCTION__);
        } else {
            skb_put(skb, MIN_SPU_PKT_SIZE - buf_len);
            memset(skb->data + buf_len, 0, (MIN_SPU_PKT_SIZE - buf_len));
            /* store padding size in third byte of EMH.  PAE FW will extract it to remove padding pre-SPU */
            skb->data[6] |= (MIN_SPU_PKT_SIZE - buf_len) & 0x3f;
            buf_len += (MIN_SPU_PKT_SIZE - buf_len);
        }
    }

    flow_log("  skb->len:%d\n", skb->len);
    packet_dump("  skb: ", skb->data, buf_len);

    /* if (skb->len < 1450) { */
    /*     printk(" shrt? skb_len: %u\n", skb->len); */
    /* } else  if (skb->len > 1496) { */
    /*     printk(" long? skb_len: %u\n", skb->len); */
    /* } else { */
    /*     printk(" skb_len: %u\n", skb->len); */
    /* } */

    if (skb->len > (MAX_SPU_PKT_SIZE - 4)) {
        printk(KERN_ERR " long? skb_len: %u\n", skb->len);
    }

    return skb;
}
