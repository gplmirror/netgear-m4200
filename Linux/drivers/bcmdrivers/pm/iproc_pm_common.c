/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <linux/types.h>
#include <linux/sizes.h>
#include <linux/err.h>
#include <linux/delay.h>
#include <linux/suspend.h>
#include <asm/io.h>
#include <asm/bug.h>
#include <asm/tlbflush.h>
#include <asm/cacheflush.h>
#include <asm/proc-fns.h>
#include "iproc_pm.h"
#include "iproc_pm_device.h"

extern int iproc_arm_cpu_suspend(unsigned long, int (*)(unsigned long));//cpu_v7_do_suspend()
extern void iproc_pm_cpu_idle(void);//proc-v7.S: cpu_v7_do_idle()
extern void iproc_pm_finish_switch(unsigned long state);
extern void iproc_pm_cpu_resume(void); //cpu_v7_do_resume()
extern void iproc_pm_disable_L1_D_cache(void);
extern void iproc_pm_enable_L1_D_cache(void);
extern void iproc_pm_flush_disable_L1_D_cache(void);

static struct iproc_pm_reg iproc_common_saved_regs[] = {
//	IPROC_SAVEREG(IHOST_PROC_CLK_WR_ACCESS),
	IPROC_SAVEREG(IHOST_SCU_CONTROL),
	IPROC_SAVEREG(IHOST_SCU_CONFIG),
	IPROC_SAVEREG(IHOST_SCU_FILTER_START),
	IPROC_SAVEREG(IHOST_SCU_FILTER_END),
	IPROC_SAVEREG(IHOST_SCU_ACCESS_CONTROL),
	IPROC_SAVEREG(IHOST_SCU_SECURE_ACCESS),
	IPROC_SAVEREG(IHOST_GICCPU_CONTROL),
	IPROC_SAVEREG(IHOST_GICCPU_PRIORITY_MASK),
	IPROC_SAVEREG(IHOST_GTIM_GLOB_LOW),
	IPROC_SAVEREG(IHOST_GTIM_GLOB_HI),
	IPROC_SAVEREG(IHOST_GTIM_GLOB_CTRL),
	IPROC_SAVEREG(IHOST_GTIM_GLOB_COMP_LOW),
	IPROC_SAVEREG(IHOST_GTIM_GLOB_COMP_HI),
	IPROC_SAVEREG(IHOST_GTIM_GLOB_INCR),
	IPROC_SAVEREG(IHOST_L2C_CONTROL),
	IPROC_SAVEREG(IHOST_L2C_AUX_CONTROL),
	IPROC_SAVEREG(IHOST_L2C_TAG_RAM_CTRL),
	IPROC_SAVEREG(IHOST_L2C_DATA_RAM_CTRL),
	IPROC_SAVEREG(IHOST_L2C_EVENT_CTR_CTRL),
	IPROC_SAVEREG(IHOST_L2C_EVENT_CTR_CFG0),
	IPROC_SAVEREG(IHOST_L2C_EVENT_CTR_CFG1),
	IPROC_SAVEREG(IHOST_L2C_PWR_CTRL),
};

static struct iproc_pm_device_regs iproc_pm_dev_common = {
	.devname  = "SoC",
	.regs     = &iproc_common_saved_regs,
	.regs_num = ARRAY_SIZE(iproc_common_saved_regs),
	.read	  = iproc_reg32_read,
	.write	  = iproc_reg32_write,
};

///////////////////////////////////////////////////////////////////////////////////////////
#ifdef iproc_bitmask
#error "iproc_bitmask has re-defined!"
#else
#define iproc_bitmask(len)						((1<<len) -1)
#define iproc_bitmask_shifted(len, offset)		(iproc_bitmask(len)<<offset)
#endif

static inline void iproc_reg32_write_masked(const uint32_t addr, const uint32_t start, const uint32_t len, const uint32_t value)
{
	uint32_t curvalue, mask, mask_shifted, newvalue, tmpvalue;

	if((start+len)>32)
	{
		iproc_err("Error argument!\n");
		return ;
	}

	curvalue = iproc_reg32_read(addr);
	mask = iproc_bitmask(len);
	mask_shifted = mask<<start;
	tmpvalue = curvalue & (~mask_shifted);
	newvalue = tmpvalue | ((value & mask)<<start);

	iproc_reg32_write(addr, newvalue);
}

static inline uint32_t iproc_reg32_getbit(const uint32_t addr, const uint32_t right_start)
{
	return (iproc_reg32_read(addr)>>right_start) & 0x1;
}

static inline void iproc_reg32_setbit(const uint32_t addr, const uint32_t right_start)
{
	iproc_reg32_write(addr, iproc_reg32_read(addr) | (1<< right_start));
}

static inline void iproc_reg32_clrbit(const uint32_t addr, const uint32_t right_start)
{
	iproc_reg32_write(addr, iproc_reg32_read(addr) & (~(1<< right_start)) );
}

static inline uint32_t iproc_reg32_getbits(uint32_t addr, uint32_t right_start, uint32_t len)
{
	return (iproc_reg32_read(addr)>> right_start) & iproc_bitmask(len);
}

static inline void iproc_reg32_setbits(uint32_t addr, uint32_t right_start, uint32_t len)
{
	iproc_reg32_write(addr, iproc_reg32_read(addr) | iproc_bitmask_shifted(len, right_start));
}

static inline void iproc_reg32_clrbits(uint32_t addr, uint32_t right_start, uint32_t len)
{
	iproc_reg32_write(addr, iproc_reg32_read(addr) & (~ iproc_bitmask_shifted(len, right_start)) );
}

static inline void iproc_spru_reg_access_done(void)
{
	uint32_t stat=0;
	
	do{
		stat = iproc_reg32_read(SPRU_BBL_STATUS);
		iproc_dbg("stat=0x%08x\n", stat);
	}while(test_bit(SPRU_BBL_STATUS__ACC_DONE, (const volatile unsigned long *)&stat));
}

static inline uint32_t iproc_spru_reg_read(uint32_t addr)
{
	uint32_t cmd=0;
	
	cmd = (addr & 0x3ff) | (1<<SPRU_BBL_CMD__IND_RD);
	iproc_reg32_write(SPRU_BBL_CMD, cmd);

	iproc_spru_reg_access_done();
		
	return iproc_reg32_read(SPRU_BBL_RDATA);
}

static inline void iproc_spru_reg_write(uint32_t addr, uint32_t data)
{	
	uint32_t cmd=0;
	
	iproc_reg32_write(SPRU_BBL_WDATA, data);
	
	cmd = (addr & 0x3ff) | (1<<SPRU_BBL_CMD__IND_WR);
	iproc_reg32_write(SPRU_BBL_CMD, cmd);
	
	iproc_spru_reg_access_done();
}

/////////////////////////////////////////////////////////////////////////////////////////
static void iproc_pm_flush_disable_l1_caches(void)
{
	iproc_prt("Flush & disable L1 caches!\n");
	dsb();
	isb();
//	__cpuc_flush_icache_all();
//	__cpuc_flush_kern_all();   // this calls v7_flush_dcache_all()
//	__cpuc_flush_user_all();
	iproc_pm_flush_disable_L1_D_cache();
}

/* 
  *dump ddr ctrl and phy regs
  */
void iproc_dump_ddr_regs(void)
{
	uint32_t addr;

	for(addr=DDR_DENALI_CTL_00; addr<=DDR_DENALI_CTL_304;)
	{
		printk("DDR ctrl reg [0x%08x]=0x%08x\n", addr, iproc_reg32_read(addr));
		addr+=4;
	}
	
	for(addr=DDR_BistConfig; addr<=DDR_BistLastDataErrWord0;)
	{
		printk("DDR bist reg [0x%08x]=0x%08x\n", addr, iproc_reg32_read(addr));
		addr+=4;
	}
	
	for(addr=DDR_PHY_CONTROL_REGS_REVISION; addr<=DDR_PHY_CONTROL_REGS_RO_PROC_MON_STATUS;)
	{
		printk("phy ctrl reg [0x%08x]=0x%08x\n", addr, iproc_reg32_read(addr));
		addr+=4;
	}
	
	for(addr=DDR_PHY_BYTE_LANE_0_VDL_CONTROL_WR_DQS_P; addr<=DDR_PHY_BYTE_LANE_0_VDL_LDE_CONTROL;)
	{
		printk("phy lane0 vdl reg [0x%08x]=0x%08x\n", addr, iproc_reg32_read(addr));
		addr+=4;
	}

	for(addr=DDR_PHY_BYTE_LANE_0_RD_EN_DLY_CYC; addr<=DDR_PHY_BYTE_LANE_0_BL_SPARE_REG;)
	{
		printk("phy lane0 reg [0x%08x]=0x%08x\n", addr, iproc_reg32_read(addr));
		addr+=4;
	}

	for(addr=DDR_PHY_BYTE_LANE_1_VDL_CONTROL_WR_DQS_P; addr<=DDR_PHY_BYTE_LANE_1_VDL_LDE_CONTROL;)
	{
		printk("phy lane1 vdl reg [0x%08x]=0x%08x\n", addr, iproc_reg32_read(addr));
		addr+=4;
	}

	for(addr=DDR_PHY_BYTE_LANE_1_RD_EN_DLY_CYC; addr<=DDR_PHY_BYTE_LANE_1_BL_SPARE_REG;)
	{
		printk("phy lane1 reg [0x%08x]=0x%08x\n", addr, iproc_reg32_read(addr));
		addr+=4;
	}
}

/*
  * save some ddr related regs in idram, restore in bootloader
  */
int iproc_save_ddr_regs_in_idram(void)
{
	int i=0, counts=0;
	IPROC_DDR_REGS_BAKUP_t *ddr=NULL;
	volatile uint32_t *idram_vaddr1=NULL, *idram_vaddr2=NULL;
	uint32_t regaddr;

	if (sizeof(IPROC_DDR_REGS_BAKUP_t) > IPROC_DDR_REGS_BAKUP_MAXLEN)
	{
		iproc_err("*************** DDR CTRL REGS BAKUP OVERSIZE **********!!!");
		return -1;
	}

	//step 2: save
	counts = 0;
	idram_vaddr1 = (uint32_t *)((uint8_t *)m0_idram_vbase + IPROC_DDR_REGS_BAKUP_OFFSET);

	/* ddr_ctl regs */
	idram_vaddr2 = idram_vaddr1 + counts;
	counts = ARRAY_SIZE(ddr->ddr_ctl);
	regaddr = DDR_DENALI_CTL_00;
	for(i=0; i<counts; i++)
	{
		*(idram_vaddr2 + i) = iproc_reg32_read(regaddr + 4*i);
	}
	iproc_dbg("Save %d ddr_ctl regs\n", counts);
	
	/* ddr_phy_ctl_vdl regs */
	idram_vaddr2 = idram_vaddr1 + counts;
	counts = ARRAY_SIZE(ddr->ddr_phy_ctl_vdl);
	regaddr = DDR_PHY_CONTROL_REGS_VDL_CALIBRATE;
	for(i=0; i<counts; i++)
	{
		*(idram_vaddr2 + i) = iproc_reg32_read(regaddr + 4*i);
	}
	iproc_dbg("Save %d ddr_phy_ctl_vdl regs\n", counts);

	return 0;
}

static void iproc_dump_m0_isr_args_idram(IPROC_M0_ISR_ARGS_t *args, char *des)
{
	int i;
	int args_size = sizeof(IPROC_M0_ISR_ARGS_t);
	
	if(!args)
	{
		return ;
	}

	iproc_dbg("M0 ISR args %s, size=%d, start addr=0x%p.\n", des, args_size, args);
	iproc_dbg("   iproc_pm_state=%d\n", args->iproc_pm_state);
	
	for(i=0; i<IPROC_PM_STATE_END; i++)	
		iproc_dbg("   iproc_clock_policies[%d]=0x%08x\n", i, args->iproc_clock_policies[i].u.data);

	for(i=0; i<IPROC_PM_STATE_END; i++) 
		iproc_dbg("   iproc_power_policies[%d]=0x%08x\n", i, args->iproc_power_policies[i].u.data);
}

/*
  * Update the user defined M0 ISR args to idram, ensure this is called before entering power saving modes
  */
static int iproc_sync_m0_isr_args_to_idram(void)
{
	IPROC_M0_ISR_ARGS_t *args_src = &m0_isr_args;
	int args_size = sizeof(m0_isr_args);
	unsigned char *args_vdest=NULL;
	unsigned int args_pdest;

	iproc_dbg("Sync M0 ISR parameters from DDR to IDRAM\n");

	if(args_size>IPROC_M0_ISR_ARGS_MAXLEN)
	{
		iproc_err("M0 ISR args oversize: 0x%x, max 0x%x.\n", args_size, IPROC_M0_ISR_ARGS_MAXLEN);
		return -EINVAL;
	}

	args_src->iproc_pm_state = iproc_get_current_pm_state();
	iproc_dump_m0_isr_args_idram(args_src, "(DDR)");

	args_pdest = idram_res.start + IPROC_M0_ISR_ARGS_OFFSET;
	args_vdest = (unsigned char *)m0_idram_vbase + IPROC_M0_ISR_ARGS_OFFSET;
	iproc_dbg("M0 ISR args: args_pdest=0x%08x, args_vdest=0x%p\n", args_pdest, args_vdest);
	
	memcpy((unsigned char *)args_vdest, (unsigned char *)args_src, args_size);

	iproc_dump_m0_isr_args_idram((IPROC_M0_ISR_ARGS_t *)args_vdest, "(IDRAM)");

	return 0;
}

/*
  * Write the mailbox control code, then trigger the mailbox interrupt on M0
  */
int iproc_send_mailbox_interrupt_to_mcu(iproc_power_status_e state)
{
	uint32_t word0, word1;

	switch(state)
	{
		case IPROC_PM_STATE_RUN:
			word0 = MAILBOX_CODE0_RUN;
			word1 = MAILBOX_CODE1_RUN;
			break;
		case IPROC_PM_STATE_STANDBY:
			word0 = MAILBOX_CODE0_STANDBY;
			word1 = MAILBOX_CODE1_STANDBY;
			break;
		case IPROC_PM_STATE_SLEEP:
			word0 = MAILBOX_CODE0_SLEEP;
			word1 = MAILBOX_CODE1_SLEEP;
			break;
		case IPROC_PM_STATE_DEEPSLEEP:
			word0 = MAILBOX_CODE0_DEEPSLEEP;
			word1 = MAILBOX_CODE1_DEEPSLEEP;
			break;
		default:
			iproc_err("Not supported PM state: %d!\n", state);
			return -EINVAL;
	}

	iproc_dbg("Send mail box interrupt to MCU: word0=0x%08x, word1=0x%08x\n\n\n", word0, word1);
	iproc_reg32_write(IPROC_CRMU_MAIL_BOX0, word0); //word0 as a callback function index for M0. 
	iproc_reg32_write(IPROC_CRMU_MAIL_BOX1, word1); //trigger M0 IRQ31

	return 0;
}

/*
  * enable/disable mcu access all regs
  */
static void iproc_set_mcu_access_control(int en)
{
	if(en) /*MCU can access all regs*/
	{
		iproc_dbg("Enable MCU access all regs\n");
		iproc_reg32_write(CRMU_MCU_ACCESS_CONTROL, 0x2);
	}
	else /*MCU uses address window, default*/
	{
		iproc_dbg("Disable MCU access all regs\n");
		iproc_reg32_write(CRMU_MCU_ACCESS_CONTROL, 0x1);
	}
}

static int iproc_set_SCU_status(iproc_power_status_e state)
{
	uint32_t pm_val=0;

	switch(state)
	{
		case IPROC_PM_STATE_RUN:
		case IPROC_PM_STATE_STANDBY:	
			pm_val = 0xfffffffc;
			break;
		case IPROC_PM_STATE_SLEEP:
		case IPROC_PM_STATE_DEEPSLEEP:
			pm_val = 0xffffffff;
			break;
		default:
			iproc_err("Not supported PM state: %d!\n", state);
			return -EINVAL;
	}

	iproc_dbg("Set SCU power status to 0x%08x\n", pm_val);
	iproc_reg32_write(IHOST_SCU_POWER_STATUS, pm_val); //scu_power_mode(scu_base_addr, SCU_PM_POWEROFF);

	return 0;
}

/*
  * write 2'bxx to SCU CPU Power Status Reg
  */
static int iproc_set_PWRCTRLO_state(iproc_power_status_e state)
{
	int pm_val=0;
	uint32_t PWRCTRLO = CRMU_IHOST_POWER_CONFIG;

	switch(state)
	{
		case IPROC_PM_STATE_RUN:
		case IPROC_PM_STATE_STANDBY:	
			pm_val = 0x0;
			break;
		case IPROC_PM_STATE_SLEEP:
			pm_val = 0x2;
			break;
		case IPROC_PM_STATE_DEEPSLEEP:
			pm_val = 0x3;
			break;
		default:
			iproc_err("Not supported PM state: %d!\n", state);
			return -EINVAL;
	}

	iproc_dbg("Set PWRCTRLO to %d(%s)\n", pm_val, iproc_get_pm_state_str_by_id(state));

	iproc_reg32_write(PWRCTRLO, pm_val);

	return 0;
}

static void iproc_exec_wfi(void)
{
	iproc_pm_cpu_idle();
}

/*
  * Enable/disable iproc_gtimer, used by iproc_clocksource_init()
  */
static void iproc_set_system_timer(int en)
{
	if(en)//restore
	{
		iproc_reg32_setbit(IHOST_GTIM_GLOB_CTRL, IHOST_GTIM_GLOB_CTRL__Timer_en_G);
		iproc_dbg("System timer enabled\n");
	}
	else//save
	{
		iproc_reg32_clrbit(IHOST_GTIM_GLOB_CTRL, IHOST_GTIM_GLOB_CTRL__Timer_en_G);
		iproc_dbg("System timer disabled\n");
	}
}

///////////////////////////////////////////////////////////////////////////////
//@src: the IDRAM mapped vaddr, or 0
static void iproc_dump_m0_bin(int irq, unsigned char *idram_vbase)
{
	char *des=NULL;
	unsigned char *vstart=NULL, *vend=NULL;
	unsigned int i, len;
	unsigned int pstart=0, idram_pbase=idram_res.start;

	if(!iproc_get_msglevel())
		return;

	switch(irq)
	{
		case MCU_MAILBOX_EVENT:
			des = "MailBox";
			if(!idram_vbase)//RAW BIN
			{
				vstart  = &M0_MBOX_BIN_START;
				vend    = &M0_MBOX_BIN_END;
			}
			else //IDRAM BIN
			{
				vstart  = idram_vbase + IPROC_MAILBOX_ISR_OFFSET;
				pstart  = idram_pbase + IPROC_MAILBOX_ISR_OFFSET;
				vend    = vstart + (&M0_MBOX_BIN_END - &M0_MBOX_BIN_START);
			}				
			break;
		case MCU_AON_GPIO_INTR:
		case MCU_TIMER_INTR:
		case MCU_WDOG_INTR:
		case MCU_SPRU_ALARM_EVENT:
			des = "WakeUp";
			if(!idram_vbase)//RAW BIN
			{
				vstart  = &M0_WAKEUP_BIN_START;
				vend    = &M0_WAKEUP_BIN_END;
			}
			else //IDRAM BIN
			{
				vstart  = idram_vbase + IPROC_WAKEUP_ISR_OFFSET;
				pstart  = idram_pbase + IPROC_WAKEUP_ISR_OFFSET;
				vend    = vstart + (&M0_WAKEUP_BIN_END - &M0_WAKEUP_BIN_START);
			}
			break;
		default:
			iproc_err("Not supported IRQ%d on M0!\n", irq);
			return ;
	}
	
	len = vend - vstart;
	
	iproc_dbg("User %s ISR bin in %s for M0: (pstart=0x%08x, vstart=0x%p, len=%d)\n",
		des, idram_vbase?"IDRAM":"RAW", pstart, vstart, len);
	
	for(i=0;i<len;i++)
	{
		if(i%16==0 && i>0)
			printk("    \n");
		
		printk("%02x ", vstart[i]);
	}
	
	iproc_dbg("\n");
}

void iproc_dump_m0_isr_bins(int src, IPROC_MCU_IRQ_EVENT_NUM_e binno)
{
	void * __iomem vbase=NULL;

	if(!iproc_get_msglevel())
		return;

	if(src)//idram
	{
		vbase = m0_idram_vbase;
	}

	if(binno==MCU_INTR_EVENT_END)
	{
		iproc_dump_m0_bin(MCU_MAILBOX_EVENT, (unsigned char *)vbase);
		iproc_dump_m0_bin(MCU_AON_GPIO_INTR, (unsigned char *)vbase);
	}
	else
	{
		iproc_dump_m0_bin(binno, (unsigned char *)vbase);
	}
}

/*
  * Dump the contents of idram_res
  */
void iproc_dump_m0_idram(void)
{
	unsigned int i, len=resource_size(&idram_res);
	unsigned int *idram=NULL, *addr=NULL;
	unsigned int val;

	if(!iproc_get_msglevel())
		return;

	iproc_dbg("IDRAM content:\n");

	idram = (unsigned int *)m0_idram_vbase;

	for(i=0;i<len/sizeof(unsigned int);i++)
	{
		addr = (unsigned int *)(idram + i);
		val = *addr;
		
		if(i%4==0)
			printk("%p: ", addr);

		printk("%08x", val);
		
		if((i+1)%4==0)
			printk("\n");
		else
			printk(" ");
	}
	
	iproc_dbg("\n");
}

/*
  * Enable/disable the user M0 ISR by writing the indirect jump reg with new ISR entry addr or 0
  */
static int iproc_set_new_m0_isr(int irq, int en)
{
	unsigned int IRQx_vector_USER, offset;
	
	iproc_dbg("%s user ISR for M0 IRQ%d(%s)\n", en ? "Enable":"Disable", irq, iproc_get_mcu_irq_desc_by_id(irq));

	switch(irq)
	{
		case MCU_MAILBOX_EVENT:
			offset = IPROC_MAILBOX_ISR_OFFSET;
			break;
		case MCU_AON_GPIO_INTR:
		case MCU_TIMER_INTR:
		case MCU_WDOG_INTR:
		case MCU_SPRU_ALARM_EVENT:
			offset = IPROC_WAKEUP_ISR_OFFSET;
			break;
		default:
			iproc_err("Not supported IRQ%d on M0!\n", irq);
			return -1;
	}

	IRQx_vector_USER  = CRMU_IRQx_vector_USER(irq);//(CRMU_IRQx_vector_USER_START + 4*irq);

	if(en)
		iproc_reg32_write(IRQx_vector_USER, CRUM_M0_IDRAM_START + offset);
	else
		iproc_reg32_write(IRQx_vector_USER, 0);
	
	iproc_dbg("Set M0 IRQ%d_vector_USER(%s)(0x%08x) = 0x%08x\n", irq, iproc_get_mcu_irq_desc_by_id(irq), IRQx_vector_USER, iproc_reg32_read(IRQx_vector_USER));
	
	return 0;
}

/*
  * Install/uninstall a sepcified user irq handler in M0 IDRAM
  */
static int iproc_install_new_m0_isr(int irq, void * __iomem m0_idram_vbase, int install)
{
	unsigned char *start=NULL, *end=NULL, *vdest=NULL;
	unsigned int len, offset, max_len;
	unsigned int pdest;

	iproc_dbg("\n");
	
	switch(irq)
	{
		case MCU_MAILBOX_EVENT:
			start   = &M0_MBOX_BIN_START;
			end     = &M0_MBOX_BIN_END;
			offset  = IPROC_MAILBOX_ISR_OFFSET;
			max_len = IPROC_MAILBOX_ISR_MAXLEN;
			break;
		case MCU_AON_GPIO_INTR:
		case MCU_TIMER_INTR:
		case MCU_WDOG_INTR:
		case MCU_SPRU_ALARM_EVENT:
			start   = &M0_WAKEUP_BIN_START;
			end     = &M0_WAKEUP_BIN_END;
			offset  = IPROC_WAKEUP_ISR_OFFSET;
			max_len = IPROC_WAKEUP_ISR_MAXLEN;
			break;
		default:
			iproc_err("Not supported IRQ%d on M0!\n", irq);
			return -EINVAL;
	}

	pdest = idram_res.start + offset;
	vdest = (unsigned char *)m0_idram_vbase + offset;
	
	//firstly clear the existing content
	memset(vdest, 0, max_len);
	iproc_dbg("User ISR for M0 IRQ%d(%s) erased in IDRAM, pdest=0x%08x, len=%u\n", irq, iproc_get_mcu_irq_desc_by_id(irq), pdest, max_len);

	if(install)
	{
		len = end - start;
		if(len > max_len)
		{
			iproc_err("User ISR bin for M0 IRQ%d(%s) oversize: real len=%u, max len=%u!\n", irq, iproc_get_mcu_irq_desc_by_id(irq), len, max_len);
			return -EINVAL;
		}
		
		memcpy(vdest, start, len);
		iproc_dbg("User ISR for M0 IRQ%d(%s) copyed in IDRAM, pdest=0x%08x, len=%u\n", irq, iproc_get_mcu_irq_desc_by_id(irq), pdest, len);
	}

	return 0;
}

/*
  * Install/uninstall all user irq handlers in M0 IDRAM
  */
int iproc_install_m0_isr_bins(int install)
{
	//To unload the ISR, disable it firstly
	//To install the ISR, enable it when going to power save mode
	if(install==IPROC_DISABLED)
	{
		iproc_set_new_m0_isr(MCU_MAILBOX_EVENT, IPROC_DISABLED);
		iproc_set_new_m0_isr(MCU_AON_GPIO_INTR, IPROC_DISABLED);
	}

	if(iproc_install_new_m0_isr(MCU_MAILBOX_EVENT, m0_idram_vbase, install)<0)
		return -1;
	
	if(iproc_install_new_m0_isr(MCU_AON_GPIO_INTR, m0_idram_vbase, install)<0)
		return -1;

	return 0;
}

/*
  *  Enable/disable a specified M0 irq
  */
static int iproc_set_mcu_irq(IPROC_MCU_IRQ_EVENT_NUM_e irq, int en)
{
	uint32_t intr_mask_reg=CRMU_MCU_INTR_MASK;
	uint32_t event_mask_reg=CRMU_MCU_EVENT_MASK;
	uint32_t mask_reg;
	int mask_bit;
	/*
	mask_bit:
	   0 = intr enabled.
	   1 = intr masked (disabled).
	 */
	switch(irq)
	{
		/*The followings are sleep src*/
		case MCU_MAILBOX_EVENT : 
			mask_reg = event_mask_reg;
			mask_bit = CRMU_MCU_EVENT_MASK__MCU_MAILBOX_EVENT_MASK;
			break;
		case MCU_IPROC_STANDBYWFI_EVENT : 
			mask_reg = event_mask_reg;
			mask_bit = CRMU_MCU_EVENT_MASK__MCU_IPROC_STANDBYWFI_EVENT_MASK;
			break;
		case MCU_IPROC_STANDBYWFE_EVENT : 
			mask_reg = event_mask_reg;
			mask_bit = CRMU_MCU_EVENT_MASK__MCU_IPROC_STANDBYWFE_EVENT_MASK;
			break;

		/*The followings are wakeup src*/
		case MCU_WDOG_INTR : 
			mask_reg = intr_mask_reg;
			mask_bit = CRMU_MCU_INTR_MASK__MCU_WDOG_INTR_MASK;
			break;
		case MCU_TIMER_INTR: 
			mask_reg = intr_mask_reg;
			mask_bit = CRMU_MCU_INTR_MASK__MCU_TIMER_INTR_MASK;
			break;
		case MCU_AON_GPIO_INTR : 
			mask_reg = intr_mask_reg;
			mask_bit = CRMU_MCU_INTR_MASK__MCU_AON_GPIO_INTR_MASK;
			break;
		default:
			iproc_err("Not supported irq/event %d to set!\n", irq);
			return -EINVAL;
	}

	iproc_dbg("%s M0 IRQ%d(%s)\n", en?"Unmask":"Mask", irq, iproc_get_mcu_irq_desc_by_id(irq));
	
	if(en)
	{
		iproc_reg32_clrbit(mask_reg, mask_bit);
	}
	else
	{
		iproc_reg32_setbit(mask_reg, mask_bit);
	}
	
	iproc_dbg("New M0 %s mask reg 0x%08x = 0x%08x\n", mask_reg==CRMU_MCU_INTR_MASK ? "interrupt" : "event", mask_reg, iproc_reg32_read(mask_reg));
	
	iproc_set_new_m0_isr(irq, en);
	
	return 0;
}

static int iproc_set_wakeup_IRQs_ISRs(int en)
{
	uint delay1 = iproc_get_rtc_alarm_delay();
	uint delay2 = delay1 % 128;

//	extern void rtc_reg_write(unsigned int reg_addr, unsigned int data);
	
	if(iproc_get_valid_mcu_wakeup_src()<0)
		return -1;
	
	if(iproc_check_mcu_wakeup_src_supported(ws_alarm)) //rtc alarm
	{
		if(en)
		{
			iproc_dbg("Enable M0 rtc alarm irq with delay %d seconds.\n", delay1);
			iproc_spru_reg_write(BBL_INTERRUPT_EN, iproc_spru_reg_read(BBL_INTERRUPT_EN) | (1<<BBL_INTERRUPT_EN__bbl_match_intr_en));
			iproc_spru_reg_write(BBL_CONFIG, iproc_spru_reg_read(BBL_CONFIG) & ~(1<<BBL_CONFIG__bbl_alarm_mask));
			iproc_spru_reg_write(BBL_RTC_MATCH, delay2);
//			rtc_reg_write(BBL_RTC_MATCH, delay);
		}
		else
		{
			iproc_dbg("Disable M0 rtc alarm irq.\n");
			iproc_spru_reg_write(BBL_INTERRUPT_EN, iproc_spru_reg_read(BBL_INTERRUPT_EN) & ~(1<<BBL_INTERRUPT_EN__bbl_match_intr_en));
			iproc_spru_reg_write(BBL_CONFIG, iproc_spru_reg_read(BBL_CONFIG) | (1<<BBL_CONFIG__bbl_alarm_mask));
			iproc_spru_reg_write(BBL_RTC_MATCH, 0xFFFF);
		}
		iproc_set_new_m0_isr(ws_alarm, en);
	}

	if(iproc_check_mcu_wakeup_src_supported(ws_wdog))
		iproc_set_mcu_irq(ws_wdog,  en);

	if(iproc_check_mcu_wakeup_src_supported(ws_timer))
		iproc_set_mcu_irq(ws_timer, en);

	if(iproc_check_mcu_wakeup_src_supported(ws_gpio))
		iproc_set_mcu_irq(ws_gpio,  en);

	return 0;
}

static int iproc_set_powersave_IRQ_ISR(int en)
{
	iproc_set_mcu_irq(MCU_MAILBOX_EVENT, en);

	iproc_dbg("CRMU_MCU_INTR_STATUS=0x%08x\n",iproc_reg32_read(CRMU_MCU_INTR_STATUS));
	iproc_dbg("CRMU_MCU_INTR_MASK=0x%08x",iproc_reg32_read(CRMU_MCU_INTR_MASK));
	iproc_dbg("CRMU_MCU_INTR_CLEAR=0x%08x\n",iproc_reg32_read(CRMU_MCU_INTR_CLEAR));
	iproc_dbg("CRMU_MCU_EVENT_STATUS=0x%08x\n",iproc_reg32_read(CRMU_MCU_EVENT_STATUS));
	iproc_dbg("CRMU_MCU_EVENT_CLEAR=0x%08x\n",iproc_reg32_read(CRMU_MCU_EVENT_CLEAR));
	iproc_dbg("CRMU_MCU_EVENT_MASK=0x%08x\n",iproc_reg32_read(CRMU_MCU_EVENT_MASK));

//	iproc_dump_m0_isr_bins(1, MCU_INTR_EVENT_END);
	
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/*
LCPLL0 has 6 channels, CRMU_LCPLL0_CONTROL0:
  cdru_pcie0_phy_ref_clk_p/n     100      [lcpll0_ch0_cmlp/n]      channel_0
  cdru_pcie1_phy_ref_clk_p/n     100      [lcpll0_ch0_cml2p/n]    channel_0
  iproc_ddr_clk                            50        [lcpll0_clkout[1]]          channel_1
  iproc_sdio_clk                           200      [lcpll0_clkout[2]]          channel_2
  usb_phy_ref_clk                        52        [lcpll0_clkout[3]]          channel_3
  cdru_asiu_smart_card_clk         40        [lcpll0_clkout[4]]          channel_4
  */
//ddrclk=(refclk/pdiv)*ndiv/mdiv
uint32_t iproc_get_ddr3_clock_mhz(void)
{
	uint32_t c0,c1,c2;
	uint32_t ndiv, pdiv, mdiv, ddrclk;

	c0 = iproc_reg32_read(CRMU_LCPLL0_CONTROL0);
	c1 = iproc_reg32_read(CRMU_LCPLL0_CONTROL1);
	c2 = iproc_reg32_read(CRMU_LCPLL0_CONTROL2);
	
	pdiv = iproc_reg32_getbits(CRMU_LCPLL0_CONTROL1, CRMU_LCPLL0_CONTROL1__LCPLL0_PDIV_R,     CRMU_LCPLL0_CONTROL1__LCPLL0_PDIV_WIDTH);
	ndiv = iproc_reg32_getbits(CRMU_LCPLL0_CONTROL1, CRMU_LCPLL0_CONTROL1__LCPLL0_NDIV_INT_R, CRMU_LCPLL0_CONTROL1__LCPLL0_NDIV_INT_WIDTH);
	mdiv = iproc_reg32_getbits(CRMU_LCPLL0_CONTROL2, CRMU_LCPLL0_CONTROL2__LCPLL0_CH1_MDIV_R, CRMU_LCPLL0_CONTROL2__LCPLL0_CH1_MDIV_WIDTH);

	mdiv = (mdiv ? mdiv:256);

	ddrclk = (25 * ndiv * 2) / mdiv;
	
	if(ddrclk == 466 || ddrclk == 666)
		ddrclk +=1;
	
	iproc_dbg("CRMU_LCPLL0_CONTROL0=0x%08x, CRMU_LCPLL0_CONTROL1=0x%08x, CRMU_LCPLL0_CONTROL2=0x%08x\n",c0,c1, c2);
	iproc_dbg("DRAM pdiv(%u), ndiv(%u), mdiv(%u), ddrclk(%uMHz)\n", pdiv, ndiv, mdiv, ddrclk);

	return(ddrclk);
}

int iproc_change_pcie_timings(int upscaling)
{
	int ch=0;
	uint32_t old_div, new_div;

	//PCIe controller0: 1G-300M, LCPLL0_CH0
	//PCIe serdes0: 1G-300M, LCPLL0_CH0
	iproc_dbg("CRMU_LCPLL0_CONTROL2=0x%08x\n", iproc_reg32_read(CRMU_LCPLL0_CONTROL2));
	old_div = iproc_reg32_getbits(CRMU_LCPLL0_CONTROL2, CRMU_LCPLL0_CONTROL2__LCPLL0_CH0_MDIV_R, CRMU_LCPLL0_CONTROL2__LCPLL0_CH0_MDIV_WIDTH);
	if(upscaling)
		new_div = (old_div * 300 / 1000);
	else
		new_div = (old_div * 1000 / 300);

	//disable and hold LCPLL0_CH0
	iproc_reg32_setbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_HOLD_CH_R + ch);
	iproc_reg32_setbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_ENABLEB_CH_R + ch);

	//change mdiv of LCPLL0_CH0
	iproc_reg32_write_masked(CRMU_LCPLL0_CONTROL2, CRMU_LCPLL0_CONTROL2__LCPLL0_CH0_MDIV_R, CRMU_LCPLL0_CONTROL2__LCPLL0_CH0_MDIV_WIDTH, new_div);

	//enable and release LCPLL0_CH0
	iproc_reg32_clrbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_ENABLEB_CH_R + ch);
	iproc_reg32_clrbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_HOLD_CH_R + ch);

	//wait for pll locked & stable
	while(!iproc_reg32_getbit(CRMU_LCPLL0_STATUS, CRMU_LCPLL0_STATUS__LCPLL0_LOCK));

	return 0;
}

static void iproc_change_ddr3_clock_mhz(int upscaling)
{
	int ch=1;
	uint32_t old_div, new_div;
	//modify DDR clock by changing ddr_axi_clk mdiv, which is derived from iproc_axi21_clk
	/*...*/

	//modify DDR PHY clock by changing LCPLL0 channel_1 mdiv
	iproc_dbg("CRMU_LCPLL0_CONTROL2=0x%08x\n", iproc_reg32_read(CRMU_LCPLL0_CONTROL2));
	old_div = iproc_reg32_getbits(CRMU_LCPLL0_CONTROL2, CRMU_LCPLL0_CONTROL2__LCPLL0_CH1_MDIV_R, CRMU_LCPLL0_CONTROL2__LCPLL0_CH1_MDIV_WIDTH);
	if(upscaling)
		new_div = (old_div * 200 / 800);
	else
		new_div = (old_div * 800 / 200);

	//disable and hold LCPLL0_CH1
	iproc_reg32_setbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_HOLD_CH_R + ch);
	iproc_reg32_setbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_ENABLEB_CH_R + ch);

	//change mdiv of LCPLL0_CH1
	iproc_reg32_write_masked(CRMU_LCPLL0_CONTROL2, CRMU_LCPLL0_CONTROL2__LCPLL0_CH1_MDIV_R, CRMU_LCPLL0_CONTROL2__LCPLL0_CH1_MDIV_WIDTH, new_div);

	//enable and release LCPLL0_CH1
	iproc_reg32_clrbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_ENABLEB_CH_R + ch);
	iproc_reg32_clrbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_HOLD_CH_R + ch);

	//wait for pll locked & stable
	while(!iproc_reg32_getbit(CRMU_LCPLL0_STATUS, CRMU_LCPLL0_STATUS__LCPLL0_LOCK));

	//Reset control of the post-dividers (not sure needed or not)
//	iproc_reg32_clrbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_POST_RESETB);
//	iproc_reg32_setbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_POST_RESETB);

	udelay(1000);
	iproc_dbg("CRMU_LCPLL0_CONTROL2=0x%08x\n", iproc_reg32_read(CRMU_LCPLL0_CONTROL2));
}

int iproc_change_usb_timings(int upscaling)
{
	int ch=3;
	uint32_t old_div, new_div;

	//USB2.0 controller,phy: 400M-250M, LCPLL0_CH3, CDRU_USBPHY_P0_CTRL_0, CDRU_USBPHY_P1_CTRL_0, CDRU_USBPHY_P2_CTRL_0
	iproc_dbg("CRMU_LCPLL0_CONTROL3=0x%08x\n", iproc_reg32_read(CRMU_LCPLL0_CONTROL3));
	old_div = iproc_reg32_getbits(CRMU_LCPLL0_CONTROL3, CRMU_LCPLL0_CONTROL3__LCPLL0_CH3_MDIV_R, CRMU_LCPLL0_CONTROL3__LCPLL0_CH3_MDIV_WIDTH);
	if(upscaling)
		new_div = (old_div * 250 / 400);
	else
		new_div = (old_div * 400 / 250);

	//disable and hold LCPLL0_CH3
	iproc_reg32_setbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_HOLD_CH_R + ch);
	iproc_reg32_setbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_ENABLEB_CH_R + ch);

	//change mdiv of LCPLL0_CH3
	iproc_reg32_write_masked(CRMU_LCPLL0_CONTROL3, CRMU_LCPLL0_CONTROL3__LCPLL0_CH3_MDIV_R, CRMU_LCPLL0_CONTROL3__LCPLL0_CH3_MDIV_WIDTH, new_div);

	//enable and release LCPLL0_CH3
	iproc_reg32_clrbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_ENABLEB_CH_R + ch);
	iproc_reg32_clrbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_HOLD_CH_R + ch);

	//wait for pll locked & stable
	while(!iproc_reg32_getbit(CRMU_LCPLL0_STATUS, CRMU_LCPLL0_STATUS__LCPLL0_LOCK));

	return 0;
}

int iproc_change_axi_fabric_timings(int upscaling)
{
	//AXI Fabric: 500M-250M, unknow
	return 0;
}


iproc_freq_e iproc_get_a9_freq(int cpu)
{
	u32 mdiv, h_mdiv;
	u32 ndiv, pdiv;
	u32 crystal_clk=25;
	u32 arm_clk=0;
	char freq_id;

	freq_id = iproc_reg32_getbits(IHOST_PROC_CLK_POLICY_FREQ, IHOST_PROC_CLK_POLICY_FREQ__policy0_freq_R,  IHOST_PROC_CLK_POLICY_FREQ__policy0_freq_WIDTH);
	pdiv    = iproc_reg32_getbits(IHOST_PROC_CLK_PLLARMA,     IHOST_PROC_CLK_PLLARMA__pllarm_pdiv_R,       IHOST_PROC_CLK_PLLARMA__pllarm_pdiv_WIDTH); // 0 = divide by 16, 1-15 = divide by 1-15.
	ndiv    = iproc_reg32_getbits(IHOST_PROC_CLK_PLLARMA,     IHOST_PROC_CLK_PLLARMA__pllarm_ndiv_int_R,   IHOST_PROC_CLK_PLLARMA__pllarm_ndiv_int_WIDTH); // 0 = divide by 1024, 1-11: not supported by the PLL, 12-1023 = divide by 12-1023.
	mdiv    = iproc_reg32_getbits(IHOST_PROC_CLK_PLLARMC,     IHOST_PROC_CLK_PLLARMC__pllarm_mdiv_R,       IHOST_PROC_CLK_PLLARMC__pllarm_mdiv_WIDTH);  // 0 = divide by 256, 1-255 = divide by 1-255.
	h_mdiv  = iproc_reg32_getbits(IHOST_PROC_CLK_PLLARMCTRL5, IHOST_PROC_CLK_PLLARMCTRL5__pllarm_h_mdiv_R, IHOST_PROC_CLK_PLLARMCTRL5__pllarm_h_mdiv_WIDTH); // 0 = divide by 256, 1-255 = divide by 1-255.

//	iproc_dbg("cpu=%d, freq_id=%d, pdiv=%d, ndiv=%d, mdiv=%d, h_mdiv=%d, crystal_clk=%d, arm_clk=%d.\n", cpu, freq_id, pdiv, ndiv, mdiv, h_mdiv, crystal_clk, arm_clk);
	
	pdiv   = (pdiv   ? pdiv   : 16);
	ndiv   = (ndiv   ? ndiv   : 1024);
	mdiv   = (mdiv   ? mdiv   : 256);
	h_mdiv = (h_mdiv ? h_mdiv : 256);

	if(freq_id==6)
	{
		arm_clk = ((crystal_clk/pdiv)*ndiv)/mdiv;
	}
	else if(freq_id==7)
	{
		arm_clk = ((crystal_clk/pdiv)*ndiv)/h_mdiv;
	}

	iproc_dbg("cpu=%d, freq_id=%d, pdiv=%d, ndiv=%d, mdiv=%d, h_mdiv=%d, crystal_clk=%d, arm_clk=%d.\n", cpu, freq_id, pdiv, ndiv, mdiv, h_mdiv, crystal_clk, arm_clk);

	switch(arm_clk) {
#ifdef IPROC_SUPPORT_FREQ_200M
		case 200: return IPROC_FREQ_200M;
#endif
#ifdef IPROC_SUPPORT_FREQ_400M
		case 400: return IPROC_FREQ_400M;
#endif
#ifdef IPROC_SUPPORT_FREQ_500M
		case 500: return IPROC_FREQ_500M;
#endif
#ifdef IPROC_SUPPORT_FREQ_1000M
		case 1000: return IPROC_FREQ_1000M;
#endif
#ifdef IPROC_SUPPORT_FREQ_1250M
		case 1250: return IPROC_FREQ_1250M;
#endif
		default:
			iproc_err("Not supported cpu freq: %dMHz.\n", arm_clk);
			return IPROC_FREQ_UNSUPPORTED;
	}
}

/* ARM core freq selecter:
  * channel 0(ARM core Frequency ID=6): pll_clk     = ((crystal_clk / pllarm_pdiv) * pllarm_ndiv ) /pllarm_mdiv;
  * channel 1(ARM core Frequency ID=7): pll_h_clk = ((crystal_clk / pllarm_pdiv) * pllarm_ndiv ) / pllarm_h_mdiv;
  * crystal_clk=25 MHz
  * pllarm_ndiv = pllarm_ndiv_int + (pllarm_ndiv_frac/2^20)
  */
void iproc_set_a9_freq (iproc_freq_e newfreq)
{
	unsigned int i_loop  ;

	if( !(newfreq == IPROC_FREQ_1000M || newfreq == IPROC_FREQ_500M))
	{
		iproc_err("Not supported cpu freq index: %d.\n", newfreq);
		return;
	}
	
	// Before PLL locking change the Frequency ID to 2 'default'  
	iproc_reg32_write(IHOST_PROC_CLK_WR_ACCESS, 0xA5A501);	     // Write KPROC_CLK_MGR_REG_WR_ACCESS = 32'h00A5A501 to enable clk manager access. 
	iproc_reg32_write(IHOST_PROC_CLK_POLICY_FREQ, 0x82020202);  // Select the frequency ID =2 for all policies
	iproc_reg32_write(IHOST_PROC_CLK_POLICY_CTL , (1 << IHOST_PROC_CLK_POLICY_CTL__GO) | ( 1 << IHOST_PROC_CLK_POLICY_CTL__GO_AC));  // Set the GO and GO_AC bit 
	while ((iproc_reg32_read(IHOST_PROC_CLK_POLICY_CTL) & (1 << IHOST_PROC_CLK_POLICY_CTL__GO) ) != 0);   // Wait for Go bit to get clear 
	
	if (newfreq == IPROC_FREQ_1000M)
	{
		iproc_dbg("Set a9 freq to 1000M\n");
		// Reset the PLL and post-divider
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMA , 0x1005000);
		for (i_loop =0 ; i_loop < 5 ; i_loop++) ;	             // Dummy loop for reste propagation
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMCTRL5 , (iproc_reg32_read(IHOST_PROC_CLK_PLLARMCTRL5) & 0xffffff00) | 0x2);  // Set h_mdiv = 0x2
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMC,      (iproc_reg32_read(IHOST_PROC_CLK_PLLARMC) & 0xffffff00) | 0x4);	// Set mdiv = 0x4
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMA ,      0x1005001);// Set ndiv=0x50, pdiv=1, Therefore pll_h_clk= (((crystal_clk/pdiv)* ndiv)/h_mdiv) = 1000
	}
	else if (newfreq == IPROC_FREQ_400M)   // 400 MHz
	{
		iproc_dbg("Set a9 freq to 400M\n");
		//VCO Frequency should be in between 1568.00/4080.00 MHz, set by vco_range = High 
		// Reset the PLL and post-divider
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMA , 1002000);
		for (i_loop =0 ; i_loop < 5 ; i_loop++) ;	             // Dummy loop for reste propagation
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMCTRL5 , (iproc_reg32_read(IHOST_PROC_CLK_PLLARMCTRL5) & 0xffffff00) | 0x4);  // Set h_mdiv = 0x4
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMC,      (iproc_reg32_read(IHOST_PROC_CLK_PLLARMC) & 0xffffff00) | 0x8);	// Set mdiv = 0x8
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMA ,      0x1004001);// Set ndiv=0x40 , pdiv=1. Therefore pll_h_clk= (((crystal_clk/pdiv)* ndiv)/h_mdiv) = 400
	}
	else if (newfreq == IPROC_FREQ_500M)   // 500 MHz, 
	{
		iproc_dbg("Set a9 freq to 500M\n");
		//VCO Frequency should be in between 1568.00/4080.00 MHz, set by vco_range = High 
		// Reset the PLL and post-divider
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMA , 1002000);
		for (i_loop =0 ; i_loop < 5 ; i_loop++) ;	             // Dummy loop for reste propagation
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMCTRL5 , (iproc_reg32_read(IHOST_PROC_CLK_PLLARMCTRL5) & 0xffffff00) | 0x4);  // Set h_mdiv = 4
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMC,      (iproc_reg32_read(IHOST_PROC_CLK_PLLARMC) & 0xffffff00) | 0x8);	// Set mdiv = 8
		iproc_reg32_write(IHOST_PROC_CLK_PLLARMA ,      0x1004001);// Set ndiv=0x50 , pdiv=1. Therefore pll_h_clk= (((crystal_clk/pdiv)* ndiv)/h_mdiv) = 500
	}

	while ( !(iproc_reg32_read(IHOST_PROC_CLK_PLLARMA) & (1 <<IHOST_PROC_CLK_PLLARMA__pllarm_lock)) );  // Wait for PLL lock to be set 
	iproc_reg32_setbit(IHOST_PROC_CLK_PLLARMA,IHOST_PROC_CLK_PLLARMA__pllarm_soft_post_resetb );
	iproc_reg32_write(IHOST_PROC_CLK_POLICY_FREQ, 0x87070707);  // Switch to frequency ID 7 
	iproc_reg32_write(IHOST_PROC_CLK_POLICY_CTL , (1 << IHOST_PROC_CLK_POLICY_CTL__GO) | ( 1 << IHOST_PROC_CLK_POLICY_CTL__GO_AC));  // Set the GO and GO_AC bit 

	while ((iproc_reg32_read(IHOST_PROC_CLK_POLICY_CTL) & (1 << IHOST_PROC_CLK_POLICY_CTL__GO) ) != 0);   // Wait for Go bit to get clear 
	iproc_reg32_write(IHOST_PROC_CLK_CORE0_CLKGATE ,     0x00000301);
	iproc_reg32_write(IHOST_PROC_CLK_CORE1_CLKGATE ,     0x00000301);
	iproc_reg32_write(IHOST_PROC_CLK_ARM_SWITCH_CLKGATE, 0x00000301);
	iproc_reg32_write(IHOST_PROC_CLK_ARM_PERIPH_CLKGATE, 0x00000301);
	iproc_reg32_write(IHOST_PROC_CLK_APB0_CLKGATE ,      0x00000303);

}

///////////////////////////////////////////////////////////////////////////////////////////////
static void iproc_pm_save_user_regs(void)
{
	iproc_pm_save_device_regs(&iproc_pm_dev_common);
}

static void iproc_pm_restore_user_regs(void)
{
	iproc_pm_restore_device_regs(&iproc_pm_dev_common);
}

///////////////////////////////////////////////////////////////////////////////
/*
  * Power on all components and full clock
  */
static int iproc_restore_to_fast_run(void)
{
	iproc_dbg("\n");

	//Power on all components
	iproc_reg32_clrbit (CRMU_ISO_CELL_CONTROL,     CRMU_ISO_CELL_CONTROL__CRMU_ISO_PDSYS_PLL_LOCKED);
	iproc_reg32_clrbit (CRMU_ISO_CELL_CONTROL,     CRMU_ISO_CELL_CONTROL__CRMU_ISO_PDSYS_POWER_GOOD);
	iproc_reg32_clrbit (CRMU_ISO_CELL_CONTROL,     CRMU_ISO_CELL_CONTROL__CRMU_ISO_PDSYS);
	iproc_reg32_clrbits(CRMU_XTAL_CHANNEL_CONTROL, CRMU_XTAL_CHANNEL_CONTROL__CRMU_XTAL_CML_CH_PWR_DOWN_R, CRMU_XTAL_CHANNEL_CONTROL__CRMU_XTAL_CML_CH_PWR_DOWN_WIDTH);
	iproc_reg32_setbits(CRMU_XTAL_CHANNEL_CONTROL, CRMU_XTAL_CHANNEL_CONTROL__CRMU_XTAL_CMOS_CH_EN_R, CRMU_XTAL_CHANNEL_CONTROL__CRMU_XTAL_CMOS_CH_EN_WIDTH);
	iproc_reg32_write  (CRMU_PLL_AON_CTRL,         0x1e1e66);
	iproc_reg32_setbit (CRMU_LDO_CTRL,		        CRMU_LDO_CTRL__PMU_VREGCNTL_EN); //enable ldo program
	iproc_reg32_clrbit (CRMU_RGMII_LDO_CTRL,       CRMU_RGMII_LDO_CTRL__RGMII_LDOPWRDN);//RGMII
	iproc_reg32_clrbit (CRMU_ADC_LDO_CTRL,	        CRMU_ADC_LDO_CTRL__ADC_LDOPWRDN);//ADC
	iproc_reg32_clrbit (CRMU_USB_PHY_AON_CTRL,     CRMU_USB_PHY_AON_CTRL__CRMU_USBPHY_P2_PHY_ISO);
	iproc_reg32_clrbit (CRMU_USB_PHY_AON_CTRL,     CRMU_USB_PHY_AON_CTRL__CRMU_USBPHY_P1_PHY_ISO);
	iproc_reg32_clrbit (CRMU_USB_PHY_AON_CTRL,     CRMU_USB_PHY_AON_CTRL__CRMU_USBPHY_P0_PHY_ISO);
	iproc_dbg("\n");

	//Re-enable clock
	iproc_reg32_write(CRMU_CLOCK_GATE_CTRL, 0xffffffff);
	iproc_reg32_write(CRMU_CLOCK_GATE_CONTROL, 0xffffffff);
	iproc_reg32_write(ASIU_TOP_CLK_GATING_CTRL, 0xffffffff);
	iproc_dbg("\n");
	
	//Enable GENPLL & LCPLL0 channels
	iproc_reg32_clrbits(CRMU_GENPLL_CONTROL1, CRMU_GENPLL_CONTROL1__GENPLL_ENABLEB_CH_R, CRMU_GENPLL_CONTROL1__GENPLL_ENABLEB_CH_WIDTH);//enable channels
	iproc_reg32_clrbits(CRMU_GENPLL_CONTROL1, CRMU_GENPLL_CONTROL1__GENPLL_BYP_EN_CH_R, CRMU_GENPLL_CONTROL1__GENPLL_BYP_EN_CH_WIDTH);//disable bypass channels
	iproc_reg32_setbit (CRMU_GENPLL_CONTROL4, CRMU_GENPLL_CONTROL4__GENPLL_SEL_SW_SETTING);//apply
	iproc_reg32_clrbits(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_ENABLEB_CH_R, CRMU_LCPLL0_CONTROL0__LCPLL0_ENABLEB_CH_WIDTH);
	iproc_reg32_clrbits(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_BYP_EN_CH_R, CRMU_LCPLL0_CONTROL0__LCPLL0_BYP_EN_CH_WIDTH);
	iproc_reg32_setbit (CRMU_LCPLL0_CONTROL1, CRMU_LCPLL0_CONTROL1__LCPLL_SEL_SW_SETTING);//apply
	iproc_dbg("\n");
	
	//ARM PLL on
	iproc_reg32_clrbit(IHOST_PROC_CLK_PLLARMA, IHOST_PROC_CLK_PLLARMA__pllarm_idle_pwrdwn_sw_ovrride);
	iproc_reg32_clrbit(IHOST_PROC_CLK_PLLARMA, IHOST_PROC_CLK_PLLARMA__pllarm_pwrdwn);
	iproc_reg32_setbit(IHOST_PROC_CLK_PLLARMA, IHOST_PROC_CLK_PLLARMA__pllarm_soft_resetb);
	iproc_dbg("\n");
	
	//Clear pwrctlo reg
	iproc_set_PWRCTRLO_state(IPROC_PM_STATE_RUN);
	iproc_dbg("\n");
	
	//Set A9 to 1GHz
	if(iproc_get_a9_freq(0)!=IPROC_FREQ_1000M)
		iproc_set_a9_freq(IPROC_FREQ_1000M);
	
	return 0;
}

/////////////////////////////////////////////////////////////////////////////////
/*
  * This is the last step to enter new power mode
  */
static int iproc_prepare_enter_target_pm_mode(unsigned long state)
{
	if(state==IPROC_PM_STATE_RUN)
		return 0;

	if(iproc_sync_m0_isr_args_to_idram()<0)//update the spcific M0 ISR args
		return -1;

	if(iproc_set_wakeup_IRQs_ISRs(IPROC_ENABLED)<0)
		return -1;

	iproc_set_powersave_IRQ_ISR(IPROC_ENABLED);
	
	iproc_set_ihost_wakeup_irqs(IPROC_DISABLED);

	iproc_set_mcu_access_control(1);

	iproc_set_aon_gpio_led(IPROC_AON_GPIO_LED_ALL_OFF);
	
	iproc_set_PWRCTRLO_state(state);

	iproc_set_SCU_status(state);

	iproc_reg32_clrbit(CRU_status, CRU_status__detected_wfi);

	dsb();
	isb();
	
	/*The following codes can ONLY be executed by STANDBY*/
//	iproc_prt("~~~~1st step wakeup from %s!\n", iproc_get_pm_state_str_by_id(state) );

	return 0;
}

void iproc_dump_resume_entry(void)
{
	uint32_t reg_phy_addr;
	uint32_t *addr;

	reg_phy_addr = (uint32_t)iproc_pm_cpu_resume;
	addr=(uint32_t *)reg_phy_addr;
	iproc_dbg("iproc_pm_cpu_resume() vaddr = 0x%08x\n", reg_phy_addr);
	iproc_dbg("iproc_pm_cpu_resume() paddr = 0x%08x\n", virt_to_phys((void *)reg_phy_addr));
	iproc_dbg("iproc_pm_cpu_resume() code  = 0x%08x, 0x%08x, 0x%08x\n", *addr, *(addr+1), *(addr+2));
}

/*
  * Put the resume entry into sw persistent reg, note that virt_to_phys() can only be used for built-in code, not usable for external module
  */
static void iproc_set_resume_entry(void)
{
	iproc_dbg("Save system resume entry\n");

	//Only valid for built-in module, don't use it in external module
	iproc_reg32_write(CRMU_IHOST_SW_PERSISTENT_REG4, virt_to_phys(iproc_pm_cpu_resume));

	iproc_dbg("CRMU_IHOST_SW_PERSISTENT_REG4 = 0x%08x\n", iproc_reg32_read(CRMU_IHOST_SW_PERSISTENT_REG4));

	iproc_dump_resume_entry();
}

///////////////////////////////////////////////////////////////////////////////////////
static int iproc_do_msr_policy(iproc_part_clock_e clk, iproc_part_power_e pwr)
{
	unsigned int regval;
	char *name="MSR";
		
	iproc_dbg("Policy %s: clk=%d, pwr=%d.\n", name, clk, pwr);
	//if power off, then no need to check clock.
	if(pwr==IPROC_POWER_OFF)
	{
		iproc_dbg("Power off %s!\n", name);
		return 0;
	}
	else if(pwr==IPROC_POWER_ON)
	{
		iproc_dbg("Power on %s!\n", name);
	}

	if(clk==IPROC_CLOCK_OFF)
	{
		iproc_dbg("Clock gate %s!(TBD...)\n", name);
	}
	else if(clk==IPROC_CLOCK_ON)
	{
		iproc_dbg("Clock on %s!(TBD...)\n", name);
	}

	return 0;
}


static int iproc_do_sci_policy(iproc_part_clock_e clk, iproc_part_power_e pwr)
{
	unsigned int regval;
	char *name="SCI";
		
	iproc_dbg("Policy %s: clk=%d, pwr=%d.\n", name, clk, pwr);
	//if power off, then no need to check clock.
	if(pwr==IPROC_POWER_OFF)
	{
		iproc_dbg("Power off %s!\n", name);
		return 0;
	}
	else if(pwr==IPROC_POWER_ON)
	{
		iproc_dbg("Power on %s!\n", name);
	}

	if(clk==IPROC_CLOCK_OFF)
	{
		iproc_dbg("Clock gate %s!(TBD...)\n", name);
	}
	else if(clk==IPROC_CLOCK_ON)
	{
		iproc_dbg("Clock on %s!(TBD...)\n", name);
	}

	return 0;
}

static int iproc_do_neon_policy(iproc_part_clock_e clk, iproc_part_power_e pwr)
{
	unsigned int regval;
	char *name="NEON";
		
	iproc_dbg("Policy %s: clk=%d, pwr=%d.\n", name, clk, pwr);
	
	//if power off, then no need to check clock.
	if(pwr==IPROC_POWER_OFF)
	{
		iproc_dbg("Power off %s!\n", name);
		/*	
		1.	CRU_CONTROL.IHOST_NEON0_HW_RESET_N = 0x0
		2.	CRU_IHOST_PWRDWN_EN.LOGIC_CLAMP_ON_NEON0 = 0x1
		3.	Wait 0.5 us 1
		4.	CRU_IHOST_PWRDWN_EN.LOGIC_PWROKIN_NEON0 = 0x0
		5.	Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWROKOUT_NEON0 == 0x0
		6.	Wait 0.5 us
		7.	CRU_IHOST_PWRDWN_EN.LOGIC_PWRONIN_NEON0 = 0x0
		8.	Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWRONOUT_NEON0 == 0x0
		*/
		iproc_reg32_clrbit(CRU_control, CRU_control__ihost_neon0_hw_reset_n);
		iproc_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_clamp_on_neon0);
		udelay(1);
		iproc_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwrokin_neon0);
		do{
			regval = iproc_reg32_getbit(CRU_ihost_pwrdwn_status, CRU_ihost_pwrdwn_status__logic_pwrokout_neon0);
		}while(regval==1);
		udelay(1);
		iproc_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwronin_neon0);
		do{
			regval = iproc_reg32_getbit(CRU_ihost_pwrdwn_status, CRU_ihost_pwrdwn_status__logic_pwronout_neon0);
		}while(regval==1);
		
		return 0;
	}
	else if(pwr==IPROC_POWER_ON)
	{
		iproc_dbg("Power on %s!\n", name);
		/*
		1.	CRU_CONTROL.IHOST_NEON0_HW_RESET_N = 0x0
		2.	CRU_IHOST_PWRDWN_EN.LOGIC_PWRONIN_NEON0 = 0x1
		3.	Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWRONOUT_NEON0 == 0x1
		4.	Wait 0.5 us
		5.	CRU_IHOST_PWRDWN_EN.LOGIC_PWROKIN_NEON0 = 0x1
		6.	Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWROKOUT_NEON0 == 0x1
		7.	Wait 0.5 us
		8.	CRU_IHOST_PWRDWN_EN.LOGIC_CLAMP_ON_NEON0 = 0x0
		9.	Wait 0.5 us
		10. CRU_CONTROL.IHOST_NEON0_HW_RESET_N = 0x1
		 */
		iproc_reg32_clrbit(CRU_control, CRU_control__ihost_neon0_hw_reset_n);
		iproc_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwronin_neon0);
		do{
			regval = iproc_reg32_getbit(CRU_ihost_pwrdwn_status, CRU_ihost_pwrdwn_status__logic_pwronout_neon0);
		}while(regval==0);
		udelay(1);
		iproc_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwrokin_neon0);
		do{
			regval = iproc_reg32_getbit(CRU_ihost_pwrdwn_status, CRU_ihost_pwrdwn_status__logic_pwrokout_neon0);
		}while(regval==0);
		udelay(1);
		iproc_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_clamp_on_neon0);
		udelay(1);
		iproc_reg32_setbit(CRU_control, CRU_control__ihost_neon0_hw_reset_n);
	}
	
	if(clk==IPROC_CLOCK_OFF)
	{
		iproc_dbg("Clock gate %s!(TBD...)\n", name);
	}
	else if(clk==IPROC_CLOCK_ON)
	{
		iproc_dbg("Clock on %s!(TBD...)\n", name);
	}
	
	return 0;
}

static int iproc_do_core_policy(iproc_part_clock_e clk, iproc_part_power_e pwr)
{
	unsigned int regval;
	char *name="CORE";
	
	iproc_dbg("Policy %s: clk=%d, pwr=%d.\n", name, clk, pwr);

	//if power off, then no need to check clock.
	if(pwr==IPROC_POWER_OFF)
	{
		iproc_dbg("Power off %s!\n", name);
		/*	
		Option 1 - Self power down
		1.	Power down Neon0
		2.	CRU_CPU0_POWERDOWN.WAIT_FOR_WFI = 0x1 (This is the default value)
		3.	CRU_CPU0_POWERDOWN.START_CPU0_POWERDOWN_SEQ = 0x1
		4.	Execute WFI
		5.	When CPU enter WFI state STANDBYWFI signal will go high and then CRU FSM will start powerdown sequence
		6.	CRU_CPU0_POWERDOWN.START_CPU0_POWERDOWN_SEQ will be cleared automatically at the end of powerdown sequence
		*/
		iproc_do_neon_policy(IPROC_CLOCK_END, IPROC_POWER_OFF);
		iproc_reg32_setbit(CRU_cpu0_powerdown, CRU_cpu0_powerdown__wait_for_wfi);
		iproc_reg32_setbit(CRU_cpu0_powerdown, CRU_cpu0_powerdown__start_cpu0_powerdown_seq);
		iproc_exec_wfi();
		
		return 0;
	}
	else if(pwr==IPROC_POWER_ON)
	{
		/*	
		Option1 
		Issue iProc reset. When iproc_rst_l is released, CRU FSM will bring CPU0 into power-up state. 
		
		Option2
		Software can set CRU_CPU0_POWERUP.START_CPU0_POWERUP_SEQ register bit to start CPU0 powerup sequence. When FSM completes CPU powerup sequence, it will clear this register bit.
		*/
		iproc_dbg("Power on %s!\n", name);
		iproc_reg32_setbit(CRU_cpu0_powerup, CRU_cpu0_powerup__start_cpu0_powerup_seq);
	}
	
	if(clk==IPROC_CLOCK_OFF)
	{
		iproc_dbg("Clock gate %s!(TBD...)\n", name);
	}
	else if(clk==IPROC_CLOCK_ON)
	{
		iproc_dbg("Clock on %s!(TBD...)\n", name);
	}
	
	return 0;
}

static int iproc_do_usb_policy(iproc_part_clock_e clk, iproc_part_power_e pwr)
{
	unsigned int regval;
	char *name="USB";
		
	iproc_dbg("Policy %s: clk=%d, pwr=%d.\n", name, clk, pwr);
	//if power off, then no need to check clock.
	if(pwr==IPROC_POWER_OFF)
	{
		iproc_dbg("Power off %s!\n", name);
		return 0;
	}
	else if(pwr==IPROC_POWER_ON)
	{
		iproc_dbg("Power on %s!\n", name);
	}

	if(clk==IPROC_CLOCK_OFF)
	{
		iproc_dbg("Clock gate %s!(TBD...)\n", name);
	}
	else if(clk==IPROC_CLOCK_ON)
	{
		iproc_dbg("Clock on %s!(TBD...)\n", name);
	}

	return 0;
}


/////////////////////////////////////////////////////////////////////////////////
static int iproc_soc_do_pm_policies(iproc_power_status_e pm)
{
	int i, ret;
	iproc_soc_pid_e pid;
	iproc_part_clock_e clk;
	iproc_part_power_e pwr;
	iproc_part_policy_t *part_policy=NULL;

	//the order is important!!!
	iproc_soc_pid_e parts[]={SOC_PID_NEON, SOC_PID_SCI};
	iproc_part_policy_t iproc_soc_parts_policy[SOC_PID_END]={
		[SOC_PID_SCI]	= {SOC_PID_SCI,  "sci",  iproc_do_sci_policy },
		[SOC_PID_USB]	= {SOC_PID_SCI,  "usb",  iproc_do_usb_policy },
		[SOC_PID_MSR]	= {SOC_PID_SCI,  "msr",  iproc_do_msr_policy },
		[SOC_PID_NEON]	= {SOC_PID_NEON, "neon", iproc_do_neon_policy},
	};

	for(i=0; i<ARRAY_SIZE(parts); i++)
	{
		pid = parts[i];
		part_policy = &iproc_soc_parts_policy[pid];

		if(pid != part_policy->pid)
		{
			iproc_err("Invalid parts policy info: i=%d, pid=%d, policy->pid=%d!\n", i, pid, part_policy->pid);
			return -EINVAL;
		}

		if(!part_policy->policy)
		{
			iproc_dbg("No policy callback for %s!\n", part_policy->name);
			continue;
		}
		
		clk = iproc_get_policy_attr(IPROC_PM_POLICY_CLK, pm, pid);
		pwr = iproc_get_policy_attr(IPROC_PM_POLICY_PWR, pm, pid);

		ret = part_policy->policy(clk, pwr);
		if(ret)
		{
			iproc_err("Failed to do policy callback for %s, clk=%d, pwr=%d, ret=%d!\n", part_policy->name, clk, pwr, ret);
			return ret;
		}
		
		iproc_dbg("Success to do policy callback for %s, clk=%d, pwr=%d!\n", part_policy->name, clk, pwr);
	}

	return 0;
}

/*common operation after wakeup*/
static void iproc_pm_common_task_after_wakeup(void)
{
#ifndef CONFIG_CPU_DCACHE_DISABLE
	iproc_dbg("Re-enable L1 D cache\n");
	iproc_pm_enable_L1_D_cache();
#endif

	iproc_set_SCU_status(IPROC_PM_STATE_RUN);

	iproc_set_ihost_wakeup_irqs(IPROC_ENABLED);

	iproc_set_system_timer(IPROC_ENABLED);

	iproc_set_mcu_access_control(0);
}

static int __iproc_enter_standby(void)
{
	iproc_dbg("Go to standby\n");
	iproc_prepare_enter_target_pm_mode(IPROC_PM_STATE_STANDBY);

	iproc_pm_finish_switch(IPROC_PM_STATE_STANDBY);

//	iproc_send_mailbox_interrupt_to_mcu(IPROC_PM_STATE_STANDBY);
//	iproc_exec_wfi();//zZZ...
	
	iproc_pm_common_task_after_wakeup();

	return 0;
}

//Linux Suspend to Standby
static int __iproc_enter_sleep(void)
{
	iproc_set_resume_entry();
	
	iproc_pm_save_user_regs();

	local_flush_tlb_all();
	flush_cache_all();

	//Save core regs, then suspending, wakeup in the __cpu_suspend()
	iproc_prepare_enter_target_pm_mode(IPROC_PM_STATE_SLEEP);

	iproc_dbg("Go sleeping\n");
	dsb();
	isb();
	iproc_arm_cpu_suspend(IPROC_PM_STATE_SLEEP, iproc_pm_finish_switch);

	iproc_pm_common_task_after_wakeup();

	iproc_pm_restore_user_regs();

	return 0;
}

//Linux Suspend to Ram
static int __iproc_enter_deepsleep(void)
{
	iproc_set_resume_entry();

	iproc_pm_save_user_regs();//Save user defined regs;

//	iproc_dump_ddr_regs();

	iproc_set_system_timer(IPROC_DISABLED);

	local_irq_disable();

	local_flush_tlb_all();
//	flush_cache_all();		 //cpu_suspend() also do this

	iproc_pm_set_a9_sram(1);

	//Save core regs, then suspending, wakeup in the __cpu_suspend()
	iproc_prepare_enter_target_pm_mode(IPROC_PM_STATE_DEEPSLEEP);

	iproc_dbg("Go deep sleeping\n");

	iproc_pm_flush_disable_l1_caches(); 

	iproc_arm_cpu_suspend(IPROC_PM_STATE_DEEPSLEEP, iproc_pm_finish_switch);

	iproc_pm_common_task_after_wakeup();
	iproc_pm_set_a9_sram(0);
	iproc_pm_restore_user_regs();

	iproc_reg32_write(CRMU_IHOST_POR_WAKEUP_FLAG, 0x0);
	iproc_dbg("IHOST POR WAKEUP FLAG cleared\n");

	return 0;
}

static int iproc_soc_enter_run(void)
{
	iproc_dbg("Entering %s\n", iproc_get_pm_state_str_by_id(IPROC_PM_STATE_RUN));

	iproc_set_current_pm_state(IPROC_PM_STATE_RUN);

	//set A9 to fast run
	iproc_restore_to_fast_run();

    //mask M0 mbox & wakeup IRQs, set user handler jump to NULL
	iproc_set_wakeup_IRQs_ISRs(IPROC_DISABLED);
	iproc_set_powersave_IRQ_ISR(IPROC_DISABLED);

	return 0;
}

static int iproc_soc_enter_standby(void)
{
	iproc_dbg("Entering %s\n", iproc_get_pm_state_str_by_id(IPROC_PM_STATE_STANDBY));

	iproc_set_current_pm_state(IPROC_PM_STATE_STANDBY);

	__iproc_enter_standby();

	return 0;
}

int iproc_soc_enter_sleep(void)
{
	iproc_dbg("Entering %s\n", iproc_get_pm_state_str_by_id(IPROC_PM_STATE_SLEEP));

	iproc_set_current_pm_state(IPROC_PM_STATE_SLEEP);

	__iproc_enter_sleep();

	return 0;
}

int iproc_soc_enter_deepsleep(void)
{
	iproc_dbg("Entering %s\n", iproc_get_pm_state_str_by_id(IPROC_PM_STATE_DEEPSLEEP));

	iproc_set_current_pm_state(IPROC_PM_STATE_DEEPSLEEP);
	
	__iproc_enter_deepsleep();

	return 0;
}

/*
 * Called by "echo new_state > /sys/devices/platform/PM/pm_state"
 */
int iproc_pm_state_switchover(iproc_power_status_e new_state)
{
	iproc_power_status_e old_state = iproc_get_current_pm_state();

	iproc_dbg("Change PM state: %s -> %s.\n", iproc_get_pm_state_str_by_id(old_state), iproc_get_pm_state_str_by_id(new_state));
	
	if(new_state == old_state)
	{
		iproc_dbg("PM state no change.\n");
		return 0;
	}
	
	if(iproc_soc_do_pm_policies(new_state))
	{
		iproc_err("Failed to do PM state policies.\n");
		return -EAGAIN;
	}

	switch(new_state)
	{
		case IPROC_PM_STATE_END:
			break;
		case IPROC_PM_STATE_RUN:
			iproc_soc_enter_run();
			break;
		case IPROC_PM_STATE_STANDBY:  
			iproc_soc_enter_standby();
			break;
		case IPROC_PM_STATE_SLEEP:
			/* Equal to 'echo standby > /sys/power/state', it calls kernel/power/main.c: state_store()  */
		 	/* This finally calls iproc_pm_enter() */
			pm_suspend(PM_SUSPEND_STANDBY);
			break;
		case IPROC_PM_STATE_DEEPSLEEP:
			/*Equal to 'echo mem > /sys/power/state' */
			pm_suspend(PM_SUSPEND_MEM);
			break;
		default:
			iproc_err("Not supported new PM state: %d!\n", new_state);
			return -EINVAL;
	}

	/*Back to fast run for all cases after wakeup*/
	if(iproc_get_current_pm_state()!=IPROC_PM_STATE_RUN)
	{
		iproc_dbg("\n\n");
		iproc_dbg("<--- SOC BACK TO RUN --->\n\n");
		iproc_pm_state_switchover(IPROC_PM_STATE_RUN);
	}
	
	return 0;
}

 
