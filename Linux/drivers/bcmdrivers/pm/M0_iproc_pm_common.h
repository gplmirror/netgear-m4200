/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef _M0_IPROC_COMMON_H
#define _M0_IPROC_COMMON_H

#define iproc_bit_mask(n)              ((unsigned int)1<<n)

#define MAILBOX_CODE0_RUN               0x000a1000
#define MAILBOX_CODE1_RUN               0xffffffff
#define MAILBOX_CODE0_STANDBY           0x000a1001
#define MAILBOX_CODE1_STANDBY           0xffffffff
#define MAILBOX_CODE0_SLEEP             0x000a1002
#define MAILBOX_CODE1_SLEEP             0xffffffff
#define MAILBOX_CODE0_DEEPSLEEP         0x000a1003
#define MAILBOX_CODE1_DEEPSLEEP         0xffffffff

#define MAILBOX_CODE0_iProcSoftReset    0x00000000
#define MAILBOX_CODE1_iProcSoftReset    0xffffffff

typedef enum IPROC_MCU_IRQ_EVENT_NUM {
	MCU_DEC_ERR_INTR=0,
	MCU_AON_UART_INTR,
	MCU_AON_GPIO_INTR,//2
	MCU_AVS_MONITOR_INTR,//3
	MCU_AVS_VDDC_MON_WARN0_INTR,//4
	MCU_AVS_VDDC_MON_WARN1_INTR,//5
	MCU_AVS_TEMP_RESET_INTR,//6
	MCU_TIMER_INTR,//7
	MCU_WDOG_INTR,//8
	MCU_ERROR_LOG_INTR,//9
	MCU_POWER_LOG_INTR,//10
	MCU_RESET_LOG_INTR,//11
	MCU_VOLTAGE_GLITCH_INTR,//12
	MCU_CLK_GLITCH_INTR,//13
	MCU_SECURITY_INTR,//14
	MCU_ASIU_GPIO_EVENT,//15
	MCU_ASIU_SGPIO_EVENT,//16
	MCU_IPROC_GPIO_EVENT,//17
	MCU_IPROC_SGPIO_EVENT,//18
	MCU_IPROC_SWDOG_RESET_EVENT,//19
	MCU_IPROC_WDOG_RESET_EVENT,//20
	MCU_IPROC_IHOST_WDOG_RESET_EVENT,//21
	MCU_SPL_WDOG_EVENT,//22
	MCU_SPL_RST_EVENT,//23
	MCU_SPL_PVT_EVENT,//24
	MCU_SPL_FREQ_EVENT,//25
	MCU_SPRU_ALARM_EVENT,//26
	MCU_SPRU_RTC_EVENT,//27
	MCU_SPRU_RTC_PERIODIC_EVENT,//28
	MCU_IPROC_STANDBYWFE_EVENT,//29
	MCU_IPROC_STANDBYWFI_EVENT,//30
	MCU_MAILBOX_EVENT,//31
	MCU_INTR_EVENT_END,
}IPROC_MCU_IRQ_EVENT_NUM_e;

#define MCU_WAKEUP_SOURCE (iproc_bit_mask(MCU_SPRU_RTC_EVENT) | iproc_bit_mask(MCU_WDOG_INTR) | iproc_bit_mask(MCU_TIMER_INTR) | iproc_bit_mask(MCU_AON_GPIO_INTR))

typedef enum iproc_gpio_status{
	IPROC_AON_GPIO_LED_A=0,
	IPROC_AON_GPIO_LED_B=1,
	IPROC_AON_GPIO_LED_C=2,
	IPROC_AON_GPIO_LED_D=3,
	IPROC_AON_GPIO_LED_E=4,
	IPROC_AON_GPIO_LED_F=5,
	IPROC_AON_GPIO_LED_SINGLE_END,
	IPROC_AON_GPIO_LED_ALL_OFF,
	IPROC_AON_GPIO_LED_ALL_ON,
}iproc_gpio_status_e;


typedef enum iproc_power_status {
	IPROC_PM_STATE_RUN=0,
	IPROC_PM_STATE_STANDBY,
	IPROC_PM_STATE_SLEEP,
	IPROC_PM_STATE_DEEPSLEEP,
	IPROC_PM_STATE_END
}iproc_power_status_e;

typedef struct iproc_policy_word {
	union {
		unsigned int data;
		struct {
			unsigned char core:1;
			unsigned char neon:1;
			unsigned char cache:1; //L2Cache
			unsigned char ddr:1;
			unsigned char sci:1;
			unsigned char dte:1;
			unsigned char msr:1;
			unsigned char sd:1; //sdio
			unsigned char et:1; //ethernet GMAC
			unsigned char usb:1; // 3 usb controller & phy
			unsigned char adc:1; //touch screen
			unsigned char gphy:1; //ethernet phy
			unsigned char pwm:1;
			unsigned char i2c:1; //tdm
			unsigned char spi:1;
			unsigned char uart:1;
			unsigned char d1w:1;
			unsigned char lcd:1; //sram interface
			unsigned char mipi:1;
			unsigned char cam:1; //camer
			unsigned char sflash:1; //s-flash interface
		}s;
	}u;
}iproc_policy_word_t;

typedef struct IPROC_M0_ISR_ARGS {
	iproc_power_status_e iproc_pm_state;
	iproc_policy_word_t  iproc_clock_policies[IPROC_PM_STATE_END];
	iproc_policy_word_t  iproc_power_policies[IPROC_PM_STATE_END];
}IPROC_M0_ISR_ARGS_t;

#endif //_M0_IPROC_COMMON_H
