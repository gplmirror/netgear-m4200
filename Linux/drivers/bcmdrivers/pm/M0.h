/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef _M0_H
#define _M0_H

#include "M0_iproc_pm_common.h"

#define uint8_t  unsigned char
#define uint16_t unsigned short
#define uint32_t unsigned int
#define uint64_t unsigned long long

#define BIT8     0x1
#define BIT16    0x2
#define BIT32    0x4
#define BIT64    0x8

#define MCU_LED_DEBUG                 0
#define IPROC_MCU_AON_GPIO_WAKEUP_SRC IPROC_AON_GPIO_LED_A

///////////////////////////////////////////////////////////////////////////////////
#define cpu_reg32_rd(addr)         (*((volatile uint32_t*)(addr)))
#define cpu_reg32_wr(addr, data)   *((volatile uint32_t *)(addr)) = (uint32_t) (data)

#define cpu_rd_single(addr,size) *((volatile uint32_t*)addr)
#define cpu_wr_single(addr,data,size) \
	do{ \
		if(size == BIT8)         *((volatile uint8_t  *)(addr)) = (uint8_t)  (data); \
		else if(size == BIT16)   *((volatile uint16_t *)(addr)) = (uint16_t) (data); \
		else if(size == BIT64)   *((volatile uint64_t *)(addr)) = (uint64_t) (data); \
		else                     *((volatile uint32_t *)(addr)) = (uint32_t) (data); \
	}while(0)

/////////////////////////////////////////////////////////////////////////////////////
#define  PLL_PWRON_WAIT       0x50
#define  PLL_CONFIG_WAIT      0x50
#define  PWRDN_WAIT           0x50

#define  PMU_TIMEOUT          0x200
#define  PMU_TIMEOUT_2        0x3FFFFFFF
#define  POWER_GOOD_TIMEOUT   0x200
#define  POWER_GOOD_TIMEOUT_2 0x3FFFFFFF
#define  PLL_TIMEOUT          0x6000
#define  PLL_TIMEOUT_2        0x3FFFFFFF

#define  iHOST_PWR_TIMEOUT    0x20
#define  DFT_BISR_TIMEOUT     0x6000
#define  DFT_BISR_TIMEOUT_2   0xFFFFFFFF



/////////////////////////////////////////////////////////////////////////////////////////////

uint32_t cpu_reg32_getbit(uint32_t addr, uint32_t bitno);
void cpu_reg32_clrbit(uint32_t addr, uint32_t bitno);
void cpu_reg32_setbit(uint32_t addr, uint32_t bitno);
void cpu_reg32_wr_mask(uint32_t addr, uint32_t rightbit, uint32_t len, uint32_t value);
void combo_board_leds(int a, int b, int c, int d, int e, int f);
void combo_board_led(int num);
void MCU_die(void);
void MCU_delay(unsigned int cnt);
void MCU_timer_delay(unsigned int time);
void MCU_wdog_delay(unsigned int time);
void MCU_set_wakeup_aon_gpio_output(int out);
void MCU_led_print_hex(unsigned int num);
void MCU_led_on(iproc_gpio_status_e led);
void MCU_led_race(unsigned int cnt);
void MCU_set_pdsys_master_clock_gating(int en);
void MCU_set_ddr_self_refresh_mode(int en);
int MCU_get_ultra_low_power_mode(void);
void MCU_set_ultra_low_power_mode(int en);
void MCU_set_crmu_clk_gating(int en);
void MCU_set_ADC_LDO_off(int en);
void MCU_set_RGMII_LDO_off(int en);



#endif//_M0_H
