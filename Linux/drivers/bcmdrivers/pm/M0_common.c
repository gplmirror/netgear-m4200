/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include "socregs.h"
#include "M0.h"

///////////////////////////////////////////////////////////////////////////////
uint32_t cpu_reg32_getbit(uint32_t addr, uint32_t bitno)
{
	uint32_t regval = cpu_reg32_rd(addr);
	
	return (regval>>bitno)&0x1?1:0;
}
void cpu_reg32_clrbit(uint32_t addr, uint32_t bitno)
{
	uint32_t regval = cpu_reg32_rd(addr);

	regval &= (~(0x00000001<<bitno));
	
	cpu_reg32_wr(addr, regval);
}
void cpu_reg32_setbit(uint32_t addr, uint32_t bitno)
{
	uint32_t regval = cpu_reg32_rd(addr);
	
	regval |= (0x00000001<<bitno);

	cpu_reg32_wr(addr, regval);
}

void cpu_reg32_wr_mask(uint32_t addr, uint32_t rightbit, uint32_t len, uint32_t value)
{
	uint32_t mask = (1<<len)-1;
	uint32_t mask_shift = mask<<rightbit;
	uint32_t curvalue = cpu_reg32_rd(addr);
	uint32_t tmpvalue = curvalue & (~mask_shift);
	uint32_t newvalue = tmpvalue | ((value&mask)<<rightbit);

	cpu_reg32_wr(addr, newvalue);
}

///////////////////////////////////////////////////////////////////////////////////////
void MCU_delay(unsigned int cnt)
{
	unsigned int count = cnt ? cnt : 200000;

	do{}while(count--);
}

void MCU_die(void)
{
	int temp=1;

	do{}while(temp);
}

void MCU_timer_delay(unsigned int time)
{
	cpu_reg32_wr(CRMU_TIM_TIMER1Control, 0x80);  // Only enable crmu timer
	cpu_reg32_wr(CRMU_TIM_TIMER1IntClr, 0x1);	 // Interrupt clear
	cpu_reg32_wr(CRMU_TIM_TIMER1Load, time);     // Unit is in 40ns
	while(!cpu_reg32_rd(CRMU_TIM_TIMER1RIS));	 // Wait for timeout
	cpu_reg32_wr(CRMU_TIM_TIMER1Control, 0x0);	 // Disable timer
}

void MCU_wdog_delay(unsigned int time)
{
	cpu_reg32_setbit(CRMU_WDT_WDOGCONTROL, CRMU_WDT_WDOGCONTROL__ITEN);//Enable intr
    cpu_reg32_wr(CRMU_WDT_WDOGINTCLR, 0x1); // Interrupt clear 
    cpu_reg32_wr(CRMU_WDT_WDOGLOAD, time);  // Unit is in 40ns
    while(!cpu_reg32_rd(CRMU_WDT_WDOGRIS));// Wait for raw interrupt
}

///////////////////////////////////////////////////////////////////////////////////////
void MCU_set_wakeup_aon_gpio_output(int out)
{
	cpu_reg32_wr(CRMU_IOMUX_CONTROL, (cpu_reg32_rd(CRMU_IOMUX_CONTROL)|1));

	if(out)//output
		cpu_reg32_setbit(GP_OUT_EN, IPROC_MCU_AON_GPIO_WAKEUP_SRC);
	else//input
		cpu_reg32_clrbit(GP_OUT_EN, IPROC_MCU_AON_GPIO_WAKEUP_SRC);	
}

/* Basic LED control routine
  COMBO board leds: 1=on, 0=off
  Left  Right
     c     f
     b    e
     a    d
 */
void combo_board_leds(int a, int b, int c, int d, int e, int f)
{
#if (MCU_LED_DEBUG==1)
	uint32_t val = ((a<<0)|(b<<1)|(c<<2)|(d<<3)|(e<<4)|(f<<5));
	int delay=0x50000;

	if(MCU_get_ultra_low_power_mode())
		delay = 0x300;

//	cpu_reg32_wr(CRMU_IOMUX_CONTROL, (cpu_reg32_rd(CRMU_IOMUX_CONTROL)|1));

//	cpu_reg32_wr(GP_INT_CLR, 0x3f);
//	cpu_reg32_wr(GP_INT_MSK, 0x00);

//	cpu_reg32_wr(GP_OUT_EN, 0x3f);
//	cpu_reg32_wr(GP_OUT_EN, (0x3f&(~iproc_bit_mask(IPROC_MCU_AON_GPIO_WAKEUP_SRC)))); 

//	cpu_reg32_wr(GP_DATA_OUT, 0x3f);
	cpu_reg32_wr(GP_DATA_OUT, ~val);

//	cpu_reg32_wr(GP_INT_CLR, val);//clear aon gpio interrupt caused by this operation

	MCU_delay(delay);
#endif
}

void combo_board_led(int num)
{
#if (MCU_LED_DEBUG==1)
	if(num == IPROC_AON_GPIO_LED_ALL_ON)	
		combo_board_leds(1,1,1,1,1,1);
	else if(num == IPROC_AON_GPIO_LED_ALL_OFF) 
		combo_board_leds(0,0,0,0,0,0);
	else if(num == IPROC_AON_GPIO_LED_A) 
		combo_board_leds(1,0,0,0,0,0);
	else if(num == IPROC_AON_GPIO_LED_B) 
		combo_board_leds(0,1,0,0,0,0);
	else if(num == IPROC_AON_GPIO_LED_C) 
		combo_board_leds(0,0,1,0,0,0);
	else if(num == IPROC_AON_GPIO_LED_D) 
		combo_board_leds(0,0,0,1,0,0);
	else if(num == IPROC_AON_GPIO_LED_E) 
		combo_board_leds(0,0,0,0,1,0);
	else if(num == IPROC_AON_GPIO_LED_F) 
		combo_board_leds(0,0,0,0,0,1);
	else
		combo_board_leds(0,0,0,0,0,0);
#endif
}

//@num: must <0x40
void MCU_led_print_hex(unsigned int num)
{
#if (MCU_LED_DEBUG==1)
	int a=0,b=0,c=0,d=0,e=0,f=0;
	
	a=(num&0x01)?1:0;
	b=(num&0x02)?1:0;
	c=(num&0x04)?1:0;
	d=(num&0x08)?1:0;
	e=(num&0x10)?1:0;
	f=(num&0x20)?1:0;
	
	combo_board_leds(a,b,c,d,e,f);
#endif
}

void MCU_led_on(iproc_gpio_status_e led)
{
#if (MCU_LED_DEBUG==1)
	combo_board_led(led);
#endif
}

void MCU_led_race(unsigned int cnt)
{
#if (MCU_LED_DEBUG==1)
	unsigned int count = cnt ? cnt : 1000;
	int i;

	MCU_led_on(IPROC_AON_GPIO_LED_ALL_OFF);
	
	for(i=0; i<count; i++)
	{
		MCU_led_on(IPROC_AON_GPIO_LED_A);
		MCU_led_on(IPROC_AON_GPIO_LED_B);
		MCU_led_on(IPROC_AON_GPIO_LED_C);
		MCU_led_on(IPROC_AON_GPIO_LED_D);
		MCU_led_on(IPROC_AON_GPIO_LED_E);
		MCU_led_on(IPROC_AON_GPIO_LED_F);
	}

	MCU_led_on(IPROC_AON_GPIO_LED_ALL_OFF);
#endif
}

//////////////////////////////////////////////////////////////////////////
void MCU_set_pdsys_master_clock_gating(int en)
{
	if(en) /*disable master clock for PDSYS clocks*/
		cpu_reg32_wr(CRMU_CLOCK_GATE_CTRL, 0x0);
	else /*enable master clock for PDSYS clocks*/
		cpu_reg32_wr(CRMU_CLOCK_GATE_CTRL, 0xffffffff);
}

void MCU_set_ddr_self_refresh_mode(int en)
{
	if(en) /*SRE: Self Refresh Entry*//*if let A9 do the SRE, this code MUST not be in DDR, because in SR mode, A9 cannot access DDR!*/
	{
		// Enter DDR self refresh mode
		cpu_reg32_wr(DDR_DENALI_CTL_56, 0x0a050505);
		// Assert DFI request from PHY to mask any interaction with MEMC
		cpu_reg32_wr(DDR_PHY_CONTROL_REGS_DFI_CNTRL, 0x3E1);
		
		// ISOLATE Phy interface
		cpu_reg32_wr(CRMU_DDR_PHY_AON_CTRL, (cpu_reg32_rd(CRMU_DDR_PHY_AON_CTRL) | 0x7));

		//cpu_reg32_wr(DDR_PHY_CONTROL_REGS_STANDBY_CONTROL, (cpu_reg32_rd(DDR_PHY_CONTROL_REGS_STANDBY_CONTROL) | 0x80000));
		//cpu_reg32_wr(DDR_S1_IDM_IO_CONTROL_DIRECT, (cpu_reg32_rd(DDR_S1_IDM_IO_CONTROL_DIRECT) | 0x2000));

		// Power on - power ok = 0
		//cpu_reg32_wr(CRMU_DDR_PHY_AON_CTRL, (cpu_reg32_rd(CRMU_DDR_PHY_AON_CTRL) & 0x27));

		// set iHOST flag
		cpu_reg32_wr(CRMU_IHOST_POR_WAKEUP_FLAG, 0x1);
	}
	else /*SRX: Self Refresh Exit*/
	{
		if ( cpu_reg32_rd(CRMU_IHOST_POR_WAKEUP_FLAG) == 0x1 )
		{
			// Enter Self refresh (dummy) , to keep Denali happy
			cpu_reg32_wr(DDR_DENALI_CTL_56, 0x0a050505);
			// Assert DFI request from PHY to mask any interaction with MEMC
			cpu_reg32_wr(DDR_PHY_CONTROL_REGS_DFI_CNTRL, 0x0);
			// Exit Self refresh
			cpu_reg32_wr(DDR_DENALI_CTL_56, 0x09050505);
			
			// Clear iHOST flag
			cpu_reg32_wr(CRMU_IHOST_POR_WAKEUP_FLAG, 0x0);
		}
	}
}

int MCU_get_ultra_low_power_mode(void)
{
	if(cpu_reg32_rd(CRMU_CLOCK_SWITCH_STATUS) == 0x4)
		return 1;//in ULP mode
	else
		return 0;//not in ULP mode
}

void MCU_set_ultra_low_power_mode(int en)
{
	if(en) /*enter ULP mode, switch to BBL 32KHz clock from SPRU, power down the 25MHz XTAL OSC*/
	{
		//Disable ISO of PDBBL & PDSYS_PLL_LOCKED
		cpu_reg32_wr(CRMU_ISO_CELL_CONTROL, cpu_reg32_rd(CRMU_ISO_CELL_CONTROL) & 0xFFFEEFFF);

		cpu_reg32_wr(CRMU_ULTRA_LOW_POWER_CTRL, 0x2);//switch to BBL 32K clock

		while ( cpu_reg32_rd(CRMU_CLOCK_SWITCH_STATUS) != 0x4 );
		while ( cpu_reg32_rd(CRMU_CLOCK_SWITCH_STATUS) != 0x4 );
		
		cpu_reg32_wr (CRMU_XTAL_POWER_DOWN, 0x0);//power off xtal
	}
	else /*exit ULP mode, switch to 25MHz XTAL clock*/
	{
		cpu_reg32_wr(CRMU_ISO_CELL_CONTROL, cpu_reg32_rd(CRMU_ISO_CELL_CONTROL) & 0xFFFEEFFF);

		cpu_reg32_wr (CRMU_XTAL_POWER_DOWN, 0x1);

		MCU_timer_delay(0x400);

		cpu_reg32_wr(CRMU_ULTRA_LOW_POWER_CTRL, 0x1);//switch to xtal 25M clock

		while ( cpu_reg32_rd(CRMU_CLOCK_SWITCH_STATUS) != 0x1 );
		while ( cpu_reg32_rd(CRMU_CLOCK_SWITCH_STATUS) != 0x1 );
	}
}

void MCU_set_crmu_clk_gating(int en)
{
	if(en) /* CRMU CLOCK Gate enable*/
	{
		cpu_reg32_clrbit(CRMU_CLOCK_GATE_CONTROL, CRMU_CLOCK_GATE_CONTROL__CRMU_BSPI_CLK_ENABLE);
		cpu_reg32_clrbit(CRMU_CLOCK_GATE_CONTROL, CRMU_CLOCK_GATE_CONTROL__CRMU_UART_CLK_ENABLE);
		cpu_reg32_clrbit(CRMU_CLOCK_GATE_CONTROL, CRMU_CLOCK_GATE_CONTROL__CRMU_PCIE1_AUX_CLK_ENABLE);
		cpu_reg32_clrbit(CRMU_CLOCK_GATE_CONTROL, CRMU_CLOCK_GATE_CONTROL__CRMU_PCIE0_AUX_CLK_ENABLE);
	}
	else /* CRMU CLOCK Gate disable*/
	{
		cpu_reg32_wr(CRMU_CLOCK_GATE_CONTROL, 0xFFFFFFFF); //No clock gating
	}
}

void MCU_set_ADC_LDO_off(int en)
{
	cpu_reg32_setbit(CRMU_LDO_CTRL, CRMU_LDO_CTRL__PMU_VREGCNTL_EN); //enable ldo program

	if(en) /*Power down ADC LDO*/
		cpu_reg32_setbit(CRMU_ADC_LDO_CTRL, CRMU_ADC_LDO_CTRL__ADC_LDOPWRDN);
	else /*Power on ADC LDO*/
		cpu_reg32_clrbit(CRMU_ADC_LDO_CTRL, CRMU_ADC_LDO_CTRL__ADC_LDOPWRDN);
}

void MCU_set_RGMII_LDO_off(int en)
{
	cpu_reg32_setbit(CRMU_LDO_CTRL, CRMU_LDO_CTRL__PMU_VREGCNTL_EN); //enable ldo program

	if(en) /*Power down RGMII LDO*/
		cpu_reg32_setbit(CRMU_RGMII_LDO_CTRL, CRMU_RGMII_LDO_CTRL__RGMII_LDOPWRDN);
	else /*Power on RGMII LDO*/
		cpu_reg32_clrbit(CRMU_RGMII_LDO_CTRL, CRMU_RGMII_LDO_CTRL__RGMII_LDOPWRDN);
}
