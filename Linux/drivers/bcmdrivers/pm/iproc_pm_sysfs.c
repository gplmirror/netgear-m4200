/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/*
 * SYSFS infrastructure specific Broadcom SoCs
 */
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/sysfs.h>

#include "iproc_pm.h"
#include "plat/shm.h"

typedef struct iproc_policy_word_value{
	iproc_soc_pid_e PID;
	int clk[IPROC_PM_STATE_END];
	int pwr[IPROC_PM_STATE_END];
}iproc_policy_word_value_t;

//static iproc_policy_word_t  iproc_policy_word_clock[IPROC_PM_STATE_END];
//static iproc_policy_word_t  iproc_policy_word_power[IPROC_PM_STATE_END];


static iproc_policy_word_value_t iproc_default_policy_words[SOC_PID_END] = {
	//id                                clk in run standby sleep deepsleep    pwr in  run standby sleep deepsleep
	{.PID=SOC_PID_CORE,  .clk={1,  0,  0,  0},         .pwr={1,  1,  0,  0} },
	{.PID=SOC_PID_NEON,  .clk={0,  0,  0,  0},         .pwr={0,  0,  0,  0} },
	{.PID=SOC_PID_CACHE, .clk={1,  1,  0,  0},         .pwr={1,  1,  0,  0} },
	{.PID=SOC_PID_DDR,   .clk={1,  1,  1,  1},         .pwr={1,  1,  1,  1} },
	{.PID=SOC_PID_DTE,   .clk={1,  1,  0,  0},         .pwr={1,  1,  0,  0} },
	{.PID=SOC_PID_MSR,   .clk={1,  1,  0,  0},         .pwr={1,  1,  0,  0} },
	{.PID=SOC_PID_SCI,   .clk={1,  1,  0,  0},         .pwr={1,  1,  0,  0} },
	{.PID=SOC_PID_SD,    .clk={1,  1,  0,  0},         .pwr={1,  1,  0,  0} },
	{.PID=SOC_PID_ET,    .clk={1,  1,  0,  0},         .pwr={1,  1,  0,  0} },
	{.PID=SOC_PID_USB,   .clk={1,  1,  0,  0},         .pwr={1,  1,  0,  0} },
	{.PID=SOC_PID_ADC,   .clk={1,  1,  0,  0},         .pwr={1,  1,  0,  0} },
};

#define IPROC_DEV_ATTR_RW(file) 	DEVICE_ATTR(file, 0644, file##_show, file##_store);
#define IPROC_DEV_ATTR_RO(file) 	DEVICE_ATTR(file, 0444, file##_show, NULL);
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
static ssize_t pm_state_show(struct device *dev, struct device_attribute *attr, char *buf)
{
	int cnt=0;

	cnt = sprintf(buf, "[%s] / %s %s %s %s %s\n", 
		iproc_get_pm_state_str_by_id(iproc_get_current_pm_state()),
		iproc_get_pm_state_str_by_id(IPROC_PM_STATE_RUN),
		iproc_get_pm_state_str_by_id(IPROC_PM_STATE_STANDBY),
		iproc_get_pm_state_str_by_id(IPROC_PM_STATE_SLEEP),
		iproc_get_pm_state_str_by_id(IPROC_PM_STATE_DEEPSLEEP),
		iproc_get_pm_state_str_by_id(IPROC_PM_STATE_END) );
	
	return cnt;
}

static ssize_t pm_state_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t n)
{
	iproc_power_status_e new_state=IPROC_PM_STATE_END;
	int stateid=0;
	char *state_str=NULL;

	for(stateid=0; stateid<=IPROC_PM_STATE_END; stateid++)
	{
		state_str = iproc_get_pm_state_str_by_id(stateid);
		
		if(!strncmp(buf, state_str, strlen(state_str)))
		{
			new_state = stateid;
			break;
		}
	}

	if(stateid>IPROC_PM_STATE_END)
		return -EINVAL;

	iproc_pm_state_switchover(new_state);
	
	return n;
}

/////////////////////////////////////////////////////////////////////////////////////
static iproc_policy_word_t *iproc_get_policy(iproc_pm_policy_e policy, iproc_power_status_e PM)
{
	iproc_policy_word_t *policy_word = NULL;
	
	switch(policy)
	{
		case IPROC_PM_POLICY_CLK: policy_word = &m0_isr_args.iproc_clock_policies[PM]; break;
		case IPROC_PM_POLICY_PWR: policy_word = &m0_isr_args.iproc_power_policies[PM]; break;
		case IPROC_PM_POLICY_END: return NULL; 
	}
	return policy_word;
}

int iproc_get_policy_attr(iproc_pm_policy_e policy, iproc_power_status_e PM, iproc_soc_pid_e PID)
{
	int ret;
	iproc_policy_word_t *policy_word = iproc_get_policy(policy, PM);

	if(!policy_word)
	{
		iproc_err("Fail to get policy word: policy=%d, PM=%d.\n", policy, PM);
		return -EINVAL;
	}

	switch(PID)
	{
		case SOC_PID_CORE  : ret = policy_word->u.s.core ; break;
		case SOC_PID_NEON  : ret = policy_word->u.s.neon ; break;
		case SOC_PID_CACHE : ret = policy_word->u.s.cache; break;
		case SOC_PID_DDR   : ret = policy_word->u.s.ddr  ; break;
		case SOC_PID_SCI   : ret = policy_word->u.s.sci  ; break;
		case SOC_PID_DTE   : ret = policy_word->u.s.dte  ; break;
		case SOC_PID_MSR   : ret = policy_word->u.s.msr  ; break;
		case SOC_PID_SD    : ret = policy_word->u.s.sd   ; break;
		case SOC_PID_ET    : ret = policy_word->u.s.et   ; break;
		case SOC_PID_USB   : ret = policy_word->u.s.usb  ; break;
		case SOC_PID_ADC   : ret = policy_word->u.s.adc  ; break;
		default:
			iproc_err("Not supported part id: %d!\n", PID);
			return -EINVAL;
	}

	return ret;
}

int iproc_set_policy_attr(iproc_pm_policy_e policy, iproc_power_status_e PM, iproc_soc_pid_e PID, int val)
{
	iproc_policy_word_t *policy_word = iproc_get_policy(policy, PM);

	if(!policy_word)
	{
		iproc_err("Fail to get policy word: policy=%d, PM=%d.\n", policy, PM);
		return -EINVAL;
	}

	switch(PID)
	{
		case SOC_PID_CORE  : policy_word->u.s.core  = val; break;
		case SOC_PID_NEON  : policy_word->u.s.neon  = val; break;
		case SOC_PID_CACHE : policy_word->u.s.cache = val; break;
		case SOC_PID_DDR   : policy_word->u.s.ddr   = val; break;
		case SOC_PID_SCI   : policy_word->u.s.sci   = val; break;
		case SOC_PID_DTE   : policy_word->u.s.dte   = val; break;
		case SOC_PID_MSR   : policy_word->u.s.msr   = val; break;
		case SOC_PID_SD    : policy_word->u.s.sd    = val; break;
		case SOC_PID_ET    : policy_word->u.s.et    = val; break;
		case SOC_PID_USB   : policy_word->u.s.usb   = val; break;
		case SOC_PID_ADC   : policy_word->u.s.adc   = val; break;
		default:
			iproc_err("Not supported part id: %d!\n", PID);
			return -EINVAL;
	}

//	iproc_update_policy_attr();

	return 0;
}


static void iproc_initialize_policy_words(iproc_policy_word_value_t default_policy_words[], int len)
{
	int i;
	iproc_policy_word_value_t *policy_value=NULL;
	iproc_soc_pid_e pid;
	iproc_power_status_e PM;
	iproc_pm_policy_e policy;

	for(i=0; i<len; i++)
	{
		policy_value = &default_policy_words[i];

		pid = policy_value->PID;
		
		for(policy=0; policy<IPROC_PM_POLICY_END; policy++)
		{
			for(PM=0; PM<IPROC_PM_STATE_END; PM++)
			{

				if(policy==IPROC_PM_POLICY_CLK)
					iproc_set_policy_attr(policy, PM, pid, policy_value->clk[PM]);
			
				if(policy==IPROC_PM_POLICY_PWR)
					iproc_set_policy_attr(policy, PM, pid, policy_value->pwr[PM]);
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////

#define IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power) \
	do{ \
		if (sscanf(buf, "%d %d", &clock, &power)!= 2) \
			return -EINVAL; \
		if(clock<0 || clock>1) \
			return -EINVAL; \
		if(power<0 && power>1) \
			return -EINVAL; \
	}while(0);

#define IPROC_SHOW_POLICY_ATTR_BY_ID(buf, pm, id) \
	sprintf(buf, "%d %d\n", iproc_get_policy_attr(IPROC_PM_POLICY_CLK, pm ,id), iproc_get_policy_attr(IPROC_PM_POLICY_PWR, pm ,id));

#define IPROC_STORE_POLICY_ATTR_BY_ID(pm, id, clock, power) \
	do{ \
		iproc_set_policy_attr(IPROC_PM_POLICY_CLK, pm, id, clock); \
		iproc_set_policy_attr(IPROC_PM_POLICY_PWR, pm, id, power); \
	}while(0);

static ssize_t core_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_CORE);
}
static ssize_t core_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;

	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);

	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_CORE, clock, power);

	return n;
}

static ssize_t neon_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_NEON);
}
static ssize_t neon_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;

	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);

	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_NEON, clock, power);

	return n;
}

static ssize_t cache_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_CACHE);
}
static ssize_t cache_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;
	
	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);

	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_CACHE, clock, power);

	return n;
}

static ssize_t ddr_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_DDR);
}
static ssize_t ddr_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;
	
	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);

	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_DDR, clock, power);

	return n;
}

static ssize_t dte_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_DTE);
}
static ssize_t dte_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;
	
	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);

	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_DTE, clock, power);

	return n;
}

static ssize_t msr_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_MSR);
}
static ssize_t msr_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;
	
	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);

	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_MSR, clock, power);

	return n;
}

static ssize_t et_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_ET);
}
static ssize_t et_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;
	
	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);

	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_ET, clock, power);

	return n;
}

static ssize_t sci_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_SCI);
}
static ssize_t sci_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;
	
	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);

	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_SCI, clock, power);

	return n;
}

static ssize_t sd_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_SD);
}
static ssize_t sd_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;
	
	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);

	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_SD, clock, power);

	return n;
}

static ssize_t usb_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_USB);
}
static ssize_t usb_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;

	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);
	
	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_USB, clock, power);

	return n;
}

static ssize_t adc_show(iproc_power_status_e PM, char *buf)
{
	return IPROC_SHOW_POLICY_ATTR_BY_ID(buf, PM, SOC_PID_ADC);
}
static ssize_t adc_store(iproc_power_status_e PM, const char *buf, size_t n)
{
	int clock=0, power=0;

	IPROC_CHECK_POLICY_ATTR_INPUT(buf, clock, power);
	
	IPROC_STORE_POLICY_ATTR_BY_ID(PM, SOC_PID_ADC, clock, power);

	return n;
}

/////////////////////////////////////////////////////////////////////////////////////////
// The real attr access callbacks
#define IPROC_ATTR_ACCESS_FUNCTIONS(attr) \
	static ssize_t run_##attr##_show(struct device *dev, struct device_attribute *attr, char *buf) \
	{ \
		return attr##_show(IPROC_PM_STATE_RUN, buf); \
	} \
	static ssize_t run_##attr##_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t n) \
	{ \
		return attr##_store(IPROC_PM_STATE_RUN, buf, n); \
	} \
	static ssize_t standby_##attr##_show(struct device *dev, struct device_attribute *attr, char *buf) \
	{ \
		return attr##_show(IPROC_PM_STATE_STANDBY, buf); \
	} \
	static ssize_t standby_##attr##_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t n) \
	{ \
		return attr##_store(IPROC_PM_STATE_STANDBY, buf, n); \
	} \
	static ssize_t sleep_##attr##_show(struct device *dev, struct device_attribute *attr, char *buf) \
	{ \
		return attr##_show(IPROC_PM_STATE_SLEEP, buf); \
	} \
	static ssize_t sleep_##attr##_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t n) \
	{ \
		return attr##_store(IPROC_PM_STATE_SLEEP, buf, n); \
	} \
	static ssize_t deepsleep_##attr##_show(struct device *dev, struct device_attribute *attr, char *buf) \
	{ \
		return attr##_show(IPROC_PM_STATE_DEEPSLEEP, buf); \
	} \
	static ssize_t deepsleep_##attr##_store(struct device *dev, struct device_attribute *attr, const char *buf, size_t n) \
	{ \
		return attr##_store(IPROC_PM_STATE_DEEPSLEEP, buf, n); \
	}

	
IPROC_ATTR_ACCESS_FUNCTIONS(core);
IPROC_ATTR_ACCESS_FUNCTIONS(neon);
IPROC_ATTR_ACCESS_FUNCTIONS(cache);
IPROC_ATTR_ACCESS_FUNCTIONS(ddr);
IPROC_ATTR_ACCESS_FUNCTIONS(dte);
IPROC_ATTR_ACCESS_FUNCTIONS(msr);
IPROC_ATTR_ACCESS_FUNCTIONS(sci);
IPROC_ATTR_ACCESS_FUNCTIONS(sd);
IPROC_ATTR_ACCESS_FUNCTIONS(et);
IPROC_ATTR_ACCESS_FUNCTIONS(usb);
IPROC_ATTR_ACCESS_FUNCTIONS(adc);

///////////////////////////////////////////////////////////////////////////////////////////

#define IPROC_PM_POLICY_COMMON_ATTR(file, run_rw_mode, standby_rw_mode, sleep_rw_mode, deepsleep_rw_mode) \
	static IPROC_DEV_ATTR_##run_rw_mode(run##_##file); \
	static IPROC_DEV_ATTR_##standby_rw_mode(standby##_##file); \
	static IPROC_DEV_ATTR_##sleep_rw_mode(sleep##_##file); \
	static IPROC_DEV_ATTR_##deepsleep_rw_mode(deepsleep##_##file);

                                          // attr      run   standby   sleep   deepsleep
IPROC_PM_POLICY_COMMON_ATTR(core,  RO, RO,     RO,   RO);
IPROC_PM_POLICY_COMMON_ATTR(neon,  RW, RW,     RO,   RO);
IPROC_PM_POLICY_COMMON_ATTR(cache, RO, RO,     RO,   RO);
IPROC_PM_POLICY_COMMON_ATTR(ddr,   RO, RO,     RO,   RO);
IPROC_PM_POLICY_COMMON_ATTR(dte,   RW, RW,     RW,   RO);
IPROC_PM_POLICY_COMMON_ATTR(msr,   RW, RW,     RW,   RO);
IPROC_PM_POLICY_COMMON_ATTR(sci,   RW, RW,     RW,   RO);
IPROC_PM_POLICY_COMMON_ATTR(sd,    RW, RW,     RW,   RO);
IPROC_PM_POLICY_COMMON_ATTR(et,    RW, RW,     RW,   RO);
IPROC_PM_POLICY_COMMON_ATTR(usb,   RW, RW,     RW,   RO);
IPROC_PM_POLICY_COMMON_ATTR(adc,   RW, RW,     RW,   RO);


#define IPROC_PM_POLICY_WITH_N_ATTRS(pmmode, file1, file2, file3, file4, file5, file6, file7, file8, file9, file10, file11) \
	static struct attribute *policy_##pmmode##_attrs[] = { \
		&dev_attr_##pmmode##_##file1.attr, \
		&dev_attr_##pmmode##_##file2.attr, \
		&dev_attr_##pmmode##_##file3.attr, \
		&dev_attr_##pmmode##_##file4.attr, \
		&dev_attr_##pmmode##_##file5.attr, \
		&dev_attr_##pmmode##_##file6.attr, \
		&dev_attr_##pmmode##_##file7.attr, \
		&dev_attr_##pmmode##_##file8.attr, \
		&dev_attr_##pmmode##_##file9.attr, \
		&dev_attr_##pmmode##_##file10.attr, \
		&dev_attr_##pmmode##_##file11.attr, \
		NULL \
	}; \
	static struct attribute_group policy_##pmmode##_attr_group = { \
		.name = "policy_"__stringify(pmmode), \
		.attrs = policy_##pmmode##_attrs, \
	};

#define IPROC_PM_POLICY_ATTRS_NAMES   \
	core, neon, cache, ddr, dte, msr, sci, sd, et, usb, adc

#define INVOKE_IPROC_PM_POLICY_WITH_N_ATTRS(...)    IPROC_PM_POLICY_WITH_N_ATTRS(__VA_ARGS__)

INVOKE_IPROC_PM_POLICY_WITH_N_ATTRS(run,       IPROC_PM_POLICY_ATTRS_NAMES);
INVOKE_IPROC_PM_POLICY_WITH_N_ATTRS(standby,   IPROC_PM_POLICY_ATTRS_NAMES);
INVOKE_IPROC_PM_POLICY_WITH_N_ATTRS(sleep,     IPROC_PM_POLICY_ATTRS_NAMES);
INVOKE_IPROC_PM_POLICY_WITH_N_ATTRS(deepsleep, IPROC_PM_POLICY_ATTRS_NAMES);


//////////////////////////////////////////////////////////////////////////////
static IPROC_DEV_ATTR_RW(pm_state);
static struct attribute *pm_state_attrs[] = {
	&dev_attr_pm_state.attr,
	NULL
};

static struct attribute_group pm_state_attr_group = {
	.attrs = pm_state_attrs,
};
//////////////////////////////////////////////////////////////////////////////

int iproc_pm_sysfs_register(struct platform_device *pldev)
{
	int ret;
	memset(&m0_isr_args, 0, sizeof(m0_isr_args));
	iproc_initialize_policy_words(iproc_default_policy_words, ARRAY_SIZE(iproc_default_policy_words));

	if((ret = iproc_sysfs_create_group(&pldev->dev.kobj, &pm_state_attr_group)))
		goto err_exit;

	if((ret = iproc_sysfs_create_group(&pldev->dev.kobj, &policy_run_attr_group)))
		goto err_exit0;

	if((ret = iproc_sysfs_create_group(&pldev->dev.kobj, &policy_standby_attr_group)))
		goto err_exit1;
	
	if((ret = iproc_sysfs_create_group(&pldev->dev.kobj, &policy_sleep_attr_group)))
		goto err_exit2;

	if((ret = iproc_sysfs_create_group(&pldev->dev.kobj, &policy_deepsleep_attr_group)))
		goto err_exit3;

	return 0;

err_exit4:
	iproc_sysfs_remove_group(&pldev->dev.kobj, &policy_deepsleep_attr_group);	
err_exit3:
	iproc_sysfs_remove_group(&pldev->dev.kobj, &policy_sleep_attr_group);	
err_exit2:
	iproc_sysfs_remove_group(&pldev->dev.kobj, &policy_standby_attr_group);
err_exit1:
	iproc_sysfs_remove_group(&pldev->dev.kobj, &policy_run_attr_group);
err_exit0:
	iproc_sysfs_remove_group(&pldev->dev.kobj, &pm_state_attr_group);
err_exit:
	iproc_err("Failed to add sysfs files, ret=%d.\n", ret);
	return ret;
}

void iproc_pm_sysfs_unregister(struct platform_device *pldev)
{
	memset(&m0_isr_args, 0, sizeof(m0_isr_args));

	iproc_sysfs_remove_group(&pldev->dev.kobj, &pm_state_attr_group);
	iproc_sysfs_remove_group(&pldev->dev.kobj, &policy_run_attr_group);
	iproc_sysfs_remove_group(&pldev->dev.kobj, &policy_standby_attr_group);
	iproc_sysfs_remove_group(&pldev->dev.kobj, &policy_sleep_attr_group);
	iproc_sysfs_remove_group(&pldev->dev.kobj, &policy_deepsleep_attr_group);
	
	iproc_dbg("All sysfs files removed.\n");
}
