/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include "socregs.h"
#include "M0.h"

unsigned int MCU_ddr_crc32(void) // For DDR sanity check
{
	register unsigned int crc = 0, offset = 0;
	register volatile unsigned int *buf = (unsigned int *)0x60000000;
	register unsigned int len = (unsigned int)0x00100000;//in word
	
	for(offset=0; offset<len; offset++)
	{
		crc ^= *buf++;
	}

	cpu_reg32_wr(0x03012A00, crc);
	
	return crc;
}
////////////////////////////////////////////////////////////////////////////

/* Interrupt trigger mode: high level 
    J9076 pin1 = AON_GPIO0
    J9022 pin1 = 3.3v
    J9022 pin6 = GND
 */
void MCU_set_aon_gpio_as_wakeup(uint32_t intrNo)
{
	if(intrNo>=IPROC_AON_GPIO_LED_SINGLE_END)
		return;
	
	if(intrNo==IPROC_AON_GPIO_LED_F)
		cpu_reg32_wr(CRMU_IOMUX_CONTROL, (cpu_reg32_rd(CRMU_IOMUX_CONTROL)|1));
	
	cpu_reg32_wr(CRMU_MCU_INTR_MASK, 0xFFFFFFFF); //mask all interrupts
	
	cpu_reg32_setbit(GP_INT_TYPE, intrNo); //0:edge				1:level
	cpu_reg32_clrbit(GP_INT_DE,   intrNo); //0:single edge			1:both edge
	cpu_reg32_setbit(GP_INT_EDGE, intrNo); //0:low level/falling edge	1:high level/rising edge
	cpu_reg32_clrbit(GP_OUT_EN,   intrNo); //0:input				1:output

	cpu_reg32_wr    (GP_INT_CLR,  0x3f);   //This line MUST be before enable intr!
	cpu_reg32_setbit(GP_INT_MSK,  intrNo); //0:disable intr			1:enable intr

	cpu_reg32_clrbit(CRMU_MCU_INTR_MASK, CRMU_MCU_INTR_MASK__MCU_AON_GPIO_INTR_MASK); //enable AON GPIO intr
}

////////////////////////////////////////////////////////////////////////////////
void MCU_power_down_neon(void)
{
	/*	
	1.	CRU_CONTROL.IHOST_NEON0_HW_RESET_N = 0x0
	2.	CRU_IHOST_PWRDWN_EN.LOGIC_CLAMP_ON_NEON0 = 0x1
	3.	Wait 0.5 us 1
	4.	CRU_IHOST_PWRDWN_EN.LOGIC_PWROKIN_NEON0 = 0x0
	5.	Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWROKOUT_NEON0 == 0x0
	6.	Wait 0.5 us
	7.	CRU_IHOST_PWRDWN_EN.LOGIC_PWRONIN_NEON0 = 0x0
	8.	Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWRONOUT_NEON0 == 0x0
	*/
//	MCU_led_print_hex(0x30);
	cpu_reg32_clrbit(CRU_control, CRU_control__ihost_neon0_hw_reset_n);//step1
	cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_clamp_on_neon0);
	MCU_timer_delay(20);
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwrokin_neon0);
	do{}while(cpu_reg32_getbit(CRU_ihost_pwrdwn_status, CRU_ihost_pwrdwn_status__logic_pwrokout_neon0)!=0);
	MCU_timer_delay(20);//step6
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwronin_neon0);
	do{}while(cpu_reg32_getbit(CRU_ihost_pwrdwn_status, CRU_ihost_pwrdwn_status__logic_pwronout_neon0)!=0);
//	MCU_led_print_hex(0x31);
}

void MCU_power_down_cpu0_l2c(void)
{
	uint32_t regval;
	/*
	 1.  Power down Neon0
	 2.  CRU_STATUS.DETECTED_WFI = 0
	 3.  Execute CPU0 WFI
	 4.  Wait for CRU_STATUS.DETECTED_WFI == 1
	 5.  CRU_CONTROL.ARM_RST_N = 0x0
	 6.  CRU_IHOST_PWRDWN_EN.RAM_CLAMP_ON_L2C = 0x1
	 7.  Wait 0.5 us
	 8.  CRU_IHOST_PWRDWN_EN.RAM_PWROKIN_L2C = 0x0
	 9.  Wait for CRU_IHOST_PWRDWN_STATUS.RAM_PWROKOUT_L2C == 0x0
	 10. Wait 0.5 us
	 11. CRU_IHOST_PWRDWN_EN.RAM_PWRONIN_L2C = 0x0
	 12. Wait for CRU_IHOST_PWRDWN_STATUS.RAM_PWRONOUT_L2C == 0x0
	 13. Wait 0.5 us
	 14. CRU_IHOST_PWRDWN_EN.RAM_PDA_L2C = 0xF
	 15. Wait 0.5 us
	 16. CRU_IHOST_PWRDWN_EN.RAM_CLAMP_ON_CPU0 = 0x1
	 17. Wait 0.5 us
	 18. CRU_IHOST_PWRDWN_EN.RAM_PWROKIN_CPU0 = 0x0
	 19. Wait for CRU_IHOST_PWRDWN_STATUS.RAM_PWROKOUT_CPU0 == 0x0
	 20. Wait 0.5 us
	 21. CRU_IHOST_PWRDWN_EN.RAM_PWRONIN_CPU0 = 0x0
	 22. Wait for CRU_IHOST_PWRDWN_STATUS. RAM_PWRONOUT_CPU0 == 0x0
	 23. Wait 0.5 us
	 24. CRU_IHOST_PWRDWN_EN.RAM_PDA_CPU0 = 0x1
	 25. Wait 0.5 us
	 26. CRU_IHOST_PWRDWN_EN.LOGIC_CLAMP_ON_CPU0 = 0x1
	 27. Wait 0.5 us
	 28. CRU_IHOST_PWRDWN_EN.PLL_CLAMP_ON = 0x1
	 29. Wait 0.5 us
	 30. CRU_IHOST_PWRDWN_EN.LOGIC_PWROKIN_CPU0 = 0x7
	 31. Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWROKOUT_CPU0 == 0x7
	 32. CRU_IHOST_PWRDWN_EN.LOGIC_PWROKIN_CPU0 = 0x3
	 33. Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWROKOUT_CPU0 == 0x3
	 34. CRU_IHOST_PWRDWN_EN.LOGIC_PWROKIN_CPU0 = 0x1
	 35. Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWROKOUT_CPU0 == 0x1
	 36. CRU_IHOST_PWRDWN_EN.LOGIC_PWROKIN_CPU0 = 0x0
	 37. Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWROKOUT_CPU0 == 0x0
	 38. Wait 0.5 us
	 39. CRU_IHOST_PWRDWN_EN.LOGIC_PWRONIN_CPU0 = 0x7
	 40. Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWRONOUT_CPU0 == 0x7
	 41. CRU_IHOST_PWRDWN_EN.LOGIC_PWRONIN_CPU0 = 0x3
	 42. Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWRONOUT_CPU0 == 0x3
	 43. CRU_IHOST_PWRDWN_EN.LOGIC_PWRONIN_CPU0 = 0x1
	 44. Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWRONOUT_CPU0 == 0x1
	 45. CRU_IHOST_PWRDWN_EN.LOGIC_PWRONIN_CPU0 = 0x0
	 46. Wait for CRU_IHOST_PWRDWN_STATUS.LOGIC_PWRONOUT_CPU0 == 0x0
	 47. Wait 0.5 us
	 48. CRU_IHOST_PWRDWN_EN.PLL_PWRON = 0x0
	 49. Wait 1.5 us
	 50. CRU_IHOST_PWRDWN_EN.PLL_LDO_PWRON = 0x0
	 */
//	MCU_led_print_hex(0x20);
//	do{}while(cpu_reg32_getbit(CRU_status, CRU_status__detected_wfi)!=1);
	cpu_reg32_clrbit(CRU_control, CRU_control__arm_rst_n);//step5
	cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_clamp_on_l2c);
	MCU_timer_delay(20);
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pwrokin_l2c);
	do{}while(cpu_reg32_getbit(CRU_ihost_pwrdwn_status, CRU_ihost_pwrdwn_status__ram_pwrokout_l2c)!=0);
	MCU_timer_delay(20);//step 10
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pwronin_l2c);
	do{}while(cpu_reg32_getbit(CRU_ihost_pwrdwn_status, CRU_ihost_pwrdwn_status__ram_pwronout_l2c)!=0);
	MCU_timer_delay(20);
	cpu_reg32_wr_mask(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pda_l2c_R, CRU_ihost_pwrdwn_en__ram_pda_l2c_WIDTH, 0xf);
	MCU_timer_delay(20);//step 15
	cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_clamp_on_cpu0);
	MCU_timer_delay(20);
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pwrokin_cpu0);
	do{}while(cpu_reg32_getbit(CRU_ihost_pwrdwn_status, CRU_ihost_pwrdwn_status__ram_pwrokout_cpu0)!=0);
	MCU_timer_delay(20);//step20
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pwronin_cpu0);
	do{}while(cpu_reg32_getbit(CRU_ihost_pwrdwn_status, CRU_ihost_pwrdwn_status__ram_pwronout_cpu0)!=0);
	MCU_timer_delay(20);
	cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pda_cpu0);
	MCU_timer_delay(20);//step25
	cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_clamp_on_cpu0);
	MCU_timer_delay(20);
	cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__pll_clamp_on);
	MCU_timer_delay(20);
//	MCU_led_print_hex(0x21);

	cpu_reg32_wr_mask(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwrokin_cpu0_R, CRU_ihost_pwrdwn_en__logic_pwrokin_cpu0_WIDTH, 0x7);//step30
	do{regval = (((cpu_reg32_rd(CRU_ihost_pwrdwn_status))>>CRU_ihost_pwrdwn_status__logic_pwrokout_cpu0_R)&0x0f);}while(regval!=0x7);
	cpu_reg32_wr_mask(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwrokin_cpu0_R, CRU_ihost_pwrdwn_en__logic_pwrokin_cpu0_WIDTH, 0x3);
	do{regval = (((cpu_reg32_rd(CRU_ihost_pwrdwn_status))>>CRU_ihost_pwrdwn_status__logic_pwrokout_cpu0_R)&0x0f);}while(regval!=0x3);
	cpu_reg32_wr_mask(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwrokin_cpu0_R, CRU_ihost_pwrdwn_en__logic_pwrokin_cpu0_WIDTH, 0x1);
	do{regval = (((cpu_reg32_rd(CRU_ihost_pwrdwn_status))>>CRU_ihost_pwrdwn_status__logic_pwrokout_cpu0_R)&0x0f);}while(regval!=0x1);//step35
	cpu_reg32_wr_mask(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwrokin_cpu0_R, CRU_ihost_pwrdwn_en__logic_pwrokin_cpu0_WIDTH, 0x0);
	do{regval = (((cpu_reg32_rd(CRU_ihost_pwrdwn_status))>>CRU_ihost_pwrdwn_status__logic_pwrokout_cpu0_R)&0x0f);}while(regval!=0x0);
	MCU_timer_delay(20);
//	MCU_led_print_hex(0x23);

	cpu_reg32_wr_mask(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwronin_cpu0_R, CRU_ihost_pwrdwn_en__logic_pwronin_cpu0_WIDTH, 0x7);
	do{regval = (((cpu_reg32_rd(CRU_ihost_pwrdwn_status))>>CRU_ihost_pwrdwn_status__logic_pwronout_cpu0_R)&0x0f);}while(regval!=0x7);//step40
//	MCU_led_print_hex(0x25);
	cpu_reg32_wr_mask(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwronin_cpu0_R, CRU_ihost_pwrdwn_en__logic_pwronin_cpu0_WIDTH, 0x3);
	do{regval = (((cpu_reg32_rd(CRU_ihost_pwrdwn_status))>>CRU_ihost_pwrdwn_status__logic_pwronout_cpu0_R)&0x0f);}while(regval!=0x3);
//	MCU_led_print_hex(0x26);
	cpu_reg32_wr_mask(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwronin_cpu0_R, CRU_ihost_pwrdwn_en__logic_pwronin_cpu0_WIDTH, 0x1);
	do{regval = (((cpu_reg32_rd(CRU_ihost_pwrdwn_status))>>CRU_ihost_pwrdwn_status__logic_pwronout_cpu0_R)&0x0f);}while(regval!=0x1);
//	MCU_led_print_hex(0x27);
	cpu_reg32_wr_mask(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_pwronin_cpu0_R, CRU_ihost_pwrdwn_en__logic_pwronin_cpu0_WIDTH, 0x0);//step45
	do{regval = (((cpu_reg32_rd(CRU_ihost_pwrdwn_status))>>CRU_ihost_pwrdwn_status__logic_pwronout_cpu0_R)&0x0f);}while(regval!=0x0);
	MCU_timer_delay(20);

	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__pll_pwron);
	MCU_timer_delay(60);
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__pll_ldo_pwron);//step50
//	MCU_led_print_hex(0x2a);
}

void ihost_power_down_seq(void)
{
	//power down sequence through hardware FSM
	cpu_reg32_wr(CRU_cpu0_powerdown, 0x80000000);  // [31] = 1
}

//////////////////////////////////////////////////////////////////////////////////////
/* ------- Standby --------
   Core = Clock gate
   Neon = Power off/on
   Cache = on
   DMU  = Policy
   System = Policy
   DDR = on
 ---------------------------*/
int MCU_SoC_Standby_Handler(void)
{
	MCU_set_pdsys_master_clock_gating(1);
	
	return 1;
}

/* ------- Sleep --------
   Core,Neon,Cache = Power Off
   DMU  = Policy
   System = Policy
   DDR = on
 ---------------------------*/
int MCU_SoC_Sleep_Handler(void)
{
	MCU_set_pdsys_master_clock_gating(1);

	ihost_power_down_seq();

    return 1;
}

/* ------- DeepSleep --------
   Core,Neon,Cache = Power Off
   DMU  = Policy
   System = Power Off
   DDR = Self-refresh
 ---------------------------*/
int MCU_SoC_DeepSleep_Handler(void)
{
//	MCU_timer_delay(0x400);//delay for A9 finish all task before powering down it

//	MCU_ddr_crc32();//this must be done before putting DDR into SR
	MCU_led_print_hex(0x20);

	/////////////////// Put DDR in SR mode
	MCU_set_ddr_self_refresh_mode(1); // done for iHOST, for ihost is in wfi
	MCU_led_print_hex(0x21);

    /////////////////// Power down PD_SYS sequence
    //1]. Assert reset
    cpu_reg32_wr(CRMU_CDRU_APB_RESET_CTRL, 0); //Chip DRU is reset
	MCU_led_print_hex(0x24);

	//3]. Enable ISO PDSYS_PLL_LOCKED
	cpu_reg32_setbit(CRMU_ISO_CELL_CONTROL, CRMU_ISO_CELL_CONTROL__CRMU_ISO_PDSYS_PLL_LOCKED);
	MCU_led_print_hex(0x25);

	//2]. Power down PLLs
	// ASIU_MIPI_GENPLL, ASIU_AUDIO_GENPLL, LCPLL0, GENPLL
	// fistly latch input value so that interface core supply can be power-down.
	// then set to power down mode
	cpu_reg32_wr(CRMU_PLL_AON_CTRL, cpu_reg32_rd(CRMU_PLL_AON_CTRL) | 0x10111);
	cpu_reg32_wr(CRMU_PLL_AON_CTRL, cpu_reg32_rd(CRMU_PLL_AON_CTRL) & 0x10111);
	MCU_led_print_hex(0x26);

	//cpu_wr_single(CRMU_DDR_PHY_AON_CTRL, 0x7, 4); //done in SRE()
	
	cpu_reg32_wr(CRMU_USB_PHY_AON_CTRL, 0x10101);//latch inputs of P0~P2
	MCU_led_print_hex(0x27);

	//3]. Enable ISO PD_SYS & PDSYS_POWER_GOOD
	cpu_reg32_setbit(CRMU_ISO_CELL_CONTROL, CRMU_ISO_CELL_CONTROL__CRMU_ISO_PDSYS_POWER_GOOD);
	cpu_reg32_setbit(CRMU_ISO_CELL_CONTROL, CRMU_ISO_CELL_CONTROL__CRMU_ISO_PDSYS);
	MCU_led_print_hex(0x28);

	//4]. Disable PD_SYS SWREG
	cpu_reg32_setbit(CRMU_SWREG_CTRL, CRMU_SWREG_CTRL__PDSYS_SWREG_DIS);
	MCU_led_print_hex(0x29);
	
	/////////////////// Power down peripherals' LDO(RGMII, ADC, AUDIO)
	MCU_set_ADC_LDO_off(1);
	MCU_led_print_hex(0x2a);

	MCU_set_RGMII_LDO_off(1);
	MCU_led_print_hex(0x2b);

	/////////////////// Enter ultra low power mode
	MCU_set_ultra_low_power_mode(1);
	MCU_led_print_hex(0x2c);

	/////////////////// Clock gate all crmu components
	MCU_set_crmu_clk_gating(1);
	MCU_led_print_hex(0x2d);

    return 1;
}
