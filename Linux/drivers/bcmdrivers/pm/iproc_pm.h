/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef _IPROC_PM_H
#define _IPROC_PM_H

#include <linux/ioport.h>
#include <linux/platform_device.h>

#include "socregs.h"
#include "M0_iproc_pm_common.h"

#define IPROC_BUILD_X(X) (defined(X) || defined(X##_MODULE))

/*Module info*/
#define SOC_NAME          "Cygnus"
#define DRV_NAME          "PM"
#define DRV_VER           "1.0"
#define DRV_INFO_PREFIX   "iProc-PM: "
#define DRV_ERR_PREFIX    "iProc-PM Err: "

typedef struct IPROC_Resources {
	struct resource *res;
	uint32_t * __iomem vaddr;
}IPROC_Resources_t;


/*Debug functions*/
#define IPROC_PRINT_PREFIX "PM:"
int iproc_get_msglevel(void);
char *iproc_get_filename_in_path(const char *path);
#define iproc_prt(format, arg...) \
	do{ \
		int msglevel = iproc_get_msglevel();\
		if(msglevel>0) \
			printk(KERN_INFO "[%s %4d@%s] "format, IPROC_PRINT_PREFIX, __LINE__, iproc_get_filename_in_path(__FILE__), ##arg);	\
		else if(msglevel==0) \
			printk(KERN_INFO format, ##arg); \
	}while(0)

#define iproc_err(format, arg...) \
	do{ \
		int msglevel = iproc_get_msglevel();\
		if(msglevel>0) \
			printk(KERN_ERR "[%s %4d@%s] ***ERROR*** "format, IPROC_PRINT_PREFIX, __LINE__, iproc_get_filename_in_path(__FILE__), ##arg);	\
		else if(msglevel==0) \
			printk(KERN_ERR format, ##arg); \
	}while(0)

#define iproc_dbg(format, arg...) \
	do{ \
		if(iproc_get_msglevel()>0) \
		  printk(KERN_DEBUG "[%s %4d@%s] "format, IPROC_PRINT_PREFIX, __LINE__, iproc_get_filename_in_path(__FILE__), ##arg);	\
	}while(0)

/*Module structures*/
typedef enum iproc_part_clock {
	IPROC_CLOCK_OFF=0,
	IPROC_CLOCK_ON,
	IPROC_CLOCK_END
}iproc_part_clock_e;

typedef enum iproc_part_power {
	IPROC_POWER_OFF=0,
	IPROC_POWER_ON,
	IPROC_POWER_END
}iproc_part_power_e;

typedef enum iproc_soc_pid {
	SOC_PID_CORE=0,
	SOC_PID_NEON,
	SOC_PID_CACHE,
	SOC_PID_DDR,
	SOC_PID_SCI,
	SOC_PID_DTE,
	SOC_PID_MSR,
	SOC_PID_SD,
	SOC_PID_ET,
	SOC_PID_USB,
	SOC_PID_ADC,
	SOC_PID_END
}iproc_soc_pid_e;

typedef struct iproc_part_policy {
	iproc_soc_pid_e pid;
	char *name;
	int (*policy)(iproc_part_clock_e clk, iproc_part_power_e pwr);	
}iproc_part_policy_t;


typedef enum iproc_pm_policy {
	IPROC_PM_POLICY_CLK=0,
	IPROC_PM_POLICY_PWR,
	IPROC_PM_POLICY_END
}iproc_pm_policy_e;


typedef enum iproc_freq {
	IPROC_FREQ_200M=0,
	IPROC_FREQ_400M,
	IPROC_FREQ_500M,
	IPROC_FREQ_1000M,
	IPROC_FREQ_1250M,
	IPROC_FREQ_UNSUPPORTED
}iproc_freq_e;

#undef  IPROC_SUPPORT_FREQ_200M
#define IPROC_SUPPORT_FREQ_400M
#define IPROC_SUPPORT_FREQ_500M
#define IPROC_SUPPORT_FREQ_1000M
#undef IPROC_SUPPORT_FREQ_1250M


typedef enum iproc_enable_disable {
	IPROC_DISABLED = 0,
	IPROC_ENABLED = 1,
}iproc_enable_disable_t;


/*M0 ISRs bin, must <16KB*/

typedef enum IPROC_WAKEUP_SRC{
	ws_gpio  = MCU_AON_GPIO_INTR,
	ws_timer = MCU_TIMER_INTR,
	ws_wdog  = MCU_WDOG_INTR,
	ws_alarm = MCU_SPRU_ALARM_EVENT
}IPROC_WAKEUP_SRC_t;

typedef struct IPROC_DDR_REGS_BAKUP {
	uint32_t ddr_ctl[305];//0x18010000 ~ 0x180104c0
//	uint32_t ddr_bist[38];//0x18010c00~0x18010c94
  #if 0
	uint32_t ddr_phy_ctl[149]; //0x18011000~0x18011250
  #else
	uint32_t ddr_phy_ctl_vdl[47]; //0x18011060~0x18011118
  #endif
//	uint32_t ddr_phy_byte_lane0[70];//0x18011400~0x18011514
//	uint32_t ddr_phy_byte_lane1[70];//0x18011600~0x18011714
}IPROC_DDR_REGS_BAKUP_t;



#define CRUM_A9_SRAM_START		0x02000000
#define CRUM_A9_SRAM_END		0x0203FFFF

/*INST RAM:
    IDRAM len is 0x8000, the usage map:
	0x03010000
	    xxxxxxxx     //filled
	0x03011000
	    0......0        // ALL ZERO
	0x03013a00
	    xxxxxxxx     //filled
	0x03015000
	    0......0        // ALL ZERO
	0x03017a00
	    xxxxxxxx     //filled
	0x03017fff

 IDRAM memory map:
 Last address 0x03013FFC is used for M0 stack.	
 So use initial section of IDRAM from start address.
 Start address --- End address------Size --------Usage
  0x03010000         0x030100FF           0x100             ISR Args
  0x03010100         0X030110FF           0x1000           Mbox user ISR
  0x03011100         0x030129FF           0x1900           Wakeup user ISR
  0x03012A00         0x030139FF           0x1000           DDR regs bakup
  ...                                                     0x400             Gap
  0x03013E00         0x03013FFF           0x200             MCU Stack
 */
#define CRUM_M0_IDRAM_START					0x03010000
#define CRUM_M0_IDRAM_END					0x03017FFF
#define CRUM_M0_IDRAM_LEN					(CRUM_M0_IDRAM_END - CRUM_M0_IDRAM_START + 1)

//Check point reg
#define CRMU_IRQx_vector_USER_START         0x03018F80
#define CRMU_IRQx_vector_USER(irqno)        (CRMU_IRQx_vector_USER_START + 4*irqno)
#define CRMU_IRQx_vector_USER_END           (CRMU_IRQx_vector_USER(32)-1)

//M0 new ISRs & args' Offsets to IDRAM
#define IPROC_M0_STACK_OFFSET          (0x03013FFC)//Down growth
#define IPROC_M0_STACK_SIZE            (0x200)//NOT real size, just my thought
#define IPROC_M0_USER_MAX_TOTAL_SIZE   (0x4000-IPROC_M0_STACK_SIZE)
#define IPROC_M0_USER_BASE             (0x0000) //Offset to CRUM_M0_IDRAM_START

#define IPROC_M0_ISR_ARGS_OFFSET	   (IPROC_M0_USER_BASE) //Offset to CRUM_M0_IDRAM_START
#define IPROC_M0_ISR_ARGS_MAXLEN	   (0x100)
#define IPROC_MAILBOX_ISR_OFFSET	   (IPROC_M0_ISR_ARGS_OFFSET + IPROC_M0_ISR_ARGS_MAXLEN)    //Mbox ISR offset in IDRAM
#define IPROC_MAILBOX_ISR_MAXLEN       (0x1000)
#define IPROC_WAKEUP_ISR_OFFSET		   (IPROC_MAILBOX_ISR_OFFSET + IPROC_MAILBOX_ISR_MAXLEN)    //WAKEUP ISR offset in IDRAM
#define IPROC_WAKEUP_ISR_MAXLEN		   (0x1900)
#define IPROC_DDR_REGS_BAKUP_OFFSET    (IPROC_WAKEUP_ISR_OFFSET + IPROC_WAKEUP_ISR_MAXLEN)
#define IPROC_DDR_REGS_BAKUP_MAXLEN    (0x1000)

#define IPROC_M0_USER_END              (IPROC_DDR_REGS_BAKUP_OFFSET + IPROC_DDR_REGS_BAKUP_MAXLEN)

#if ((IPROC_M0_USER_END - IPROC_M0_USER_BASE)> IPROC_M0_USER_MAX_TOTAL_SIZE)
#error "*************** IDRAM CONTENTS OVERSIZE **********!!!"
#endif

extern unsigned char M0_MBOX_BIN_START;
extern unsigned char M0_MBOX_BIN_END;
extern unsigned char M0_WAKEUP_BIN_START;
extern unsigned char M0_WAKEUP_BIN_END;


//Module functions
extern void * __iomem m0_idram_vbase; //idram_res mapped vaddr
extern struct resource idram_res;
extern IPROC_M0_ISR_ARGS_t m0_isr_args;
uint32_t iproc_reg32_read(const uint32_t addr);
void iproc_reg32_write(const uint32_t addr, const uint32_t value);
iproc_power_status_e iproc_get_current_pm_state(void);
void iproc_set_aon_gpio_led(iproc_gpio_status_e gpio_pin);
void iproc_set_current_pm_state(iproc_power_status_e state);
char * iproc_get_pm_state_str_by_id(iproc_power_status_e state);
char *iproc_get_mcu_irq_desc_by_id(IPROC_MCU_IRQ_EVENT_NUM_e irq);
uint iproc_get_rtc_alarm_delay(void);
int iproc_check_mcu_wakeup_src_supported(IPROC_WAKEUP_SRC_t ws);
int iproc_get_valid_mcu_wakeup_src(void);
void iproc_dump_m0_isr_bins(int src, IPROC_MCU_IRQ_EVENT_NUM_e binno);
void iproc_dump_m0_idram(void);
int iproc_install_m0_isr_bins(int install);
int iproc_pm_state_switchover(iproc_power_status_e new_state);
//int iproc_set_irq_as_wakeup_src(unsigned int irq, int en);
int iproc_set_ihost_wakeup_irqs(int en);
void iproc_dump_resume_entry(void);



#if IPROC_BUILD_X(CONFIG_CYGNUS_PM_DFS)
//CONFIG_IPROC_CPUFREQ
int iproc_update_dram_timings(int upscaling, iproc_freq_e f);
int iproc_cpufreq_find_freq_by_id(int id);
int iproc_cpufreq_find_id_by_freq(unsigned int f);
iproc_freq_e iproc_get_a9_freq(int cpu);
void iproc_set_a9_freq (iproc_freq_e newfreq);
int iproc_cpufreq_init(void);
void iproc_cpufreq_exit(void);
#endif

//CONFIG_IPROC_SUSPEND
void iproc_pm_set_a9_sram(int save);
int iproc_save_ddr_regs_in_idram(void);
int iproc_soc_enter_sleep(void);
int iproc_soc_enter_deepsleep(void);
int iproc_suspend_init(void);
void iproc_suspend_exit(void);

//sysfs interfaces
int iproc_get_policy_attr(iproc_pm_policy_e policy, iproc_power_status_e PM, iproc_soc_pid_e PID);
int iproc_set_policy_attr(iproc_pm_policy_e policy, iproc_power_status_e PM, iproc_soc_pid_e PID, int val);
int iproc_pm_sysfs_register(struct platform_device *pldev);
void iproc_pm_sysfs_unregister(struct platform_device *pldev);


#endif//_IPROC_PM_H
