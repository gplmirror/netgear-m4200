/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include "socregs.h"
#include "M0.h"

void MCU_set_peripherals_clock(int en)
{
	if(en)/*re-enable clock*/
	{
		// NEON
		cpu_reg32_clrbit(IHOST_PROC_CLK_ARM_DIV, IHOST_PROC_CLK_ARM_DIV__neon_clock_off);

		//iHost
		cpu_reg32_setbit(IHOST_S0_IO_CONTROL_DIRECT, IHOST_S0_IO_CONTROL_DIRECT__CLK_EN);
		cpu_reg32_clrbit(IHOST_S0_RESET_CONTROL, IHOST_S0_RESET_CONTROL__RESET);
		// 3 ports switch, in GENPLL
		cpu_reg32_wr(CRMU_GENPLL_CONTROL1, 0x0);
		// 2 GPhy
		cpu_reg32_clrbit(CRMU_XTAL_CHANNEL_CONTROL, 6);
		cpu_reg32_clrbit(CRMU_XTAL_CHANNEL_CONTROL, 7);
		cpu_reg32_clrbit(CRMU_XTAL_CHANNEL_CONTROL, 8);
		cpu_reg32_clrbit(CRMU_XTAL_CHANNEL_CONTROL, 9);
		// GMAC/RGMII
		cpu_reg32_setbit(AMAC_IDM0_IO_CONTROL_DIRECT, AMAC_IDM0_IO_CONTROL_DIRECT__CLK_EN);
		cpu_reg32_clrbit(AMAC_IDM0_IDM_RESET_CONTROL, AMAC_IDM0_IDM_RESET_CONTROL__RESET);
		cpu_reg32_setbit(AMAC_IDM1_IO_CONTROL_DIRECT, AMAC_IDM1_IO_CONTROL_DIRECT__CLK_EN);
		cpu_reg32_clrbit(AMAC_IDM1_IDM_RESET_CONTROL, AMAC_IDM1_IDM_RESET_CONTROL__RESET);
		cpu_reg32_setbit(CRMU_LDO_CTRL, CRMU_LDO_CTRL__PMU_VREGCNTL_EN);
		cpu_reg32_clrbit(CRMU_RGMII_LDO_CTRL, CRMU_RGMII_LDO_CTRL__RGMII_LDOPWRDN);
		while(!(cpu_reg32_getbit(CRMU_PWR_GOOD_STATUS, CRMU_PWR_GOOD_STATUS__RGMII_POWER_GOOD)));
		// DMA
		cpu_reg32_setbit(DMAC_M0_IDM_IO_CONTROL_DIRECT, DMAC_M0_IDM_IO_CONTROL_DIRECT__CLK_EN);
		cpu_reg32_clrbit(DMAC_M0_IDM_RESET_CONTROL, DMAC_M0_IDM_RESET_CONTROL__RESET);
		// USBH
		cpu_reg32_setbit(USB2_IDM_IDM_IO_CONTROL_DIRECT, USB2_IDM_IDM_IO_CONTROL_DIRECT__clk_enable);
		cpu_reg32_clrbit(USB2_IDM_IDM_RESET_CONTROL, USB2_IDM_IDM_RESET_CONTROL__RESET);
		// USBD
		cpu_reg32_setbit(USB2D_IDM_IDM_IO_CONTROL_DIRECT, USB2D_IDM_IDM_IO_CONTROL_DIRECT__clk_enable);
		cpu_reg32_clrbit(USB2D_IDM_IDM_RESET_CONTROL, USB2D_IDM_IDM_RESET_CONTROL__RESET);
		// usb_phy_ref_clk 52, pcie0_phy, pcie1_phy, ddr_clk 50, sdio_clk 200, smart_card_clk 40, in LCPLL0
		cpu_reg32_wr(CRMU_LCPLL0_CONTROL0, cpu_reg32_rd(CRMU_LCPLL0_CONTROL0)&0xfff80000);
		cpu_reg32_setbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_RESETB);
		cpu_reg32_setbit(CRMU_LCPLL0_CONTROL0, CRMU_LCPLL0_CONTROL0__LCPLL0_POST_RESETB);
		cpu_reg32_setbit(CRMU_LCPLL0_CONTROL1, CRMU_LCPLL0_CONTROL1__LCPLL_SEL_SW_SETTING);
		// SDIO
		cpu_reg32_setbit(SDIO_IDM0_IO_CONTROL_DIRECT, SDIO_IDM0_IO_CONTROL_DIRECT__clk_enable);
		cpu_reg32_clrbit(SDIO_IDM0_IDM_RESET_CONTROL, SDIO_IDM0_IDM_RESET_CONTROL__RESET);
		cpu_reg32_setbit(SDIO_IDM1_IO_CONTROL_DIRECT, SDIO_IDM1_IO_CONTROL_DIRECT__clk_enable);
		cpu_reg32_clrbit(SDIO_IDM1_IDM_RESET_CONTROL, SDIO_IDM1_IDM_RESET_CONTROL__RESET);
		//PCIE Controller
		cpu_reg32_setbit(AXI_PCIE_M0_IDM_IO_CONTROL_DIRECT,   AXI_PCIE_M0_IDM_IO_CONTROL_DIRECT__CLK_ENABLE);
		cpu_reg32_clrbit(AXI_PCIE_M0_IDM_M_IDM_RESET_CONTROL, AXI_PCIE_M0_IDM_M_IDM_RESET_CONTROL__RESET);
		cpu_reg32_setbit(AXI_PCIE_M1_IDM_IO_CONTROL_DIRECT,   AXI_PCIE_M1_IDM_IO_CONTROL_DIRECT__CLK_ENABLE);
		cpu_reg32_clrbit(AXI_PCIE_M1_IDM_M_IDM_RESET_CONTROL, AXI_PCIE_M1_IDM_M_IDM_RESET_CONTROL__RESET);
		// SMAU total
		cpu_reg32_setbit(SMAU_S0_IDM_IO_CONTROL_DIRECT, SMAU_S0_IDM_IO_CONTROL_DIRECT__clk_enable);
		cpu_reg32_clrbit(SMAU_S0_IDM_RESET_CONTROL, SMAU_S0_IDM_RESET_CONTROL__RESET);
		// SMAU nand
		cpu_reg32_setbit(NAND_IDM_IDM_IO_CONTROL_DIRECT, NAND_IDM_IDM_IO_CONTROL_DIRECT__clk_enable);
		// SMAU rom
		cpu_reg32_setbit(ROM_S0_IDM_IO_CONTROL_DIRECT, ROM_S0_IDM_IO_CONTROL_DIRECT__clk_enable);
		// SMAU pnor
		cpu_reg32_setbit(PNOR_IDM_IDM_IO_CONTROL_DIRECT, PNOR_IDM_IDM_IO_CONTROL_DIRECT__clk_enable);
		// SMAU qspi
		cpu_reg32_setbit(QSPI_IDM_IDM_IO_CONTROL_DIRECT, QSPI_IDM_IDM_IO_CONTROL_DIRECT__clk_enable);
		// SMAU crypto
		cpu_reg32_setbit(SMAU_CRYPTO_S0_IDM_IO_CONTROL_DIRECT, SMAU_CRYPTO_S0_IDM_IO_CONTROL_DIRECT__clk_enable_R);
		cpu_reg32_clrbit(SMAU_CRYPTO_S0_IDM_RESET_CONTROL, SMAU_CRYPTO_S0_IDM_RESET_CONTROL__RESET);
		// AUDION/I2S/TDM
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__AUD_CLK_GATE_EN);
		// D1W
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__D1W_CLK_GATE_EN);
		// LCD
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__LCD_CLK_GATE_EN);
		// ADC
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__ADC_CLK_GATE_EN);
		// MIPI
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__MIPI_DSI_CLK_GATE_EN);
		// Camera
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__CAM_CLK_GATE_EN);
		// SmartCard
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__SMARTCARD_CLK_GATE_EN);
		// Keypad
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__KEYPAD_CLK_GATE_EN);
		// Crypto
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__CRYPTO_CLK_GATE_EN);
		// CAN
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__CAN_CLK_GATE_EN);
		// 2D Graphics
		cpu_reg32_setbit(ASIU_TOP_CLK_GATING_CTRL, ASIU_TOP_CLK_GATING_CTRL__GFX_CLK_GATE_EN);
		// SRAM
		cpu_reg32_setbit(SRAM_S0_IDM_IO_CONTROL_DIRECT, SRAM_S0_IDM_IO_CONTROL_DIRECT__clk_enable);
		cpu_reg32_clrbit(SRAM_S0_IDM_RESET_CONTROL, SRAM_S0_IDM_RESET_CONTROL__RESET);
		// APBX(PWM,WDOG,GPIOs,SRAB,GP_TIMERS,BSC,MDIO)
		cpu_reg32_setbit(APBX_IDM_IDM_IO_CONTROL_DIRECT, APBX_IDM_IDM_IO_CONTROL_DIRECT__clk_enable);
		cpu_reg32_clrbit(APBX_IDM_IDM_RESET_CONTROL, APBX_IDM_IDM_RESET_CONTROL__RESET);
		// APBY(UART,SPI)
		cpu_reg32_setbit(APBY_IDM_IDM_IO_CONTROL_DIRECT, APBY_IDM_IDM_IO_CONTROL_DIRECT__clk_enable);
		cpu_reg32_clrbit(APBY_IDM_IDM_RESET_CONTROL, APBY_IDM_IDM_RESET_CONTROL__RESET);
		// APBZ(SOTP,PKA,RNG,TZPC,S-GPIOs,S-WDOG)
		cpu_reg32_setbit(APBZ_IDM_IDM_IO_CONTROL_DIRECT, APBZ_IDM_IDM_IO_CONTROL_DIRECT__clk_enable);
		cpu_reg32_clrbit(APBZ_IDM_IDM_RESET_CONTROL, APBZ_IDM_IDM_RESET_CONTROL__RESET);
	}
	else/*clock-gate*/
	{
	}
}

//used for DEEPSLEEP wakeup
void MCU_set_crmu_pll(void)
{
	//-----------------------------------------------------------
	// Power on GENPLL, LCPLL0
	// Power off ASIU_AUDIO_GENPLL, ASIU_MIPI_GENPLL
	// Enable ISO these PLLs
	//-----------------------------------------------------------
	cpu_reg32_wr(CRMU_PLL_AON_CTRL, ((uint32_t)0                          |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__GENPLL_ISO_IN)               |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__GENPLL_PWRON_LDO)            |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__GENPLL_PWR_ON)               |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__LCPLL0_ISO_IN)               |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__LCPLL0_PWRON_LDO)            |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__LCPLL0_PWR_ON)               |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__ASIU_AUDIO_GENPLL_ISO_IN)    |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_AUDIO_GENPLL_PWRON_LDO) |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_AUDIO_GENPLL_PWRON_BG)  |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_AUDIO_GENPLL_PWRON_PLL) |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_AUDIO_GENPLL_PWRON)     |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__ASIU_MIPI_GENPLL_ISO_IN)     |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_MIPI_GENPLL_PWRON_LDO)  |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_MIPI_GENPLL_PWRON_BG)   |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_MIPI_GENPLL_PWRON_PLL)  |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_MIPI_GENPLL_PWRON)        ));

	MCU_timer_delay(PLL_PWRON_WAIT);

	//--------------------------------------------------------------
	// Remove GENPLL, LCPLL0 ISO,  keep ISO ASIU_AUDIO_GENPLL, ASIU_MIPI_GENPLL
	//--------------------------------------------------------------
	cpu_reg32_wr(CRMU_PLL_AON_CTRL, ((uint32_t)0                          |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__GENPLL_ISO_IN)               |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__GENPLL_PWRON_LDO)            |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__GENPLL_PWR_ON)               |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__LCPLL0_ISO_IN)               |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__LCPLL0_PWRON_LDO)            |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__LCPLL0_PWR_ON)               |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__ASIU_AUDIO_GENPLL_ISO_IN)    |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_AUDIO_GENPLL_PWRON_LDO) |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_AUDIO_GENPLL_PWRON_BG)  |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_AUDIO_GENPLL_PWRON_PLL) |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_AUDIO_GENPLL_PWRON)     |
		( (uint32_t)1  << CRMU_PLL_AON_CTRL__ASIU_MIPI_GENPLL_ISO_IN)     |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_MIPI_GENPLL_PWRON_LDO)  |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_MIPI_GENPLL_PWRON_BG)   |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_MIPI_GENPLL_PWRON_PLL)  |
		( (uint32_t)0  << CRMU_PLL_AON_CTRL__ASIU_MIPI_GENPLL_PWRON)        ));

	//--------------------------------------------------------------
	// Reconfigure GENPLL &  LCPLL0 values with reset
	//--------------------------------------------------------------
	cpu_reg32_wr(CRMU_GENPLL_CONTROL0, ((uint32_t)0               |
		((uint32_t)3 << CRMU_GENPLL_CONTROL0__GENPLL_KP_R)        | // JIRA CYGNUS-899 fix
		((uint32_t)2 << CRMU_GENPLL_CONTROL0__GENPLL_KI_R)        |
		((uint32_t)0 << CRMU_GENPLL_CONTROL0__GENPLL_KA_R)        |
		((uint32_t)0 << CRMU_GENPLL_CONTROL0__GENPLL_POST_RESETB) |
		((uint32_t)0 << CRMU_GENPLL_CONTROL0__GENPLL_RESETB)        ));

	cpu_reg32_wr(CRMU_LCPLL0_CONTROL0, ((uint32_t)0                   |
		((uint32_t)0x20 << CRMU_LCPLL0_CONTROL0__LCPLL0_ENABLEB_CH_R) |
		((uint32_t)0    << CRMU_LCPLL0_CONTROL0__LCPLL0_BYP_EN_CH_R)  |
		((uint32_t)0    << CRMU_LCPLL0_CONTROL0__LCPLL0_KPP_R)        |
		((uint32_t)3    << CRMU_LCPLL0_CONTROL0__LCPLL0_KP_R)         | // JIRA CYGNUS-899 fix
		((uint32_t)1    << CRMU_LCPLL0_CONTROL0__LCPLL0_KI_R)         | // JIRA CYGNUS-899 fix
		((uint32_t)0    << CRMU_LCPLL0_CONTROL0__LCPLL0_POST_RESETB)  |
		((uint32_t)0    << CRMU_LCPLL0_CONTROL0__LCPLL0_RESETB)         ));

	//--------------------------------------------------------------
	// Release reset GENPLL &  LCPLL0
	//--------------------------------------------------------------
	cpu_reg32_wr(CRMU_GENPLL_CONTROL0, ((uint32_t)0               |
		((uint32_t)3 << CRMU_GENPLL_CONTROL0__GENPLL_KP_R)        | // JIRA CYGNUS-899 fix
		((uint32_t)2 << CRMU_GENPLL_CONTROL0__GENPLL_KI_R)        |
		((uint32_t)0 << CRMU_GENPLL_CONTROL0__GENPLL_KA_R)        |
		((uint32_t)1 << CRMU_GENPLL_CONTROL0__GENPLL_POST_RESETB) |
		((uint32_t)1 << CRMU_GENPLL_CONTROL0__GENPLL_RESETB)        ));

	cpu_reg32_wr(CRMU_LCPLL0_CONTROL0, ((uint32_t)0                   |
		((uint32_t)0x20 << CRMU_LCPLL0_CONTROL0__LCPLL0_ENABLEB_CH_R) |
		((uint32_t)0    << CRMU_LCPLL0_CONTROL0__LCPLL0_BYP_EN_CH_R)  |
		((uint32_t)0    << CRMU_LCPLL0_CONTROL0__LCPLL0_KPP_R)        |
		((uint32_t)3    << CRMU_LCPLL0_CONTROL0__LCPLL0_KP_R)         | // JIRA CYGNUS-899 fix
		((uint32_t)1    << CRMU_LCPLL0_CONTROL0__LCPLL0_KI_R)         | // JIRA CYGNUS-899 fix
		((uint32_t)1    << CRMU_LCPLL0_CONTROL0__LCPLL0_POST_RESETB)  |
		((uint32_t)1    << CRMU_LCPLL0_CONTROL0__LCPLL0_RESETB)         ));

	//--------------------------------------------------------------
	// Wait for GENPLL &  LCPLL0 locked
	//--------------------------------------------------------------
	do{}while(!cpu_reg32_getbit(CRMU_GENPLL_STATUS, CRMU_GENPLL_STATUS__GENPLL_LOCK));
	do{}while(!cpu_reg32_getbit(CRMU_LCPLL0_STATUS, CRMU_LCPLL0_STATUS__LCPLL0_LOCK));

	//--------------------------------------------------------------
	//Remove ISO from PD SYS PLL
	//--------------------------------------------------------------
	cpu_reg32_clrbit(CRMU_ISO_CELL_CONTROL, CRMU_ISO_CELL_CONTROL__CRMU_ISO_PDSYS_PLL_LOCKED);
	
}

//used for SLEEP & DEEPSLEEP wakeup
void MCU_power_up_ihost_seq(void)
{
	MCU_led_print_hex(0x30);
	// iProc rst_l de-assertion, ASIU_MASTER, SWITCH, IPROC_DDR, IPROC
	cpu_reg32_wr(CRMU_RESET_CTRL , 0xF);	

	// Wait for AXICC_UP
	do{}while(!cpu_reg32_getbit(CRMU_IPROC_NIC_RESET_STATUS, CRMU_IPROC_NIC_RESET_STATUS__IPROC_CDRU_RESET_OUTN_DMU_M0_SIDE_BUS));
	MCU_led_print_hex(0x31);
	// Enable WDT interrupt, but disable WDT reset output
	cpu_reg32_wr(CRMU_WDT_WDOGCONTROL, 0x1);

	// Reset ihost = arm_rst_n & ssi_rst_n
	//cpu_reg32_wr(CRU_control, cpu_reg32_rd(CRU_control) & 0xFFFFFFCF);  // [4/5] = 0
	cpu_reg32_wr(CRU_control, 0x100); // default reset value
	MCU_led_print_hex(0x32);
	MCU_timer_delay(0x40);

	//---------------------------------------------------
    // Power up ihost : Sleep --> Run
	//---------------------------------------------------
	//============================================================================================================================
   // reset default - for ihostpwrdn_en
	cpu_reg32_wr(CRU_ihost_pwrdwn_en, 0x0093E601);
	MCU_timer_delay(0x40);

    //1]. power up LDO+PLL
    //pll_ldo_pwron
    cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__pll_ldo_pwron);
	MCU_led_print_hex(0x33);

	MCU_wdog_delay(0x40);//2.5us
	MCU_led_print_hex(0x32);
    //pll_pwron
    cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__pll_pwron);
	MCU_wdog_delay(0x26);//1.5us
	MCU_led_print_hex(0x34);
	//============================================================================================================================
	//2]. pwronin_cpu0. power up logic of CPU0+ TOP
	cpu_reg32_wr(CRU_ihost_pwrdwn_en, cpu_reg32_rd(CRU_ihost_pwrdwn_en) | 0x02);  // logic_pwronin_cpu0[0] = 1
    while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x02))//poll for logic_pwronout_cpu0;
	MCU_led_print_hex(0x35);
    cpu_reg32_wr(CRU_ihost_pwrdwn_en, cpu_reg32_rd(CRU_ihost_pwrdwn_en) | 0x04);  // logic_pwronin_cpu0[1] = 1
    while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x04));
    MCU_led_print_hex(0x36);
    cpu_reg32_wr(CRU_ihost_pwrdwn_en, cpu_reg32_rd(CRU_ihost_pwrdwn_en) | 0x08);  // logic_pwronin_cpu0[2] = 1
    while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x08));
	MCU_led_print_hex(0x37);

    cpu_reg32_wr(CRU_ihost_pwrdwn_en, cpu_reg32_rd(CRU_ihost_pwrdwn_en) | 0x10);  // logic_pwronin_cpu0[3] = 1
    while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x10));
	MCU_led_print_hex(0x38);

	MCU_wdog_delay(0xD);//0.5us

	//============================================================================================================================
	//3]. pwrokin_cpu0. power up logic of CPU0+ TOP
	cpu_reg32_wr(CRU_ihost_pwrdwn_en, cpu_reg32_rd(CRU_ihost_pwrdwn_en) | 0x20);  // logic_pwrokin_cpu0[0] = 1
	while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x20))//poll for logic_pwrokout_cpu0;
	MCU_led_print_hex(0x39);

	cpu_reg32_wr(CRU_ihost_pwrdwn_en, cpu_reg32_rd(CRU_ihost_pwrdwn_en) | 0x40);  // logic_pwrokin_cpu0[1] = 1
	while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x40));
	MCU_led_print_hex(0x3a);

	cpu_reg32_wr(CRU_ihost_pwrdwn_en, cpu_reg32_rd(CRU_ihost_pwrdwn_en) | 0x80);  // logic_pwrokin_cpu0[2] = 1
	while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x80));
	MCU_led_print_hex(0x3b);

	cpu_reg32_wr(CRU_ihost_pwrdwn_en, cpu_reg32_rd(CRU_ihost_pwrdwn_en) | 0x100);  // logic_pwrokin_cpu0[3] = 1
	while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x100));
	MCU_led_print_hex(0x3c);

	MCU_wdog_delay(0xD);//0.5us

	//============================================================================================================================
    //4]. Deactivate PLL input ISO
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__pll_clamp_on);
	MCU_wdog_delay(0xD);//0.5us

	//Deactivate ISO on top outputs
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__logic_clamp_on_cpu0);
	MCU_wdog_delay(0xD);//0.5us

	//============================================================================================================================
	//5]. power up CPU+RAMs
	// power up array of CPU+SCU RAMs
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pda_cpu0);
	MCU_wdog_delay(0xD);//0.5us

	// power up periphery of CPU+SCU RAMs
	cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pwronin_cpu0);
	while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x800));
	MCU_wdog_delay(0xD);//0.5us
	MCU_led_print_hex(0x3d);

	cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pwrokin_cpu0);
	while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x1000));
	MCU_wdog_delay(0xD);//0.5us
	MCU_led_print_hex(0x3e);

    //Deactivate ISO for CPU+SCU RAMs
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_clamp_on_cpu0);
	MCU_wdog_delay(0xD);//0.5us

	//========================================================================================================
	//6].power up L2C
	// power up array of L2 RAMs
	cpu_reg32_wr_mask(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pda_l2c_R, CRU_ihost_pwrdwn_en__ram_pda_l2c_WIDTH, 0x0); // ram_pda_l2c [17:14] = 0
	MCU_wdog_delay(0xD);//0.5us

	// power up periphary of L2 RAMs
	cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pwronin_l2c);
	while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x40000))//poll for ram_pwroninout_l2c;
	MCU_wdog_delay(0xD);//0.5us
	MCU_led_print_hex(0x3f);

	cpu_reg32_setbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_pwrokin_l2c);
	while(!(cpu_reg32_rd(CRU_ihost_pwrdwn_status) & 0x80000))//poll for ram_pwrokinout_l2c;
	MCU_wdog_delay(0xD);//0.5us

    //Deactivate ISO for ram_l2c
	cpu_reg32_clrbit(CRU_ihost_pwrdwn_en, CRU_ihost_pwrdwn_en__ram_clamp_on_l2c);
	MCU_wdog_delay(0x26);//1.5us
	
	//-----------------------------------------------------------------------------------------------------------------
	// BISR :
	//
	cpu_reg32_wr(CRMU_WDT_WDOGINTCLR, 0x1);        // Interrupt clear 
  #ifdef DFT_BISR_TIMEOUT
	cpu_reg32_wr(CRMU_WDT_WDOGLOAD, 0xFF);
  #else
	cpu_reg32_wr(CRMU_WDT_WDOGLOAD, 0xFFFF);
  #endif
	cpu_reg32_wr(CRMU_WDT_WDOGCONTROL, 0x1);

	//PD group ( Top ) will be loaded with BISR data
	cpu_reg32_wr(CRMU_BISR_PDG_MASK, 0x7);   // PDG mask for Top

	cpu_reg32_wr(CRMU_CHIP_OTPC_RST_CNTRL, 0x0);   // JIRA : CYGNUS-574
	cpu_reg32_wr(CRMU_CHIP_OTPC_RST_CNTRL, 0x1);   // JIRA : CYGNUS-574

	if(!(cpu_reg32_rd(CRMU_CHIP_OTPC_STATUS) & 0x10000))
	{
		while(!cpu_reg32_rd(CRMU_WDT_WDOGRIS)) 
		{
			if(cpu_reg32_rd(BISR_LOAD_DONE_STATUS) & 0x1)
			{
				//PASS MSG --> GPIO   //TBD
				break;
			}
		}
	}

	cpu_reg32_wr(CRMU_WDT_WDOGCONTROL, 0x0);  // Disable WDOG timer

	//Release HWRESET after 4 sys_clk
	//cpu_reg32_setbit(CRU_control, CRU_control__arm_rst_n);
	//-------------------------------------------------------
	// Release HWRESET after 4 sys_clk arm_rst_n & ssi_rst_n // JIRA-916
	//-------------------------------------------------------
	cpu_reg32_wr(CRU_control, cpu_reg32_rd(CRU_control) | 0x30);  // [4/5] = 1

}    



/////////////////////////////////////////////////////////////////////////////////////////////
// ISR2 API :  Power Up sequence
//              M0 GPIO ISR or timer ISR routine for wakeup
// 
// Description :
//              - Power up PD_SYS ( A9 system ) sequence
//              - Power up iHost ( A9 )
//              - M0 return and goes into __wfi() sleep mode
/////////////////////////////////////////////////////////////////////////////////////////////
//
int MCU_SoC_Wakeup_Handler(uint32_t code0, uint32_t code1)
{
	MCU_led_print_hex(0x20);

	if(code0==0 && code1==0)
		return -1;
	
	cpu_reg32_setbit(CRMU_MCU_INTR_MASK, CRMU_MCU_INTR_MASK__MCU_AON_GPIO_INTR_MASK);//disable AON GPIO intr
	cpu_reg32_wr(GP_INT_CLR,0x3f);

	cpu_reg32_wr(CRMU_TIM_TIMER1Control, 0x80); //enable crmu timer
	cpu_reg32_wr(CRMU_WDT_WDOGLOCK, 0x1ACCE551);//enable write other crmu wdog regs
	cpu_reg32_wr(CRMU_WDT_WDOGCONTROL, 0x1); //enable crmu wdog intr output, but no reset output

	MCU_led_print_hex(0x21);

	if(code0==MAILBOX_CODE0_RUN && code1==MAILBOX_CODE1_RUN)
	{
		MCU_led_print_hex(0x22);
	}	
	else if(code0==MAILBOX_CODE0_STANDBY && code1==MAILBOX_CODE1_STANDBY)
	{
		MCU_led_print_hex(0x23);
		MCU_set_pdsys_master_clock_gating(0);
	}
	else if(code0==MAILBOX_CODE0_SLEEP&& code1==MAILBOX_CODE1_SLEEP)
	{
		MCU_led_print_hex(0x24);
		MCU_power_up_ihost_seq();
		MCU_set_pdsys_master_clock_gating(0);
	}
	else if(code0==MAILBOX_CODE0_DEEPSLEEP && code1==MAILBOX_CODE1_DEEPSLEEP)
	{
		MCU_timer_delay(0x00ffffff);
		MCU_led_print_hex(0x25);

		// Enable PD_SYS SWREG
		cpu_reg32_wr(CRMU_SWREG_CTRL, cpu_reg32_rd(CRMU_SWREG_CTRL) & 0xE);
		MCU_led_print_hex(0x26);

		// Exit ULP mode, disable crmu clock gating
		MCU_set_ultra_low_power_mode(0); 
		MCU_led_print_hex(0x27);

		MCU_set_crmu_clk_gating(0); 
		MCU_led_print_hex(0x28);
		
		// Enable ADC, RGMII LDO
		MCU_set_ADC_LDO_off(0);
		MCU_set_RGMII_LDO_off(0);
		MCU_led_print_hex(0x29);
		
		// Poll for pdsys pmu_stable
		do{}while(!cpu_reg32_rd(CRMU_SWREG_STATUS));// 1 - PDSYS is stable
		MCU_led_print_hex(0x2a);
		
		// Disable PD_SYS Power good ISO
		cpu_reg32_clrbit(CRMU_ISO_CELL_CONTROL, CRMU_ISO_CELL_CONTROL__CRMU_ISO_PDSYS_POWER_GOOD);
		do{}while(!cpu_reg32_getbit(CRMU_POWER_POLL, CRMU_POWER_POLL__PDSYS_POWER_POLL_STAT));// Polling for power good
		MCU_led_print_hex(0x2b);
		
		// Disable PD_SYS ISO
		cpu_reg32_clrbit(CRMU_ISO_CELL_CONTROL, CRMU_ISO_CELL_CONTROL__CRMU_ISO_PDSYS);
		MCU_led_print_hex(0x2c);
		
		// De-asssert APB reset
		cpu_reg32_wr(CRMU_CDRU_APB_RESET_CTRL, 0x1);
  		MCU_led_print_hex(0x2d);

		// Capture strap
		cpu_reg32_setbit(CRMU_CHIP_STRAP_CTRL, CRMU_CHIP_STRAP_CTRL__CAPTURE_STRAP_PINS);
		MCU_led_print_hex(0x2e);
		
		// Generate software controlled Power Up Reset to IPROC.
		cpu_reg32_wr(CRMU_SW_POR_RESET_CTRL , 0x0);
		cpu_reg32_wr(CRMU_SW_POR_RESET_CTRL , 0x1);
  		MCU_led_print_hex(0x2f);

		// Enable, Config PLL, GENPLL, LCPLL0
		MCU_set_crmu_pll(); //crmu_pll_config();
		MCU_led_print_hex(0x20);

		// Release strap 
		cpu_reg32_clrbit(CRMU_CHIP_STRAP_CTRL2, CRMU_CHIP_STRAP_CTRL2__FORCE_STRAP_OEB_TO_INPUT);
		MCU_led_print_hex(0x21);
		
		// Power on iHost
		MCU_power_up_ihost_seq(); 
		MCU_led_print_hex(0x22);

		// Enable the main clock control
		cpu_reg32_wr(CRMU_CLOCK_GATE_CTRL, 0xffffffff);
		cpu_reg32_wr(CRMU_CHIP_POR_CTRL, 0xffffffff);
		MCU_led_print_hex(0x23);

		// De-assert peripherals' reset & Disable clock gate
		MCU_set_peripherals_clock(1);
		MCU_led_print_hex(0x24);
	}
	else
	{
		MCU_led_print_hex(0x2f);
	}
	
	return 0;
}
