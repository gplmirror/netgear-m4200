/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/smp.h>
#include <linux/sched.h>
#include <linux/cpufreq.h>
#include <linux/proc_fs.h>
#include <linux/sysfs.h>
#include "iproc_pm.h"

struct iproc_dram_regs{
	int valid;
	iproc_freq_e iproc_freq;
	u32 dram_freq;
};

static struct cpufreq_frequency_table iproc_freq_table[] = {
	{ IPROC_FREQ_200M,        CPUFREQ_ENTRY_INVALID },
	{ IPROC_FREQ_400M,        400000 },
//	{ IPROC_FREQ_400M,		  CPUFREQ_ENTRY_INVALID },
	{ IPROC_FREQ_500M,        500000                }, //KHz
	{ IPROC_FREQ_1000M,       1000000               },
	{ IPROC_FREQ_1250M,       CPUFREQ_ENTRY_INVALID },
	{ IPROC_FREQ_UNSUPPORTED, CPUFREQ_TABLE_END     },
};

static struct iproc_dram_regs iproc_dram_settings_tbl[] = {
	{ 1, IPROC_FREQ_500M,        200,},    //DDR, DDR Phy, DDR LDO Power = 250MHz, 200MHz, On
	{ 1, IPROC_FREQ_1000M,       800,},    //DDR, DDR Phy, DDR LDO Power = 500MHz, 800MHz, On
	{ 0, IPROC_FREQ_UNSUPPORTED, 0,  },    //This must be the last line
};

static DEFINE_MUTEX(iproc_cpufreq_lock);

int iproc_cpufreq_find_id_by_freq(unsigned int f) //f in KHz
{
	int i;
	struct cpufreq_frequency_table *t=iproc_freq_table;
	int cnt=ARRAY_SIZE(iproc_freq_table);

	iproc_dbg("Try to find matched freq id in table, f=%u.\n", f);

	if(f == CPUFREQ_ENTRY_INVALID || f == CPUFREQ_TABLE_END)
	{
		iproc_err("Invalid cpu freq: %uMHz.\n",f/1000);
		return -1;
	}
	
	for(i=0; i<cnt; i++)
	{
		if(f == t[i].frequency)
			return i;
	}
	
	iproc_err("Not supported cpu freq:%uMHz.\n",f/1000);
	
	return -1;
}

int iproc_cpufreq_find_freq_by_id(int id)
{
	int i;
	struct cpufreq_frequency_table *t=iproc_freq_table;
	int cnt=ARRAY_SIZE(iproc_freq_table);

	iproc_dbg("Try to find matched freq in table, id=%d.\n", id);

	if(id == IPROC_FREQ_UNSUPPORTED )
	{
		iproc_err("Invalid cpu freq id: %d.\n", id);
		return -1;
	}
	
	for(i=0; i<cnt; i++)
	{
		if(id == t[i].index && t[i].frequency != CPUFREQ_ENTRY_INVALID)
			return t[i].frequency;
	}
	
	iproc_err("Not supported cpu freq id:%d.\n", id);
	
	return -1;
}


int iproc_update_dram_timings(int upscaling, iproc_freq_e f)
{
	int i=0;
	uint32_t cur_dram_freq;
	struct iproc_dram_regs *t = iproc_dram_settings_tbl;

	for(i=0; i<ARRAY_SIZE(iproc_dram_settings_tbl); i++)
	{	
		if(t[i].valid && t[i].iproc_freq==f)
			break;
	}
	
	if(i==IPROC_FREQ_UNSUPPORTED)
	{
		iproc_err("No matched cpu freq index: %d!\n", f);
		return -1;
	}
	
	cur_dram_freq=iproc_get_ddr3_clock_mhz();

	if(cur_dram_freq == t[i].dram_freq)
	{
		iproc_dbg("Current dram speed(%dMHz) is as expected, no need to change.\n", cur_dram_freq);
		return 0;
	}

	iproc_dbg("Cpu freq %dMHz, update dram freq %dMHz -> %dMHz. !\n", 
		iproc_cpufreq_find_freq_by_id(f), cur_dram_freq, t[i].dram_freq);

	//this seems only change the ddr phy clock, not including the ddr controller
	iproc_change_ddr3_clock_mhz(upscaling);

//	iproc_reg32_write(CRMU_IHOST_POWER_CONFIG, 0x0);

	cur_dram_freq = iproc_get_ddr3_clock_mhz();
	iproc_dbg("Dram freq now %dMHz.\n", cur_dram_freq);

	return 0;
}

static int iproc_do_cpufreq_scaling(struct cpufreq_freqs *freqs)
{
	int ret=0, i;

	iproc_dbg("Try to do cpu%d clock scaling, %u MHz --> %u MHz.\n", freqs->cpu, freqs->old/1000, freqs->new/1000);
	
	if((i=iproc_cpufreq_find_id_by_freq(freqs->new))<0)
		return -1;

	if(freqs->new > freqs->old)//upscaling, update dram first;
	{
		iproc_dbg("cpu%d clock upscaling, %u MHz --> %u MHz.\n", freqs->cpu, freqs->old/1000, freqs->new/1000);
		iproc_update_dram_timings(1, i);
//		iproc_set_xxx_freq();
		iproc_set_a9_freq(i);
	}
	if(freqs->new < freqs->old)//downscaling, update cpu first; 
	{
		iproc_dbg("cpu%d clock downscaling, %u MHz --> %u MHz.\n", freqs->cpu, freqs->old/1000, freqs->new/1000);
		iproc_set_a9_freq(i);
		iproc_update_dram_timings(0, i);
//		iproc_set_xxx_freq();
	}
	
	return ret;
}

static unsigned int iproc_getspeed(unsigned int cpu)
{
	iproc_freq_e i;

	iproc_dbg("Try to get current cpu%d speed\n", cpu);

	if (cpu >= NR_CPUS)
	{
		iproc_err("Invalid cpu num: %d\n", cpu);
		return 0;
	}
	
	i = iproc_get_a9_freq(cpu);

	return iproc_freq_table[i].frequency;
}

static int iproc_verify_speed(struct cpufreq_policy *policy)
{
	iproc_dbg("Try to verify policy (%u - %u kHz) for cpu %u\n",
					policy->min, policy->max, policy->cpu);
	
	return cpufreq_frequency_table_verify(policy, iproc_freq_table);
}

static int iproc_target(struct cpufreq_policy *policy, unsigned int target_freq, unsigned int relation)
{
	unsigned int i=0;
	int index=0;
	int ret = 0;
	struct cpufreq_freqs freqs;

	iproc_dbg("Set cpu target freq: %uKHz\n", target_freq);
	
	ret = cpufreq_frequency_table_target(policy, iproc_freq_table, target_freq, relation, &index);
	if (ret)
	{
		iproc_err("cpu%d: no matched freq for target: %d, ret=%d.\n", policy->cpu, target_freq, ret);
		return ret;
	}

	freqs.new = iproc_freq_table[index].frequency;

	if (!freqs.new)
	{
		iproc_err("cpu%d: invalid target freq: %d.\n", policy->cpu, target_freq);
		return -EINVAL;
	}

	freqs.old = iproc_getspeed(policy->cpu);
	freqs.cpu = policy->cpu;

	iproc_dbg("Adjusted target freq: %uKHz -> %uKHz\n", target_freq, freqs.new);

	if (freqs.old == freqs.new && policy->cur == freqs.new)
	{
		iproc_dbg("New freq equals old, nothing need to do for cpufreq.\n");
		return ret;
	}

	/* notifiers */
	for_each_cpu(i, policy->cpus) 
	{
		freqs.cpu = i;
		cpufreq_notify_transition(&freqs, CPUFREQ_PRECHANGE);
	}
	
	ret = iproc_do_cpufreq_scaling(&freqs);
	if (ret) 
	{
		iproc_err("Fail to do cpufreq scaling on cpu%d.\n", policy->cpu);
		return ret;
	}

	freqs.new = iproc_getspeed(policy->cpu);
	iproc_dbg("cpu%d running at %uMHz now.\n", policy->cpu, freqs.new/1000);

	/* notifiers */
	for_each_cpu(i, policy->cpus)
	{
		freqs.cpu = i;
		cpufreq_notify_transition(&freqs, CPUFREQ_POSTCHANGE);
	}

	return 0;
}

static int iproc_init(struct cpufreq_policy *policy)
{
	iproc_dbg("Initialising cpu %d policy.\n", policy->cpu);
	
	if (policy->cpu != 0)
	{
		iproc_err("Invalid cpu id: %d.\n", policy->cpu);		
		return -EINVAL;
	}
	
	policy->cur = policy->min = policy->max = iproc_getspeed(policy->cpu);
	policy->cpuinfo.min_freq = iproc_freq_table[IPROC_FREQ_500M].frequency;
	policy->cpuinfo.max_freq = iproc_freq_table[IPROC_FREQ_1000M].frequency;
//	policy->cpuinfo.transition_latency = CPUFREQ_ETERNAL;
	policy->cpuinfo.transition_latency = 10000;

	cpufreq_frequency_table_cpuinfo(policy, iproc_freq_table);


	iproc_dbg("Set cpu %d policy: \n"
		"  cur=%u min=%u max=%u \n"
		"  cpuinfo.min=%u, cpuinfo.max=%u cpuinfo.transition_latency=%u \n", 
		policy->cpu, 
		policy->cur, policy->min, policy->max, 
		policy->cpuinfo.min_freq, policy->cpuinfo.max_freq, policy->cpuinfo.transition_latency);

	return 0;
}

static int iproc_exit(struct cpufreq_policy *policy)
{
	iproc_dbg("Exit policy cpu %d.\n", policy->cpu);
	
	return 0;
}

static struct cpufreq_driver iproc_cpufreq_driver = {
	.owner		= THIS_MODULE,
	.name		= SOC_NAME,
	.flags		= CPUFREQ_STICKY,
	.init		= iproc_init,
	.verify		= iproc_verify_speed,
	.target		= iproc_target,
	.get		= iproc_getspeed,
	.exit       = iproc_exit,
};

int iproc_cpufreq_init(void)
{
	iproc_prt("Initializing cpufreq\n");

	return cpufreq_register_driver(&iproc_cpufreq_driver);
}

void iproc_cpufreq_exit(void)
{
	iproc_prt("Unloading cpufreq\n");

	cpufreq_unregister_driver(&iproc_cpufreq_driver);
}
