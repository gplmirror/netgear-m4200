/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/*
 * !!! DO NOT ADD ANY OTHER FUNCTIONS IN THIS FILE !!!
 */
#include "socregs.h"
#include "M0.h"

void MCU_set_aon_gpio_as_wakeup(unsigned int gpio_pin);
int MCU_SoC_Standby_Handler(void);
int MCU_SoC_Sleep_Handler(void);
int MCU_SoC_DeepSleep_Handler(void);

//
// !!! DO NOT CHANGE THE NAME OF THIS ROUTINE, IT IS REFERRED IN MAKEFILE !!!
//
int user_mbox_handler(void)
{
	uint32_t code0 = cpu_reg32_rd(IPROC_CRMU_MAIL_BOX0);
	uint32_t code1 = cpu_reg32_rd(IPROC_CRMU_MAIL_BOX1);

	MCU_set_wakeup_aon_gpio_output(1);
	MCU_led_print_hex(0x17);

	cpu_reg32_wr(CRMU_MCU_EVENT_CLEAR, cpu_reg32_rd(CRMU_MCU_EVENT_STATUS));//clear all mcu events
	cpu_reg32_setbit(CRMU_MCU_EVENT_CLEAR, CRMU_MCU_EVENT_CLEAR__MCU_MAILBOX_EVENT_CLR); //only clear MBOX event

	if(code0==MAILBOX_CODE0_RUN && code1==MAILBOX_CODE1_RUN)
	{
		MCU_led_print_hex(1);
		MCU_led_race(1);
		
		goto none_exit;
	}
	else if(code0==MAILBOX_CODE0_STANDBY && code1==MAILBOX_CODE1_STANDBY)
	{
		MCU_led_print_hex(2);
		MCU_led_race(2);

		MCU_SoC_Standby_Handler();
	}
	else if(code0==MAILBOX_CODE0_SLEEP && code1==MAILBOX_CODE1_SLEEP)
	{
		MCU_led_print_hex(3);
		MCU_led_race(3);

		MCU_SoC_Sleep_Handler();
	}
    else if(code0==MAILBOX_CODE0_DEEPSLEEP && code1==MAILBOX_CODE1_DEEPSLEEP)
	{
		MCU_led_print_hex(4);
		MCU_led_race(4);

		MCU_SoC_DeepSleep_Handler();
	}
    else //other mbox handlers can be added here
	{
		MCU_led_print_hex(0xa);
		MCU_led_race(0xa);
		
		goto none_exit;
	}
	
	MCU_led_on(IPROC_AON_GPIO_LED_ALL_ON);
	MCU_led_on(IPROC_AON_GPIO_LED_ALL_OFF);

	MCU_set_aon_gpio_as_wakeup(IPROC_MCU_AON_GPIO_WAKEUP_SRC);

	/* Don't use led debug after this point in this bin !!!!! */

none_exit:

	return 1;//M0 always go to sleep by executing wfi, don't care the return code
}
