/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* SoC state   <-----> Linux implementation method
 *
 *   RUN            <==>    RUN + Policy
 *   STANDBY    <==>    WFI + Policy
 *   SLEEP         <==>    Linux Suspend to Standby + Policy
 *   DEEPSLEEP <==>    Linux Suspend to Ram + Policy
 *
 *   DFS            <==>    Linux CPUfreq
 *   AVS            <==>    tbd...
 */
#include <linux/types.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/err.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <mach/io_map.h>
#include <asm/hardware/gic.h>

#include "iproc_pm.h"
#include "iproc_pm_device.h"
#include "plat/shm.h"



/* Directly mask/unmask interrupts(SGI/PPI/SPI) on GIC */
extern int iproc_raw_set_irq(int irq, int en);

#define IPROC_USE_REQUEST_REGION 0
/////////////////////////////////////////////////////////////////////////////////
int iproc_pm_debug=1;
module_param_named(debug, iproc_pm_debug, int, 0644);
MODULE_PARM_DESC(debug, "debug info enable or not (default: 1)");

static uint rtc_alarm_delay=0;
module_param_named(alarm_delay, rtc_alarm_delay, uint, 0644);
MODULE_PARM_DESC(alarm_delay, "Delay time to wakeup system by rtc alarm, in seconds, resolution 128s (default: 0, not supported)");

//A9 wakeup src from WFI, not used for MCU wakeup
static uint wakeup_src_rtc=0;
static uint wakeup_src_wdog=0;
static uint wakeup_src_timer=0;
static uint wakeup_src_sci=0;
static uint wakeup_src_aongpio=1;
static uint wakeup_src_uart=1;
module_param_named(w_rtc, wakeup_src_rtc, uint, 0644);
MODULE_PARM_DESC(w_rtc, "Wakeup by SPRU RTC alarm, default=0");
module_param_named(w_wdog, wakeup_src_wdog, uint, 0644);
MODULE_PARM_DESC(w_wdog, "Wakeup by WDog, default=0");
module_param_named(w_timer, wakeup_src_timer, uint, 0644);
MODULE_PARM_DESC(w_timer, "Wakeup by timer, default=0");
module_param_named(w_sci, wakeup_src_sci, uint, 0644);
MODULE_PARM_DESC(w_sci, "Wakeup by smart card, default=0");
module_param_named(w_gpio, wakeup_src_aongpio, uint, 0644);
MODULE_PARM_DESC(w_gpio, "Wakeup by AON_GPIO, default=1");
module_param_named(w_uart0, wakeup_src_uart, uint, 0644);
MODULE_PARM_DESC(w_uart0, "Wakeup by uart, default=1");




__asm__("\n\t"
"	.data\n\t"
"	.global M0_MBOX_BIN_START\n\t"
"M0_MBOX_BIN_START:\n\t"
"	.incbin \"../../bcmdrivers/pm/M0_mbox.bin\"\n\t"
"	.global M0_MBOX_BIN_END\n\t"
"M0_MBOX_BIN_END:\n\t"
"	.word 0x11223344\n\t"
"\n\t"
"	.global M0_WAKEUP_BIN_START\n\t"
"M0_WAKEUP_BIN_START:\n\t"
"	.incbin \"../../bcmdrivers/pm/M0_wakeup.bin\"\n\t"
"	.global M0_WAKEUP_BIN_END\n\t"
"M0_WAKEUP_BIN_END:\n\t"
"	.word 0x11223344\n\t"
"\n\t"
);

//////////////////////////////////////////////////////////////////////////////
void * __iomem m0_idram_vbase=NULL; //idram_res mapped vaddr
struct resource idram_res = {
	.name  = "M0_IDRAM",
	.start = CRUM_M0_IDRAM_START,
	.end   = CRUM_M0_IDRAM_END,
	.flags = IORESOURCE_MEM,
};

void * __iomem a9_sram_vbase=NULL; //sram mapped vaddr
unsigned char *a9_sram_save=NULL; //sram saved addr
struct resource a9_sram_res = {
	.name  = "A9_SRAM",
	.start = CRUM_A9_SRAM_START,
	.end   = CRUM_A9_SRAM_END,
	.flags = IORESOURCE_MEM,
};

static struct resource iproc_pm_resources[] = {
	/*MEM resources*/
	{
		.name  = "m0_isr_ctl",
		.start = CRMU_IRQx_vector_USER_START,
		.end   = CRMU_IRQx_vector_USER_END,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "crmu_dru",
		.start = CRMU_XTAL_CHANNEL_CONTROL,
		.end   = BSTI_COMMAND-1,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "chip_dru",
		.start = CRMU_GENPLL_CONTROL0,
		.end   = CDRU_USBPHY_P2_CTRL_0+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "crmu_wdg",
		.start = CRMU_WDT_WDOGLOAD ,
		.end   = CRMU_WDT_WDOGITOP+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "crmu_timer",
		.start = CRMU_TIM_TIMER1Load ,
		.end   = CRMU_TIM_TIMERPCellID3+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "dmu",
		.start = CRMU_STRAP_DATA,
		.end   =  CRMU_SOTP_NEUTRALIZE_ENABLE+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "bbl",
		.start = SPRU_BBL_WDATA,
		.end   = SPRU_BBL_RDATA+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "cru",
		.start = CRU_control,
		.end   = CRU_timer+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "ddr",
		.start = DDR_DENALI_CTL_00,
		.end   = DDR_PHY_BYTE_LANE_1_BL_SPARE_REG+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "asiu_icsr",
		.start = ASIU_INTR_STATUS,
		.end   = ASIU_PWM_SHUTDOWN+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "gmac0_io",
		.start = AMAC_IDM0_IO_CONTROL_DIRECT,
		.end   = AMAC_IDM0_IO_CONTROL_DIRECT+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "gmac0_rst",
		.start = AMAC_IDM0_IDM_RESET_CONTROL,
		.end   = AMAC_IDM0_IDM_RESET_CONTROL+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "gmac1_io",
		.start = AMAC_IDM1_IO_CONTROL_DIRECT,
		.end   = AMAC_IDM1_IO_CONTROL_DIRECT+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "gmac1_rst",
		.start = AMAC_IDM1_IDM_RESET_CONTROL,
		.end   = AMAC_IDM1_IDM_RESET_CONTROL+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "ihost_clk",
		.start = IHOST_PROC_CLK_WR_ACCESS,
		.end   = IHOST_PROC_RST_A9_CORE_SOFT_RSTN+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "scu",
		.start = IHOST_SCU_CONTROL,
		.end   = IHOST_SCU_SECURE_ACCESS+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "gic_cpu",
		.start = IHOST_GICCPU_CONTROL,
		.end   = IHOST_GICCPU_CPU_IDENT+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "scu_gtimer",
		.start = IHOST_GTIM_GLOB_LOW,
		.end   = IHOST_GTIM_GLOB_INCR+3,
		.flags = IORESOURCE_MEM,
	},
/*
	{
		.name  = "scu_ptimer",
		.start = IHOST_PTIM_TIMER_LOAD,
		.end   = IHOST_PTIM_WATCHDOG_DISABLE+3,
		.flags = IORESOURCE_MEM,
	},
*/
	{
		.name  = "gic_dist",
		.start = IHOST_GICDIST_enable_s,
		.end   = IHOST_GICDIST_priority_level0+3,
		.flags = IORESOURCE_MEM,
	},
	{
		.name  = "ihost_l2c",
		.start = IHOST_L2C_CACHE_ID,
		.end   = IHOST_L2C_PWR_CTRL+3,
		.flags = IORESOURCE_MEM,
	},
};
#define IPROC_RESOURCES_NUM        ARRAY_SIZE(iproc_pm_resources)

static IPROC_Resources_t IPROC_PM_RES[IPROC_RESOURCES_NUM];

static iproc_power_status_e current_pm_state=IPROC_PM_STATE_END;

static char *iproc_pm_states_str[] = {
	[IPROC_PM_STATE_RUN]        = "RUN",
	[IPROC_PM_STATE_STANDBY]    = "STANDBY",
	[IPROC_PM_STATE_SLEEP]      = "SLEEP",
	[IPROC_PM_STATE_DEEPSLEEP]  = "DEEPSLEEP",
	[IPROC_PM_STATE_END]        = "UNKNOW",
};

static char *iproc_mcu_irq_desc[] = {
	[MCU_MAILBOX_EVENT]         = "MAILBOX",
	[MCU_IPROC_STANDBYWFE_EVENT]= "STANDBYWFE",
	[MCU_IPROC_STANDBYWFI_EVENT]= "STANDBYWFI",
	[MCU_AON_GPIO_INTR]         = "AON_GPIO",
	[MCU_TIMER_INTR]            = "TIMER",
	[MCU_WDOG_INTR]             = "WDOG",
	[MCU_SPRU_ALARM_EVENT]      = "SPRU_ALARM",
	[MCU_INTR_EVENT_END]        = "UNKNOW",
};

IPROC_M0_ISR_ARGS_t m0_isr_args;
static uint32_t iproc_gicdist_enable_clr_regs[8];
static uint32_t iproc_gicdist_enable_set_regs[8];

static DEFINE_SPINLOCK(reg_lock);

/////////////////////////////////////////////////////////////////////////////
static uint32_t iproc_pm_reg32_get_vaddr(const uint32_t reg)
{
	uint32_t i;
	uint32_t address=0;
	IPROC_Resources_t *pRes=NULL;

	for(i=0; i<IPROC_RESOURCES_NUM; i++)
	{
		pRes = &IPROC_PM_RES[i];
	
		if(!pRes->res || !pRes->vaddr)
		{
//			iproc_err("Resource %d (%s) not initialized!\n", i, pRes->res->name);
			continue ;
		}

		if(pRes->res->flags != IORESOURCE_MEM)
		{
//			iproc_err("Resource %d (%s) is not MEM!\n", i, pRes->res->name);
			continue ;
		}

		if(reg >= pRes->res->start && reg <= pRes->res->end)
		{
			address = ((uint32_t)pRes->vaddr + (reg - pRes->res->start));
			break;
		}
	}

	return address;
}

uint32_t iproc_reg32_read(const uint32_t addr)
{
	uint32_t val;
	uint32_t address = iproc_pm_reg32_get_vaddr(addr);
	
	if(!address)
	{
		iproc_err("Invalid reg addr: 0x%08x, caller: [%pS]\n", addr, __builtin_return_address(0));
		return 0;
	}
	
	spin_lock(&reg_lock);
	val = ioread32(address);
	spin_unlock(&reg_lock);

	return val;
}

void iproc_reg32_write(const uint32_t addr, const uint32_t value)
{
	uint32_t address = iproc_pm_reg32_get_vaddr(addr);
	
	if(!address)
	{
		iproc_err("Invalid reg addr: 0x%08x, caller: [%pS]\n", addr, __builtin_return_address(0));
		return ;
	}
	
	spin_lock(&reg_lock);
	iowrite32(value, address);
	spin_unlock(&reg_lock);
}

/*
 * Initialize one IORESOURCE_MEM resource, return the iomapped vaddr.
 */
static uint32_t *iproc_resource_init_one(struct platform_device *pldev, const char *name)
{
	void * __iomem reg_base;
	struct resource *res = NULL;
	struct resource *iomem_res = NULL;

	if(!pldev || !name)
	{
		iproc_err("Invalid arg!\n");
		return NULL;
	}
	
	res = iproc_platform_get_resource_byname(pldev, IORESOURCE_MEM, name);
	if(res == NULL)
	{
		iproc_err("Failed to get resource by name `%s'!\n", name);
		return NULL;
	}

	if(res->start > res->end)
	{
		iproc_err("Invalid resource `%s'[0x%08x~0x%08x]!\n", name, res->start, res->end);
		return NULL;
	}

#if IPROC_USE_REQUEST_REGION
	iomem_res = request_mem_region(res->start, resource_size(res), pldev->name);
	if(iomem_res == NULL)
	{
		iproc_err("Failed to request_mem_region for resource `%s'!\n", name);
		return NULL;
	}
#endif

	reg_base = ioremap_nocache(res->start, resource_size(res));
	if(reg_base == NULL)
	{
		iproc_err("Failed to do ioremap for resource `%s'!\n", name);
		return NULL;
	}

	return (uint32_t *)reg_base;
}

static int iproc_pm_resource_init(struct platform_device *pldev)
{
	int i=0, j=0, ret=0;
	uint32_t * __iomem mapped_addr;
	struct resource *r=NULL;
	IPROC_Resources_t *pRes=NULL;

	//Common resources init
	for(i=0; i<IPROC_RESOURCES_NUM; i++)
	{
		pRes = &IPROC_PM_RES[i];
		r = &iproc_pm_resources[i];

		pRes->res = r;
		pRes->vaddr = 0;

		if(r->flags != IORESOURCE_MEM)
			continue;
		
		mapped_addr = iproc_resource_init_one(pldev, r->name);

		if(!mapped_addr)
		{
			ret = -ENXIO;
			break;
		}
		
		pRes->vaddr = mapped_addr;

		iproc_dbg("Resource %02d[%10s: 0x%08x~0x%08x/0x%08x] mapped to 0x%p\n", i, pRes->res->name, pRes->res->start, pRes->res->end, resource_size(pRes->res), pRes->vaddr);

		j++;
	}

	//IDRAM resource init
#if IPROC_USE_REQUEST_REGION
	BUG_ON(request_resource(&iomem_resource, &idram_res));
#endif
	m0_idram_vbase = ioremap(idram_res.start, resource_size(&idram_res));
	BUG_ON(IS_ERR_OR_NULL(m0_idram_vbase));
	iproc_dbg("Resource %02d[%10s: 0x%08x~0x%08x/0x%08x] mapped to 0x%p\n", i, idram_res.name, idram_res.start, idram_res.end, resource_size(&idram_res), m0_idram_vbase);

	//SRAM resource init
#if IPROC_USE_REQUEST_REGION
	BUG_ON(request_resource(&iomem_resource, &a9_sram_res));
#endif
	i++, j++;
	a9_sram_vbase = ioremap(a9_sram_res.start, resource_size(&a9_sram_res));
	BUG_ON(IS_ERR_OR_NULL(a9_sram_vbase));
	iproc_dbg("Resource %02d[%10s: 0x%08x~0x%08x/0x%08x] mapped to 0x%p\n", i, a9_sram_res.name, a9_sram_res.start, a9_sram_res.end, resource_size(&a9_sram_res), a9_sram_vbase);
	a9_sram_save = (unsigned char *)kmalloc(resource_size(&a9_sram_res), GFP_KERNEL);
	if(!a9_sram_save)
	{
		iproc_err("Failed to malloc sram save, size:0x%x\n", resource_size(&a9_sram_res));
		return -ENOMEM;
	}
	iproc_dbg("%d MEM resources initialized\n", ++j);

	return ret;
}

static int iproc_pm_resource_exit(void)
{
	int i=0;
	IPROC_Resources_t *pRes=NULL;

	iproc_prt("Free resources!\n");

	//Common resources release
	for(i=0; i<IPROC_RESOURCES_NUM; i++)
	{
		pRes = &IPROC_PM_RES[i];
		
		if(pRes->res && pRes->res->flags==IORESOURCE_MEM && pRes->vaddr)
		{
			iounmap(pRes->vaddr);
#if IPROC_USE_REQUEST_REGION
			release_mem_region(pRes->res->start, resource_size(pRes->res));
#endif
			iproc_dbg("  Resource %d freed, vaddr 0x%p.\n", i, pRes->vaddr);
		}
	}

	//IDRAM resource release
	iounmap(m0_idram_vbase);
#if IPROC_USE_REQUEST_REGION
	release_resource(&idram_res);
#endif

	//SRAM resource release
	kfree(a9_sram_save);
	iounmap(a9_sram_vbase);
#if IPROC_USE_REQUEST_REGION
	release_resource(&a9_sram_res);
#endif

	return 0;
}

//////////////////////////////////////////////////////////////////////////////
void iproc_pm_save_device_regs(struct iproc_pm_device_regs *pm_dev)
{
	int i=0;
	struct iproc_pm_reg *saveReg = NULL;

	iproc_dbg("Save %s regs:\n", pm_dev->devname);
	
	for (i=0; i<pm_dev->regs_num; i++)
	{
		saveReg = *(pm_dev->regs) + i;
		saveReg->regval= pm_dev->read((uint32_t)(saveReg->regaddr));
		iproc_dbg("    [0x%p]=0x%08lx\n", saveReg->regaddr, saveReg->regval);
	}
}
EXPORT_SYMBOL(iproc_pm_save_device_regs);

void iproc_pm_restore_device_regs(struct iproc_pm_device_regs *pm_dev)
{
	int i=0;
	struct iproc_pm_reg *saveReg = NULL;

	iproc_dbg("Restore %s regs:\n", pm_dev->devname);

	for (i=0; i<pm_dev->regs_num; i++)
	{
		saveReg = *(pm_dev->regs) + i;

		iproc_dbg("    [0x%p]=0x%08lx\n", saveReg->regaddr, saveReg->regval);
		pm_dev->write((uint32_t)(saveReg->regaddr), (uint32_t)(saveReg->regval));
	}
}
EXPORT_SYMBOL(iproc_pm_restore_device_regs);
//////////////////////////////////////////////////////////////////////////////

void iproc_pm_set_a9_sram(int save)
{
	if(save)//save
	{
		iproc_dbg("A9 sram saved.\n");
		memcpy(a9_sram_save, a9_sram_vbase, resource_size(&a9_sram_res));
	}
	else//restore
	{
		iproc_dbg("A9 sram restored.\n");
		memcpy(a9_sram_vbase, a9_sram_save, resource_size(&a9_sram_res));
	}
}

/*Set the 1 to 6 aon gpio led on*/
void iproc_set_aon_gpio_led(iproc_gpio_status_e gpio_pin)
{
	uint32_t outval = 0;
	
	iproc_reg32_write(CRMU_IOMUX_CONTROL, (iproc_reg32_read(CRMU_IOMUX_CONTROL)|1));
	iproc_reg32_write(GP_DATA_OUT, 0x3f);
	iproc_reg32_write(GP_OUT_EN, 0x3f);
	
	if(gpio_pin==IPROC_AON_GPIO_LED_ALL_OFF)
	{
		iproc_dbg("Set aon gpio led: all off\n");
		outval = 0x3f;
	}
	else if(gpio_pin==IPROC_AON_GPIO_LED_ALL_ON)
	{
		iproc_dbg("Set aon gpio led: all on\n");
		outval = 0x00;
	}
	else if(gpio_pin<IPROC_AON_GPIO_LED_SINGLE_END)
	{
		iproc_dbg("Set aon gpio led: %d on\n", gpio_pin);
		outval = ~(iproc_bit_mask(gpio_pin));
	}
	else
	{
		iproc_err("Invalid gpio led to set: %d!\n", gpio_pin);
		return ;
	}

	iproc_reg32_write(GP_DATA_OUT, outval);

	mdelay(100);	
}

char *iproc_get_filename_in_path(const char *path)
{
	char *ptr = (char *)path;
	
	while (*ptr != '\0')
		ptr++;
	
	while (*ptr != '/')
	{
		if(ptr==path)
			return ptr;
		
		 ptr--;
	}
	
	return (ptr+1);
}

int iproc_get_msglevel(void)
{
#ifdef CONFIG_PM_DEBUG
	return iproc_pm_debug;
#else
	return 0;
#endif
}

iproc_power_status_e iproc_get_current_pm_state(void)
{
	return current_pm_state;
}

void iproc_set_current_pm_state(iproc_power_status_e new_state)
{
	iproc_dbg("Set curret pm state to %s\n", iproc_get_pm_state_str_by_id(new_state));
	
	current_pm_state = new_state;
}

char *iproc_get_pm_state_str_by_id(iproc_power_status_e stateid)
{
	if(stateid<=IPROC_PM_STATE_END)
		return iproc_pm_states_str[stateid];
	else
		return NULL;
}

char *iproc_get_mcu_irq_desc_by_id(IPROC_MCU_IRQ_EVENT_NUM_e irq)
{
	char *ret=NULL;

	if(irq<MCU_INTR_EVENT_END)
	{
		ret=iproc_mcu_irq_desc[irq];
		
		if(ret)
			return ret;
	}

	return "UNKNOW";
}

uint iproc_get_rtc_alarm_delay(void)
{
	uint delay = rtc_alarm_delay;
	
	if(delay%128==0)
		return delay;
	else
	{
		iproc_err("Delay of rtc alarm must be 128 aligned, current is %u\n", delay);
		return 0;
	}
}

static void iproc_dump_gic_regs(char *desc)
{
	int i;
	iproc_dbg("%s", desc);
	
	for(i=0;i<8;i++)
		iproc_dbg("  IHOST_GICDIST_enable_set%d=0x%08x\n", i, iproc_reg32_read(IHOST_GICDIST_enable_set0+i*4));

	for(i=0;i<8;i++)
		iproc_dbg("  IHOST_GICDIST_enable_clr%d=0x%08x\n", i, iproc_reg32_read(IHOST_GICDIST_enable_clr0+i*4));
}

static void iproc_save_gic_regs(int save)
{
	int i=0,j=0;
	uint32_t irq=0, bit=0;
	
	if(save)//save
	{
		iproc_dbg("Save current GIC interrupt states\n");
		for(i=0;i<8;i++)
		{
			iproc_gicdist_enable_set_regs[i]=iproc_reg32_read(IHOST_GICDIST_enable_set0+i*4);
			iproc_gicdist_enable_clr_regs[i]=iproc_reg32_read(IHOST_GICDIST_enable_clr0+i*4);
		}
	}
	else//restore
	{
		iproc_dbg("Restore GIC interrupt states\n");
		for(i=0;i<8;i++)
		{
			for(j=0;j<32;j++)
			{
				irq = i*32+j;
				bit = (iproc_gicdist_enable_set_regs[i]>>j)&0x1;
				iproc_raw_set_irq(irq, bit);
			}
		}
	}
}

int iproc_set_ihost_wakeup_irqs(int en)
{
	int i=0,ret=0;
	char *str=NULL;
	unsigned long flags=0;

//	iproc_dump_gic_regs("Current GIC reg values:\n");

	if(en==IPROC_DISABLED)
	{
		local_irq_save(flags);
		
		iproc_dbg("Mask all ihost interrupts except wakeup irqs\n");

		/*1: save
		       Only STANDBY needs to save/restore these regs. SLEEP & DEEPSLEEP can auto call: 
			pm_suspend()-> syscore_suspend() -> cpu_pm_suspend()
				-> cpu_pm_enter()-> gic_cpu_save()
				-> cpu_cluster_pm_enter()-> gic_dist_save()
			However, do a more save/restore for SLEEP/DEEPSLEEP shouldn't be harmful
		*/
		iproc_save_gic_regs(1); //save GIC regs

		/*2: mask all*/
		for(i=0;i<256;i++)//ALL, should start from 32 which ignores SGIs and PPIs ???
		{
			iproc_raw_set_irq(i, IPROC_DISABLED); //system timer irq: BCM_INT_ID_PPI11
		}
				
		/*3: only unmask user specified A9 wakeup irqs*/
		if(wakeup_src_uart) //BCM_INT_ID_CCB_UART0
		{
			iproc_dbg("Unmask ihost uart irqs\n");
			iproc_raw_set_irq(105, IPROC_ENABLED);
			iproc_raw_set_irq(106, IPROC_ENABLED);
			iproc_raw_set_irq(107, IPROC_ENABLED);
			iproc_raw_set_irq(108, IPROC_ENABLED);//UART3 on COMBO board
			iproc_raw_set_irq(109, IPROC_ENABLED);
		}

		if(wakeup_src_sci)
		{
			iproc_dbg("Unmask ihost smartcard irqs\n");
			iproc_raw_set_irq(193, IPROC_ENABLED);
			iproc_raw_set_irq(194, IPROC_ENABLED);
			iproc_raw_set_irq(195, IPROC_ENABLED);
		}
		/*
		if(wakeup_src_rtc)
		{
			iproc_dbg("Unmask ihost rtc irqs\n");
			iproc_raw_set_irq(164,  IPROC_ENABLED);   //SPRU_RTC=164 ? SPRU_RTC_PERIODIC=174 ?
		}

		if(wakeup_src_wdog)
		{
			iproc_dbg("Unmask ihost wdog irqs\n");
			iproc_raw_set_irq(126,  IPROC_ENABLED);
		}
		
		if(wakeup_src_timer)
		{
			iproc_dbg("Unmask ihost rtc irqs\n");
			iproc_raw_set_irq(,  IPROC_ENABLED);
		}
		
		if(wakeup_src_aongpio) //AON_GPIO
		{
			iproc_dbg("Unmask ihost aon gpio irqs\n");
			iproc_raw_set_irq(, IPROC_ENABLED);
		}
		*/
		local_irq_restore(flags);
	}
	else
	{
		//restore GIC regs
		iproc_save_gic_regs(0);
	}
	
//	iproc_dump_gic_regs("New GIC reg values:\n");

	return 0;
}

//The MCU wakeup sources are basically fixed, no need be configurable,
//so need not pass this value to MCU as ISR args
static uint iproc_init_mcu_wakeup_src(void)
{
	return MCU_WAKEUP_SOURCE;
}

int iproc_get_valid_mcu_wakeup_src(void)
{
	uint init_src = iproc_init_mcu_wakeup_src();
	uint delay    = iproc_get_rtc_alarm_delay();
	uint rtc_bit  = (1<<MCU_SPRU_ALARM_EVENT);
	

	if((init_src & rtc_bit) == rtc_bit && delay==0)
	{
		iproc_err("Rtc alarm used as MCU wakeup source but delay is 0! Ignored!\n");
		init_src &= ~rtc_bit;
	}
	
	if(!init_src)
	{
		iproc_err("No valid MCU wakeup source!\n");
		return -1;
	}
	
	return init_src;
}

int iproc_check_mcu_wakeup_src_supported(IPROC_WAKEUP_SRC_t ws)
{
	uint valid_src = iproc_get_valid_mcu_wakeup_src();
	uint rtc_bit   = (1<<MCU_SPRU_ALARM_EVENT);
	uint wdog_bit  = (1<<MCU_WDOG_INTR);
	uint timer_bit = (1<<MCU_TIMER_INTR);
	uint gpio_bit  = (1<<MCU_AON_GPIO_INTR);
	
	switch(ws)
	{
		case ws_alarm: return ((valid_src & rtc_bit)   == rtc_bit);
		case ws_wdog:  return ((valid_src & wdog_bit)  == wdog_bit);
		case ws_timer: return ((valid_src & timer_bit) == timer_bit);
		case ws_gpio:  return ((valid_src & gpio_bit)  == gpio_bit);
		default:
			iproc_err("Not supported wakeup source.\n");
			return 0;
	}
}

/////////////////////////////////////////////////////////////////////////////////
static void iproc_pm_dev_release(struct device *dev)
{
	iproc_dbg("Release device.\n");
	return ;
}

static struct platform_device iproc_pm_pdev = {
	.name   = DRV_NAME,
	.id	    = -1,
	.dev	= {
		.release = iproc_pm_dev_release,
	},
	.resource		 = iproc_pm_resources,
	.num_resources	 = IPROC_RESOURCES_NUM,
};


static int __init iproc_pm_init(void)
{
	int err=-1;

	iproc_dbg("Loading PM driver!\n");
	
	err = iproc_platform_device_register(&iproc_pm_pdev);
	if(err)
	{
		iproc_err("Failed to register platform device, err:%d\n", err);
		return err;
	}
	
	if((err = iproc_pm_resource_init(&iproc_pm_pdev)))
	{
		iproc_err("Failed to init resources!\n");
		goto err_free_res;
	}

	if((err=iproc_get_valid_mcu_wakeup_src())<0)
	{
		iproc_err("Invalid module parameters!");
		goto err_unregister_dev;
	}
	
//	iproc_dump_m0_isr_bins(0, MCU_INTR_EVENT_END);
//	iproc_dump_m0_idram();

	if((err=iproc_install_m0_isr_bins(1))<0)
	{
		iproc_err("Fail to install m0 ISR bins!");
		goto err_unregister_dev;
	}
	iproc_dbg("New M0 ISR bins installed.\n");

#if IPROC_BUILD_X(CONFIG_CYGNUS_PM_DFS)
	if((err=iproc_cpufreq_init())<0)
	{
		iproc_err("Fail to init Linux Cpufreq!");
		goto err_uninstall_m0_isr_bins;
	}
	iproc_dbg("Linux Cpufreq framework supported.\n");
#endif

	if(iproc_suspend_init()<0)
	{
		iproc_err("Fail to init Linux Suspend!");
		goto err_cpufreq_exit;
	}
	iproc_dbg("Linux Suspend framework supported.\n");

	if(iproc_pm_sysfs_register(&iproc_pm_pdev)<0)
	{
		iproc_err("Fail to init sysfs!");
		goto err_suspend_exit;
	}
	iproc_dbg("All sysfs files added.\n");

	iproc_save_ddr_regs_in_idram();
	iproc_dbg("DDR regs saved in IDRAM.\n");

	iproc_dump_resume_entry();
	
	iproc_set_aon_gpio_led(IPROC_AON_GPIO_LED_ALL_ON);

	iproc_prt("Cyngnus PM initialized, version %s\n", DRV_VER);
	iproc_dbg("Build time:%s, %s.\n", __DATE__, __TIME__);

	return 0;

err_suspend_exit:
	iproc_suspend_exit();
err_cpufreq_exit:
#if IPROC_BUILD_X(CONFIG_CYGNUS_PM_DFS)
	iproc_cpufreq_exit();
#endif
err_uninstall_m0_isr_bins:
	iproc_install_m0_isr_bins(0);
err_free_res:
	iproc_pm_resource_exit();
err_unregister_dev:
	iproc_platform_device_unregister(&iproc_pm_pdev);
	return err;
}

static void __exit iproc_pm_exit(void)
{
	iproc_prt("Unloading PM driver.\n");

	iproc_set_aon_gpio_led(IPROC_AON_GPIO_LED_ALL_OFF);
	iproc_pm_sysfs_unregister(&iproc_pm_pdev);

#if IPROC_BUILD_X(CONFIG_CYGNUS_PM_DFS)
	iproc_cpufreq_exit();
#endif
	iproc_suspend_exit();
	iproc_install_m0_isr_bins(0);
	iproc_pm_resource_exit();
	iproc_platform_device_unregister(&iproc_pm_pdev);
}	

module_init(iproc_pm_init);
module_exit(iproc_pm_exit);

MODULE_VERSION(DRV_VER);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("An Luo, <an.luo@broadcom.com>");
MODULE_DESCRIPTION("Cygnus SoC Power Management Driver");
