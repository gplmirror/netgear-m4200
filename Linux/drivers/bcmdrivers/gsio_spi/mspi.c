/*
 * $Id: mspi.c 1.18 Broadcom SDK $
 * $Copyright: Copyright 2012 Broadcom Corporation.
 * This program is the proprietary software of Broadcom Corporation
 * and/or its licensors, and may only be used, duplicated, modified
 * or distributed pursuant to the terms and conditions of a separate,
 * written license agreement executed between you and Broadcom
 * (an "Authorized License").  Except as set forth in an Authorized
 * License, Broadcom grants no license (express or implied), right
 * to use, or waiver of any kind with respect to the Software, and
 * Broadcom expressly reserves all rights in and to the Software
 * and all intellectual property rights therein.  IF YOU HAVE
 * NO AUTHORIZED LICENSE, THEN YOU HAVE NO RIGHT TO USE THIS SOFTWARE
 * IN ANY WAY, AND SHOULD IMMEDIATELY NOTIFY BROADCOM AND DISCONTINUE
 * ALL USE OF THE SOFTWARE.  
 *  
 * Except as expressly set forth in the Authorized License,
 *  
 * 1.     This program, including its structure, sequence and organization,
 * constitutes the valuable trade secrets of Broadcom, and you shall use
 * all reasonable efforts to protect the confidentiality thereof,
 * and to use this information only in connection with your use of
 * Broadcom integrated circuit products.
 *  
 * 2.     TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS
 * PROVIDED "AS IS" AND WITH ALL FAULTS AND BROADCOM MAKES NO PROMISES,
 * REPRESENTATIONS OR WARRANTIES, EITHER EXPRESS, IMPLIED, STATUTORY,
 * OR OTHERWISE, WITH RESPECT TO THE SOFTWARE.  BROADCOM SPECIFICALLY
 * DISCLAIMS ANY AND ALL IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY,
 * NONINFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF VIRUSES,
 * ACCURACY OR COMPLETENESS, QUIET ENJOYMENT, QUIET POSSESSION OR
 * CORRESPONDENCE TO DESCRIPTION. YOU ASSUME THE ENTIRE RISK ARISING
 * OUT OF USE OR PERFORMANCE OF THE SOFTWARE.
 * 
 * 3.     TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT SHALL
 * BROADCOM OR ITS LICENSORS BE LIABLE FOR (i) CONSEQUENTIAL,
 * INCIDENTAL, SPECIAL, INDIRECT, OR EXEMPLARY DAMAGES WHATSOEVER
 * ARISING OUT OF OR IN ANY WAY RELATING TO YOUR USE OF OR INABILITY
 * TO USE THE SOFTWARE EVEN IF BROADCOM HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES; OR (ii) ANY AMOUNT IN EXCESS OF
 * THE AMOUNT ACTUALLY PAID FOR THE SOFTWARE ITSELF OR USD 1.00,
 * WHICHEVER IS GREATER. THESE LIMITATIONS SHALL APPLY NOTWITHSTANDING
 * ANY FAILURE OF ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.$
 *
 * File:        mspi.c
 * Purpose:     SPI Master mode Implementation
 * Requires:    
 */

#include <linux/io.h>
#include <linux/delay.h>
#include <linux/mspi.h>

static void * baseAddr, *gpioBaseAddr;

#define R_REG(reg)       ioread32(baseAddr + (reg&0xffff))
#define W_REG(reg, val)        iowrite32(val, baseAddr + (reg&0xffff))

#define GPIO_R_REG(reg)       ioread32(gpioBaseAddr + (reg&0xffff))
#define GPIO_W_REG(reg, val)        iowrite32(val, gpioBaseAddr + (reg&0xffff))

int max342xInit(void);

int soc_mspi_init(void) {
    uint32_t rval;

    baseAddr = ioremap(CMIC_BASE, 0x40000);
    gpioBaseAddr = ioremap(IPROC_CCA_REG_BASE, 0x1000);

    rval = R_REG(CMIC_OVERRIDE_STRAP);
    rval |= 0x24;
    W_REG(CMIC_OVERRIDE_STRAP, rval);

    rval = R_REG(CMICM_BSPI_MAST_N_BOOT);
    rval |= 0x01;
    W_REG(CMICM_BSPI_MAST_N_BOOT, rval);
    /* 
     * Set speed and transfer size
     */
    rval = R_REG(MSPI_SPCR0_LSB);
    rval = 0x08;
    W_REG(MSPI_SPCR0_LSB, rval);
    
    /* rval = R_REG(MSPI_SPCR0_MSB);
    rval |= 0x08 << 2;
    W_REG(MSPI_SPCR0_MSB, rval);*/

    rval = R_REG(MSPI_SPCR1_LSB);
    rval |= 0x80;
    W_REG(MSPI_SPCR1_LSB, rval);

    rval = R_REG(MSPI_SPCR1_MSB);
    rval |= 0x80;
    W_REG(MSPI_SPCR1_MSB, rval);

    rval = R_REG(GPIO_OUTEN);
    rval |= 0x08;
    W_REG(GPIO_OUTEN, rval);

    rval = R_REG(GPIO_OUT);
    rval |= (1 << 3);
    W_REG(GPIO_OUT, rval);


    return 0;
}

#define NUM_CDRAM 16
#define MIN(x,y) ((x<y)?x:y)

int soc_mspi_writeread8(uint8_t *wbuf, int wlen, uint8_t *rbuf, int rlen)
{
    int i, tlen, rv = -1;
    uint8_t *wdatptr;
    uint8_t *rdatptr;
    uint8_t *datptr;
    uint32_t rval=0;
    unsigned int chunk;
    unsigned int  queues;
    int bytes;


    rval = R_REG(MSPI_SPCR0_MSB);
    rval |= 0x00000020;
    W_REG(MSPI_SPCR0_MSB, rval);

    W_REG(MSPI_STATUS, 0x00);

    #if 1
    bytes=wlen + rlen;
    tlen = wlen ;
    rdatptr = rbuf;
    wdatptr = wbuf;
    while(bytes){

	W_REG(MSPI_STATUS, 0x00);    
        chunk = MIN(bytes, NUM_CDRAM );
        queues = chunk;
        bytes -= chunk;
        
        if ((wbuf != NULL) && (tlen > NUM_CDRAM)) {
            udelay(1000);
            for (i=0; i<chunk; i++){
		W_REG((MSPI_TXRAM_BASE + 4*(2*i)), (uint32_t) *wdatptr);
                wdatptr++;
            }
        }else if ((wbuf != NULL) && (tlen >0)){
            udelay(1000);
            for (i=0; i<chunk; i++) {
                /* Use only Even index of TXRAM for 8 bit xmit */
                if(i<tlen){
		    W_REG((MSPI_TXRAM_BASE + 4*(2*i)), (uint32_t) *wdatptr);
                    wdatptr++;
                }else{
		    W_REG((MSPI_TXRAM_BASE + 4*(2*i)), (uint32_t) 0xff);
                }
            }
        }else{
            for(i=0;i<chunk;i++)
		W_REG((MSPI_TXRAM_BASE + 4*(2*i)), (uint32_t) 0xff);
        }

        for (i=0; i<chunk; i++) {
            /* Release CS ony on last byute */
            if(bytes==0)
		W_REG((MSPI_CDRAM_BASE + (4*i)), (i == (chunk-1)) ? 0 : 0x80);
            else
		W_REG((MSPI_CDRAM_BASE + (4*i)), 0x80);
        }

        /* Set queue pointers */
	W_REG(MSPI_NEWQP, 0);
	W_REG(MSPI_ENDQP, (chunk-1));

        /* Start SPI transfer */
        rval = 0xc0; /* SPE=1, SPIFIE=WREN=WRT0=LOOPQ=HIE=HALT=0 */
	W_REG(MSPI_SPCR2, rval);

        rval = 0;
 
        do {
            rval ++;
	    if (R_REG(MSPI_STATUS) & 0x1) {
                rv = 0;
                break;
            }
        } while(rval < 10000);

        if (rv == -1) {
            return -1;
        }

        if(tlen >NUM_CDRAM){
            tlen -=chunk;
        }else{
        
            if ((rbuf != NULL) && (rlen > 0)) {
            /* CMLAI workarround delay time*/
                udelay(2000);

                for (i=tlen; i<chunk; i++) {
                    /* Use only Odd index of RXRAM for 8 bit Recv */
		    *rdatptr = (uint8_t) (R_REG(MSPI_RXRAM_BASE + 4*(2*i+1)) & 0xff);
                    rdatptr++;
                }
            }
            tlen =0;
        }
    }
    #endif
    return 0;
}

int soc_mspi_read8(uint8_t *buf, int len)
{
    return  soc_mspi_writeread8(NULL, 0, buf, len);
}

int soc_mspi_write8(uint8_t *buf, int len)
{
    return  soc_mspi_writeread8(buf, len, NULL, 0);
}

void max_spi_ss_low(void){
    uint32_t rval;
    rval = R_REG(GPIO_OUT);
    rval &= ~(1 << 3);
    W_REG(GPIO_OUT, rval);

    return;
}

void max_spi_ss_high(void){
    uint32_t rval;

    rval = R_REG(GPIO_OUT);
    rval |= (1 << 3);
    W_REG(GPIO_OUT, rval);

    return;
}


unsigned char max_spi_write(unsigned char *datbuf,int datlen)
{
    return soc_mspi_write8(datbuf,datlen);
}


unsigned char max_spi_read(unsigned char *datbuf,int datlen)
{
    return soc_mspi_read8(datbuf,datlen);
}

unsigned char max_spi_rw(unsigned char *wbuf,int wlen,unsigned char *rbuf,int rlen)
{
    return soc_mspi_writeread8(wbuf,wlen,rbuf,rlen);
}


EXPORT_SYMBOL(soc_mspi_read8);
EXPORT_SYMBOL(soc_mspi_write8);
EXPORT_SYMBOL(max_spi_ss_low);
EXPORT_SYMBOL(max_spi_ss_high);


