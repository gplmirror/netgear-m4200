/*
 *  This program is free software; upcan redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 */
#include <linux/compat.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/watchdog.h>
#include <linux/miscdevice.h>
#include <linux/poll.h>
#include <linux/io.h>
#include <linux/delay.h>

/* Kernel timer is not required for boards having granularity more */
#if defined(CONFIG_IPROC_IPM6100_BOARD)    || defined(CONFIG_IPROC_IPM6100_VEGA_BOARD) || defined(CONFIG_IPROC_IPM4300_R2_BOARD) || defined(CONFIG_IPROC_IPM4300_NS_BOARD) || defined(CONFIG_IPROC_IPM4200_BOARD)
static int  timer_needed = 0;
#else
static int  timer_needed = 1;
#endif

static unsigned long watchdog_timer_interval=500; /* 500 ms*/
static long watchdog_counter ;

struct timer_list watchdog_timer;
static long watchdog_timeout= 300;
static int wdt_running=0;

/*
** Platform specific extern APIs
*/
extern void watchdog_disable(void);
#ifdef CONFIG_FXC_P2020
extern void watchdog_setup(void);
#endif
extern int watchdog_enable(void);
extern void watchdog_service(void);
extern int watchdog_get_timer_interval(void);

/*
** Timer Function for punching watchdog
*/
static void watchdog_timer_func(unsigned long arg) 
{
	if (wdt_running) {
		if (watchdog_counter > 0) {
			watchdog_service();
			watchdog_counter--;
		}
		else {
			printk (KERN_ALERT "WATCHDOG COUNTDOWN EXPIRED.\n");
		}
	}
	else {
#ifdef CONFIG_FXC_P2020
		/* actively service watchdog when not enabled at app level */
		watchdog_service();
#endif
	}
	init_timer(&watchdog_timer);
  	watchdog_timer.function = &watchdog_timer_func;
	watchdog_timer_interval = watchdog_get_timer_interval();
	watchdog_timer.expires = jiffies + (HZ*watchdog_timer_interval/1000);
	add_timer(&watchdog_timer);
}

/*
** Gets current watchdog timeout period
*/
static unsigned long get_watchdog_timeout(void)
{
	return watchdog_timeout;
}
/*
** Updates watchdog timeout period
*/
static int set_watchdog_timeout(unsigned long new_heartbeat)
{
	watchdog_timeout  = new_heartbeat;
	return 0;
}

/*
** Refreshes the watchdog counter
*/
static void wdt_keepalive(void)
{
	if (!timer_needed) {
		watchdog_service();
		return;
	}

	if(wdt_running){
		watchdog_counter = (watchdog_timeout * 1000)/watchdog_timer_interval; 
	}
}

/*
** stops the watchdog functioanlity- (Disables in the hardware)
*/
static void wdt_stop(void)
{
	if(wdt_running){
		watchdog_disable();
		wdt_running = 0;
	}
}

/*
** Starts watchdog functionality.
*/
static int wdt_start(void)
{
	int ret = 0;
	if(!wdt_running){
		ret = watchdog_enable();
		if(ret != 0) {
			return ret;
		}
		wdt_running = 1;
		wdt_keepalive();
		watchdog_service();
	}
	return ret;
}
/*
 * File ops
 */

/**
 * 	watchdog_ioctl - Query Device
 * 	@file: file handle of device
 * 	@cmd: watchdog command
 * 	@arg: argument
 *
 * 	Query basic information from the device or ping it, as outlined by the
 * 	watchdog API.
 */
static long watchdog_ioctl (struct file *file, unsigned int cmd, unsigned long arg)
{
	int new_heartbeat;
	int options, retval = -EINVAL;

	switch (cmd) {
		case WDIOC_SETOPTIONS:
			if (get_user(options, (int *)arg))
				return -EFAULT;

			if (options & WDIOS_DISABLECARD) {
				wdt_stop();
				retval = 0;
			}

			if (options & WDIOS_ENABLECARD) {
				retval = wdt_start();
			}

			return retval;
		case WDIOC_KEEPALIVE:
			wdt_keepalive();
			return 0;
		case WDIOC_SETTIMEOUT:
			if (get_user(new_heartbeat, (int *)arg))
				return -EFAULT;

			if (set_watchdog_timeout(new_heartbeat))
				return -EINVAL;

			wdt_keepalive();
			/* Fall */
		case WDIOC_GETTIMEOUT:
			return put_user(get_watchdog_timeout(), (int *)arg);
		default:
			return -ENOTTY;
	}
	return 0;
}

#ifdef CONFIG_COMPAT
static long
watchdog_compat_ioctl (struct file *filp, unsigned int cmd, unsigned long arg)
{
  printk (KERN_DEBUG "watchdog_compat_ioctl called.\n");
  if (_IOC_NR (cmd) <= 3 && _IOC_SIZE (cmd) == sizeof (compat_uptr_t))
    {
      cmd &= ~(_IOC_SIZEMASK << _IOC_SIZESHIFT);
      cmd |= sizeof (void *) << _IOC_SIZESHIFT;
    }
  return watchdog_ioctl (filp, cmd, (unsigned long) compat_ptr (arg));
}
#else
#define watchdog_compat_ioctl NULL
#endif

/*
** Watchdog Open File operation 
*/
static int
watchdog_open (struct inode *inode, struct file *file)
{
	printk(KERN_DEBUG "watchdog_open called.\n");
	return 0;
}

/*
** Watchdog Close File operation 
*/
static int
watchdog_close (struct inode *inode, struct file *file)
{
	printk(KERN_DEBUG "watchdog_close called.\n");
	return 0;
}
static struct file_operations watchdog_fops = {
	.open = watchdog_open,
	.release = watchdog_close,
	.unlocked_ioctl = watchdog_ioctl,
	.compat_ioctl = watchdog_compat_ioctl,
};
static struct miscdevice watchdog_dev = {
	.minor = WATCHDOG_MINOR,
	.name = "watchdog",
	.fops = &watchdog_fops,
};
/*
** Watchdog Module Init
*/
static int watchdog_init(void) 
{
	if (timer_needed) {
		init_timer(&watchdog_timer);
		watchdog_timer.function = &watchdog_timer_func;
		watchdog_timer_interval = watchdog_get_timer_interval();
		watchdog_timer.expires = jiffies + (HZ*watchdog_timer_interval/1000);
		add_timer(&watchdog_timer);
	}

#ifdef CONFIG_FXC_P2020
	/* 
	 * Foxconn has a problem with the watchdog firing prematurely before 
	 * the system is fully stable.  To work around this, set up the 
	 * watchdog so that the timer function will explicitly service the 
	 * watchdog even during system startup, and while the kernel is
	 */
	watchdog_setup ();
#endif	

	return misc_register (&watchdog_dev);
}

/*
** Watchdog Module DeInit
*/
static void watchdog_exit(void) 
{
	wdt_stop();
	if (timer_pending(&watchdog_timer)) {
		printk(KERN_ALERT "Watchdog timer is pending to run\n");
	}
	printk(KERN_ALERT "Exiting... watchdog module\n");
}

module_init(watchdog_init);
module_exit(watchdog_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Amit Kaushik <akaushik#broadcom.com>");
MODULE_DESCRIPTION("Broadcom Watchdog Driver");
MODULE_VERSION("1.1");
