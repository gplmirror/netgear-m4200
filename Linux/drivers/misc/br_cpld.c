/*
 *  This program is free software; upcan redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 */

#include <linux/compat.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/pci.h>
#include <linux/fs.h>
#include <linux/poll.h>
#include <linux/interrupt.h>
#include <linux/cdev.h>
#include <linux/miscdevice.h>
#include <linux/spinlock.h>

#include <asm/atomic.h>
#include <asm/io.h>
#include <asm/types.h>

#include <linux/br_cpld.h>
#define DEBUG
#undef DEBUG

#ifdef DEBUG
#define DPRINTF(x, ...)  printk(x, ##__VA_ARGS__)
#else
#define DPRINTF(x, ...)
#endif

void  (*cpu_irq_process_fptr)(int,void*) = NULL;
void  (*cpu_irq_process_data)(int, void*) = NULL;
void  (*cpu_irq_enable_fptr)(int, void*) = NULL;
void  (*cpu_irq_disable_fptr)(int) = NULL;
void  (*cpu_irq_loop_enable_fptr)(int, void*) = NULL;

static irqDat isrData[BRCPLD_MAX_IRQ_ENTRIES];

/*
Any board specific functions use similar to above function pointers or extern functions
*/


spinlock_t br_cpld_lock;

/*
 * File ops
 */

static long
brcpld_ioctl (struct file *file, unsigned int cmd, unsigned long arg)
{
  void __user *argp = (void __user *) arg;
  int retval;
  struct __brcpld_waitForInt wait;

  memset (&wait, 0, sizeof (wait));

  switch (cmd)
    {
    case BRCPLD_WAIT_FOR_INTERRUPT:
      {
	unsigned long irqflags = 0;
        DPRINTF (KERN_DEBUG "brcpld_ioctl: BRCPLD_WAIT_FOR_INTERRUPT -at- \n");

	retval = copy_from_user (&wait, argp, sizeof (wait));
        if (retval)
	{
	    return -EFAULT;
	}
        if (wait.threadNo >= BRCPLD_MAX_IRQ_ENTRIES)
        {
            return -EFAULT;
        }
        sema_init (&isrData[wait.threadNo].intSem, 0);
        memset (&isrData[wait.threadNo].intSrcInfo,0, sizeof (isrData[wait.threadNo].intSrcInfo));
        if (wait.request_irq_firstTime)
        {
             retval = request_irq (wait.irqNo, brcpld_isr,
                             IRQF_SHARED | IRQF_DISABLED, "brcpld",
                              (void *) &isrData[wait.threadNo]);
             if (retval)
             {
                return -EINVAL;
             }
             /* enable all related registers for irq */
             cpu_irq_enable_fptr(wait.irqNo, (void *) &isrData[wait.threadNo].intSrcInfo);
         }
         else
         {
           /* Just enable particular irq enable register which is disabled earlier in previous interrupt */ 
           cpu_irq_loop_enable_fptr(wait.irqNo, (void *) &isrData[wait.threadNo].intSrcInfo);
         } 

	retval = down_interruptible (&isrData[wait.threadNo].intSem);
        cpu_irq_disable_fptr(wait.irqNo); /* if thread is closed intermittently, disable interrupt before leaving */
        
        /** down_interruptible can fail due to interruption to thread If so, return the same */
        if (retval == -EINTR) {
           return retval;
        }
        /* process irq data */
        cpu_irq_process_data(wait.irqNo, (void *) &isrData[wait.threadNo].intSrcInfo);
        
	spin_lock_irqsave (&br_cpld_lock, irqflags);
	memcpy ((void *) &wait.intSrcInfo, (void *) &isrData[wait.threadNo].intSrcInfo, sizeof (isrData[wait.threadNo].intSrcInfo));
	spin_unlock_irqrestore (&br_cpld_lock, irqflags);

	retval = copy_to_user (argp, &wait, sizeof (wait));
	if (retval)
	{
	  return -EFAULT;
	}
	break;
      }
    default:
      return -ENOTTY;
    }

  return 0; 
}

#ifdef CONFIG_COMPAT
static long
brcpld_compat_ioctl (struct file *filp, unsigned int cmd, unsigned long arg)
{
  DPRINTF (KERN_DEBUG "brcpld_compat_ioctl:-at- \n");
  if (_IOC_NR (cmd) <= 3 && _IOC_SIZE (cmd) == sizeof (compat_uptr_t))
    {
      cmd &= ~(_IOC_SIZEMASK << _IOC_SIZESHIFT);
      cmd |= sizeof (void *) << _IOC_SIZESHIFT;
    }
  return brcpld_ioctl (filp, cmd, (unsigned long) compat_ptr (arg));
}
#else
#define brcpld_compat_ioctl NULL
#endif

static int
brcpld_open (struct inode *inode, struct file *file)
{
  DPRINTF (KERN_DEBUG "brcpld_open:-at- \n");
  return 0;
}

static int
brcpld_close (struct inode *inode, struct file *file)
{
  DPRINTF (KERN_DEBUG "brcpld_close:-at- \n");
  return 0;
}


static struct file_operations brcpld_fops = {
  .open = brcpld_open,
  .release = brcpld_close,
  .unlocked_ioctl = brcpld_ioctl,
  .compat_ioctl = brcpld_compat_ioctl,
};
static irqreturn_t
brcpld_isr (int irq, void *data)
{
  irqDat *irqData = (struct __irqData *)data;

 /* Interrupt clearing is handled in  arch/<processor core>/XXX.c */
  cpu_irq_process_fptr(irq,&irqData->intSrcInfo);

  if  (irqData->intSrcInfo[0] == 0)
  {
     return IRQ_NONE;
  }

  DPRINTF (KERN_CRIT "brcpld_isr: irq: %d, int_evt: 0x%02x\n", 
                     irq, irqData->intSrcInfo[0]);
  cpu_irq_disable_fptr(irq); /* Disable the interrupt pin, if not req'd leave the function blank */
  up (&irqData->intSem);
  return IRQ_HANDLED;
}

void brcpld_irq_process_register(void *fptr)
{
  cpu_irq_process_fptr = fptr;
}
void brcpld_irq_enable_register(void *fptr)
{
  cpu_irq_enable_fptr = fptr;
}
void brcpld_irq_loop_enable_register(void *fptr)
{
  cpu_irq_loop_enable_fptr = fptr;
}
void brcpld_irq_disable_register(void *fptr)
{
  cpu_irq_disable_fptr = fptr;
}
void brcpld_irq_process_data(void *fptr)
{
  cpu_irq_process_data = fptr;
}

static struct miscdevice brcpld_dev = {
  .minor = 137,
  .name = "brcpld",
  .fops = &brcpld_fops,
};

static int __init
brcpld_init (void)
{
  int ret = 0;

  spin_lock_init (&br_cpld_lock);

  ret = misc_register (&brcpld_dev);
  if (ret)
    {
      printk (KERN_DEBUG "Broadcom CPLd: "
	       "Unable to register misc device.\n");
      return ret;
    }

  DPRINTF (KERN_CRIT "brcpld_init -at- \n");
  return 0;
}

static void __exit
brcpld_exit (void)
{
  misc_deregister (&brcpld_dev);
  DPRINTF (KERN_DEBUG "brcpld: module successfully removed\n");
}

module_init (brcpld_init);
module_exit (brcpld_exit);
EXPORT_SYMBOL(brcpld_irq_process_register);
MODULE_AUTHOR ("Prafulla Kota <prafulla.kota@broadcom.com>");
MODULE_DESCRIPTION ("Broadcom Linux CPLD driver");
MODULE_LICENSE ("GPL");
MODULE_VERSION (CPLD_DRV_VER);
