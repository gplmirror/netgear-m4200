/*
 * Copyright (C) 2015, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <linux/version.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/dma-mapping.h>
#include <linux/clkdev.h>
#include <asm/hardware/gic.h>

#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/irq.h>
#include <linux/input.h>
#include <linux/i2c/tsc2007.h>
#include <linux/spi/spi.h>
#include <mach/hardware.h>
#include <asm/mach/arch.h>
#include <asm/mach-types.h>
#include <mach/sdio_platform.h>
#include <mach/iproc.h>
#include <asm/io.h>
#include <mach/io_map.h>
#include <mach/reg_utils.h>
#include <linux/pwm.h>
#include <linux/amba/bus.h>
#include <mach/watchdog_platform.h>
#include <linux/i2c.h>

#include "northstar.h"
#include "common.h"

#ifdef CONFIG_MACH_GH
#include "include/mach/iproc_regs.h"
#endif

#define DEBUG
#undef DEBUG

#ifdef DEBUG
#define DPRINTF(fmt, ...)  printk(fmt, ##__VA_ARGS__)
#else
#define DPRINTF(fmt, ...)
#endif

extern void brcpld_irq_process_register(void *fptr);
extern void brcpld_irq_process_data(void *fptr);
extern void brcpld_irq_enable_register(void *fptr);
extern void brcpld_irq_loop_enable_register(void *fptr);
extern void brcpld_irq_disable_register(void *fptr);

#define SYS_CPLD_I2C_ADDR               0x30
#define SYS_CPLD_RST_WDT_REG            0x01
#define SYS_CPLD_INTR_STA_REG           0x04

#define M4200_INTERRUPT_EVT             0x01

#define GPIO_14_BIT                     (0x01 << 14)
#define M4200_GPIO_INT_BITS             (GPIO_14_BIT)
#define IPROC_GH_CCG_INT_CLR            0x24

#define WATCHDOG_TIMER_INTERVAL         5000 /* 5 seconds ms */
#define SYS_CPLD_WDT_ENABLE             0x08

#define M4200_I2C_MAX_RETRIES           0x03

static volatile void *gpio_vbase;
static u32           gpio_int_bits = M4200_GPIO_INT_BITS;

static s32 m4200_i2c_read_write(u8 address, u8 offset, u8 rd_wr, u8 rd_wr_mode, u8 *data)
{
	int rc;
	union i2c_smbus_data i2cdata;
	static struct i2c_adapter *adap;

	if (NULL == adap)
	{
		adap = i2c_get_adapter(0);
	}

	i2cdata.byte = *data;  /* For write & read */
	rc = i2c_smbus_xfer(adap, address, 0, rd_wr,
			    offset, rd_wr_mode, &i2cdata);
 
	*data = i2cdata.byte;  /* For read */
	if (rc < 0)
		printk(KERN_INFO "m4200_i2c_rw failed. rc: %x i2cAddr:%x "
				 "offset:%x rd_wr:%x rd_wr_mode:%x\n",
 				 rc, address, offset, rd_wr, rd_wr_mode);
	return rc;
}

static void m4200_gpio_init(void)
{
	if (NULL == gpio_vbase)
	{
		gpio_vbase = ioremap(ChipcommonG_GP_DATA_IN, 256);
		printk (KERN_INFO 
			"m4200_gpio_init: vbase: 0x%p, physaddr: 0x%x, size: 256\n",
				   gpio_vbase, ChipcommonG_GP_DATA_IN);
	}
}

static void m4200_irq_enable_internal(int irqNo, void *ptr)
{
	u32 reg;

	if  (NULL == gpio_vbase)
	{
		printk(KERN_INFO "M4200: gpio_vbase is NULL\n");
		return;
	}

        /** 56060-PR103-RDS.pdf page 3573. Check again */
	/* setting 0 for gpio pins as input */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_OUT_EN_BASE);
	reg &= ~((u32) gpio_int_bits);
	iowrite32(reg, (gpio_vbase+ChipcommonG_GP_OUT_EN_BASE));

	/* setting gpio pin as level sensitive interrupts */
	/* setting gpio pin as active low */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_TYPE_BASE);
	reg |= (gpio_int_bits);
	iowrite32(reg, (gpio_vbase+ChipcommonG_GP_INT_TYPE_BASE));

	/* setting gpio pin interrupt mask */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_MSK_BASE);
	reg |= (gpio_int_bits);
	iowrite32(reg, (gpio_vbase+ChipcommonG_GP_INT_MSK_BASE));

	DPRINTF(KERN_INFO "M4200: Configured GPIO interrupts \n");
}

static void m4200_irq_loop_enable_internal(int irqNo, void *ptr)
{
	u32 reg;

	if  (NULL == gpio_vbase)
	{
		printk(KERN_ERR 
			"M4200: Failed to enable GPIO interrupts for irq: %d\n",
			irqNo);
		return;
	}

	/* setting 1 for GPIO interrupt un-mask(enable interrupt) */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_MSK_BASE);
	reg |= (gpio_int_bits);
	iowrite32(reg, (gpio_vbase+ChipcommonG_GP_INT_MSK_BASE));
	udelay(100);

	DPRINTF(KERN_INFO 
		"M4200: Enabled GPIO interrupts 0x%x unmask_reg: 0x%x\n", 
		gpio_int_bits, reg);
}

static void m4200_irq_disable_internal(int irqNo)
{
	u32 reg; 

	if  (NULL == gpio_vbase)
	{
		printk(KERN_ERR 
			"M4200: Failed to disabled GPIO interrupts for irq: %d\n",
			irqNo);
		return;
	}

	/* setting gpio pins in interrupt mask(disable interrupt) */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_MSK_BASE);
	reg &= ~((u32) gpio_int_bits);
	iowrite32(reg, (gpio_vbase+ChipcommonG_GP_INT_MSK_BASE));
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_MSK_BASE);

	DPRINTF(KERN_INFO 
		"M4200: Disabled GPIO interrupts 0x%x unmask_reg: 0x%x\n", 
		gpio_int_bits, reg);
}

static void m4200_irq_process(int irqNo, void *data)
{
	u32 reg, *ptr = (u32 *)data;

	ptr[0] = 0;
	if (NULL == gpio_vbase)
	{
		printk(KERN_ERR 
			"M4200: Failed to process interrupt status for irq: %d\n",
			irqNo);
		return;
	}

	/* Read interrupt status */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_STAT_BASE);
	if (reg & gpio_int_bits)
	{
		ptr[0] = reg; /* GPIO interrupt */
	}

	DPRINTF(KERN_INFO 
		"M4200: GPIO int_status_reg: 0x%x\n", reg);

	/* clear gpio pins from interrupt clear register */
	reg = ioread32(gpio_vbase+IPROC_GH_CCG_INT_CLR);
	reg |= gpio_int_bits;
	iowrite32(reg, (gpio_vbase+IPROC_GH_CCG_INT_CLR));
	udelay(100);
}

static void m4200_irq_process_data(int irqNo, void *data)
{
	u32 reg, iter = 0;
	u32 *ptr = (u32 *)data;
	u8  int_data = 0xff;
	s32 rc;

	ptr[1] = 0xff; /* returning that there is no interrupt */
	if (NULL == gpio_vbase)
	{
		printk(KERN_ERR 
			"M4200: Failed to process interrupt data for irq: %d\n",
			irqNo);
		return;
	}

	if (0 == ptr[0]) /* no gpio interrupt */
	{
		return;
	}

	//reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_STAT_BASE);
	reg = ptr[0];
	if  (reg & GPIO_14_BIT)
	{   /* Some define value to inform that it is GPIO14 interrupt */
		ptr[0] = M4200_INTERRUPT_EVT;

		do {
			rc = m4200_i2c_read_write(SYS_CPLD_I2C_ADDR,
				SYS_CPLD_INTR_STA_REG, I2C_SMBUS_READ,
				I2C_SMBUS_BYTE_DATA, &int_data);

			if (rc >= 0)
			{
				break;
			}
			udelay(100);
		} while (++iter < M4200_I2C_MAX_RETRIES);

		ptr[1] = int_data;
	}

	DPRINTF(KERN_INFO 
		"M4200: int status: 0x%x, data: 0x%x\n", ptr[0], ptr[1]);
}

/*
 * Disable the watchdog feature
 */
void watchdog_disable(void)
{
	u8 iter = 0, data = 0;
	int rc;

	do {
        	rc = m4200_i2c_read_write(SYS_CPLD_I2C_ADDR, SYS_CPLD_RST_WDT_REG,
				I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);
		if (rc >= 0)
		{
			break;
		}
		udelay(100);
	} while (++iter < M4200_I2C_MAX_RETRIES);

	if (rc < 0)
	{
		printk (KERN_EMERG "m42xx: watchdog disable: i2c write failed(error:%d).\n", rc);
		return;
	}

	DPRINTF (KERN_INFO "m42xx: watchdog disabled\n");
}

/*
 * Service watchdog.
 */
void watchdog_service(void)
{
	u8 iter = 0, data = SYS_CPLD_WDT_ENABLE;
	int rc;

	do {
		rc = m4200_i2c_read_write(SYS_CPLD_I2C_ADDR, SYS_CPLD_RST_WDT_REG,
			 I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);

		if (rc >= 0)
		{
			break;
		}
		udelay(100);
	} while (++iter < M4200_I2C_MAX_RETRIES);

        if (rc < 0)
	{
		printk (KERN_EMERG "m42xx: watchdog service: i2c write failed(error:%d).\n", rc);
	}
}
/*
 * Enable the watchdog feature
 */
int watchdog_enable(void)
{
	u8 iter = 0, data = SYS_CPLD_WDT_ENABLE;
	int rc;

	do {
		rc = m4200_i2c_read_write(SYS_CPLD_I2C_ADDR, SYS_CPLD_RST_WDT_REG,
			 I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);

		if (rc >= 0)
		{
			break;
		}
		udelay(100);
	} while (++iter < M4200_I2C_MAX_RETRIES);

	if (rc < 0) 
		printk (KERN_EMERG "m42xx: watchdog_enable: i2c write failed(error: %d).\n", rc);
	else
		printk (KERN_INFO "m42xx: watchdog enabled\n");

	return rc;
}
/*
 * Get the Watchdog Timer Interval in ms
 */
unsigned int watchdog_get_timer_interval(void)
{
        return WATCHDOG_TIMER_INTERVAL;
}

void m4200_board_init(void)
{
	m4200_gpio_init();

	brcpld_irq_enable_register(m4200_irq_enable_internal);
	brcpld_irq_process_register(m4200_irq_process);
	brcpld_irq_loop_enable_register(m4200_irq_loop_enable_internal);
	brcpld_irq_disable_register(m4200_irq_disable_internal);
	brcpld_irq_process_data(m4200_irq_process_data);

	printk(KERN_INFO "m4200_board_init: done.\n");
}

void m4200_board_restart(struct resource *dmu_regs)
{
	unsigned char hw_reset = 0x01;
	void * __iomem reg_addr;
	u32 reg;
	int rc;

	rc = m4200_i2c_read_write(SYS_CPLD_I2C_ADDR, SYS_CPLD_RST_WDT_REG, 
                              I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &hw_reset);
	if (rc < 0)
	{
		printk(KERN_CRIT "i2c write failed: Unable to reset, "
				"power cycle required. rc:%d\n",rc);
	}
	mdelay(5000);

	/*System should reset here. Use cpu reset if not */
	printk(KERN_ERR "\nSystem reset failed. Resetting the CPU\n");

	reg_addr = (void * __iomem) dmu_regs->start + DMU_CRU_RESET_BASE ;
	/* set iproc_reset_n to 0, it may come back or not ... TBD */
	reg = __raw_readl(reg_addr);
	reg &= ~((u32) 1 << 1);
	/* Reset switch as well */
	reg &= ~((u32) 1 << 0);
	__raw_writel(reg, reg_addr);

	printk(KERN_CRIT "Unable to reset, power cycle required. rc:%d\n",rc);
	while(1);
}
