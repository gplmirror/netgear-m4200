/*
 * Copyright (C) 2015, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <linux/version.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/dma-mapping.h>
#include <linux/clkdev.h>
#include <asm/hardware/gic.h>

#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/irq.h>
#include <linux/input.h>
#include <linux/i2c/tsc2007.h>
#include <linux/spi/spi.h>
#include <mach/hardware.h>
#include <asm/mach/arch.h>
#include <asm/mach-types.h>
#include <mach/sdio_platform.h>
#include <mach/iproc.h>
#include <asm/io.h>
#include <mach/io_map.h>
#include <mach/reg_utils.h>
#include <linux/pwm.h>
#include <linux/amba/bus.h>
#include <mach/watchdog_platform.h>
#include <linux/i2c.h>

#include "northstar.h"
#include "common.h"

#ifdef CONFIG_MACH_GH
#include "include/mach/iproc_regs.h"
#endif

#define DEBUG
#undef DEBUG

#ifdef DEBUG
#define DPRINTF(fmt, ...)  printk(fmt, ##__VA_ARGS__)
#else
#define DPRINTF(fmt, ...)
#endif

extern void brcpld_irq_process_register(void *fptr);
extern void brcpld_irq_process_data(void *fptr);
extern void brcpld_irq_enable_register(void *fptr);
extern void brcpld_irq_loop_enable_register(void *fptr);
extern void brcpld_irq_disable_register(void *fptr);

/* Board Id of xs7xxt platform */


#define GPIO_BIT(pin)           (0x01 << pin)

#define XS7XXT_SFP_INT_BIT      GPIO_BIT(12)
#define XS7XXT_HWM_INT_BIT      GPIO_BIT(13)
#define XS7XXT_PHY_INT_BIT      GPIO_BIT(14)
#define XS7XXT_GPIO_INT_BITS    (XS7XXT_SFP_INT_BIT | XS7XXT_HWM_INT_BIT | XS7XXT_PHY_INT_BIT)

#define XS7XXT_SFP_INTERRUPT    0x01
#define XS7XXT_HWM_INTERRUPT    0x02
#define XS7XXT_PHY_INTERRUPT    0x03

static volatile void * __iomem gpio_vbase;
static          u32            gpio_int_bits;

#define CMIC_COMMON_CORE_GPIO  0x03202000
typedef volatile struct {
        u32 gpioIn;        /* offset: 0x000 */
        u32 gpioOut;       /* offset: 0x004 */
        u32 gpioOutEn;     /* offset: 0x008 */
} chipcregs_t;

#define IPROC_GH_CCG_INT_CLR           0x24

static void xs7xxt_gpio_init(void)
{
	if (NULL == gpio_vbase)
	{
		gpio_vbase = ioremap(ChipcommonG_GP_DATA_IN, 256);
		printk (KERN_INFO 
			"xs7xxt_gpio_init: vbase: 0x%p, physaddr: 0x%x, size: 256\n",
				   gpio_vbase, ChipcommonG_GP_DATA_IN);
	}
	gpio_int_bits = XS7XXT_GPIO_INT_BITS;
}

static void xs7xxt_irq_enable_internal(int irqNo, void *ptr)
{
	u32 reg;

	if  (NULL == gpio_vbase)
	{
		printk(KERN_INFO "XS7XXT: gpio_vbase is NULL\n");
		return;
	}

        /** 56060-PR103-RDS.pdf page 3573. Check again */
	/* setting 0 for gpio pins as input */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_OUT_EN_BASE);
	reg &= ~((u32) gpio_int_bits);
	iowrite32(reg, (gpio_vbase+ChipcommonG_GP_OUT_EN_BASE));

	/* setting gpio pin as level sensitive interrupts */
	/* setting gpio pin as active low */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_TYPE_BASE);
	reg |= (gpio_int_bits);
	iowrite32(reg, (gpio_vbase+ChipcommonG_GP_INT_TYPE_BASE));

	/* setting gpio pin interrupt mask */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_MSK_BASE);
	reg |= (gpio_int_bits);
	iowrite32(reg, (gpio_vbase+ChipcommonG_GP_INT_MSK_BASE));

	DPRINTF(KERN_INFO "XS7XXT: Configured GPIO interrupts \n");
}

static void xs7xxt_irq_loop_enable_internal(int irqNo, void *ptr)
{
	u32 reg;

	if  (NULL == gpio_vbase)
	{
		printk(KERN_ERR 
			"XS7XXT: Failed to enable GPIO interrupts for irq: %d\n",
			irqNo);
		return;
	}

	/* setting 1 for GPIO interrupt un-mask(enable interrupt) */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_MSK_BASE);
	reg |= (gpio_int_bits);
	iowrite32(reg, (gpio_vbase+ChipcommonG_GP_INT_MSK_BASE));
	udelay(100);
	DPRINTF(KERN_INFO 
		"XS7XXT: Enabled GPIO interrupts 0x%x unmask_reg: 0x%x\n", 
		gpio_int_bits, reg);
}

static void xs7xxt_irq_disable_internal(int irqNo)
{
	u32 reg; 

	if  (NULL == gpio_vbase)
	{
		printk(KERN_ERR 
			"XS7XXT: Failed to disabled GPIO interrupts for irq: %d\n",
			irqNo);
		return;
	}

	/* setting gpio pins in interrupt mask(disable interrupt) */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_MSK_BASE);
	reg &= ~((u32) gpio_int_bits);
	iowrite32(reg, (gpio_vbase+ChipcommonG_GP_INT_MSK_BASE));
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_MSK_BASE);

	DPRINTF(KERN_INFO 
		"XS7XXT: Disabled GPIO interrupts 0x%x unmask_reg: 0x%x\n", 
		gpio_int_bits, reg);
}

static void xs7xxt_irq_process(int irqNo, void *data)
{
	u32 reg, *ptr = (u32 *)data;

	ptr[0] = 0;
	if (NULL == gpio_vbase)
	{
		printk(KERN_ERR 
			"XS7XXT: Failed to process interrupt status for irq: %d\n",
			irqNo);
		return;
	}

	/* Read interrupt status */
	reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_STAT_BASE);
	if (reg & gpio_int_bits)
	{
		ptr[0] = reg; /* GPIO interrupt */
	}

	DPRINTF(KERN_INFO 
		"XS7XXT: GPIO int_status_reg: 0x%x\n", reg);

	/* clear gpio pins from interrupt clear register */
	reg = ioread32(gpio_vbase+IPROC_GH_CCG_INT_CLR);
	reg |= gpio_int_bits;
	iowrite32(reg, (gpio_vbase+IPROC_GH_CCG_INT_CLR));
	udelay(100);
}

static void xs7xxt_irq_process_data(int irqNo, void *data)
{
	u32 reg, *ptr = (u32 *)data;

	ptr[1] = 0xff; /* returning that there is no interrupt */
	if (NULL == gpio_vbase)
	{
		printk(KERN_ERR 
			"XS7XXT: Failed to process interrupt data for irq: %d\n",
			irqNo);
		return;
	}

	if (0 == ptr[0]) /* no gpio interrupt */
	{
		return;
	}

	//reg = ioread32(gpio_vbase+ChipcommonG_GP_INT_STAT_BASE);
	reg = ptr[0];
	if  (reg & XS7XXT_SFP_INT_BIT)
	{
		/* Some define value to inform that it is SFP interrupt */
		ptr[0] = XS7XXT_SFP_INTERRUPT;
	}

	if  (reg & XS7XXT_HWM_INT_BIT)
	{   /* Some define value to inform that it is HWM interrupt */
		ptr[0] = XS7XXT_HWM_INTERRUPT;
	}

	if  (reg & XS7XXT_PHY_INT_BIT)
	{   /* Some define value to inform that it is PHY interrupt */
		ptr[0] = XS7XXT_PHY_INTERRUPT;
	}

	ptr[1] = ioread32(gpio_vbase+ChipcommonG_GP_DATA_IN_BASE);
	DPRINTF(KERN_INFO 
		"XS7XXT: interrupt status: 0x%x, data: 0x%x\n", ptr[0], ptr[1]);
}

void xs7xxt_board_init(void)
{
	xs7xxt_gpio_init();

	brcpld_irq_enable_register(xs7xxt_irq_enable_internal);
	brcpld_irq_process_register(xs7xxt_irq_process);
	brcpld_irq_loop_enable_register(xs7xxt_irq_loop_enable_internal);
	brcpld_irq_disable_register(xs7xxt_irq_disable_internal);
	brcpld_irq_process_data(xs7xxt_irq_process_data);

	printk(KERN_INFO "xs7xxt_board_init: done.\n");
}

void xs7xxt_board_restart(struct resource *dmu_regs)
{
	void * __iomem reg_addr;
        volatile void * __iomem vaddr;
        chipcregs_t *chipcommon;
	u32 reg;

        vaddr = ioremap(CMIC_COMMON_CORE_GPIO,0x8);
        if (vaddr != NULL)
        {
           chipcommon = (chipcregs_t *)vaddr;

           chipcommon->gpioOut |= 0x08;  /* Pull HIGH on GPIO-3 */
           reg = chipcommon->gpioOut;
           chipcommon->gpioOutEn = 0x0d;
           reg = chipcommon->gpioOutEn;
	   udelay(100);
           chipcommon->gpioOut &= 0xfffffff7;
           reg = chipcommon->gpioOut;
           iounmap(vaddr);
        } else {
            printk(KERN_ERR "\nXS7XXT: CMIC Address Map Failed\n");
        }

	mdelay(2000);

	/*System should reset here. Use cpu reset if not */
	printk(KERN_ERR "\nSystem reset failed. Resetting the CPU\n");

	reg_addr = (void * __iomem) dmu_regs->start + DMU_CRU_RESET_BASE ;
	/* set iproc_reset_n to 0, it may come back or not ... TBD */
	reg = __raw_readl(reg_addr);
	reg &= ~((u32) 1 << 1);
#if defined(CONFIG_MACH_GH)
	/* Reset switch as well */
	reg &= ~((u32) 1 << 0);
#endif /* CONFIG_MACH_GH */
	__raw_writel(reg, reg_addr);

	printk(KERN_CRIT "Unable to reset, power cycle required.\n");
	while(1);
}
