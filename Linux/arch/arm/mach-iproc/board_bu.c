/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <linux/version.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/dma-mapping.h>
#include <linux/clkdev.h>
#include <asm/hardware/gic.h>


#include <linux/i2c.h>
#include <linux/delay.h>
#include <linux/irq.h>
#include <linux/input.h>
#include <linux/i2c/tsc2007.h>
#include <linux/spi/spi.h>
#include <mach/hardware.h>
#include <asm/mach/arch.h>
#include <asm/mach-types.h>
#include <mach/sdio_platform.h>
#include <mach/iproc.h>
#include <asm/io.h>
#include <mach/io_map.h>
#include <mach/reg_utils.h>
#include <linux/pwm.h>
#include <linux/amba/bus.h>
#include <mach/watchdog_platform.h>
#include <linux/amba/pl330.h>

#include "northstar.h"
#include "common.h"
#ifdef HNDCTF
#include <typedefs.h>
#include <ctf/hndctf.h>
#endif /* HNDCTF */

/* GSIO uses bus number 0, QSPI uses bus number 1 */
#define IPROC_QSPI_BUS_NUMBER       (1)

#include <linux/amba/pl022.h>
#if defined(CONFIG_SND_BCM5830X_SVK) || defined(CONFIG_SND_BCM5830X_SVK_MODULE)
#include <linux/i2c.h>
#endif
#if defined(CONFIG_IPROC_SD) || defined(CONFIG_IPROC_SD_MODULE)
#define IPROC_SDIO_PA   	IPROC_SDIO3_REG_BASE
#define SDIO_CORE_REG_SIZE	0x10000
#define BSC_CORE_REG_SIZE	0x1000
#define SDIO_IDM_IDM_RESET_CONTROL  (0x16800)
#define IPROC_SDIO_IRQ		(177)
#endif

#if defined(CONFIG_IPROC_IPM4300_R2_BOARD) || defined(CONFIG_IPROC_IPM4300_NS_BOARD)
extern void m4300_board_init(void);
#endif
#if defined(CONFIG_IPROC_IPM4200_BOARD)
extern void m4200_board_init(void);
#endif
#if defined(CONFIG_IPROC_XS7XXT_BOARD)
extern void xs7xxt_board_init(void);
#endif

#if defined(CONFIG_IPROC_IPM6100_BOARD) || defined(CONFIG_IPROC_IPM6100_VEGA_BOARD)
#define  CPLD_RESET_REG  1

extern void brcpld_irq_process_register(void *fptr);
extern void brcpld_irq_process_data(void *fptr);
extern void brcpld_irq_enable_register(void *fptr);
extern void brcpld_irq_loop_enable_register(void *fptr);
extern void brcpld_irq_disable_register(void *fptr);
void m6100_irq_process_fptr(int irqNo, void *ptr);
void cpldI2cAddressDetermine(void);
unsigned char cpldI2cAddress = 0;
s32 m6100_i2c_read_write(u8 address, u8 offset, u8 rd_wr, u8 rd_wr_mode, u8 *data);
struct i2c_adapter *adap;
#endif

#ifdef CONFIG_MACH_CYGNUS
#include "include/mach/iproc_regs.h"
#include <media/soc_camera.h>

struct pl022_config_chip spi_chip_info = {
	.iface = SSP_INTERFACE_MOTOROLA_SPI,	
	.hierarchy = SSP_MASTER,
	/* We can only act as master but SSP_SLAVE is possible in theory */
	/* 0 = drive TX even as slave, 1 = do not drive TX as slave */
	.slave_tx_disable = 0,
	.clk_freq = {	.cpsdvsr = 100,  .scr = 1,	},	
	.com_mode = DMA_TRANSFER,
	.rx_lev_trig = SSP_RX_4_OR_MORE_ELEM,
	.tx_lev_trig = SSP_TX_4_OR_MORE_EMPTY_LOC,
	.ctrl_len = SSP_BITS_12,
	.wait_state = SSP_MWIRE_WAIT_ZERO,
	.duplex = SSP_MICROWIRE_CHANNEL_HALF_DUPLEX,
};

struct pl022_config_chip spi1_chip_info = {
    .iface = SSP_INTERFACE_MOTOROLA_SPI,
    .hierarchy = SSP_MASTER,
    /* We can only act as master but SSP_SLAVE is possible in theory */
    /* 0 = drive TX even as slave, 1 = do not drive TX as slave */
    .slave_tx_disable = 0,
    .clk_freq = {   .cpsdvsr = 100,  .scr = 99,  },
    .com_mode = DMA_TRANSFER,
    .rx_lev_trig = SSP_RX_4_OR_MORE_ELEM,
    .tx_lev_trig = SSP_TX_4_OR_MORE_EMPTY_LOC,
    .ctrl_len = SSP_BITS_8,
    .wait_state = SSP_MWIRE_WAIT_ZERO,
    .duplex = SSP_MICROWIRE_CHANNEL_HALF_DUPLEX,
};

static struct pl022_ssp_controller ssp_platform_data[] = {
	{
		/* If you have several SPI buses this varies, we have only bus 0 */
		.bus_id = 2,
		/*
		 * On the APP CPU GPIO 4, 5 and 6 are connected as generic
		 * chip selects for SPI. (Same on U330, U335 and U365.)
		 * TODO: make sure the GPIO driver can select these properly
		 * and do padmuxing accordingly too.
		 */
		.num_chipselect = 1,
#ifdef CONFIG_IPROC_PL330_DMA_MODULE
		.enable_dma = 1,
		.dma_filter = pl330_filter,
		.dma_rx_param = (void *)DMACH_SPI2_RX ,
		.dma_tx_param = (void *)DMACH_SPI2_TX ,
#else
	    .enable_dma = 0,
#endif
     },
	{
		/* If you have several SPI buses this varies, we have only bus 0 */
		.bus_id = 3,
		/*
		 * On the APP CPU GPIO 4, 5 and 6 are connected as generic
		 * chip selects for SPI. (Same on U330, U335 and U365.)
		 * TODO: make sure the GPIO driver can select these properly
		 * and do padmuxing accordingly too.
		 */
		.num_chipselect = 1,
		.enable_dma = 0,
     },
    {},
};

static struct spi_board_info iproc_spi_devices[] = {
	{
		/* A dummy chip used for loopback tests */
		.modalias       = "spidev",
		/* Really dummy, pass in additional chip config here */
		.platform_data  = NULL,
		/* This defines how the controller shall handle the device */
		.controller_data = &spi_chip_info,
		/* .irq - no external IRQ routed from this device */
		.max_speed_hz   = 500000,
		.bus_num        = 2, /* Only one bus on this chip */
		.chip_select    = 0,
		/* Means SPI_CS_HIGH, change if e.g low CS */
		.mode           = SPI_MODE_3| SPI_NO_CS | SPI_LOOP ,
	},	
	{
		.modalias       = "spi_msr",
		.platform_data  = NULL,
		/* This defines how the controller shall handle the device */
		.controller_data = &spi1_chip_info,
		/* .irq - no external IRQ routed from this device */
		.max_speed_hz   = 1000000,
		.bus_num        = 3, /* Only one bus on this chip */
		.chip_select    = 0,
		/* Means SPI_CS_HIGH, change if e.g low CS */
		.mode           = SPI_MODE_0| SPI_NO_CS,
	},	
    {},
};

static struct resource spi0_resources[] ={
	[0] = {
	    .start = ChipcommonG_SPI0_SSPCR0,
		.end   = ChipcommonG_SPI0_SSPCR0 + 0x1000 - 1,
		.name = "ssp-pl022",
		.flags  = IORESOURCE_MEM,
    },
    [1] = {
		.start	= 110, 
		.end	= 110,
		.flags	= IORESOURCE_IRQ,
    },
	{},
};

static struct resource spi1_resources[] ={
	[0] = {
	    .start = ChipcommonG_SPI1_SSPCR0,
		.end   = ChipcommonG_SPI1_SSPCR0 + 0x1000 - 1,
		.name = "ssp-pl022",
		.flags  = IORESOURCE_MEM,
    },
    [1] = {
		.start	= 111, 
		.end	= 111,
		.flags	= IORESOURCE_IRQ,
    },
	{},
};

static struct resource spi2_resources[] ={
	[0] = {
	    .start = ChipcommonG_SPI2_SSPCR0,
		.end   = ChipcommonG_SPI2_SSPCR0 + 0x1000 - 1,
		.name = "ssp-pl022",
		.flags  = IORESOURCE_MEM,
    },
    [1] = {
		.start	= 112, 
		.end	= 112,
		.flags	= IORESOURCE_IRQ,
    },
	{},
};

static struct resource spi3_resources[] ={
	[0] = {
	    .start = ChipcommonG_SPI3_SSPCR0,
		.end   = ChipcommonG_SPI3_SSPCR0 + 0x1000 - 1,
		.name = "ssp-pl022",
		.flags  = IORESOURCE_MEM,
    },
    [1] = {
		.start	= 113, 
		.end	= 113,
		.flags	= IORESOURCE_IRQ,
    },
	{},
};

static struct platform_device board_pl022_devices[] = {
	[0] = {
		.name = "ssp-pl022",
		.id = 0,
		.dev = {
		    .platform_data = &ssp_platform_data[0],
		    },
		.num_resources = ARRAY_SIZE(spi2_resources),
		.resource = spi2_resources,
    },
    [1] = {
		.name = "ssp-pl022",
		.id = 1,
		.dev = {
		    .platform_data = &ssp_platform_data[1],
		    },
		.num_resources = ARRAY_SIZE(spi1_resources),
		.resource = spi1_resources,
    },
    {},
};
static u8 iproc_pdma0_peri[] = {
	DMACH_CRYPTO_IN,
	DMACH_CRYPTO_OUT,
	DMACH_SPI0_RX,
	DMACH_SPI0_TX,
	DMACH_SPI1_RX,
	DMACH_SPI1_TX,
	DMACH_SPI2_RX,
	DMACH_SPI2_TX,
	DMACH_SPI3_RX,
	DMACH_SPI3_TX,
	DMACH_SPI4_RX,
	DMACH_SPI4_TX,
	DMACH_SPI5_RX,
	DMACH_SPI5_TX,
	DMACH_UART0_RX,
	DMACH_UART0_TX,
	DMACH_UART1_RX,
	DMACH_UART1_TX,
	DMACH_UART2_RX,
	DMACH_UART2_TX,
	DMACH_UART3_RX,
	DMACH_UART3_TX,
	DMACH_UART4_RX,
	DMACH_UART4_TX,
	DMACH_MTOM_0,
	DMACH_MTOM_1,
	DMACH_MTOM_2,
	DMACH_MTOM_3,
	DMACH_MTOM_4,
	DMACH_MTOM_5,
	DMACH_MTOM_6,
	DMACH_MIPI,
};

static struct dma_pl330_platdata iproc_pdma0_pdata;

static struct resource pl330_resources[] ={
	[0] = {
	    .start = DMAC_pl330_DS,
		.end   = DMAC_pl330_DS + 0x1000 - 1,
		.name = "dma-pl330",
		.flags  = IORESOURCE_MEM,
    },
    [1] = {
		.start	= 84, 
		.end	= 100,
		.flags	= IORESOURCE_IRQ,
    },
	{},
};

static struct platform_device pl330_dmac_dev = {
	.name = "dma-pl330",
	.id = -1,
	.dev = {
		.platform_data = &iproc_pdma0_pdata,
		.coherent_dma_mask  = DMA_BIT_MASK(64),
	},
	.num_resources  = ARRAY_SIZE(pl330_resources),
    .resource       = pl330_resources,
	
};

void iproc_dmac_init()
{
  	/* Register any dmac devices */
	void __iomem *dma_addr; 

	dma_addr = ioremap(DMAC_M0_IDM_RESET_CONTROL,0x08);  
    __raw_writel( 1, dma_addr);  
    __raw_writel( 0, dma_addr); 	


	iproc_pdma0_pdata.nr_valid_peri =
		ARRAY_SIZE(iproc_pdma0_peri);
	iproc_pdma0_pdata.peri_id = iproc_pdma0_peri;
	dma_cap_set(DMA_SLAVE, iproc_pdma0_pdata.cap_mask);
	dma_cap_set(DMA_CYCLIC, iproc_pdma0_pdata.cap_mask);
	platform_device_register(&pl330_dmac_dev);
}

void iproc_spi_iomux(int spi)
{
	/* Register any SPI devices */
	void __iomem *spi_iomux_addr; 

	spi_iomux_addr = ioremap(CRMU_IOMUX_CTRL4,0x10);  
    __raw_writel( 0, spi_iomux_addr);
}
static struct i2c_board_info ov5640_i2c0_bus ;
static struct soc_camera_device icd ;
static struct soc_camera_link iclink_ov5640 = {
	.bus_id			= 0,
	.i2c_adapter_id		= 0,
	.board_info = &ov5640_i2c0_bus,
	.priv =  &icd,
	.module_name = "unicam-camera",
};
static struct soc_camera_device icd = {
    .link = &iclink_ov5640,
	.user_width = 600,
	.user_height = 480,
	.iface = 0,
};
static struct i2c_board_info ov5640_i2c0_bus  =  {
	.platform_data = &icd,
    I2C_BOARD_INFO("ov5640", 0x3c ),  /* ov5640  codec */
};
static struct resource unicam_camera_resource[] = {
	[0] = {
	.flags  = IORESOURCE_MEM,
	.start	 = CAM_TOP_REGS_CAMCTL, 
	.end 	 = CAM_TOP_REGS_CAMCTL + 0xfff,
	},
	[1] = {
	.flags  = IORESOURCE_IRQ,
	.start  = 177 ,/*ASIU_LCD_INTR bit[26...22] */
	.end = 177, /*ASIU_LCD_INTR bit[26...22] */
	//IRQ_OLCDC,	/* open mode??? */ /* defined in include/asm-arm/arch/irqs.h */
	},
};
static struct platform_device unicam_camera_device = {
	.name = "unicam-camera",
	.id = 0,
	.dev = {
		    .platform_data = &iclink_ov5640,
			.dma_mask           = ~(u64)0,
			.coherent_dma_mask  = 0xffffffff, 
		},
	.num_resources = ARRAY_SIZE(unicam_camera_resource),
	.resource = unicam_camera_resource,
};

static struct platform_device ams_camera_device = {
	.name   = "soc-camera-pdrv",
	.id     = 0,
	.dev    = {
		.platform_data = &iclink_ov5640,
		.dma_mask           = ~(u64)0,
		.coherent_dma_mask  = 0xffffffff, 
	},
};
void __init iproc_init_dev(void)
{
	iproc_spi_iomux(2);

	platform_device_register(&board_pl022_devices[0]);
	platform_device_register(&board_pl022_devices[1]);

	/* Register SPI bus */
	spi_register_board_info(iproc_spi_devices, 2 /* ARRAY_SIZE(iproc_spi_devices)*/);
    iproc_dmac_init();
	
	platform_device_register(&ams_camera_device);
	platform_device_register(&unicam_camera_device);
}
#endif



#if defined(CONFIG_MACH_NSP)
#define SATA_M0_IDM_IO_CONTROL_DIRECT_VA   HW_IO_PHYS_TO_VIRT(SATA_M0_IDM_IO_CONTROL_DIRECT)
#define SATA_M0_IDM_IDM_RESET_CONTROL_VA   HW_IO_PHYS_TO_VIRT(SATA_M0_IDM_IDM_RESET_CONTROL)
#define SATA_TOP_CTRL_BUS_CTRL_VA   HW_IO_PHYS_TO_VIRT(SATA_TOP_CTRL_BUS_CTRL)
#define SATA3_PCB_UPPER_REG15_VA   HW_IO_PHYS_TO_VIRT(SATA3_PCB_UPPER_REG15)
#define SATA3_PCB_UPPER_REG0_VA   HW_IO_PHYS_TO_VIRT(SATA3_PCB_UPPER_REG0)
#define SATA3_PCB_UPPER_REG1_VA   HW_IO_PHYS_TO_VIRT(SATA3_PCB_UPPER_REG1)
#define SATA3_PCB_UPPER_REG11_VA   HW_IO_PHYS_TO_VIRT(SATA3_PCB_UPPER_REG11)
#define SATA3_PCB_UPPER_REG5_VA   HW_IO_PHYS_TO_VIRT(SATA3_PCB_UPPER_REG5)
#define AXIIC_sata_m0_fn_mod_VA   HW_IO_PHYS_TO_VIRT(AXIIC_sata_m0_fn_mod)
#define BCM_INT_SATA 190
#define NSP_CHIPID 0x3F00CF1E
#endif

#ifndef CONFIG_MACH_CYGNUS
extern void request_idm_timeout_interrupts(void);
#endif

extern irqreturn_t idm_timeout_handler(int val, void *ptr);
#if (defined(CONFIG_MACH_NSP) && defined(CONFIG_IPROC_OTP))
extern void*	bcm5301x_otp_init(void);
extern int	bcm5301x_otp_exit(void);
extern int	bcm5301x_otp_read_dword(void *oh, uint wn, u32 *data);
#endif /* (defined(CONFIG_MACH_NSP) && defined(CONFIG_IPROC_OTP)) */

/* This is the main reference clock 25MHz from external crystal */
static struct clk clk_ref = {
	.name = "Refclk",
	.rate = 25 * 1000000,   /* run-time override */
	.fixed = 1,
	.type  = 0,
};
// #endif /* END of CYGNUS */

#ifdef HNDCTF
ctf_t *kcih = NULL;
EXPORT_SYMBOL(kcih);
ctf_attach_t ctf_attach_fn = NULL;
EXPORT_SYMBOL(ctf_attach_fn);
#endif /* HNDCTF */

static struct clk_lookup board_clk_lookups[] = {
	{
	.con_id         = "refclk",
	.clk            = &clk_ref,
	}
};

extern void __init northstar_timer_init(struct clk *clk_ref);

#if defined(CONFIG_IPROC_SD) || defined(CONFIG_IPROC_SD_MODULE)
/* sdio */
static struct sdio_platform_cfg sdio_platform_data = {
	.devtype = SDIO_DEV_TYPE_SDMMC,
};
static struct resource sdio_resources[] = {
	[0] = {
		.start	= IPROC_SDIO_PA,
		.end	= IPROC_SDIO_PA + SZ_4K - 1,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= IPROC_SDIO_IRQ,
		.end	= IPROC_SDIO_IRQ,
		.flags	= IORESOURCE_IRQ,
	}
};

static struct platform_device board_sdio_device = {
	.name		=	"iproc-sdio",
	.id		=	0,
	.dev		=	{
		.platform_data	= &sdio_platform_data,
	},
	.num_resources  = ARRAY_SIZE(sdio_resources),
  	.resource	= sdio_resources,
};

#if defined(CONFIG_MACH_CYGNUS)
static struct sdio_platform_cfg sdio1_platform_data = {
	.devtype = SDIO_DEV_TYPE_SDMMC,
};
static struct resource sdio1_resources[] = {
	[0] = {
		.start	= IPROC_SDIO1_PA,
		.end	= IPROC_SDIO1_PA + SZ_4K - 1,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= IPROC_SDIO1_IRQ,
		.end	= IPROC_SDIO1_IRQ,
		.flags	= IORESOURCE_IRQ,
	}
};

static struct platform_device board_sdio1_device = {
	.name		=	"iproc-sdio",
	.id		=	1,
	.dev		=	{
		.platform_data	= &sdio1_platform_data,
	},
	.num_resources  = ARRAY_SIZE(sdio1_resources),
  	.resource	= sdio1_resources,
};
#endif

static void setup_sdio(void)
{
    void __iomem *idm_base;
    idm_base = (void __iomem *)IPROC_IDM_REGISTER_VA;
	printk("%s: %d %p\n", __FUNCTION__, __LINE__, idm_base + IPROC_SDIO_IDM_RESET_CONTROL);
    __raw_writel(0, idm_base + IPROC_SDIO_IDM_RESET_CONTROL);
    platform_device_register(&board_sdio_device);
#if defined(CONFIG_MACH_CYGNUS)
    __raw_writel(0, idm_base + IPROC_SDIO1_IDM_RESET_CONTROL);
    platform_device_register(&board_sdio1_device);
#endif

}
#endif /* CONFIG_IPROC_SD || CONFIG_IPROC_SD_MODULE */

#if defined(CONFIG_IPROC_PWM) || defined(CONFIG_IPROC_PWM_MODULE)
static struct resource iproc_pwm_resources = {
	.start	= IPROC_CCB_PWM_CTL,
	.end	= IPROC_CCB_PWM_CTL + SZ_4K - 1,
	.flags	= IORESOURCE_MEM,
};

static struct platform_device board_pwm_device = {
	.name		= "iproc_pwmc",
	.id	        = -1,
  	.resource	= &iproc_pwm_resources,
  	.num_resources  = 1,
};
static struct pwm_lookup board_pwm_lookup[] = {
    PWM_LOOKUP("iproc_pwmc", 0,"iproc_pwmc","pwm-0"),
    PWM_LOOKUP("iproc_pwmc", 1,"iproc_pwmc","pwm-1"),        
    PWM_LOOKUP("iproc_pwmc", 2,"iproc_pwmc","pwm-2"),
    PWM_LOOKUP("iproc_pwmc", 3,"iproc_pwmc","pwm-3"),

};

#endif /* CONFIG_IPROC_PWM || CONFIG_IPROC_PWM_MODULE */

#if defined(CONFIG_ASIU_PWM) || defined(CONFIG_ASIU_PWM_MODULE)
static struct resource asiu_pwm_resources[] = {
	[0] = {
		.start	= ASIU_PWM_CONTROL - ASIU_PWM_CONTROL_BASE,
		.end	= (ASIU_PWM_CONTROL - ASIU_PWM_CONTROL_BASE) + SZ_4K - 1,
		.flags	= IORESOURCE_MEM,
	},
	[1] = {
		.start	= CRMU_GENPLL_CONTROL0,
		.end	= CRMU_GENPLL_CONTROL0 + 0x100 - 1,
		.flags	= IORESOURCE_MEM,
	}
};

static struct platform_device board_asiu_pwm_device = {
	.name		= "asiu_pwmc",
	.id	        = -1,
  	.resource	= asiu_pwm_resources,
  	.num_resources  = ARRAY_SIZE(asiu_pwm_resources),
};

static struct pwm_lookup board_asiu_pwm_lookup[] = {
    PWM_LOOKUP("asiu_pwmc", 0,"asiu_pwmc","pwm-0"),
    PWM_LOOKUP("asiu_pwmc", 1,"asiu_pwmc","pwm-1"),        
    PWM_LOOKUP("asiu_pwmc", 2,"asiu_pwmc","pwm-2"),
    PWM_LOOKUP("asiu_pwmc", 3,"asiu_pwmc","pwm-3"),
    PWM_LOOKUP("asiu_pwmc", 4,"asiu_pwmc","pwm-4"),
    PWM_LOOKUP("asiu_pwmc", 5,"asiu_pwmc","pwm-5"),
};
#endif /* CONFIG_ASIU_PWM || CONFIG_ASIU_PWM_MODULE */

#if defined(CONFIG_IPROC_WDT) || defined(CONFIG_IPROC_WDT_MODULE)
/* watchdog */
static struct resource wdt_resources[] = {
	[0] = {
		.start	= IPROC_CCA_REG_BASE,
		.end	= IPROC_CCA_REG_BASE + 0x1000 - 1,
		.flags	= IORESOURCE_MEM,
	},
};

static struct platform_device board_wdt_device = {
	.name		=	"iproc_wdt",
	.id		    =	-1,
	.num_resources  = ARRAY_SIZE(wdt_resources),
  	.resource	= wdt_resources,
};
#endif /* CONFIG_IPROC_WDT || CONFIG_IPROC_WDT_MODULE */

#if defined(CONFIG_IPROC_SP805_WDT) || defined(CONFIG_IPROC_SP805_WDT_MODULE)
static struct wdt_platform_data iproc_wdt_data = {
	.rsts = {
		.reg = IPROC_SP805_WDT_BOOTSTATUS,
		.bit = IPROC_SP805_WDT_BOOTSTATUS_BIT,
		.needs_clear = 1,
	},
};
static struct amba_device iproc_sp805_wdt_device = {
	.dev = {
		.init_name = "sp805-wdt",
		.id = 0,
		.platform_data = &iproc_wdt_data,
	},
	.res = DEFINE_RES_MEM(IPROC_SP805_WDT_REG_BASE, SZ_4K),
	.periphid = 0x00141805,
};
#endif

#if defined(CONFIG_CRMU_WDT) || defined(CONFIG_CRMU_WDT_MODULE)
static struct wdt_platform_data crmu_wdt_data = {
	.rsts = {
		.reg = CRMU_WDT_BOOTSTATUS,
		.bit = CRMU_WDT_BOOTSTATUS_BIT,
		.needs_clear = 0,
	},
	.clk_con_id = "refclk",
};
static struct amba_device crmu_sp805_wdt_device = {
	.dev = {
		.init_name = "sp805-wdt.1",
		.id = 1,
		.platform_data = &crmu_wdt_data,
	},
	.res = DEFINE_RES_MEM(CRMU_WDT_REG_BASE, SZ_4K),
	.periphid = 0x00141805,
};
#endif


#if defined(CONFIG_IPROC_CCB_TIMER) || defined(CONFIG_IPROC_CCB_TIMER_MODULE)
static struct resource ccb_timer_resources[] = {
	[0] = {
		.start	= IPROC_CCB_TIMER_INT_START,
		.end	= IPROC_CCB_TIMER_INT_START + IPROC_CCB_TIMER_INT_COUNT - 1,
		.flags	= IORESOURCE_IRQ,
	},
	[1] = {
		.start	= IPROC_CCB_TIMER0_REGS_VA,
		.end	= IPROC_CCB_TIMER0_REGS_VA + 0x20 - 1,
		.flags	= IORESOURCE_MEM,
	},
	[2] = {
		.start	= IPROC_CCB_TIMER1_REGS_VA,
		.end	= IPROC_CCB_TIMER1_REGS_VA + 0x20 - 1,
		.flags	= IORESOURCE_MEM,
	},
	[3] = {
		.start	= IPROC_CCB_TIMER2_REGS_VA,
		.end	= IPROC_CCB_TIMER2_REGS_VA + 0x20 - 1,
		.flags	= IORESOURCE_MEM,
	},
	[4] = {
		.start	= IPROC_CCB_TIMER3_REGS_VA,
		.end	= IPROC_CCB_TIMER3_REGS_VA + 0x20 - 1,
		.flags	= IORESOURCE_MEM,
	},
};

static struct platform_device board_timer_device = {
	.name		=	"iproc_ccb_timer",
	.id		    =	-1,
	.num_resources  = ARRAY_SIZE(ccb_timer_resources),
  	.resource	= ccb_timer_resources,
};
#endif /* CONFIG_IPROC_CCB_TIMER || CONFIG_IPROC_CCB_TIMER_MODULE */

#if defined(CONFIG_IPROC_FA2)
#if defined(CONFIG_MACH_NSP)
static struct resource fa2_resources[] = {
    [0] = {
        .start  = CTF_CONTROL_REG, /* Macro is in socregs_nsp.h */
        .end    = CTF_CONTROL_REG + SZ_1K - 1,
        .flags  = IORESOURCE_MEM,
    },
    [1] = {
        .start  = 178, 
        .end    = 178,
        .flags  = IORESOURCE_IRQ,
    }
};
#endif
#endif /* CONFIG_IPROC_FA2 */

#ifdef CONFIG_IPROC_I2C
/* SMBus (I2C/BSC) block */

#if defined(CONFIG_MACH_HX4) || defined(CONFIG_MACH_KT2)
/* Helix4 */
#ifdef CONFIG_IPROC_PCT_BOARD
static struct i2c_board_info iproc_pct_i2c_devs __initdata = {
                I2C_BOARD_INFO("m41st85", 0x68),
};
#endif
static struct resource smbus_resources[] = {
    [0] = {
        .start  = ChipcommonB_SMBus_Config, /* Macro is in socregs_hx4.h */
        .end    = ChipcommonB_SMBus_Config + SZ_4K - 1,
        .flags  = IORESOURCE_MEM,
    },
    [1] = {
        .start  = 127, 
        .end    = 127,
        .flags  = IORESOURCE_IRQ,
    }
};

static struct resource smbus_resources1[] = {
    [0] = {
        .start  = ChipcommonB_SMBus1_SMBus_Config, /* Macro is in socregs_hx4.h */
        .end    = ChipcommonB_SMBus1_SMBus_Config + SZ_4K - 1,
        .flags  = IORESOURCE_MEM
    },
    [1] = {
        .start  = 128, /* macro in irqs.h (plat-iproc) */
        .end    = 128,
        .flags  = IORESOURCE_IRQ,
    }
};
#elif defined(CONFIG_MACH_HR2)
#define CMIC_COMMON_CORE_SMBUS  0x48000000
static struct resource smbus_resources[] = {
    [0] = {
        .start  = CMIC_COMMON_CORE_SMBUS,
        .end    = CMIC_COMMON_CORE_SMBUS + SZ_4K - 1,
        .flags  = IORESOURCE_MEM,
    },
};
#elif defined(CONFIG_MACH_SB2)
static struct resource smbus_resources[] = {
    [0] = {
        .start  = ChipcommonG_SMBus0_SMBus_Config,  /* Macro is in socregs_p7_open.h */
        .end    = ChipcommonG_SMBus0_SMBus_Config + SZ_4K - 1,
        .flags  = IORESOURCE_MEM,
    },
    [1] = {
        .start  = BCM_INT_ID_CCG_SMBUS0, 
        .end    = BCM_INT_ID_CCG_SMBUS0,
        .flags  = IORESOURCE_IRQ,
    }
};

static struct resource smbus_resources1[] = {
    [0] = {
        .start  = ChipcommonG_SMBus1_SMBus_Config, /* Macro is in socregs_hx4.h */
        .end    = ChipcommonG_SMBus1_SMBus_Config + SZ_4K - 1,
        .flags  = IORESOURCE_MEM
    },
    [1] = {
        .start  = BCM_INT_ID_CCG_SMBUS1, 
        .end    = BCM_INT_ID_CCG_SMBUS1,
        .flags  = IORESOURCE_IRQ,
    }
};
#elif defined(CONFIG_MACH_CYGNUS) || defined(CONFIG_MACH_GH)
static struct resource smbus_resources[] = {
    [0] = {
        .start  = ChipcommonG_SMBus0_SMBus_Config, 
        .end    = ChipcommonG_SMBus0_SMBus_Config + SZ_4K - 1,
        .flags  = IORESOURCE_MEM,
    },
    [1] = {
        .start  = 117, 
        .end    = 117,
        .flags  = IORESOURCE_IRQ,
    }
};

#if !defined(CONFIG_MACH_GH)
static struct resource smbus_resources1[] = {
    [0] = {
        .start  = ChipcommonG_SMBus1_SMBus_Config, 
        .end    = ChipcommonG_SMBus1_SMBus_Config + SZ_4K - 1,
        .flags  = IORESOURCE_MEM
    },
    [1] = {
        .start  = 118,
        .end    = 118,
        .flags  = IORESOURCE_IRQ,
    }
};
#endif
#elif defined(CONFIG_MACH_NSP)
/* Northstar plus */
static struct resource smbus_resources[] = {
    [0] = {
        .start  = ChipcommonB_SMBus_Config, /* Macro is in socregs_hx4.h */
        .end    = ChipcommonB_SMBus_Config + SZ_4K - 1,
        .flags  = IORESOURCE_MEM,
    },
    [1] = {
        .start  = 121, 
        .end    = 121,
        .flags  = IORESOURCE_IRQ,
    }
};
#else
/* Northstar */
static struct resource smbus_resources[] = {
    [0] = {
        .start  = CCB_SMBUS_START, /* Define this macro is socregs.h, or
                                    in iproc_regs.h */
        .end    = CCB_SMBUS_START + SZ_4K - 1,
        .flags  = IORESOURCE_MEM,
    },
    [1] = {
        .start  = BCM_INT_ID_CCB_SMBUS, /* macro in irqs.h (plat-iproc) */
        .end    = BCM_INT_ID_CCB_SMBUS,
        .flags  = IORESOURCE_IRQ,
    }
};
#endif

/* Common to Northstar, Helix4 */
static struct platform_device board_smbus_device = {
    .name= "iproc-smb",
    .id = 0,
    .dev= {
        .platform_data  = NULL, /* Can be defined, if reqd */
    },
    .num_resources = ARRAY_SIZE(smbus_resources),
    .resource = smbus_resources,
};
#if defined(CONFIG_MACH_HX4) || defined(CONFIG_MACH_KT2) || defined(CONFIG_MACH_CYGNUS)
static struct platform_device board_smbus_device1 = {
    .name= "iproc-smb",
    .id = 1,
    .dev= {
        .platform_data  = NULL, /* Can be defined, if reqd */
    },
    .num_resources = ARRAY_SIZE(smbus_resources1),
    .resource = smbus_resources1,
};

#endif /* CONFIG_MACH_HX4 */

#endif /* CONFIG_IPROC_I2C */

#if defined(CONFIG_IPROC_FA2)
#if defined(CONFIG_MACH_NSP)
static struct platform_device board_fa2_device = {
    .name= "fa2",
    .id = 0,
    .dev= {
        .platform_data  = NULL, /* Can be defined, if reqd */
    },
    .num_resources = ARRAY_SIZE(fa2_resources),
    .resource = fa2_resources,
};
#endif
#endif /* CONFIG_IPROC_FA2 */

#ifdef CONFIG_IPROC_USB3H
static struct resource bcm_xhci_resources[] = {
        [0] = {
                .start  = USB30_BASE,
                .end    = USB30_BASE + SZ_4K - 1,
                .flags  = IORESOURCE_MEM,
        },
        [1] = {
                .start  = BCM_INT_ID_USB3H2CORE_USB2_INT0,
                .end    = BCM_INT_ID_USB3H2CORE_USB2_INT0,
                .flags  = IORESOURCE_IRQ,
        },
};

static u64 xhci_dmamask = DMA_BIT_MASK(32);

static struct platform_device bcm_xhci_device = {
	.name		=	"bcm-xhci",
	.id		=	0,
	.dev		=	{
//		 .platform_data	= &xhci_platform_data,
		 .dma_mask       = &xhci_dmamask,
		 .coherent_dma_mask = DMA_BIT_MASK(32),
	},
  	.resource	= bcm_xhci_resources,
	.num_resources  = ARRAY_SIZE(bcm_xhci_resources),
};
#endif /* CONFIG_IPROC_USB3 */

#ifdef CONFIG_USB_EHCI_BCM

static u64 ehci_dmamask = DMA_BIT_MASK(32);

static struct resource usbh_ehci_resource[] = {
	[0] = {
		.start = IPROC_USB20_REG_BASE,
#if !defined(CONFIG_MACH_CYGNUS) && !defined(CONFIG_MACH_GH)
		.end = IPROC_USB20_REG_BASE + 0x0FFF,
#else
		.end = IPROC_USB20_REG_BASE + 0x07FF,
#endif
		.flags = IORESOURCE_MEM,
	},
	[1] = {
#if !defined(CONFIG_MACH_CYGNUS) && !defined(CONFIG_MACH_GH)
		.start = BCM_INT_ID_USB2H2CORE_USB2_INT,
		.end = BCM_INT_ID_USB2H2CORE_USB2_INT,
#else
		.start = 104,
		.end = 104,
#endif
		.flags = IORESOURCE_IRQ,
	},
};

static struct platform_device usbh_ehci_device =
{
	.name = "bcm-ehci",
	.id = 0,
	.resource = usbh_ehci_resource,
	.num_resources = ARRAY_SIZE(usbh_ehci_resource),
	.dev = {
		.dma_mask = &ehci_dmamask,
		.coherent_dma_mask = DMA_BIT_MASK(32),
	},
};
#endif

#ifdef CONFIG_USB_OHCI_BCM

static u64 ohci_dmamask = DMA_BIT_MASK(32);

static struct resource usbh_ohci_resource[] = {
	[0] = {
#if !defined(CONFIG_MACH_CYGNUS) && !defined(CONFIG_MACH_GH)
		.start = IPROC_USB20_REG_BASE + 0x1000,
		.end = IPROC_USB20_REG_BASE + 0x1000 + 0x0FFF,
#else
		.start = IPROC_USB20_REG_BASE + 0x800,
		.end = IPROC_USB20_REG_BASE + 0x800 + 0x07FF,
#endif
		.flags = IORESOURCE_MEM,
	},
	[1] = {
#if !defined(CONFIG_MACH_CYGNUS) && !defined(CONFIG_MACH_GH)
		.start = BCM_INT_ID_USB2H2CORE_USB2_INT,
		.end = BCM_INT_ID_USB2H2CORE_USB2_INT,
#else
		.start = 104,
		.end = 104,
#endif
		.flags = IORESOURCE_IRQ,
	},
};

static struct platform_device usbh_ohci_device =
{
	.name = "bcm-ohci",
	.id = 0,
	.resource = usbh_ohci_resource,
	.num_resources = ARRAY_SIZE(usbh_ohci_resource),
	.dev = {
		.dma_mask = &ohci_dmamask,
		.coherent_dma_mask = DMA_BIT_MASK(32),
	},
};
#endif

#ifdef CONFIG_SATA_AHCI_PLATFORM
static struct resource bcm_sata_resources[] = {
        [0] = {
                .start  = SATA_AHCI_GHC_HBA_CAP,
                .end    = SATA_AHCI_GHC_HBA_CAP + SZ_4K - 1,
                .flags  = IORESOURCE_MEM,
        },
        [1] = {
                .start  = BCM_INT_SATA,
                .end    = BCM_INT_SATA,
                .flags  = IORESOURCE_IRQ,
        },
};
static u64 sata_dmamask = DMA_BIT_MASK(32);

static struct platform_device bcm_sata_device = {
	.name		=	"strict-ahci",
	.id		=	0,
	.dev		=	{
		 .dma_mask       = &sata_dmamask,
		 .coherent_dma_mask = DMA_BIT_MASK(32),
	},
  	.resource	= bcm_sata_resources,
	.num_resources  = ARRAY_SIZE(bcm_sata_resources),
};
#endif

#if defined(CONFIG_BRCM_CE_28nm_OTP) || defined(CONFIG_BRCM_CE_28nm_OTP_MODULE)
static struct resource bcm_ce_28nm_otp_resources[] = {
    [0] = {
        .start  = CHIP_OTPC_REG_BASE,
        .end    = CHIP_OTPC_REG_BASE + SZ_4K - 1,
        .flags  = IORESOURCE_MEM,
    },
};
static struct platform_device bcm_ce_28nm_otp_device = {
	.name		=	"brcm-ce-28nm-otp",
	.id		    =	-1,
	.num_resources  = ARRAY_SIZE(bcm_ce_28nm_otp_resources),
  	.resource	= bcm_ce_28nm_otp_resources,
};
#endif

#ifdef CONFIG_DMAC_PL330
#include "../../$(BCM_DRIVER_PATH)/bcmdrivers/dma/pl330-pdata.h"
static struct iproc_pl330_data iproc_pl330_pdata =	{
	/* Non Secure DMAC virtual base address */
	.dmac_ns_base = IPROC_DMAC_REG_VA,
	/* Secure DMAC virtual base address */
	.dmac_s_base = IPROC_DMAC_REG_VA,
	/* # of PL330 dmac channels 'configurable' */
	.num_pl330_chans = 8,
	/* irq number to use */
	.irq_base = BCM_INT_ID_DMAC,
	/* # of PL330 Interrupt lines connected to GIC */
	.irq_line_count = 16,
};

static struct platform_device pl330_dmac_device = {
	.name = "iproc-dmac-pl330",
	.id = 0,
	.dev = {
		.platform_data = &iproc_pl330_pdata,
		.coherent_dma_mask  = DMA_BIT_MASK(64),
	},
};
#endif

#ifdef CONFIG_PL330_DMA
static AMBA_AHB_DEVICE(pl330_driver, "dma-pl330", 0x00041330, 
                       DMAC_pl330_DS, {(BCM_INT_ID_DMAC - 1)}, NULL);
#endif

#if defined(CONFIG_IPROC_MDIO) || defined(CONFIG_IPROC_MDIO_MODULE)
#include "../../../drivers/bcmdrivers/mdio/iproc_mdio.h"
static struct iproc_mdiobus_data iproc_mdiobus_data[] = {
	[0] = {
		.phybus_num = 0,
		.phybus_type = IPROC_MDIOBUS_TYPE_INTERNAL,
		.logbus_num = 0,
		.logbus_type = IPROC_MDIOBUS_TYPE_INTERNAL,
	},
	[1] = {
		.phybus_num = 2,
		.phybus_type = IPROC_MDIOBUS_TYPE_EXTERNAL,
		.logbus_num = 0,
		.logbus_type = IPROC_MDIOBUS_TYPE_EXTERNAL,
	},
};
#if (defined(CONFIG_MACH_GH) || defined(CONFIG_MACH_SB2))
static struct platform_device board_mdiobus_int0_device = {
	.name = "iproc_ccg_mdio",
	.id = 0,
	.dev = {
		.platform_data = &iproc_mdiobus_data[0],
	},
};
static struct platform_device board_mdiobus_ext0_device = {
	.name = "iproc_cmicd_mdio",
	.id = 0,
	.dev = {
		.platform_data = &iproc_mdiobus_data[1],
	},
};
#else
static struct platform_device board_mdiobus_int0_device = {
	.name = "iproc_ccb_mdio",
	.id = 0,
	.dev = {
		.platform_data = &iproc_mdiobus_data[0],
	},
};
static struct platform_device board_mdiobus_ext0_device = {
	.name = "iproc_ccb_mdio",
	.id = 1,
	.dev = {
		.platform_data = &iproc_mdiobus_data[1],
	},
};
#endif
#endif

#if defined(CONFIG_MACH_NSP)
void config_AHCI( void )
{
	volatile unsigned int sata_clk_enable;
	volatile unsigned int bustopcfg;

	printk("\nConfigure AHCI ...\n");
	sata_clk_enable = __raw_readl(SATA_M0_IDM_IO_CONTROL_DIRECT_VA);
    sata_clk_enable |= 0x1;
    __raw_writel( sata_clk_enable, SATA_M0_IDM_IO_CONTROL_DIRECT_VA);
    sata_clk_enable = __raw_readl(SATA_M0_IDM_IO_CONTROL_DIRECT_VA);
    udelay(1000);


    /* Reset core */
    __raw_writel(0x0, SATA_M0_IDM_IDM_RESET_CONTROL_VA);
    udelay(1000);
    sata_clk_enable = __raw_readl(SATA_M0_IDM_IDM_RESET_CONTROL_VA);
    bustopcfg = __raw_readl(SATA_TOP_CTRL_BUS_CTRL_VA);
    bustopcfg &= ~ (( 3 << 2) | ( 3 << 4 ));
    bustopcfg |= (( 2 << 2) | ( 2 << 4 ));//| ( 2<< 6 ));
    //bustopcfg |= ( ( 0x2 << 8 ) | ( 0x2 << 17 ) );
    __raw_writel(bustopcfg, SATA_TOP_CTRL_BUS_CTRL_VA);
}
void configure_SATA_PHY ( void )
{
	unsigned int i, tmp;
	void __iomem *bs = ioremap(AXIIC_sata_m0_fn_mod, 4);

	void __iomem *id = IOMEM(IPROC_CCA_CORE_REG_VA);

	printk("\nConfigure PHY ...\n");

	__raw_writel(0x0150,SATA3_PCB_UPPER_REG15_VA);
	__raw_writel( 0xF6F6, SATA3_PCB_UPPER_REG0_VA);
	__raw_writel( 0x2e96, SATA3_PCB_UPPER_REG1_VA);

	__raw_writel(0x0160,SATA3_PCB_UPPER_REG15_VA);
	__raw_writel( 0xF6F6, SATA3_PCB_UPPER_REG0_VA);
	__raw_writel( 0x2e96, SATA3_PCB_UPPER_REG1_VA);

	//access SATA PLL
	__raw_writel(0x0050,SATA3_PCB_UPPER_REG15_VA);
	//Audio PLL 0x8B
	i = __raw_readl(SATA3_PCB_UPPER_REG11_VA);
	i &= ~ (( 0x1f) << 9 );
	i |= ( 0xC << 9);
	__raw_writel( i, SATA3_PCB_UPPER_REG11_VA);

	//Sequence for restarting PLL. Please try after changing the divider.
	//'PLL_CapCtrl[10] = 1, PLL_CapCtrl[7:0] = F0h
	//SATA3_PLL: PLL Register Bank address 0x50

	//set register SATA3_PLL_capControl ( 0x85 )
	i = __raw_readl(SATA3_PCB_UPPER_REG5_VA);
	i =  ( i | 0x4f0 ) & 0xFF0;
	__raw_writel( i, SATA3_PCB_UPPER_REG5_VA);

	//'PLL_Ctrl[13:12] = 11
	//Set register SATA3_PLL_CONTROL ( 0x81 )
	i = __raw_readl(SATA3_PCB_UPPER_REG1_VA);
	i |= 0x3000;
	__raw_writel( i, SATA3_PCB_UPPER_REG1_VA);

	//'PLL_ReStart
	i = __raw_readl(SATA3_PCB_UPPER_REG1_VA);
	i &= 0x7FFF;
	__raw_writel( i, SATA3_PCB_UPPER_REG1_VA);
	mdelay(100);
	i = __raw_readl(SATA3_PCB_UPPER_REG1_VA);
	i |= 0x8000;
	__raw_writel( i, SATA3_PCB_UPPER_REG1_VA);
	mdelay(1000);
	__raw_writel(0x0000,SATA3_PCB_UPPER_REG15_VA);
	i = __raw_readl(SATA3_PCB_UPPER_REG1_VA);
	tmp = readl(id);
	if (tmp == NSP_CHIPID)
	{
		tmp = readl(bs);
		tmp |= 0x3;
		writel(tmp, bs);
		tmp = readl(bs);
	}
}
#endif

void __init board_map_io(void)
{

	/*
	 * Install clock sources in the lookup table.
	 */
	clkdev_add_table(board_clk_lookups,
			ARRAY_SIZE(board_clk_lookups));

	/* Map machine specific iodesc here */
	northstar_map_io();
}

void __init iproc_init_early(void)
{

	/*
	 * SDK allocates coherent buffers from atomic
	 * context. Increase size of atomic coherent pool to make sure such
	 * the allocations won't fail.
	 */
#ifdef CONFIG_CMA 
	init_dma_coherent_pool_size(SZ_1M * 16);
#endif
}

static struct platform_device *board_sata_device[] __initdata = {
#ifdef CONFIG_SATA_AHCI_PLATFORM
	&bcm_sata_device,
#endif
};

static struct platform_device *board_devices[] __initdata = {
#ifdef CONFIG_IPROC_I2C
    &board_smbus_device,
#if defined(CONFIG_MACH_HX4) || defined(CONFIG_MACH_KT2) || defined(CONFIG_MACH_CYGNUS)
    &board_smbus_device1,
#endif
#endif /* CONFIG_IPROC_I2C */

#if defined(CONFIG_IPROC_MDIO) || defined(CONFIG_IPROC_MDIO_MODULE)
	&board_mdiobus_int0_device,
	&board_mdiobus_ext0_device,
#endif

#if defined(CONFIG_IPROC_FA2)
#if defined(CONFIG_MACH_NSP)
    &board_fa2_device,
#endif
#endif /* FA+ */

#if defined(CONFIG_IPROC_CCB_TIMER) || defined(CONFIG_IPROC_CCB_TIMER_MODULE)
    &board_timer_device,
#endif /* CONFIG_IPROC_CCB_TIMER || CONFIG_IPROC_CCB_TIMER_MODULE */
#if defined(CONFIG_IPROC_WDT) || defined(CONFIG_IPROC_WDT_MODULE)
    &board_wdt_device,
#endif /* CONFIG_IPROC_WDT || CONFIG_IPROC_WDT_MODULE */
#if defined(CONFIG_IPROC_PWM) || defined(CONFIG_IPROC_PWM_MODULE)
	&board_pwm_device,
#endif /* CONFIG_IPROC_PWM || CONFIG_IPROC_PWM_MODULE */
#if defined(CONFIG_ASIU_PWM) || defined(CONFIG_ASIU_PWM_MODULE)
	&board_asiu_pwm_device,
#endif /* CONFIG_ASIU_PWM || CONFIG_ASIU_PWM_MODULE */
#ifdef CONFIG_IPROC_USB3H
	&bcm_xhci_device,
#endif
#ifdef CONFIG_USB_EHCI_BCM
	&usbh_ehci_device,
#endif
#ifdef CONFIG_USB_OHCI_BCM
	&usbh_ohci_device,
#endif
#ifdef CONFIG_DMAC_PL330
	&pl330_dmac_device,
#endif
#if defined(CONFIG_BRCM_CE_28nm_OTP) || defined(CONFIG_BRCM_CE_28nm_OTP_MODULE)
	&bcm_ce_28nm_otp_device,
#endif
};

static struct amba_device *amba_devs[] __initdata = {
#if defined(CONFIG_IPROC_SP805_WDT) || defined(CONFIG_IPROC_SP805_WDT_MODULE)
	&iproc_sp805_wdt_device,
#endif
#if defined(CONFIG_CRMU_WDT) || defined(CONFIG_CRMU_WDT_MODULE)
	&crmu_sp805_wdt_device,
#endif
};

#ifdef CONFIG_IPROC_H2B_BOARD
static struct i2c_board_info iproc_h2b_i2c_devs[] __initdata = {
	{ I2C_BOARD_INFO("max6639", 0x58 >> 1) }, /* fan controller */
	{ I2C_BOARD_INFO("ltc4215", 0x80 >> 1) }, /* current monitor */
	{ I2C_BOARD_INFO("lm75",    0x92 >> 1) }, /* thermal sensor */
	{ I2C_BOARD_INFO("lm75",    0x94 >> 1) }, /* thermal sensor */
	{ I2C_BOARD_INFO("m41t80",  0xD0 >> 1) }, /* RTC */
};
#endif /* CONFIG_IPROC_H2B_BOARD */

static void __init board_add_devices(void)
{
	int i;

	platform_add_devices(board_devices, ARRAY_SIZE(board_devices));
#ifdef CONFIG_IPROC_PCT_BOARD
	i2c_register_board_info(0, &iproc_pct_i2c_devs, 1);
#endif
#ifdef CONFIG_IPROC_H2B_BOARD
	i2c_register_board_info(0, iproc_h2b_i2c_devs, 
	                        ARRAY_SIZE(iproc_h2b_i2c_devs));
#endif
//    if (iproc_get_chipid() == 53010) {
//    }
	for (i = 0; i < ARRAY_SIZE(amba_devs); i++) {
		amba_device_register(amba_devs[i], &iomem_resource);
	}
}

static void __init board_add_sata_device(void)
{
	platform_add_devices(board_sata_device, 1);
}


#if defined(CONFIG_IPROC_IPGS7XXTX_BOARD) || defined(CONFIG_IPROC_H2B_BOARD)
static struct spi_board_info max3421_spi_device[] = {
	{
		.modalias = "max3421-hcd",
		.platform_data = NULL,
		.controller_data = NULL,
		.max_speed_hz = CONFIG_IPROC_QSPI_MAX_HZ,
		.bus_num = IPROC_QSPI_BUS_NUMBER,
		.chip_select = 1,
		.mode = SPI_MODE_3,
		.irq = 123,
	},
};
#else
/* SPI device info of GSIO(SPI) interface */
static struct spi_board_info bcm5301x_spi_device[] = {
	{
		.modalias = "spidev",
		.platform_data = NULL,
		.controller_data = NULL,
		.max_speed_hz = 2 * 1000 * 1000,
		.bus_num = 0,
		.chip_select = 0,
		.mode = SPI_MODE_0,
	},
};
#endif

void __init board_timer_init(void)
{
	northstar_timer_init(&clk_ref);
}

struct sys_timer board_timer = {
	.init   = board_timer_init,
};

#if defined(CONFIG_SND_BCM5830X_SVK) || defined(CONFIG_SND_BCM5830X_SVK_MODULE)
struct i2c_board_info audio_i2c1_bus[] =  {
    {
        I2C_BOARD_INFO("tlv320aic3x", 0x18),  /* audio codec */
    },
};
struct i2c_board_info audio_i2c0_bus[] =  {
    {
        I2C_BOARD_INFO("wm8750", 0x1a),  /* audio codec */
    },
};

static struct platform_device bcm5830x_audio_device = {
	.name		= "bcm5830x-audio",
	.id		= -1,
};

static struct resource bcm5830x_i2s_resources[] = {
	[0] = {
		.name   = "dmairq",
		.flags  = IORESOURCE_IRQ,
		.start  = CHIP_INTR1__ASIU_AUDIO_DMA_INTR + 161,
		.end    = CHIP_INTR1__ASIU_AUDIO_DMA_INTR + 161,
	},

};

static struct platform_device bcm5830x_i2s0_device = {
	.name		= "bcm5830x-i2s",
	.id		= 0,
};

static struct platform_device bcm5830x_i2s1_device = {
	.name		= "bcm5830x-i2s",
	.id		= 1,
};

static struct platform_device bcm5830x_i2s2_device = {
	.name		= "bcm5830x-i2s",
	.id		= 2,
};
/* spdif port */
static struct platform_device bcm5830x_i2s3_device = {
	.name		= "bcm5830x-i2s",
	.id		= 3,
};

static struct platform_device bcm5830x_pcm_device = {
	.name		= "bcm5830x-pcm",
	.id		= -1,
};

static struct platform_device bcm5830x_spdif_device = {
	.name		= "spdif-dit",
	.id		= -1,
};
#endif

#if defined(CONFIG_IPROC_IPM6100_BOARD) || defined(CONFIG_IPROC_IPM6100_VEGA_BOARD)
#define NORTHSTAR_GPIO_PULLUP_REG_BASE       0x1800C1C0
#define NORTHSTAR_GPIO_PULLUP_REG_VAL        0xFCF
#define CPLD_MIN_ADDR                        0x31  /* Slot 1 i2c addr and it will incremnt 1 for next slot */
#define CPLD_MAX_ADDR                        0x3F  /* Slot is not plugged in */
#define CPLD_BLADE_MISC_REG                  0xc  
#define CPLD_ADDR                            0x30  /* base addr */
#define CCA_GPIO_OUT_EN                      0x68
#define GPIO_0_BIT                           0x1 
#define GPIO_1_BIT                           0x2

#if defined(CONFIG_IPROC_IPM6100_BOARD) || defined(CONFIG_IPROC_IPM6100_VEGA_BOARD)
static int __init m6100_usb_init (void)
{
  unsigned char data = 0;
        data = 0;
        /* i2c address will be taken care automatically in m6100_i2c_read_write function,
           sending 0 as address is not relavent */
        m6100_i2c_read_write(0, CPLD_BLADE_MISC_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);
  return 0;
}
late_initcall(m6100_usb_init);
#endif

static volatile void *vaddr = NULL;
static void gpioRempapCheck(void)
{
   if(vaddr == NULL)
   { 
      vaddr = ioremap(IPROC_CCA_REG_BASE,0x100);
   }
#if defined(CONFIG_IPROC_IPM6100_VEGA_BOARD)        
   {
     static volatile *vaddr_vega = NULL;
     if(vaddr_vega == NULL)
     { 
       vaddr_vega = ioremap_nocache(NORTHSTAR_GPIO_PULLUP_REG_BASE,0x10);
       if(vaddr_vega != NULL)
       { 
         u32 reg;
         /* setting 0 for bit-1 gpio1 as input */
          reg = ioread32(vaddr_vega);
          reg |= NORTHSTAR_GPIO_PULLUP_REG_VAL;
          iowrite32(reg, vaddr_vega);
          reg = ioread32(vaddr_vega);
          iounmap(vaddr_vega);
       }
     }
   }
#endif
}

void cpldI2cAddressDetermine(void)
{
  int err; u8 data;

  if(cpldI2cAddress >= CPLD_MIN_ADDR && cpldI2cAddress <= CPLD_MAX_ADDR)
    return;

  cpldI2cAddress = CPLD_ADDR | 1;
  err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RESET_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &data);
  if (err < 0)
  {
    cpldI2cAddress = CPLD_ADDR | 2;
    err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RESET_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &data);
    if (err < 0)
    {
      cpldI2cAddress = CPLD_ADDR | 3;
      err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RESET_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &data);
      if (err < 0)
      {
        cpldI2cAddress = CPLD_ADDR | 4;
        err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RESET_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &data);
        if (err < 0)
        {
          cpldI2cAddress = CPLD_ADDR | 0xf;
          err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RESET_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &data);
          if(err < 0)
          {
            printk(KERN_CRIT "unable to determine the cpld i2c address err:%d **** \n",err);
          }
        }  
      }
    }
  } /* end of cpld i2c probe */
}

void m6100_enable_internal_irq(int irqNo, void *ptr)
{
    u32 reg;
    gpioRempapCheck();
    if(vaddr != NULL)
    {
        /* setting 0 for bit-1 gpio 0 & 1 as input */
        reg = ioread32(vaddr+CCA_GPIO_OUT_EN);
        reg &= ~((u32) 3 << 0);
        iowrite32(reg, (vaddr+CCA_GPIO_OUT_EN));

        /* setting gpio 0 & 1 as active low */
        reg = ioread32(vaddr+ChipcommonA_GPIOIntPolarity_BASE);
        reg |= ((u32) 3 << 0);
        iowrite32(reg, (vaddr+ChipcommonA_GPIOIntPolarity_BASE));

        /* setting gpio 0 & 1 interrupt mask */
        reg = ioread32(vaddr+ChipcommonA_GPIOIntMask_BASE);
        reg |= ((u32) 3 << 0);
        iowrite32(reg, (vaddr+ChipcommonA_GPIOIntMask_BASE));

        /* setting global interrupt enable */
        reg = ioread32(vaddr+IPROC_CCA_INT_MASK);
        reg |= 1;
        iowrite32(reg, (vaddr+IPROC_CCA_INT_MASK));
    }
    else
    {
      printk(KERN_INFO "***m6100_enable_internal_irq vaddr is NULL\n");
    }
}
void m6100_enable_loop_internal_irq(int irqNo, void *ptr)
{
    u32 reg;
    if(vaddr != NULL)
    {
      /* setting gpio1 as interrupt mask--disable interrupt */
         reg = ioread32(vaddr+ChipcommonA_GPIOIntMask_BASE);
         reg |= ((u32) 3 << 0);
         iowrite32(reg, (vaddr+ChipcommonA_GPIOIntMask_BASE));
    }
}
void m6100_disable_internal_irq(int irqNo)
{
    u32 reg;
    if(vaddr != NULL)
    {
         /* setting gpio1 as interrupt mask--disable interrupt */
         reg = ioread32(vaddr+ChipcommonA_GPIOIntMask_BASE);
         reg &= ~((u32) 3 << 0);
         iowrite32(reg, (vaddr+ChipcommonA_GPIOIntMask_BASE));
    }
}

void m6100_irq_process_fptr(int irqNo, void *ptr)
{
    u32 reg;
    u8 *ptr1 = (u8 *)ptr;
    if(vaddr != NULL)
    {
        /* setting global interrupt enable */
        reg = ioread32(vaddr+IPROC_CCA_INT_STS);
        if((reg & 0x1) == 1) /* gpio interrupt */
        { 
           ptr1[0] = 1;   /* GPIO interrupt */
        } 
        else
        {
            ptr1[0] = 0;
        } 
    }
}
#define CPLD_INTR_STA_REG   0x4
void m6100_irq_process_data(int irqNo, void *ptr)
{
    u32 reg;
    u8 *ptr1 = (u8 *)ptr;
    s32 err = 0;

    if(vaddr != NULL)
    {
        if(ptr1[0] == 1) /* gpio interrupt */
        {
           reg = ioread32(vaddr+ChipcommonA_GPIOInput_BASE);
           if( (reg & GPIO_1_BIT) == 0)
           {
               ptr1[0] = GPIO_1_BIT;   /* Some define value to inform that it is GPIO1 interrupt */
               err = m6100_i2c_read_write(cpldI2cAddress, CPLD_INTR_STA_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &ptr1[1]);
               if(err < 0)
               {
                  udelay(100);
                  err = m6100_i2c_read_write(cpldI2cAddress, CPLD_INTR_STA_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &ptr1[1]);
                  if(err < 0)
                  {
                     udelay(100);
                     err = m6100_i2c_read_write(cpldI2cAddress, CPLD_INTR_STA_REG, I2C_SMBUS_READ, I2C_SMBUS_BYTE_DATA, &ptr1[1]);
                     if(err < 0)
                        ptr1[1] = 0xff; /* returning that there is no interrupt */
                  }
               } 
           }   
           if( (reg & GPIO_0_BIT) == 0 )
           {
               ptr1[0] = GPIO_0_BIT;   /* Some define value to inform that it is GPIO0 interrupt */
           }
        }
    }
}

s32 m6100_i2c_read_write(u8 address, u8 offset, u8 rd_wr, u8 rd_wr_mode, u8 *data)
{
    int err = 0;
    union i2c_smbus_data i2cdata;
    if (adap == NULL)
    {
       adap = i2c_get_adapter(0);
    }
    cpldI2cAddressDetermine();
    i2cdata.byte = *data;  /* For write & read */
    err = i2c_smbus_xfer(adap, cpldI2cAddress, 0, rd_wr,
                   offset, rd_wr_mode, &i2cdata);
 
    *data = i2cdata.byte;  /* For read */
    if(err < 0)
          printk(KERN_INFO "m6100_i2c_read_write failure err: %x address:%x offset:%x rd_wr:%x rd_wr_mode:%x \n",
                                err, cpldI2cAddress, offset, rd_wr, rd_wr_mode);
    return err;
}

#define WATCHDOG_TIMER_INTERVAL               5000 /* 5 seconds ms */
#define CPLD_WDT_ENABLE                       0x8
#define CPLD_RST_WDT_REG                      0x1
/*
** Disable the watchdog feature
*/
void watchdog_disable(void)
{
    u8 data = 0; /* WDT disable */
    s32 err = 0;

        err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RST_WDT_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);

        if (err < 0)
           printk (KERN_EMERG "Error:watchdog_disable: m6100_i2c_read_write access failure.\n");
        else
           printk (KERN_INFO "cpld watchdog disable success\n");
}
/*
** Service watchdog.
*/
void watchdog_service(void)
{

    u8 data = CPLD_WDT_ENABLE;
    s32 err = 0;

        err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RST_WDT_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);
        if(err < 0)
        {
            udelay(100);
            err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RST_WDT_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);
            if(err < 0)
            {
                udelay(100);
                err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RST_WDT_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);
            } 
        }
        if (err < 0)
           printk (KERN_EMERG "Error:watchdog_service:m6100_i2c_read_write access failure.\n");
}
/*
** Enable the watchdog feature
*/
int watchdog_enable(void)
{
    u8 data = CPLD_WDT_ENABLE;
    s32 err = 0;

        err = m6100_i2c_read_write(cpldI2cAddress, CPLD_RST_WDT_REG, I2C_SMBUS_WRITE, I2C_SMBUS_BYTE_DATA, &data);   
       
        if (err < 0) 
           printk (KERN_EMERG "Error: mtchdog_enable:6100_i2c_read_write access failure.\n");
        else
           printk (KERN_INFO "watchdog_enable: cpld watchdog register success\n");

        return err;
}
/*
** Get the Watchdog Timer Interval in ms
*/
unsigned int watchdog_get_timer_interval(void)
{
        return WATCHDOG_TIMER_INTERVAL;
}

#endif

void __init board_init(void)
{
	uint32_t	sata_enable=0;
#if (defined(CONFIG_MACH_NSP) && defined(CONFIG_IPROC_OTP))
	void		*oh;
#endif /* (defined(CONFIG_MACH_NSP) && defined(CONFIG_IPROC_OTP)) */

	printk(KERN_DEBUG "board_init: Enter\n");

	/*
	 * Add common platform devices that do not have board dependent HW
	 * configurations
	 */
	board_add_common_devices(&clk_ref);
#ifndef CONFIG_MACH_CYGNUS
#ifndef CONFIG_MACH_NS 
	/* register IDM timeout interrupt handler */
	request_idm_timeout_interrupts();
#endif
#endif

#if defined(CONFIG_MACH_NSP)
  #if defined(CONFIG_IPROC_OTP)
	/* read otp row 0xd to figure if sata is enabled */
	oh = bcm5301x_otp_init();
	if (oh != NULL)
	{
		bcm5301x_otp_read_dword(oh, 0xd, &sata_enable);
		printk("%s: %d %08x\n", __FUNCTION__, __LINE__, sata_enable);
		if ((sata_enable & 0x40000000) == 0x40000000)
		{
			config_AHCI();
			configure_SATA_PHY();
		}
		bcm5301x_otp_exit();
	}
	else
		printk("%s: %d bcm5301x_otp_init failed\n", __FUNCTION__, __LINE__);
  #else /* defined(CONFIG_IPROC_OTP) */
	printk("%s(): IPROC OTP not configured, can not determine if SATA is enabled.\n", __FUNCTION__);
  #endif /* defined(CONFIG_IPROC_OTP) */
#endif

	board_add_devices();
	if ((sata_enable & 0x40000000) == 0x40000000)
		board_add_sata_device();


#if defined(CONFIG_IPROC_SD) || defined(CONFIG_IPROC_SD_MODULE)   
#if defined(CONFIG_MACH_NS)
	/* only bcm53012 support sdio */
	if ((__REG32(IPROC_IDM_REGISTER_VA + 0xd500) & 0xc) == 0x0) {
#endif	
		setup_sdio();
#if defined(CONFIG_MACH_NS)
	}
#endif	
#endif

#if defined(CONFIG_IPROC_PWM) || defined(CONFIG_IPROC_PWM_MODULE)
        __raw_writel(0xf, IPROC_CCB_GPIO_REG_VA + IPROC_GPIO_CCB_AUX_SEL);
    	pwm_add_table(board_pwm_lookup, ARRAY_SIZE(board_pwm_lookup));
#endif  

#if defined(CONFIG_ASIU_PWM) || defined(CONFIG_ASIU_PWM_MODULE)
	pwm_add_table(board_asiu_pwm_lookup, ARRAY_SIZE(board_asiu_pwm_lookup));
#endif

#if defined(CONFIG_MACH_CYGNUS)
	iproc_init_dev();
#else
#if !defined(CONFIG_IPROC_IPGS7XXTX_BOARD) && !defined(CONFIG_IPROC_H2B_BOARD)
	/* Register SPI device info */
	spi_register_board_info(bcm5301x_spi_device, 
	ARRAY_SIZE(bcm5301x_spi_device)); 
#endif
#endif /* defined(CONFIG_MACH_CYGNUS) */

#if defined(CONFIG_SND_BCM5830X_SVK) || defined(CONFIG_SND_BCM5830X_SVK_MODULE)
	i2c_register_board_info(1, audio_i2c1_bus, ARRAY_SIZE(audio_i2c1_bus));
	i2c_register_board_info(0, audio_i2c0_bus, ARRAY_SIZE(audio_i2c0_bus));
	platform_device_register(&bcm5830x_audio_device);
	platform_device_register(&bcm5830x_i2s0_device);
	platform_device_register(&bcm5830x_i2s1_device);
	platform_device_register(&bcm5830x_i2s2_device);
	platform_device_register(&bcm5830x_i2s3_device);
	platform_device_register(&bcm5830x_pcm_device);
	platform_device_register(&bcm5830x_spdif_device);
#endif
	/* Register SPI device info */
#if defined(CONFIG_IPROC_IPGS7XXTX_BOARD) || defined(CONFIG_IPROC_H2B_BOARD)
	spi_register_board_info(max3421_spi_device,
	ARRAY_SIZE(max3421_spi_device));
#else
	spi_register_board_info(bcm5301x_spi_device,
	ARRAY_SIZE(bcm5301x_spi_device));
#endif

#if defined(CONFIG_IPROC_IPM6100_BOARD) || defined(CONFIG_IPROC_IPM6100_VEGA_BOARD) 
        brcpld_irq_enable_register(&m6100_enable_internal_irq);
        brcpld_irq_process_register(&m6100_irq_process_fptr);
        brcpld_irq_loop_enable_register(&m6100_enable_loop_internal_irq);
        brcpld_irq_disable_register(&m6100_disable_internal_irq);
        brcpld_irq_process_data(&m6100_irq_process_data);
        gpioRempapCheck();
#endif

#if defined(CONFIG_IPROC_IPM4300_R2_BOARD) || defined(CONFIG_IPROC_IPM4300_NS_BOARD)
	m4300_board_init();
#endif
#if defined(CONFIG_IPROC_IPM4200_BOARD)
        m4200_board_init();
#endif
#if defined(CONFIG_IPROC_XS7XXT_BOARD)
	xs7xxt_board_init();
#endif
	printk(KERN_DEBUG "board_init: Done\n");
}

MACHINE_START(IPROC, "Broadcom iProc")

// 	Used micro9 as a reference.  Micro9 removed these two fields,
//	and replaced them with a call to ep93xx_map_io(), which in turn
// 	calls iotable_init().  Northstar appears to have an equivalent
//	init (refer to northstar_io_desc[] array, in io_map.c
	.map_io = board_map_io,
	.init_early	= iproc_init_early,
	.init_irq = iproc_init_irq,
        .handle_irq     = gic_handle_irq,
	.timer  = &board_timer,
	.init_machine = board_init,
MACHINE_END

