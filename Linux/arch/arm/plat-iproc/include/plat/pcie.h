/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
/*
 *
 * Broadcom IPROC based Northstar SoC PCIe handling.
 *
 */

#ifndef __PLAT_PCIE_H
#define __PLAT_PCIE_H

u32 iproc_pcie_dev_id(void __iomem *base);
u32 iproc_pcie_rev(void __iomem *base);
int iproc_pcie_link_up(void __iomem *base);
int iproc_pcie_x4_mode(void __iomem *base);
int iproc_pcie_get_local_bus_nr(void __iomem *base);
void iproc_pcie_set_local_bus_nr(void __iomem *base, int nr);
void iproc_pcie_setup(void __iomem *base,
		      struct mbus_dram_target_info *dram);
int iproc_pcie_rd_conf(void __iomem *base, struct pci_bus *bus,
		       u32 devfn, int where, int size, u32 *val);
int iproc_pcie_rd_conf_tlp(void __iomem *base, struct pci_bus *bus,
			   u32 devfn, int where, int size, u32 *val);
int iproc_pcie_rd_conf_wa(void __iomem *wa_base, struct pci_bus *bus,
			  u32 devfn, int where, int size, u32 *val);
int iproc_pcie_wr_conf(void __iomem *base, struct pci_bus *bus,
		       u32 devfn, int where, int size, u32 val);


/*
 * Broadcom IPROC address maps.
 *
 * phys		virt		size
 * c8000000	fdb00000	1M		Cryptographic SRAM
 * 08000000	@runtime	128M	PCIe-0 Memory space
 * 40000000	@runtime	128M	PCIe-1 Memory space
 * 48000000	@runtime	128M	PCIe-2 Memory space
 * f1000000	fde00000	8M		on-chip south-bridge registers
 * f1800000	fe600000	8M		on-chip north-bridge registers
 * f2000000	fee00000	1M		PCIe-0 I/O space
 * f2100000	fef00000	1M		PCIe-1 I/O space
 * 18000000				4KB		Chip Common A
 * 18001000				60KB	Chip Common B
 * 18011000				4KB		DMA core register region
 * 
 * 0x1802_4000 - 0x1802_4FFF	GMAC0 Core register region	4KB
 * 0x1802_5000 - 0x1802_5FFF	GMAC1 Core register region	4KB
 * 0x1802_6000 - 0x1802_6FFF	GMAC2 Core register region	4KB
 * 0x1802_7000 - 0x1802_7FFF	GMAC3 Core register region	4KB
 * 0x1801_2000 - 0x1801_2FFF	PCIE-AXI Bridge 0 Core register region	4KB
 * 0x1801_3000 - 0x1801_3FFF	PCIE-AXI Bridge 1 Core register region	4KB
 * 0x1801_4000 - 0x1801_4FFF	PCIE-AXI Bridge 2 Core register region	4KB
 * 0x1802_1000 - 0x1802_1FFF	USB 2.0 Core register region	4KB
 * 0x1802_2000 - 0x1802_2FFF	USB 3.0 Core register region	4KB
 * 0x1802_3000 - 0x1802_3FFF	USB 2.0 PHY Control register region	4KB
 * 0x1802_0000 - 0x1802_0FFF	SDIO 3.0 Core register region	4KB
 * 0x1801_0000 - 0x1801_0FFF	DDR2/DDR3 Controller register region	4KB
 * 0x180x_xxxx - 0x180x_xxxx	NAND Flash Controller register region	4KB
 * 0x180x_xxxx - 0x180x_xxxx	SPI flash controller register region	4KB
 * 0x1800_9000 - 0x1800_9FFF	TDM/I2S core register region	4KB
*/

#define IPROC_CESA_PHYS_BASE		0xc8000000
#define IPROC_CESA_VIRT_BASE		0xfdb00000
#define IPROC_CESA_SIZE				SZ_1M

#define IPROC_PCIE0_MEM_PHYS_BASE	0x08000000
#define IPROC_PCIE0_MEM_SIZE		SZ_128M

#define IPROC_PCIE1_MEM_PHYS_BASE	0x40000000
#define IPROC_PCIE1_MEM_SIZE		SZ_128M

#define IPROC_PCIE2_MEM_PHYS_BASE	0x48000000
#define IPROC_PCIE2_MEM_SIZE		SZ_128M

#define IPROC_BOOTROM_PHYS_BASE		0xf8000000
#define IPROC_BOOTROM_SIZE			SZ_128M

#define IPROC_SCRATCHPAD_PHYS_BASE	0xf0000000
#define IPROC_SCRATCHPAD_VIRT_BASE	0xfdd00000
#define IPROC_SCRATCHPAD_SIZE		SZ_1M

#define IPROC_SB_REGS_PHYS_BASE		0xf1000000
#define IPROC_SB_REGS_VIRT_BASE		0xfde00000
#define IPROC_SB_REGS_SIZE			SZ_8M

#define IPROC_NB_REGS_PHYS_BASE		0xf1800000
#define IPROC_NB_REGS_VIRT_BASE		0xfe600000
#define IPROC_NB_REGS_SIZE			SZ_8M

#define IPROC_PCIE0_IO_PHYS_BASE	0xf2000000
#define IPROC_PCIE0_IO_VIRT_BASE	0xfee00000
#define IPROC_PCIE0_IO_BUS_BASE		0x00000000
#define IPROC_PCIE0_IO_SIZE			SZ_4K

#define IPROC_PCIE1_IO_PHYS_BASE	0xf2100000
#define IPROC_PCIE1_IO_VIRT_BASE	0xfef00000
#define IPROC_PCIE1_IO_BUS_BASE		0x00100000
#define IPROC_PCIE1_IO_SIZE			SZ_4K

#define IPROC_PCIE2_IO_PHYS_BASE	0xf2200000
#define IPROC_PCIE2_IO_VIRT_BASE	0xff000000
#define IPROC_PCIE2_IO_BUS_BASE		0x00200000
#define IPROC_PCIE2_IO_SIZE			SZ_4K

/*
 * IPROC Core Registers Map
 */

/* SPI, I2C, UART */
#define IPROC_I2S_PHYS_BASE		(0x18009000)
#define IPROC_UART0_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0x12000)
#define IPROC_UART0_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0x12000)
#define IPROC_UART1_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0x12100)
#define IPROC_UART1_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0x12100)
#define IPROC_UART2_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0x12200)
#define IPROC_UART2_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0x12200)
#define IPROC_UART3_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0x12300)
#define IPROC_UART3_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0x12300)
#define IPROC_SPI0_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0x10600)
#define IPROC_NAND_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0x14600)

/* North-South Bridge */
#define BRIDGE_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0x20000)

/* Cryptographic Engine */
#define IPROC_CRYPT_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0x30000)

/* PCIe 0 */
#define IPROC_PCIE0_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0x40000)

/* USB */
#define IPROC_USB2_PHYS_BASE				(0x18021000)
#define IPROC_USB3_PHYS_BASE				(0x18022000)
#define IPROC_USB2_PHY_CONTROL_PHYS_BASE	(0x18023000)

/* Gigabit Ethernet */
#define IPROC_GE00_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0x70000)

/* PCIe 1 */
#define IPROC_PCIE1_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0x80000)

/* PCIe 2 */
#define IPROC_PCIE2_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0xC0000)


/* MPP, GPIO, Reset Sampling */
#define IPROC_MPP_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0xd0200)
#define IPROC_PMU_MPP_GENERAL_CTRL (IPROC_MPP_VIRT_BASE + 0x10)
#define IPROC_RESET_SAMPLE_LO	(IPROC_MPP_VIRT_BASE | 0x014)
#define IPROC_RESET_SAMPLE_HI	(IPROC_MPP_VIRT_BASE | 0x018)
#define IPROC_GPIO_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0xd0400)
#define IPROC_MPP_GENERAL_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0xe803c)
#define  IPROC_AU1_SPDIFO_GPIO_EN	(1 << 1)
#define  IPROC_NAND_GPIO_EN		(1 << 0)
#define IPROC_MPP_CTRL4_VIRT_BASE	(IPROC_GPIO_VIRT_BASE + 0x40)


/* Power Management */
#define IPROC_PMU_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0xd0000)

/* Real Time Clock */
#define IPROC_RTC_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0xd8500)

/* AC97 */
#define IPROC_AC97_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0xe0000)
#define IPROC_AC97_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0xe0000)

/* Peripheral DMA */
#define IPROC_PDMA_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0xe4000)
#define IPROC_PDMA_VIRT_BASE	(IPROC_SB_REGS_VIRT_BASE | 0xe4000)

#define IPROC_GLOBAL_CONFIG_1	(IPROC_SB_REGS_VIRT_BASE | 0xe802C)
#define  IPROC_TWSI_ENABLE_OPTION1	(1 << 7)
#define IPROC_GLOBAL_CONFIG_2	(IPROC_SB_REGS_VIRT_BASE | 0xe8030)
#define  IPROC_TWSI_ENABLE_OPTION2	(1 << 20)
#define  IPROC_TWSI_ENABLE_OPTION3	(1 << 21)
#define  IPROC_TWSI_OPTION3_GPIO		(1 << 22)
#define IPROC_SSP_PHYS_BASE	(IPROC_SB_REGS_PHYS_BASE | 0xec000)
#define IPROC_SSP_CTRL_STATUS_1	(IPROC_SB_REGS_VIRT_BASE | 0xe8034)
#define  IPROC_SSP_ON_AU1		(1 << 0)
#define  IPROC_SSP_CLOCK_ENABLE		(1 << 1)
#define  IPROC_SSP_BPB_CLOCK_SRC_SSP	(1 << 11)
/* Memory Controller */
#define IPROC_MC_VIRT_BASE	(IPROC_NB_REGS_VIRT_BASE | 0x00000)

#endif
