# Kernel configuration for Broadcom iProc based boards

menu	"Broadcom IPROC architecture based implementations"
	depends on ARCH_IPROC
	
choice
	prompt "Broadcom iProc SoC Type"
	default ARCH_NORTHSTAR	

config ARCH_NORTHSTAR
	bool "BROADCOM Northstar SoC"
	help
	  Support for the Broadcom Northstar SoC platform.

config MACH_IPROC
	bool "BROADCOM Generic IPROC SoC"
	help
	  Support for the Broadcom IPROC SoC platform.

endchoice

config IPROC_64K_PAGE
	bool "64K page support"
	depends on ARCH_IPROC
	help

menu "Custom iProc Boards"
	depends on MACH_IPROC

config IPROC_PCT_BOARD
	bool "Support for the iproc pct platform."
	select I2C
	select I2C_CHARDEV
	select I2C_MUX
	select IPROC_I2C
	select LVL7_IPROC_I2C
	help
	  Support for the iproc pct platform.

config IPROC_H2B_BOARD
	bool "Support for the iproc h2b platform."
	select I2C
	select I2C_CHARDEV
	select I2C_MUX
	select IPROC_I2C
	select LVL7_IPROC_I2C
	help
	  Support for the iproc h2b platform.

config IPROC_PEL_BOARD
	bool "Support for the iproc pel platform."
	help
	  Support for the iproc pel platform.


config IPROC_OSP_BOARD
       bool "Support for the iproc osp platform."
       help
         Support for the iproc osp platform.


config IPROC_IPM6100_BOARD
        bool "Support for the iproc ipm6100 platform."
        help
          Support for the iproc pct platform.

config IPROC_IPM6100_VEGA_BOARD
        bool "Support for the iproc ipm6100 vega (Netgear M6100-XCM8924x blade) platform."
        help
          Support for the iproc ipm6100 vega (Netgear M6100-XCM8924x blade) platform..

config IPROC_IPM4300_R2_BOARD
        bool "Support for the iproc ipm4300 Ranger2 (Netgear M4300 GSM43xx, XSM4316S blades) platform."
        help
          Support for the iproc ipm4300 Ranger2 (Netgear M4300 GSM43xx, XSM4316S blades) platform."

config IPROC_IPM4300_NS_BOARD
        bool "Support for the iproc ipm4300 vega (Netgear M4300 XSM4324S, XSM4348S boards) platform."
        help
          Support for the iproc ipm4300 vega (Netgear M4300 XSM4324S, XSM4348S boards) platform."

config IPROC_IPM4200_BOARD
        bool "Support for the iproc ipm4200 (Netgear M4200 boards) platform."
        select I2C
        select I2C_CHARDEV
        select IPROC_I2C
        select LVL7_IPROC_I2C
        help
          Support for the iproc ipm4200 (Netgear M4200 boards) platform."

config IPROC_XS7XXT_BOARD
        bool "Support for the iproc XS7XXT (Netgear XS7XXT boards) platform."
        help
          Support for the iproc XS7XXT (Netgear XS7XXT boards) platform."

config IPROC_IPGS7XXT_BOARD
        bool "Support for the iproc ipgs7xxt platform."
        help
          Support for the iproc ipgs7xxt platform.

config IPROC_IPGS7XXTX_BOARD
        bool "Support for the iproc ipgs7xxtx platform."
        help
          Support for the iproc ipgs7xxtx platform.

endmenu
	
config GP_TIMER_CLOCK_OFF_FIX
	bool "Enable the fix for general purpose timer clock off issue."
	depends on ARCH_RHEA || ARCH_SAMOA
	help
	  Say Y if you want to enable the general purpose timer clock off fix

config GP_TIMER_COMPARATOR_LOAD_DELAY
        bool "Enable the delay after loading general purpose timer compare register"
        depends on ARCH_RHEA || ARCH_ISLAND || ARCH_SAMOA || ARCH_HANA || ARCH_NORTHSTAR || MACH_IPROC
        default y  
	  
config IPROC_DCACHE_INVALIDATION
	bool "Have Linux invalidate D-Cache"
        default y  
	help 
	  Say Y if you want Linux to invalidate primary core D-Cache during Linux
	  decompression and boot.

config IPROC_TIMER_UNIT_TESTS
	bool "Include iProc Timer unit test code"
	help
	  Say Y if you want to test the AON,Peripheral Timer modules using the sysfs interface

config IPROC_SW_RESET_RECORD
	bool "Include Software Reset Records"
	help
	  Say Y if you want to enable interface to access Software Reset Record.
	  Software Reset Record is a set of variables whose value could be retained
	  after reset (but will be cleared if powered off).

config BRCM_PROP_MODULES
	bool "Include Broadcom proprietary modules"
	default n
	help
	  Say Y if you want to include the Broadcom proprietary modules.

config BCM_STM
	bool "Enable System Trace Module"
	default n
	help
	  Say Y if you want to enable the Broadcom System Trace Module

config DMAC_PL330
	bool "PL330 DMAC driver support for Kona architecture"
	depends on ARCH_RHEA
	select PL330
	help
	 Support for PL330 DMA Controller driver for Rhea SOC/KONA architecture

config BCM_ZRELADDR
	hex "Compressed ZREL address"

config BCM_PARAMS_PHYS
	hex "Address where tagged parameters are to be found"

config BCM_RAM_BASE
	hex "RAM base address"
	help
	 Set the physical base address of RAM

config BCM_RAM_START_RESERVED_SIZE
	hex "RAM start reserved memory size in bytes"
	default 0
	help
	 Reserve memory at the start of RAM. This memory
	 may be used for LCD frame buffer, DSP, modem, etc.

config BCM_DRIVER_PATH
	string
	depends on ARCH_IPROC
	option env=BCM_DRIVER_PATH
	

source "$BCM_DRIVER_PATH/bcmdrivers/Kconfig"

endmenu
