#ifndef _NET_RAWV6_H
#define _NET_RAWV6_H

#include <net/protocol.h>

void raw6_icmp_error(struct sk_buff *, int nexthdr,
		u8 type, u8 code, int inner_offset, __be32);
bool raw6_local_deliver(struct sk_buff *, int);

extern int			rawv6_rcv(struct sock *sk,
					  struct sk_buff *skb);

#if defined(CONFIG_IPV6_MIP6) || defined(CONFIG_IPV6_MIP6_MODULE)
int rawv6_mh_filter_register(int (*filter)(struct sock *sock,
					   struct sk_buff *skb));
int rawv6_mh_filter_unregister(int (*filter)(struct sock *sock,
					     struct sk_buff *skb));
#endif

#ifdef CONFIG_IPV6_LVL7_MROUTE
extern void rawv6_mroute_sock_deliver(struct sk_buff *skb);
extern int rawv6_mroute_mifi_add(struct sock *sk, unsigned short mifi, unsigned short pifi);
extern int rawv6_mroute_mifi_del(unsigned short mifi);
extern int ravw6_mroute_pifi_del(unsigned short pifi);
#define MRT6_BASE       200
#define MRT6_INIT       (MRT6_BASE)     /* Enable the socket upon which this is called to 
                                            send and receive multicast packets without 
                                            regard to group membership */
#define MRT6_DONE       (MRT6_BASE+1)   /* Cancel the above */
#define MRT6_ADD_MIF    (MRT6_BASE+2)   /* Enable an interface's participation in the above */
#define MRT6_DEL_MIF    (MRT6_BASE+3)   /* Disable an interface's participation in the above*/

#endif

#endif
