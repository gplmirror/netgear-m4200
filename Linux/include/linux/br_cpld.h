#ifndef BR_CPLD_H
#define BR_CPLD_H

#include <asm/types.h>

#define CPLD_DRV_VER            "R02.00"
/***********************************IOCTL Calls *********************************/
#define BRCPLD_MAX_IRQ_ENTRIES  10	/* This value is applicable for both fastpath & kernel, requires modification at both sides for any update */
struct __brcpld_waitForInt
{
    unsigned int irqNo;
    unsigned int request_irq_firstTime;
    unsigned int intSrcInfo[10];
    unsigned char threadNo;             /* This variable is to support multiple threads for either different interrupt numbers */ 
};

#define BRCPLD_IOC_MAGIC                'b'
#define BRCPLD_WAIT_FOR_INTERRUPT       _IOWR(BRCPLD_IOC_MAGIC, 1, struct __brcpld_waitForInt *)
/***********************************IOCTL Calls *********************************/

#ifdef __KERNEL__

typedef struct __irqData
{
  struct semaphore intSem;
  unsigned int intSrcInfo[10];
} irqDat;

static irqreturn_t brcpld_isr (int irq, void *data);
#endif

#endif
