#include <common.h>
#include <asm/io.h>
#include <asm/arch/iproc_regs.h>
#include <asm/iproc/iproc_common.h>
#include <iproc_i2c.h>

#if (defined(CONFIG_NORTHSTAR) || defined(CONFIG_NS_PLUS))
        #define RESET_BASE_ADDR CRU_RESET_OFFSET
#elif defined(CONFIG_CYGNUS)
	#define CRMU_MAIL_BOX1	0x03024028
#else
        #define RESET_BASE_ADDR DMU_CRU_RESET
#endif
#if defined (CONFIG_IPM6100) || (CONFIG_IPM6100_VEGA)
extern void cpldI2cAddressDetermine(void);
extern unsigned char cpldI2cAddress;
#endif
/*
 * Reset the cpu by setting software reset request bit
 */
void reset_cpu(ulong ignored)
{
#if defined (CONFIG_IPM6100) || (CONFIG_IPM6100_VEGA)
   {
    unsigned char writeval = 0x1; 
    cpldI2cAddressDetermine();
    /* establish access to the I2C attached CPLD and blink the status LED*/
    i2c_write(cpldI2cAddress, 0x1, 1, &writeval, 1);
    printf("M6100 board should reset here, if not do a manual poewr cycle might be due to I2C Io controller access failure address:0x%x \n",cpldI2cAddress);
    while(1){}
   }   
#elif (defined(CONFIG_NORTHSTAR) || defined(CONFIG_NS_PLUS))
        *(unsigned int *)(RESET_BASE_ADDR) = 0x1;
#elif defined(CONFIG_CYGNUS)
	writel(0xffffffff, CRMU_MAIL_BOX1);
#else
        *(unsigned int *)(RESET_BASE_ADDR) = 0; /* Reset all */
#endif
        while (1) {
                // Never reached
        }
}
