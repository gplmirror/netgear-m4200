/*
 * $Copyright Open Broadcom Corporation$ 
 */

#include <common.h>
#include <asm/io.h>
#include <asm/mach-types.h>
#include <config.h>
#include <asm/arch/iproc.h>
#include <asm/system.h>
#include "asm/iproc/reg_utils.h"
#include "asm/iproc/iproc_common.h"
#include <iproc_i2c.h>
#include "i2c.h"
#include <spi_flash.h>

DECLARE_GLOBAL_DATA_PTR;

/*****************************************
 * board_init -early hardware init
 *****************************************/
int board_init (void)
{
    gd->bd->bi_arch_number = CONFIG_MACH_TYPE;      /* board id for linux */
    gd->bd->bi_boot_params = LINUX_BOOT_PARAM_ADDR; /* adress of boot parameters */

    return 0;
}

#define POLYNOMIAL 0xA097

static unsigned short CrcTable[256]; /* CRC lookup table */

/************************************************************************
** NAME: UpdateCRC
**
** This function updates existing CRC
**
** INPUT:
**      DataByte - Received data byte
**
** INPUT/OUTPUT:
**      crc - Current value of CRC.
************************************************************************/
static void UpdateCRC (unsigned char DataByte, unsigned short *crc)
{
  unsigned long new_crc;

  new_crc = *crc;
  new_crc = (new_crc << 8) ^ (CrcTable[new_crc >> 8] ^ DataByte);
  *crc = (unsigned short) new_crc;
}

/************************************************************************
** NAME: GenCrcTable
**
** This function computes CRC look-up table.
************************************************************************/
static void GenCrcTable (void)
{
  unsigned short CrcCode;
  unsigned char  DataByte;
  unsigned long  index, BitNo;
  unsigned long  CarryBit, DataBit;

  for( index = 0; index <= 255; index++ )
  {
    CrcCode = 0;
    DataByte = index;

    for( BitNo = 1; BitNo <= 8; BitNo++ )
    {
      CarryBit = ((CrcCode & 0x8000) != 0);
      DataBit = ((DataByte & 0x80) != 0);
      CrcCode = CrcCode << 1;
      if( CarryBit ^ DataBit )
        CrcCode = CrcCode ^ POLYNOMIAL;
      DataByte = DataByte << 1;
    }

    CrcTable [index] = CrcCode;
  }
}

/**************************************************************************
*
* @purpose  Calculate the CRC of a given null terminated string of data
*
* @param    fp pointer of buffer to calculate CRC
*
* @returns  calculated CRC
*
* @end
*
*************************************************************************/
static unsigned short calc_crc(char *fp, int len)
{
  unsigned short crc;
  unsigned int   i;

  GenCrcTable ();
  crc = 0;

  for( i=0; i<len; i++ )
  {
    UpdateCRC (fp[i], &crc);
  }

  return(crc);
}

/*****************************************************************
 * misc_init_r - miscellaneous platform dependent initializations
 ******************************************************************/

#define VPD_LENGTH           256
#define VPD_OFFSET           0x120000
#define VPD_MAC_OFFSET       4
#define MAC_ADDR_LEN         17

#define BOARD_ID_MASK        0x0F
#define GSM4328S_BRD_ID      0x01
#define GSM4328PS_BRD_ID     0x05
#define GSM4352S_BRD_ID      0x09
#define GSM4352PS_BRD_ID     0x0d
#define XSM4316S_BRD_ID      0x08
#define SYS_CPLD_I2C_ADDR    0x30

void board_info_set(uint32_t *brd_id, int *save)
{
        uint32_t sku_id, board_id, gpio_in;
        char *boardname, *boardname_env, *boardid_env;
        char boardid_buf[8];

        /* Get SKU id of the chip */
        sku_id = (reg32_read((volatile uint32_t *)ICFG_CHIP_ID_REG) & 0xffff);

        /** Read GPIO_Pins[6-9] to get the board Id. */
        reg32_write(0x1800A020, 0x0);  /** Mask all interrupts */
        reg32_write(0x1800A008, 0x0);  /* Disable OUTPUT on all GPIO pins */
        reg32_write(0x1800A004, 0x0);  /** DATA_OUT: Make pins 6-9 INPUT only */

        gpio_in = reg32_read((volatile uint32_t *)0x1800A000);
        board_id = (gpio_in >> 6) & BOARD_ID_MASK;

        /* If the board id read through GPIO fails, use the following workaround.
         * Since the board XSM4316S has ranger2 variant of 0xb060, 
         * Identify the board id with SKU id.
         */
        if ((!board_id) && (0xb060  == sku_id)) {
           board_id = XSM4316S_BRD_ID;
        }

        *brd_id  = board_id;

        switch (board_id) {
        case GSM4328S_BRD_ID:
                boardname = "M4328S";
                break;
        case GSM4328PS_BRD_ID:
                boardname = "M4328PS";
                break;
        case GSM4352S_BRD_ID:
                boardname = "M4352S";
                break;
        case GSM4352PS_BRD_ID:
                boardname = "M4352PS";
                break;
        case XSM4316S_BRD_ID:
                boardname = "M4316S";
                break;
        default:
                boardname = "ranger2-unknown";
                break;
        }
        boardname_env = getenv("boardname");
        if ((boardname_env == 0) || (strncmp(boardname_env, boardname, 20) != 0)) {
                setenv ("boardname", boardname);
                *save = 1;
        }

        sprintf (boardid_buf, "%d", board_id);
        boardid_env = getenv("boardid");
        if ((boardid_env == 0) || (strncmp(boardid_env, boardid_buf, 8) != 0)) {
                setenv ("boardid", boardid_buf);
                *save = 1;
        }

#ifndef CONFIG_SILENT_CONSOLE
        printf ("Board SKU ID: 0x%x\n", sku_id);
        printf ("Board Name  : %s (0x%x)\n", boardname, board_id);
#endif
}

int misc_init_r (void)
{
        uchar enetaddr[6];
        uchar vpd[VPD_LENGTH + 10];
        size_t vpdSize = VPD_LENGTH;
        uint32_t brd_id = 0x9;
        u16 crc16, crc;
        int rc;
        char *env;
        struct spi_flash *flash;
        /* If the current ethaddr and environment ethaddr variable ver */
        /* differ, ensure the environment variable ver is            */
        /* updated. If not FastPath will show the incorrect value    */
        char *version = 0; char prevMac[MAC_ADDR_LEN];
        int save = 0;
        unsigned int   spMacLs4b;
        unsigned short spMacMs2b;
	
        /* Initialize the I2C controller for CPLD to perform board operations */
        i2c_init(I2C_SPEED_100KHz,0xff);
        mdelay(1000);

        board_info_set(&brd_id, &save);

        flash = spi_flash_probe(CONFIG_ENV_SPI_BUS, CONFIG_ENV_SPI_CS,
                CONFIG_ENV_SPI_MAX_HZ, CONFIG_ENV_SPI_MODE);
        if (flash) {
                rc = spi_flash_read(flash, VPD_OFFSET, VPD_LENGTH, vpd);
                if (rc <= -1) {
                        printf("Flash read fail\n");
                } else {
                        crc16 = calc_crc(&vpd[2], VPD_LENGTH-2);
                        if (crc16 == htons(*(u16 *)&vpd[0])) {
                                memcpy(enetaddr, &vpd[VPD_MAC_OFFSET], 6);

                                spMacMs2b = (enetaddr[0] << 8) | enetaddr[1];
                                spMacLs4b = (enetaddr[2] << 24) | (enetaddr[3] << 16) | 
                                            (enetaddr[4] << 8) | (enetaddr[5]);

                                /* Increment the MAC address by 1 */
                                if (0xffffffff == spMacLs4b) {
                                   spMacLs4b = 0;
                                   spMacMs2b = spMacMs2b + 1;
                                } else {
                                   spMacLs4b = spMacLs4b + 1;
                                }

                                enetaddr[0] = spMacMs2b >> 8;
                                enetaddr[1] = spMacMs2b & 0x00ff;
                                enetaddr[2] = (spMacLs4b >> 24) & 0x000000ff;
                                enetaddr[3] = (spMacLs4b >> 16) & 0x000000ff;
                                enetaddr[4] = (spMacLs4b >> 8)  & 0x000000ff;
                                enetaddr[5] = (spMacLs4b )      & 0x000000ff;

#ifndef CONFIG_SILENT_CONSOLE
                                printf("Setting ethaddr from VPD: %02X:%02X:%02X:%02X:%02X:%02X\n", 
                                       enetaddr[0], enetaddr[1], enetaddr[2], enetaddr[3], 
                                       enetaddr[4], enetaddr[5]);
#endif
                        } else {
                           enetaddr[0] = 0x00;enetaddr[1] = 0x00 | brd_id;
                           enetaddr[2] = 0x43;enetaddr[3] = 0x00;
                           enetaddr[4] = 0x41;enetaddr[5] = 0x91;
                           printf("No ethaddr in VPD. Using default 00:%02X:43:00:41:91\n", enetaddr[1]);
                        }
                }
        } else {
                printf("Flash probe fail\n");
        }
        version = getenv("ethaddr");
        if(version != NULL)
        {
          memcpy(prevMac, version, strlen(version));
        } 
        eth_setenv_enetaddr("ethaddr", enetaddr);
        version = getenv("ethaddr");

        if(version == NULL)
        {
           save = 1;
        } else if (strncmp(version, prevMac, MAC_ADDR_LEN) != 0) {
            save = 1;
        }
        if (save != 0) {
              udelay(10000);
              saveenv();
        }

        return(0);
}

/**********************************************
 * dram_init - sets uboots idea of sdram size
 **********************************************/
int dram_init (void)
{
#if !defined(CONFIG_IPROC_NO_DDR) && !defined(CONFIG_NO_CODE_RELOC)
    ddr_init2();
#endif

    gd->ram_size = CONFIG_PHYS_SDRAM_1_SIZE - CONFIG_PHYS_SDRAM_RSVD_SIZE;

    return 0;
}

int board_early_init_f (void)
{
    uint32_t sku;
    sku = reg32_read((volatile uint32_t *)ICFG_CHIP_ID_REG);
    if ((sku & 0xfff0) == 0xb060) {
        /* Ranger2 - 5606x: CPU 1250MHz, AXI 400MHz, DDR 800MHz */
        iproc_config_armpll(1250);
        iproc_config_genpll(0);
    } else {
        /* Greyhound - 5341x: CPU 600MHz, AXI 200MHz, DDR 667MHz */
        iproc_config_armpll(600);
        iproc_config_genpll(1);
    }
    
#ifndef CONFIG_IPROC_NO_DDR
    /* Initialize ATCM and BTCM for SHMOO (clear to avoid ECC error) */
    {
        int i;
        volatile int *p;
        p = (volatile int *)0x01000000;
        for(i=0; i<64*1024/4; i++, p++)
            *p = 0;
        p = (volatile int *)0x01080000;
        for(i=0; i<128*1024/4; i++, p++)
            *p = 0;
    }
#endif

    return 0;
}

int board_late_init (void) 
{
    int status = 0;

#if defined(CONFIG_L2C_AS_RAM) && !defined(CONFIG_NO_CODE_RELOC)
    extern ulong mmu_table_addr;    /* MMU on flash fix: 16KB aligned */
    
#ifndef CONFIG_SILENT_CONSOLE
    printf("Unlocking L2 Cache ...");
#endif
    l2cc_unlock();
#ifdef CONFIG_SILENT_CONSOLE
    mdelay(10);
#else
    printf("Done\n");
#endif

    /* 
     * Relocate MMU table from flash to DDR since flash may not be always accessable.
     * eg. When QSPI controller is in MSPI mode.
     */
    asm volatile ("mcr p15, 0, %0, c2, c0, 0"::"r"(mmu_table_addr)); /* Update TTBR0 */
    asm volatile ("mcr p15, 0, r1, c8, c7, 0");  /* Invalidate TLB*/
    asm volatile ("mcr p15, 0, r1, c7, c10, 4"); /* DSB */
    asm volatile ("mcr p15, 0, r0, c7, c5, 4"); /* ISB */
    
    l2cc_disable();
#endif

    disable_interrupts();
    iproc_clk_enum();
    
#ifdef STDK_BUILD
    enable_interrupts();
#endif /* STDK_BUILD */

#if !defined(CONFIG_IPROC_NO_DDR) && defined(CONFIG_SHMOO_AND28_REUSE)
     save_shmoo_to_flash();
#endif

    return status;
}

/* override enable_caches() in arch/arm/lib/cache.c */
void enable_caches(void)
{
  icache_enable();
  dcache_enable();
}

int board_eth_init(bd_t *bis)
{
	int rc = -1;

#ifdef CONFIG_BCMIPROC_ETH
#ifndef CONFIG_SILENT_CONSOLE
	printf("Registering eth\n");
#endif
	rc = bcmiproc_eth_register(0);
#endif

	return rc;
}

/**********************************************
 * Board Specific Reset Handler
 **********************************************/
int do_reset(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
   u8 hwreset = 0x01;
   int ret;

   printf("resetting %s board...\n", getenv("boardname"));

   udelay (50000);                         /* wait 50 ms */
   disable_interrupts();

   /** Trigger the hardware reset through CPLD */
   ret = i2c_write(SYS_CPLD_I2C_ADDR, 0x1, 1, &hwreset, 1);
   if (ret < 0) {
      udelay (50000);                         /* wait 50 ms */
      ret = i2c_write(SYS_CPLD_I2C_ADDR, 0x1, 1, &hwreset, 1);
   }
   mdelay (5000);   /* on XSM boards, CPLD reset demands more wait cycles. wait for more time.  */

   printf("Hardware reset failed. Power cycle recommended...\n");
   mdelay(10);
   while(1);

   /*NOTREACHED*/
   return 0;
}
