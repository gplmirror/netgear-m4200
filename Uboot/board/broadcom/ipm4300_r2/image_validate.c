#include <common.h>
#include <asm/io.h>
#include <asm/mach-types.h>
#include <config.h>
#include <asm/arch/iproc.h>
#include <asm/system.h>
#include "asm/iproc/iproc_common.h"
#include <asm/arch/iproc_regs.h>
#include "asm/arch/socregs.h"
#include "asm/iproc/reg_utils.h"

/* Fixed tags that help identify the STK files.
*/
#define STK_TAG1 ((unsigned short) 0xaa55)
#define STK_TAG2 ((unsigned long) 0x2288bb66)
#define MAX_BUFFER_SIZE                 4096
#define MAX_BUF_SIZE                    4096
#define POLYNOMIAL                      0xA097

/* Operating system types supported in the STK image.
*/
#define STK_OS_VXWORKS            1
#define STK_OS_LINUX              2
#define STK_OS_ECOS               3
#define TRUE                      1
#define FALSE                     0

#define FAILURE                   1
#define SUCCESS                   0
#define NULLVAL                   0
#define L7_CLI_MAX_STRING_LENGTH  128
#define L7_EOS      ((unsigned char)'\0')   /* ASCIIZ end-of-string character */
typedef struct
{
	unsigned short crc;
	unsigned short tag1;
	unsigned long tag2;

	unsigned long num_components; /* Number of OPR and tgz files in the STK image (may be 0) */
	unsigned long file_size; /* Total number of bytes in the STK file */
	unsigned char rel;
	unsigned char ver;
	unsigned char maint_level;
	unsigned char build_num;
	unsigned long stk_header_size; /* Number of bytes in the STk header */
	unsigned char reserved[64];    /* Reserved for future use */
} stkFileHeader_t;

typedef enum
{
	STK_SUCCESS,
	STK_FAILURE,
	STK_IMAGE_DOESNOT_EXIST,
	STK_INVALID_IMAGE,
	STK_FILE_SIZE_FAILURE,
	STK_FILE_SIZE_MISMATCH,
	STK_TOO_MANY_IMAGES_IN_STK,
	STK_STK_EMPTY,
	STK_PLATFORM_MISMATCH,
	STK_INVALID_IMAGE_FORMAT,
	STK_NOT_ACTIVATED,
	STK_ACTIVATED,
	STK_TABLE_IS_FULL,
	STK_VALID_IMAGE,
	STK_INVALID_IMAGE_VERSION,
	STK_INVALID_SIGNATURE_SIZE,
	STK_LAST_ENTRY
}STK_RC_t;

typedef enum
{
	L7_SUCCESS = 0,
	L7_FAILURE,
	L7_ERROR,
	L7_NOT_IMPLEMENTED_YET,
	L7_NOT_SUPPORTED,
	L7_NOT_EXIST,
	L7_ALREADY_CONFIGURED,
	L7_TABLE_IS_FULL,
	L7_REQUEST_DENIED,
	L7_ASYNCH_RESPONSE,
	L7_ADDR_INUSE,
	L7_NO_VALUE,
	L7_NO_MEMORY,
	L7_DEPENDENCY_NOT_MET,
	L7_HARDWARE_ERROR,
	L7_IMAGE_IN_USE,
	L7_AP_ALREADY_ADDED,
	L7_NOT_APPLICABLE,
	L7_MISMATCH,
	L7_RC_MAX
} L7_RC_t;


#define IPL_MODEL_TAG   0x54334

extern uint32_t crc32Generate(int8_t* buffer, int32_t length);

extern L7_RC_t simSetNetworkParams(uint8_t *strIpAddr, 
               uint8_t *strNetMask, uint8_t *strGateway);
extern uint8_t debLevel_g;
static unsigned short CrcTable[256]; /* CRC lookup table */
/************************************************************************
** NAME: UpdateCRC
**
** This function updates existing CRC
**
** INPUT:
**  DataByte - Received data byte
**
** INPUT/OUTPUT:
**  crc - Current value of CRC.
************************************************************************/
extern void UpdateCRC (unsigned char DataByte, unsigned short *crc)
{
	register unsigned long new_crc;

	new_crc = *crc;
	new_crc = (new_crc << 8) ^ (CrcTable[new_crc >> 8] ^ DataByte);
	*crc = (unsigned short) new_crc;
};

/************************************************************************
** NAME: GenCrcTable
**
** This function computes CRC look-up table.
************************************************************************/
extern void GenCrcTable (void)
{
	unsigned short CrcCode;
	unsigned char DataByte;
	unsigned long  index, BitNo;
	unsigned long  CarryBit, DataBit;

	for (index = 0; index <= 255; index++) {
		CrcCode = 0;
		DataByte = index;
		for (BitNo = 1; BitNo <= 8; BitNo++) {
			CarryBit = ((CrcCode & 0x8000) != 0);
			DataBit = ((DataByte & 0x80) != 0);
			CrcCode = CrcCode << 1;
			if (CarryBit ^ DataBit) CrcCode = CrcCode ^ POLYNOMIAL;
			DataByte = DataByte << 1;
		}; /* End For */
		CrcTable [index] = CrcCode;
	}; /* End for */
};
/**************************************************************************
 *
 * @purpose  This routine computes the CRC of a file in the file system.
 *
 * @param    file_ptr Name of file.
 * @param    file_size Number of bytes expected in the file.
 *
 * @returns  16-bit crc
 *
 * @comments none.
 *
 * @end
 *
 *************************************************************************/
uint16_t file_crc_compute(uint8_t * file_ptr, uint32_t file_size)
{
	uint16_t computed_crc;
	uint8_t c;
	uint32_t num_chars;
	uint8_t * ptr;

	GenCrcTable ();
	ptr = file_ptr;
	/* Compute CRC. */
	/* Skip the first two bytes of the image. The two bytes contain
	 ** CRC. */
	computed_crc = 0;
	num_chars = 2;
	ptr = ptr + 2;
	do
	{
		c = *(unsigned char *)ptr;
		ptr++;
		num_chars++;
		UpdateCRC (c, &computed_crc);
	}while( num_chars < file_size);
	return(computed_crc);
}

/*********************************************************************
* @purpose  This function return the initial CRC32 value
*
* @returns  inital CRC32 value
*
* @notes
*
* @end
*********************************************************************/
uint32_t l7_buf_crc32_init (void)
{
	return ~0;
}

/*********************************************************************
* @purpose  This function return the final CRC32 value
*
* @param    crc - current CRC32 value
*
* @returns  final CRC32 value
*
* @notes
*
* @end
*********************************************************************/
uint32_t l7_buf_crc32_finish (uint32_t crc)
{
	return ~crc;
}
/*********************************************************************
* @purpose  Returns the IPL Model Tag for this unit
*
* @returns  IPL_MODEL_TAG
*
* @notes    none
*
* @end
*********************************************************************/
uint32_t bspapiIplModelGet(void)
{
	return IPL_MODEL_TAG;
}

static STK_RC_t stkImageInfoGet(uint8_t *filePtr, uint32_t *crc_status,
                                                      uint32_t *size)
{
	stkFileHeader_t stkHeader;
	uint32_t file_size;
	uint16_t computed_crc;

	/* read first few bytes to determine STK or OPR */
	memcpy((uint8_t *)&stkHeader, filePtr,sizeof(stkHeader));
	if((ntohs(stkHeader.tag1) == STK_TAG1) &&
		(ntohl(stkHeader.tag2) == STK_TAG2)){
		 /* First, validate CRC */
   		 /* check all available opr files to get the correct
		  * opr file for this target */
		if(ntohl(stkHeader.num_components) == 0){
			return STK_STK_EMPTY;
		}
	}
	else /* STK tags not present */
	{
		return STK_INVALID_IMAGE_FORMAT;
	}
	file_size = ntohl(stkHeader.file_size);
	printf("STK file size %d\n",file_size); 
	/*Doing at the end as crc check is time consuming*/
	computed_crc = file_crc_compute(filePtr, file_size);
	if (computed_crc != ntohs(stkHeader.crc)){
		printf("Computed CRC %x header CRC %x \n",computed_crc,ntohs(stkHeader.crc));
		return STK_INVALID_IMAGE;
	}

	if(size != NULL){
		*size = file_size;
	}
	return STK_SUCCESS;
}

/**************************************************************************
 *
 * @purpose  Returns the size of the image
 *
 * @param    fileName     Operational code file.
 * @param    size         variable to return file size
 *
 * @returns  STK_SUCCESS   on successful execution
 *
 * @end
 *
 *************************************************************************/
STK_RC_t stkImageSizeGet(uint8_t *fileName, uint32_t *size)
{
	STK_RC_t rc;

	rc = stkImageInfoGet(fileName, NULL, size);
	return rc;
}

uint32_t firmwareFileValidate(uint8_t *filePtr)
{
	uint32_t stkSize;
	STK_RC_t rc;
	uint32_t valid = FAILURE;
	
	/* Verify the stk file and get the size */
	rc = stkImageSizeGet(filePtr, &stkSize);
	if (STK_SUCCESS != rc){
		printf(" STK validation failed rc %d\n",rc);
		return valid;
	}
	printf(" STK validation Success\n");
	return SUCCESS;
}
/**************************************************************************
 *
 * @purpose  Validates the image
 *
 *
 * @returns  SUCCESS   on successful execution
 *           FAILURE   on fail
 *
 * @end
 *
 *************************************************************************/
int do_validate(cmd_tbl_t *cmdtp,
        int flag, int argc, char * const argv[])
{
	uint32_t imageSize;
	uint32_t rc;
  
	rc = 1;
	printf("Validating image\n");
	rc = firmwareFileValidate((uint8_t *)CONFIG_LOADADDR);
	return rc;
}

U_BOOT_CMD(
	validate, 2, 0,  do_validate,
	"Validate the image",
	"Validate the image"
);
