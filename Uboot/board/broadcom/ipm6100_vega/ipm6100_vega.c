/*
 * $Copyright Open Broadcom Corporation$ 
 */
#include <common.h>
#include <asm/io.h>
#include <asm/mach-types.h>
#include <configs/iproc_board.h>
#include <asm/arch/iproc.h>
#include <asm/arch/socregs.h>
#include <asm/arch/reg_utils.h>
#include <asm/system.h>
#include <iproc_i2c.h>
#include <nand.h>
#include "i2c.h"

DECLARE_GLOBAL_DATA_PTR;

extern int bcmiproc_eth_register(u8 dev_num);
unsigned char cpldI2cAddress = 0x0;

#define IO_CONTROLLER_I2C_ADDR  0x30
#define CPLD_MAC_OFFSET_ADDR    0x40

/*****************************************
 * board_init -early hardware init
 *****************************************/
int board_init (void)
{
    gd->bd->bi_arch_number = CONFIG_MACH_TYPE;      /* board id for linux */
    gd->bd->bi_boot_params = LINUX_BOOT_PARAM_ADDR; /* adress of boot parameters */

    return 0;
}

/*
 *  CPLD i2c address determine
*/
#define CPLD_I2C_MIN_ADDR 0x30
#define CPLD_I2C_MAX_ADDR 0x3F

void cpldI2cAddressDetermine (void)
{
  unsigned char byte; int rc = 0;
  if( cpldI2cAddress > CPLD_I2C_MIN_ADDR ) 
  {
    return;
  }
  cpldI2cAddress = CPLD_I2C_MIN_ADDR | 1;
  if( (rc = i2c_read(cpldI2cAddress, 0, 1, &byte, 1)) != 0 )
  {
    cpldI2cAddress = CPLD_I2C_MIN_ADDR | 2;
    if( (rc = i2c_read(cpldI2cAddress, 0, 1, &byte, 1)) != 0 )
    {
      cpldI2cAddress = CPLD_I2C_MIN_ADDR | 3;
      if( (rc = i2c_read(cpldI2cAddress, 0, 1, &byte, 1)) != 0 )
      {
        cpldI2cAddress = CPLD_I2C_MIN_ADDR | 4;
        if( (rc = i2c_read(cpldI2cAddress, 0, 1, &byte, 1)) != 0 )
        {
          cpldI2cAddress = CPLD_I2C_MIN_ADDR | 0xf;
          if( (rc = i2c_read(cpldI2cAddress, 0, 1, &byte, 1)) != 0 )
          {
            printf("cpldI2cAddress: unable to determine the address \n");
            return; 
          }
        }
      }
    }
  }
}
/*****************************************************************
 * misc_init_r - miscellaneous platform dependent initializations
 ******************************************************************/
#define VPD_LENGTH              256
#define VPD_OFFSET              0x180000
#define VPD_MAC_OFFSET          4
#define SLOT_1_MAC_ADDR_OFFSET  1
#define SLOT_2_MAC_ADDR_OFFSET  4
#define SLOT_3_MAC_ADDR_OFFSET  6
#define SLOT_4_MAC_ADDR_OFFSET  8
#define NO_SLOT_MAC_ADDR_OFFSET 10
#define MAC_ADDR_LEN            17

int misc_init_r (void)
{
    uchar enetaddr[6];
#ifdef VPD_THROUGH_FLASH
    uchar vpd[VPD_LENGTH];
    size_t vpdSize = VPD_LENGTH;
    u16 crc16;
#endif
    int rc = 0, i = 0;

    /* If ethaddr is not set, attempt to read if from VPD */
#ifdef VPD_THROUGH_FLASH
        nand_read_skip_bad(&nand_info[0], VPD_OFFSET, &vpdSize, vpd);
        crc16 = calc_crc(&vpd[2], VPD_LENGTH-2);
        if (crc16 == htons(*(u16 *)&vpd[0])) {
            memcpy(enetaddr, &vpd[VPD_MAC_OFFSET], 6);
            eth_setenv_enetaddr("ethaddr", enetaddr);
        }
#else
       /* establish access to the I2C attached CPLD and blink the status LED*/
       i2c_init(I2C_SPEED_100KHz,0xff);
       mdelay(1000);
       cpldI2cAddressDetermine();

        for(i = 0; i < 6; i++)
        {
           rc = i2c_read(IO_CONTROLLER_I2C_ADDR, (CPLD_MAC_OFFSET_ADDR+i), 1, &enetaddr[i], 1);
           mdelay(10);
        }
        if(rc == 0)
        {
            /* If the current ethaddr and environment ethaddr variable ver */
            /* differ, ensure the environment variable ver is            */
            /* updated. If not FastPath will show the incorrect value    */
            char *version = 0; char prevMac[MAC_ADDR_LEN];
            int save = 0;

#ifdef SEPERATE_MAC_ADDR_FOR_SLOT
            /* MAC addresss macaddr[0] ==> network port, macaddr[1/2/3] ==> For serviceport */
            cpldI2cAddressDetermine();
            if( (cpldI2cAddress - CPLD_I2C_MIN_ADDR) == 1)
                enetaddr[5] += SLOT_1_MAC_ADDR_OFFSET;
            else if( (cpldI2cAddress - CPLD_I2C_MIN_ADDR) == 2)
                enetaddr[5] += SLOT_2_MAC_ADDR_OFFSET;
            else if( (cpldI2cAddress - CPLD_I2C_MIN_ADDR) == 3)
                enetaddr[5] += SLOT_3_MAC_ADDR_OFFSET;
            else if( (cpldI2cAddress - CPLD_I2C_MIN_ADDR) == 4)
                enetaddr[5] += SLOT_4_MAC_ADDR_OFFSET;
            else
                enetaddr[5] += NO_SLOT_MAC_ADDR_OFFSET;
#else
                enetaddr[5] += 1;
#endif
            version = getenv("ethaddr");
            memcpy(prevMac, version, strlen(version));

            eth_setenv_enetaddr("ethaddr", enetaddr);
            version = getenv("ethaddr");
            
            if(version == 0)
               save = 1;

            if (strncmp(version, prevMac, MAC_ADDR_LEN) != 0){
               save = 1;              
            }
            if (save != 0){
                  saveenv();
            }
        }  
#endif
        else
        {
            enetaddr[0]= 0x4;enetaddr[1]= 0x5;enetaddr[2]= 0x6;
            enetaddr[3]= 0x7;enetaddr[4]= 0x8;enetaddr[5]= 0x79/*(rand() & 0xff)*/; 
            eth_setenv_enetaddr("ethaddr", enetaddr);
        }
    return(0);
}

/**********************************************
 * dram_init - sets uboots idea of sdram size
 **********************************************/
int dram_init (void)
{
#ifdef CONFIG_L2C_AS_RAM
	ddr_init();
#endif
    gd->ram_size = CONFIG_PHYS_SDRAM_1_SIZE - CONFIG_PHYS_SDRAM_RSVD_SIZE;

    return 0;
}

#ifdef CONFIG_PWM0_FOR_FAN
/* Fixme: Better to have a generic PWM config function to configure any 
   PWM channel */
void bcm95301x_config_pwm0_4_fan(void)
{
	reg32_set_bits(ChipcommonB_GP_AUX_SEL, 0x01); // Select PWM 0 
	reg32_clear_bits(CRU_GPIO_CONTROL0_OFFSET, 1 << 8); // Selct PWM
	reg32_write_masked(ChipcommonB_PWM_PRESCALE, 0x3F, 24); // prescale = 24 + 1
	reg32_write(ChipcommonB_PWM_PERIOD_COUNT0, 5 * 180); 
	reg32_write(ChipcommonB_PWM_DUTYHI_COUNT0, 5 * 135);
	reg32_set_bits(ChipcommonB_PWMCTL, 0x101); // Enable PWM0
}
#endif

int board_late_init (void) 
{
	extern ulong mmu_table_addr;    /* MMU on flash fix: 16KB aligned */
	int status = 0;
	int i;
    
	/* unlock the L2 Cache */
#ifndef CONFIG_NO_CODE_RELOC
#ifndef CONFIG_SILENT_CONSOLE
	printf("Unlocking L2 Cache ...");
#endif
	l2cc_unlock();
#ifndef CONFIG_SILENT_CONSOLE
	printf("Done\n");
#endif
#endif

	/* MMU on flash fix: 16KB aligned */
	asm volatile ("mcr p15, 0, %0, c2, c0, 0"::"r"(mmu_table_addr)); /*update TTBR0 */
	asm volatile ("mcr p15, 0, r1, c8, c7, 0");  /*invalidate TLB*/
	asm volatile ("mcr p15, 0, r1, c7, c10, 4"); /* DSB */
	asm volatile ("mcr p15, 0, r0, c7, c5, 4"); /* ISB */		

	/* Systick initialization(private timer)*/
	iproc_clk_enum();
	//glb_tim_init();
#ifndef STDK_BUILD
	disable_interrupts();
#if 0 // Fixme: To be enabled later,works but linux fails to run
	/* turn off I/D-cache */
	icache_disable();
	dcache_disable();

	/* Cache flush */
	asm ("mcr p15, 0, %0, c7, c5, 0": :"r" (0));

	/* turn off L2 cache */
	l2cc_disable();
	/* invalidate L2 cache also */
	invalidate_dcache();

	i = 0;
	/* mem barrier to sync up things */
	asm("mcr p15, 0, %0, c7, c10, 4": :"r"(i));

	scu_disable();
#endif
#ifdef CONFIG_PWM0_FOR_FAN
	bcm95301x_config_pwm0_4_fan();
#endif
#else
	gic_disable_interrupt(29);
	irq_install_handler(29, systick_isr, NULL);
	gic_config_interrupt(29, 1, IPROC_INTR_LEVEL_SENSITIVE, 0, IPROC_GIC_DIST_IPTR_CPU0);
	iproc_systick_init(10000);

	/* MMU and cache setup */
	disable_interrupts();
	//printf("Enabling SCU\n");
	scu_enable();

	printf("Enabling icache and dcache\n");
	dcache_enable();
	icache_enable();

	printf("Enabling l2cache\n");
	status = l2cc_enable();
	//printf("Enabling done, status = %d\n", status);

	enable_interrupts();
#endif
	return status;
}

int board_eth_init(bd_t *bis)
{
	int rc = -1;
#ifdef CONFIG_BCMIPROC_ETH
#ifndef CONFIG_SILENT_CONSOLE
	printf("Registering eth\n");
#endif
	rc = bcmiproc_eth_register(0);
#endif
	return rc;
}
