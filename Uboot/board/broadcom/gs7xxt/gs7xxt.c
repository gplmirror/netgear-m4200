#include <common.h>
#include <asm/io.h>
#include <asm/mach-types.h>
#include <config.h>
#include <asm/arch/iproc.h>
#include <asm/system.h>
#include "asm/iproc/iproc_common.h"
#include <asm/arch/iproc_regs.h>
#include "asm/arch/socregs.h"
#include "asm/arch/reg_utils.h"
#include <spi_flash.h>
#include <net.h>
#include <version.h>
#include <command.h>
DECLARE_GLOBAL_DATA_PTR;

extern int bcmiproc_eth_register(u8 dev_num);
extern void iproc_save_shmoo_values(void);

#define VPD_LENGTH           256
#define VPD_OFFSET           0x90000
#define VPD_MAC_OFFSET       4
#define NSDP_ACTIVE_FAC_DEF  0x00000010
#define BOARD_ID_MASK        0x0F
#define BOARD_ID_716         0x0
#define BOARD_ID_724         0x02
#define BOARD_ID_748         0x04
#define POLYNOMIAL           0xA097
static unsigned short CrcTable[256]; /* CRC lookup table */
void init_timeouts(void) {

#define DDR_IDM_ERROR_LOG_CONTROL_VAL                       0x33a //0x330

#define W_REG(o, a, v) *a=(unsigned int)v
#define uint32 unsigned int

#if 1
    W_REG(osh, (uint32 *)IHOST_S0_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
    W_REG(osh, (uint32 *)IHOST_S1_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
#endif

#if 1
    W_REG(osh, (uint32 *)DDR_S1_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
    W_REG(osh, (uint32 *)DDR_S2_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
#endif

#if 1
	W_REG(osh, (uint32 *)AXI_PCIE_S0_IDM_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
#endif

#if 1
	W_REG(osh, (uint32 *)CMICD_S0_IDM_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)APBY_S0_IDM_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)ROM_S0_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)NAND_IDM_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)QSPI_IDM_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)A9JTAG_S0_IDM_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)SRAM_S0_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)APBZ_S0_IDM_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)AXIIC_DS_3_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)APBW_IDM_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)APBX_IDM_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
	W_REG(osh, (uint32 *)AXIIC_DS_0_IDM_ERROR_LOG_CONTROL, DDR_IDM_ERROR_LOG_CONTROL_VAL);
#endif

}

void init_axitrace(void)
{

	reg32_write((volatile uint32_t *)IHOST_AXITRACE_M1_ATM_CONFIG, 0x5551);
	reg32_write((volatile uint32_t *)IHOST_AXITRACE_M1_ATM_CMD, 2);
	reg32_write((volatile uint32_t *)IHOST_AXITRACE_M0_ATM_CONFIG, 0x5551);
	reg32_write((volatile uint32_t *)IHOST_AXITRACE_M0_ATM_CMD, 2);
	reg32_write((volatile uint32_t *)IHOST_AXITRACE_ACP_ATM_CONFIG, 0x5551);
	reg32_write((volatile uint32_t *)IHOST_AXITRACE_ACP_ATM_CMD, 2);
}
/*****************************************
 * board_init -early hardware init
 *****************************************/
int board_init (void)
{
    gd->bd->bi_arch_number = CONFIG_MACH_TYPE;      /* board id for linux */
    gd->bd->bi_boot_params = LINUX_BOOT_PARAM_ADDR; /* adress of boot parameters */

    return 0;
}

/************************************************************************
** NAME: UpdateCRC
**
** This function updates existing CRC
**
** INPUT:
**      DataByte - Received data byte
**
** INPUT/OUTPUT:
**      crc - Current value of CRC.
************************************************************************/
static void UpdateCRC (unsigned char DataByte, unsigned short *crc)
{
  unsigned long new_crc;

  new_crc = *crc;
  new_crc = (new_crc << 8) ^ (CrcTable[new_crc >> 8] ^ DataByte);
  *crc = (unsigned short) new_crc;
}


/************************************************************************
** NAME: GenCrcTable
**
** This function computes CRC look-up table.
************************************************************************/
static void GenCrcTable (void)
{
  unsigned short CrcCode;
  unsigned char  DataByte;
  unsigned long  index, BitNo;
  unsigned long  CarryBit, DataBit;

  for( index = 0; index <= 255; index++ )
  {
    CrcCode = 0;
    DataByte = index;

    for( BitNo = 1; BitNo <= 8; BitNo++ )
    {
      CarryBit = ((CrcCode & 0x8000) != 0);
      DataBit = ((DataByte & 0x80) != 0);
      CrcCode = CrcCode << 1;
      if( CarryBit ^ DataBit )
        CrcCode = CrcCode ^ POLYNOMIAL;
      DataByte = DataByte << 1;
    }

    CrcTable [index] = CrcCode;
  }
}
/**************************************************************************
*
* @purpose  Calculate the CRC of a given null terminated string of data
*
* @param    fp pointer of buffer to calculate CRC
*
* @returns  calculated CRC
*
* @end
*
*************************************************************************/
static unsigned short calc_crc(char *fp, int len)
{
  unsigned short crc;
  unsigned int   i;

  GenCrcTable ();
  crc = 0;

  for( i=0; i<len; i++ )
  {
    UpdateCRC (fp[i], &crc);
  }

  return(crc);
}

/*****************************************************************
 * misc_init_r - miscellaneous platform dependent initializations
 ******************************************************************/
int misc_init_r (void)
{
	uchar enetaddr[6];
	uchar vpd[VPD_LENGTH + 10];
	size_t vpdSize = VPD_LENGTH;
	int rval;
	u16 crc16, crc;
	int  i;
	int test;
	int rc;
	uint32_t box_id;
	uint32_t gpio_in;
	char buf[20];
	char *version = 0;
	char *boardname;
	char *env;
	struct spi_flash *flash;
	int save = 0;

	boardname = "GS7XXT";
	gpio_in = (reg32_read((volatile uint32_t *)0x18000060));
	printf("gpio in: %x \n",gpio_in);

	switch (gpio_in & BOARD_ID_MASK) {
	case BOARD_ID_716:
		boardname = "GS716Tv3";
		break;
	case BOARD_ID_724:
		boardname = "GS724Tv4";
		break;
	case BOARD_ID_748:
		boardname = "GS748Tv5";
		break;
	default:
		break;
	}
	env = getenv("boardname");
	if ((env == 0) || (strncmp(env, boardname, 16) != 0)){
		setenv ("boardname", boardname);
		save = 1;
	}
	/* If the current version and env version are differ, change it*/
	version = getenv("ver");
	if (version != 0){
		if (strncmp(version, version_string, 100) != 0){
			setenv ("ver", version_string);
			save = 1;
		}
	}
	else {
		setenv ("ver", version_string);
		save = 1;
	}

	if (save != 0){
		saveenv();
	}
     
	/* Get the stored mac address */
	enetaddr[0] = 0x20;
	enetaddr[1] = 0x12;
	enetaddr[2] = 0x21;
	enetaddr[3] = 0x21;
	enetaddr[4] = 0x30;
	enetaddr[5] = 0x40; 
	flash = spi_flash_probe(CONFIG_ENV_SPI_BUS, CONFIG_ENV_SPI_CS,
		CONFIG_ENV_SPI_MAX_HZ, CONFIG_ENV_SPI_MODE);
	if (flash) {
		rc = spi_flash_read(flash, VPD_OFFSET, VPD_LENGTH, vpd);
		if (rc <= -1){
			printf("Flash read fail\n");
		}
		else{
			crc16 = calc_crc(&vpd[2], VPD_LENGTH-2);
			if (crc16 == htons(*(u16 *)&vpd[0])) {
				memcpy(enetaddr, &vpd[VPD_MAC_OFFSET], 6);
			}
			else{
			printf("Vpd crc fail\n");
			}
			sprintf(buf, "%02x:%02x:%02x:%02x:%02x:%02x", enetaddr[0], 
			enetaddr[1], enetaddr[2], enetaddr[3], enetaddr[4], enetaddr[5]);
		}
	}
	else{
		printf("Flash probe fail\n");
	}
        setenv("ethaddr", buf);
	/* Check for default config ket press 
	Clear the config and boot in nsdp mode */
	gpio_in = (reg32_read((volatile uint32_t *)0x18000060));
	printf("gpio in: %x \n",gpio_in);
	if ((gpio_in & NSDP_ACTIVE_FAC_DEF) == 0){
		setenv ("defaultconfig", "TRUE");
		saveenv();
		/* Boot in NSDP mode */
		setenv ("nsdp", "true");
	}
	return(0);
}

/**********************************************
 * dram_init - sets uboots idea of sdram size
 **********************************************/
int dram_init (void)
{
	uint32_t sku_id;
	bench_screen_test1();

	ddr_init2();

	sku_id = (reg32_read((volatile uint32_t *)CMIC_DEV_REV_ID)) & 0x0000ffff;
		gd->ram_size = CONFIG_PHYS_SDRAM_1_SIZE - CONFIG_PHYS_SDRAM_RSVD_SIZE;
    return 0;
}

int board_early_init_f (void)
{
	int status = 0;
	uint32_t sku_id, val;

	sku_id = (reg32_read((volatile uint32_t *)CMIC_DEV_REV_ID)) & 0x0000ffff;
	if(sku_id == 0x8344) {
		/* Configure ARM PLL to 400MHz */
		iproc_config_armpll(400);

		status = iproc_config_genpll(4); /* Wolfhound - BCM53344, change AXI clock to 200MHz */
		/* APB clock is axiclk/4,i.e 50MHz */
		/* Set the UART clock to APB clock so we get baudrate 115200. 50000000/16/27 =  115740 */
		val = reg32_read((volatile uint32_t *)ChipcommonA_CoreCtrl);
		val &= 0xFFFFFFF7;
		reg32_write((volatile uint32_t *)ChipcommonA_CoreCtrl, val); /* Disable UART clock */
		val |= 0x9;  
		reg32_write((volatile uint32_t *)ChipcommonA_CoreCtrl, val); /* Enable and set it to APB clock(bit 0) */
	}
	else {
		/* default SKU = b150, ARM Clock is 1GHz, AXI clock is 400MHz, APB clock is 100MHz */
		/* Set UART clock to APB clock/2 i.e 50MHz to get baudrate 115200*/
		/* Configure ARM PLL to 1GHz */
		iproc_config_armpll(1000);

		//status = iproc_config_genpll(3); /* 0: 400, 2: 285, 3: 250, 4: 200, 5: 100 */
		val = reg32_read((volatile uint32_t *)ChipcommonA_ClkDiv);
		val &= 0xFFFFFF00;
		val |= 0x2;  
		reg32_write((volatile uint32_t *)ChipcommonA_ClkDiv, val); /* set UART clk = APB clk/2 */
	}

	return(status);
}
int board_late_init (void) 
{
	extern ulong mmu_table_addr;    /* MMU on flash fix: 16KB aligned */
	int status = 0;

	printf("Unlocking L2 Cache ...");
	l2cc_unlock();
	printf("Done\n");

	/* MMU on flash fix: 16KB aligned */
	asm volatile ("mcr p15, 0, %0, c2, c0, 0"::"r"(mmu_table_addr)); /*update TTBR0 */
	asm volatile ("mcr p15, 0, r1, c8, c7, 0");  /*invalidate TLB*/
	asm volatile ("mcr p15, 0, r1, c7, c10, 4"); /* DSB */
	asm volatile ("mcr p15, 0, r0, c7, c5, 4"); /* ISB */		

	iproc_clk_enum();

	disable_interrupts();

	init_timeouts();
	init_axitrace();

#if defined(CONFIG_RUN_DDR_SHMOO2) && defined(CONFIG_SHMOO_REUSE)
        /* Save DDR PHY parameters into flash if Shmoo was performed */
        iproc_save_shmoo_values();
#endif

	return status;
}

int board_eth_init(bd_t *bis)
{
	int rc = -1;
#ifdef CONFIG_BCMIPROC_ETH
	printf("Registering eth\n");
	rc = bcmiproc_eth_register(0);
#endif
	return rc;
}

unsigned long get_dram_size(void){

    return CONFIG_PHYS_SDRAM_1_SIZE;
}
