/*
 * $Copyright Open Broadcom Corporation$ 
 */
#include <common.h>
#include <asm/io.h>
#include <asm/mach-types.h>
#include <configs/iproc_board.h>
#include <asm/arch/iproc.h>
#include <asm/arch/socregs.h>
#include <asm/arch/reg_utils.h>
#include <asm/system.h>
#include <iproc_i2c.h>
#include <nand.h>
#include "i2c.h"
#include <spi_flash.h>

DECLARE_GLOBAL_DATA_PTR;

extern int bcmiproc_eth_register(u8 dev_num);

/*****************************************
 * board_init -early hardware init
 *****************************************/
int board_init (void)
{
    gd->bd->bi_arch_number = CONFIG_MACH_TYPE;      /* board id for linux */
    gd->bd->bi_boot_params = LINUX_BOOT_PARAM_ADDR; /* adress of boot parameters */

    return 0;
}

#define POLYNOMIAL 0xA097

static unsigned short CrcTable[256]; /* CRC lookup table */

/************************************************************************
** NAME: UpdateCRC
**
** This function updates existing CRC
**
** INPUT:
**      DataByte - Received data byte
**
** INPUT/OUTPUT:
**      crc - Current value of CRC.
************************************************************************/
static void UpdateCRC (unsigned char DataByte, unsigned short *crc)
{
  unsigned long new_crc;

  new_crc = *crc;
  new_crc = (new_crc << 8) ^ (CrcTable[new_crc >> 8] ^ DataByte);
  *crc = (unsigned short) new_crc;
}

/************************************************************************
** NAME: GenCrcTable
**
** This function computes CRC look-up table.
************************************************************************/
static void GenCrcTable (void)
{
  unsigned short CrcCode;
  unsigned char  DataByte;
  unsigned long  index, BitNo;
  unsigned long  CarryBit, DataBit;

  for( index = 0; index <= 255; index++ )
  {
    CrcCode = 0;
    DataByte = index;

    for( BitNo = 1; BitNo <= 8; BitNo++ )
    {
      CarryBit = ((CrcCode & 0x8000) != 0);
      DataBit = ((DataByte & 0x80) != 0);
      CrcCode = CrcCode << 1;
      if( CarryBit ^ DataBit )
        CrcCode = CrcCode ^ POLYNOMIAL;
      DataByte = DataByte << 1;
    }

    CrcTable [index] = CrcCode;
  }
}

/**************************************************************************
*
* @purpose  Calculate the CRC of a given null terminated string of data
*
* @param    fp pointer of buffer to calculate CRC
*
* @returns  calculated CRC
*
* @end
*
*************************************************************************/
static unsigned short calc_crc(char *fp, int len)
{
  unsigned short crc;
  unsigned int   i;

  GenCrcTable ();
  crc = 0;

  for( i=0; i<len; i++ )
  {
    UpdateCRC (fp[i], &crc);
  }

  return(crc);
}

/*****************************************************************
 * misc_init_r - miscellaneous platform dependent initializations
 ******************************************************************/
#define VPD_LENGTH              256
#define VPD_OFFSET              0x120000
#define VPD_MAC_OFFSET          4
#define MAC_ADDR_LEN            17
                                
#define BOARD_ID_MASK           0x0F
#define XSM4324S_BRD_ID         0x04
#define XSM4316S_BRD_ID         0x08
#define XSM4348S_BRD_ID         0x0C

#define XSM4324CS_BRD_ID         0x02
#define XSM4348CS_BRD_ID         0x0A

#define SYS_CPLD_I2C_ADDR       0x30
#define SYS_CPLD_REG_BRDID      0x05

static void board_info_set(uint32_t *brd_id, int *save)
{
        uint32_t sku_id, board_id, gpio_in;
        char *boardname, *boardname_env, *boardid_env;
        char boardid_buf[8];

        /** Read GPIO_Pins[6-9] to get the board Id. */
        reg32_write(0x1800C1C0, 0x1FFFCF);  /* Configure Muxer to use these as GPIO pins */
        reg32_write(0x18000024, 0x0);       /* Mask all interrupts */
        reg32_write(0x18000068, 0x0);       /* Disable OUTPUT on all GPIO pins */

        gpio_in = reg32_read((volatile uint32_t *)0x18000060);

        /* If board Id can not be obtained through GPIO pins, Try on I2C interface. */
        if (!gpio_in) {
            (void)i2c_read(SYS_CPLD_I2C_ADDR, SYS_CPLD_REG_BRDID, 1, &gpio_in, 1);
        }

        board_id = (gpio_in >> 6) & BOARD_ID_MASK;
        *brd_id  = board_id;

        switch (board_id) {
        case XSM4324S_BRD_ID:
                boardname = "M4324S";
                break;
        case XSM4348S_BRD_ID:
                boardname = "M4348S";
                
                /* Do the I2C fixup needed for M4348S boards.
                 * Put the Pins 4,5 in Pull High state.
                 * mw.l 0x1800c1dc 0x30;
                 * mw.l 0x1800c1e0 0xFFFFCF;
                 */
                reg32_write(0x1800c1dc, 0x30);
                reg32_write(0x1800c1e0, 0xFFFFCF);
                break;

        case XSM4324CS_BRD_ID:
                boardname = "M4324CS";
                break;

        case XSM4348CS_BRD_ID:
                boardname = "M4348CS";
                
                /* Do the I2C fixup needed for M4348CS boards.
                 * Put the Pins 4,5 in Pull High state.
                 * mw.l 0x1800c1dc 0x30;
                 * mw.l 0x1800c1e0 0xFFFFCF;
                 */
                reg32_write(0x1800c1dc, 0x30);
                reg32_write(0x1800c1e0, 0xFFFFCF);
                break;

        default:
                boardname = "vega-unknown";
                break;
        }
        boardname_env = getenv("boardname");
        if ((boardname_env == 0) || (strncmp(boardname_env, boardname, 20) != 0)) {
                setenv ("boardname", boardname);
                *save = 1;
        }

        sprintf (boardid_buf, "%d", board_id);
        boardid_env = getenv("boardid");
        if ((boardid_env == 0) || (strncmp(boardid_env, boardid_buf, 8) != 0)) {
                setenv ("boardid", boardid_buf);
                *save = 1;
        }

#ifndef CONFIG_SILENT_CONSOLE
        sku_id = reg32_read((volatile uint32_t *)ROM_S0_IDM_IO_STATUS);

        printf ("Board SKU ID: 0x%x\n", sku_id);
        printf ("Board Name  : %s (0x%x)\n", boardname, board_id);
#endif
}

int misc_init_r (void)
{
        uchar enetaddr[6];
        uchar vpd[VPD_LENGTH + 10];
        size_t vpdSize = VPD_LENGTH;
        uint32_t brd_id = 0x9;
        u16 crc16, crc;
        int rc;
        char *env;
        struct spi_flash *flash;
        /* If the current ethaddr and environment ethaddr variable ver */
        /* differ, ensure the environment variable ver is            */
        /* updated. If not FastPath will show the incorrect value    */
        char *version = 0; char prevMac[MAC_ADDR_LEN];
        int save = 0;
        unsigned int   spMacLs4b;
        unsigned short spMacMs2b;

        /* Initialize the I2C controller for CPLD to perform board operations */
        i2c_init(I2C_SPEED_100KHz, 0xff);
        mdelay(1000);

        board_info_set(&brd_id, &save);

        flash = spi_flash_probe(CONFIG_ENV_SPI_BUS, CONFIG_ENV_SPI_CS,
                CONFIG_ENV_SPI_MAX_HZ, CONFIG_ENV_SPI_MODE);
        if (flash) {
                rc = spi_flash_read(flash, VPD_OFFSET, VPD_LENGTH, vpd);
                if (rc <= -1) {
                        printf("Flash read fail\n");
                } else {
                        crc16 = calc_crc(&vpd[2], VPD_LENGTH-2);
                        if (crc16 == htons(*(u16 *)&vpd[0])) {
                                memcpy(enetaddr, &vpd[VPD_MAC_OFFSET], 6);

                                spMacMs2b = (enetaddr[0] << 8) | enetaddr[1];
                                spMacLs4b = (enetaddr[2] << 24) | (enetaddr[3] << 16) | 
                                            (enetaddr[4] << 8) | (enetaddr[5]);

                                /* Increment the MAC address by 1 */
                                if (0xffffffff == spMacLs4b) {
                                   spMacLs4b = 0;
                                   spMacMs2b = spMacMs2b + 1;
                                } else {
                                   spMacLs4b = spMacLs4b + 1;
                                }

                                enetaddr[0] = spMacMs2b >> 8;
                                enetaddr[1] = spMacMs2b & 0x00ff;
                                enetaddr[2] = (spMacLs4b >> 24) & 0x000000ff;
                                enetaddr[3] = (spMacLs4b >> 16) & 0x000000ff;
                                enetaddr[4] = (spMacLs4b >> 8)  & 0x000000ff;
                                enetaddr[5] = (spMacLs4b )      & 0x000000ff;

#ifndef CONFIG_SILENT_CONSOLE
                                printf("Setting ethaddr from VPD: %02X:%02X:%02X:%02X:%02X:%02X\n", 
                                       enetaddr[0], enetaddr[1], enetaddr[2], enetaddr[3], 
                                       enetaddr[4], enetaddr[5]);
#endif
                        } else {
                           enetaddr[0] = 0x00;enetaddr[1] = 0x00 | brd_id;
                           enetaddr[2] = 0x43;enetaddr[3] = 0x00;
                           enetaddr[4] = 0x41;enetaddr[5] = 0x91;
                           printf("No ethaddr in VPD. Using default 00:%02X:43:00:41:91\n", enetaddr[1]);
                        }
                }
        } else {
                printf("Flash probe fail\n");
        }

        version = getenv("ethaddr");
        if(version != NULL)
        {
          memcpy(prevMac, version, strlen(version));
        }
        eth_setenv_enetaddr("ethaddr", enetaddr);
        version = getenv("ethaddr");

        if(version == NULL)
        {
           save = 1;
        } else if (strncmp(version, prevMac, MAC_ADDR_LEN) != 0) {
            save = 1;
        }
        if (save != 0) {
              udelay(10000);
              saveenv();
        }

        return(0);
}

/**********************************************
 * dram_init - sets uboots idea of sdram size
 **********************************************/
int dram_init (void)
{
#ifdef CONFIG_L2C_AS_RAM
    ddr_init();
#endif
    gd->ram_size = CONFIG_PHYS_SDRAM_1_SIZE - CONFIG_PHYS_SDRAM_RSVD_SIZE;

    return 0;
}

int board_early_init_f (void)
{
    int sku_id;
    sku_id = (reg32_read((volatile uint32_t *)ROM_S0_IDM_IO_STATUS) >> 2) & 0x03;

    if (0 == sku_id)
        iproc_config_armpll(1000);
    else
        iproc_config_armpll(800);

    return 0;
}

#ifdef CONFIG_PWM0_FOR_FAN
/* Fixme: Better to have a generic PWM config function to configure any 
   PWM channel */
void bcm95301x_config_pwm0_4_fan(void)
{
	reg32_set_bits(ChipcommonB_GP_AUX_SEL, 0x01); // Select PWM 0 
	reg32_clear_bits(CRU_GPIO_CONTROL0_OFFSET, 1 << 8); // Selct PWM
	reg32_write_masked(ChipcommonB_PWM_PRESCALE, 0x3F, 24); // prescale = 24 + 1
	reg32_write(ChipcommonB_PWM_PERIOD_COUNT0, 5 * 180); 
	reg32_write(ChipcommonB_PWM_DUTYHI_COUNT0, 5 * 135);
	reg32_set_bits(ChipcommonB_PWMCTL, 0x101); // Enable PWM0
}
#endif

int board_late_init (void) 
{
	extern ulong mmu_table_addr;    /* MMU on flash fix: 16KB aligned */
	int status = 0;
	int i;
    
	/* unlock the L2 Cache */
#ifndef CONFIG_NO_CODE_RELOC
#ifndef CONFIG_SILENT_CONSOLE
	printf("Unlocking L2 Cache ...");
#endif
	l2cc_unlock();
#ifndef CONFIG_SILENT_CONSOLE
	printf("Done\n");
#endif
#endif

	/* MMU on flash fix: 16KB aligned */
	asm volatile ("mcr p15, 0, %0, c2, c0, 0"::"r"(mmu_table_addr)); /*update TTBR0 */
	asm volatile ("mcr p15, 0, r1, c8, c7, 0");  /*invalidate TLB*/
	asm volatile ("mcr p15, 0, r1, c7, c10, 4"); /* DSB */
	asm volatile ("mcr p15, 0, r0, c7, c5, 4"); /* ISB */		

	/* Systick initialization(private timer)*/
	iproc_clk_enum();
	//glb_tim_init();
#ifndef STDK_BUILD
	disable_interrupts();
#if 0 // Fixme: To be enabled later,works but linux fails to run
	/* turn off I/D-cache */
	icache_disable();
	dcache_disable();

	/* Cache flush */
	asm ("mcr p15, 0, %0, c7, c5, 0": :"r" (0));

	/* turn off L2 cache */
	l2cc_disable();
	/* invalidate L2 cache also */
	invalidate_dcache();

	i = 0;
	/* mem barrier to sync up things */
	asm("mcr p15, 0, %0, c7, c10, 4": :"r"(i));

	scu_disable();
#endif
#ifdef CONFIG_PWM0_FOR_FAN
	bcm95301x_config_pwm0_4_fan();
#endif
#else
	gic_disable_interrupt(29);
	irq_install_handler(29, systick_isr, NULL);
	gic_config_interrupt(29, 1, IPROC_INTR_LEVEL_SENSITIVE, 0, IPROC_GIC_DIST_IPTR_CPU0);
	iproc_systick_init(10000);

	/* MMU and cache setup */
	disable_interrupts();
	//printf("Enabling SCU\n");
	scu_enable();

	printf("Enabling icache and dcache\n");
	dcache_enable();
	icache_enable();

	printf("Enabling l2cache\n");
	status = l2cc_enable();
	printf("Enabling done, status = %d\n", status);

	enable_interrupts();
#endif
	return status;
}

int board_eth_init(bd_t *bis)
{
	int rc = -1;
#ifdef CONFIG_BCMIPROC_ETH
#ifndef CONFIG_SILENT_CONSOLE
	printf("Registering eth\n");
#endif
	rc = bcmiproc_eth_register(0);
#endif
	return rc;
}

/**********************************************
 * Board Specific Reset Handler
 **********************************************/
int do_reset(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
   u8 hwreset = 0x01;
   int ret;

   printf("resetting %s board...\n", getenv("boardname"));

   udelay (50000);                         /* wait 50 ms */
   disable_interrupts();

   /** Trigger the hardware reset through CPLD */
   ret = i2c_write(SYS_CPLD_I2C_ADDR, 0x1, 1, &hwreset, 1);
   if (ret < 0) {
      udelay (50000);                         /* wait 50 ms */
      ret = i2c_write(SYS_CPLD_I2C_ADDR, 0x1, 1, &hwreset, 1);
   }
   mdelay (5000);   /* on XSM boards, CPLD reset demands more wait cycles. wait for more time.  */

   printf("Hardware reset failed. Power cycle recommended...\n");
   mdelay(10);
   while(1);

   /*NOTREACHED*/
   return 0;
}
