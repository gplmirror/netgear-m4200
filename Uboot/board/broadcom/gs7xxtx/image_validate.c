#include <common.h>
#include <asm/io.h>
#include <asm/mach-types.h>
#include <config.h>
#include <asm/arch/iproc.h>
#include <asm/system.h>
#include "asm/iproc/iproc_common.h"
#include <asm/arch/iproc_regs.h>
#include "asm/arch/socregs.h"
#include "asm/arch/reg_utils.h"

/* Fixed tags that help identify the STK files.
*/
#define STK_TAG1 ((unsigned short) 0xaa55)
#define STK_TAG2 ((unsigned long) 0x2288bb66)
#define MAX_BUFFER_SIZE                 4096
#define MAX_BUF_SIZE                    4096
#define POLYNOMIAL                      0xA097
#define NSDP_PASSWORD_ENCRYPTED_SUPPORT 1
#define NSDP_FW_VALIDATION_SUPPORT      1
#define NSDP_TXT_CFG_VALIDATION_SUPPORT 1
#define NSDP_TAG ((unsigned short) 0x4E4D)

/* Operating system types supported in the STK image.
*/
#define STK_OS_VXWORKS            1
#define STK_OS_LINUX              2
#define STK_OS_ECOS               3
#define TRUE                      1
#define FALSE                     0

#define FAILURE                   1
#define SUCCESS                   0
#define NULLVAL                   0
#define L7_CLI_MAX_STRING_LENGTH  128
#define L7_EOS      ((unsigned char)'\0')   /* ASCIIZ end-of-string character */
typedef struct
{
	unsigned short crc;
	unsigned short tag1;
	unsigned long tag2;

	unsigned long num_components; /* Number of OPR and tgz files in the STK image (may be 0) */
	unsigned long file_size; /* Total number of bytes in the STK file */
	unsigned char rel;
	unsigned char ver;
	unsigned char maint_level;
	unsigned char build_num;
	unsigned long stk_header_size; /* Number of bytes in the STk header */
	unsigned char reserved[64];    /* Reserved for future use */
} stkFileHeader_t;

typedef enum
{
	STK_SUCCESS,
	STK_FAILURE,
	STK_IMAGE_DOESNOT_EXIST,
	STK_INVALID_IMAGE,
	STK_FILE_SIZE_FAILURE,
	STK_FILE_SIZE_MISMATCH,
	STK_TOO_MANY_IMAGES_IN_STK,
	STK_STK_EMPTY,
	STK_PLATFORM_MISMATCH,
	STK_INVALID_IMAGE_FORMAT,
	STK_NOT_ACTIVATED,
	STK_ACTIVATED,
	STK_TABLE_IS_FULL,
	STK_VALID_IMAGE,
	STK_INVALID_IMAGE_VERSION,
	STK_INVALID_SIGNATURE_SIZE,
	STK_LAST_ENTRY
}STK_RC_t;

typedef enum
{
	L7_SUCCESS = 0,
	L7_FAILURE,
	L7_ERROR,
	L7_NOT_IMPLEMENTED_YET,
	L7_NOT_SUPPORTED,
	L7_NOT_EXIST,
	L7_ALREADY_CONFIGURED,
	L7_TABLE_IS_FULL,
	L7_REQUEST_DENIED,
	L7_ASYNCH_RESPONSE,
	L7_ADDR_INUSE,
	L7_NO_VALUE,
	L7_NO_MEMORY,
	L7_DEPENDENCY_NOT_MET,
	L7_HARDWARE_ERROR,
	L7_IMAGE_IN_USE,
	L7_AP_ALREADY_ADDED,
	L7_NOT_APPLICABLE,
	L7_MISMATCH,
	L7_RC_MAX
} L7_RC_t;


#define IPL_MODEL_TAG   0x54334

typedef struct
{
	int8_t* sysDesc;
	int8_t* deviceFamily;
} platfInfo_t;

/* Each set contains two entries: unitDescription from hpc_unit_descriptor_db (broad_hpc_db.c file)
   and platform device family name received from the SCC 1.1 tool */
static platfInfo_t platfInfo_g[2] =
{
	{"GS7XXT", "GS7XXT" },
	{"xx", "xy"},
};

enum
{
	NSDP_MAGIC                           = 0x4E47,
	NSDP_MAGIC_LEN                       = 6, /*strlen("0x4E47")*/
	NSDP_HEADER_REVISION_LEN             = 4, /*strlen("0x01")*/
	NSDP_RESERVED1_LEN                   = 4, /*strlen("0x00")*/
	NSDP_FIRMWARE_DEVICE_FAMILY_LEN      = 20,
	NSDP_FIRMWARE_VERSION_LEN            = 20,
	NSDP_CRC_LEN                         = 10, /*strlen("0x00000000")*/
	NSDP_CRC_OFFSET                      = (NSDP_MAGIC_LEN +
                                         NSDP_HEADER_REVISION_LEN +
                                         NSDP_RESERVED1_LEN +
                                         NSDP_FIRMWARE_DEVICE_FAMILY_LEN +
                                         NSDP_FIRMWARE_VERSION_LEN),
	NSDP_RESERVED2_LEN                   = 16,
	NSDP_TXT_CNFG_HDR_LEN                = (NSDP_MAGIC_LEN +
                                          NSDP_HEADER_REVISION_LEN +
                                          NSDP_RESERVED1_LEN +
                                          NSDP_FIRMWARE_DEVICE_FAMILY_LEN +
                                          NSDP_FIRMWARE_VERSION_LEN +
                                          NSDP_CRC_LEN +
                                          NSDP_RESERVED2_LEN),
};


/* The NSDP Firmware validation header */
typedef struct
{
	unsigned short  magic;                 /* 0x4E, 0x4D */
	unsigned char   header_revision;       /* 0x01 */
	unsigned char   reservd;               /* reserved for future use */
	unsigned char   device_family[20];     /* Firmware device family */
	unsigned char   firmware_version[20];  /* Firmware version string */
	unsigned long   crc;                   /* 32CRC check sum. Computation excludes this header */
	unsigned char   reserved[16];          /* reserved for future use */
} nsdpFileHeader_t;


static uint32_t platfInfoCount_g = sizeof(platfInfo_g)/sizeof(platfInfo_g[0]);

static platfInfo_t* nsdpPlatfInfoBySysDescGet(int8_t* sysDesc);
static uint32_t nsdpPlatfInfoFamilyCheck (int8_t* sysDesc, int8_t* familyName);

extern uint32_t crc32Generate(int8_t* buffer, int32_t length);

extern L7_RC_t simSetNetworkParams(uint8_t *strIpAddr, 
               uint8_t *strNetMask, uint8_t *strGateway);
extern uint8_t debLevel_g;
static unsigned short CrcTable[256]; /* CRC lookup table */
static uint32_t l7_crc32_table_g[256];
/************************************************************************
** NAME: UpdateCRC
**
** This function updates existing CRC
**
** INPUT:
**  DataByte - Received data byte
**
** INPUT/OUTPUT:
**  crc - Current value of CRC.
************************************************************************/
extern void UpdateCRC (unsigned char DataByte, unsigned short *crc)
{
	register unsigned long new_crc;

	new_crc = *crc;
	new_crc = (new_crc << 8) ^ (CrcTable[new_crc >> 8] ^ DataByte);
	*crc = (unsigned short) new_crc;
};

/************************************************************************
** NAME: GenCrcTable
**
** This function computes CRC look-up table.
************************************************************************/
extern void GenCrcTable (void)
{
	unsigned short CrcCode;
	unsigned char DataByte;
	unsigned long  index, BitNo;
	unsigned long  CarryBit, DataBit;

	for (index = 0; index <= 255; index++) {
		CrcCode = 0;
		DataByte = index;
		for (BitNo = 1; BitNo <= 8; BitNo++) {
			CarryBit = ((CrcCode & 0x8000) != 0);
			DataBit = ((DataByte & 0x80) != 0);
			CrcCode = CrcCode << 1;
			if (CarryBit ^ DataBit) CrcCode = CrcCode ^ POLYNOMIAL;
			DataByte = DataByte << 1;
		}; /* End For */
		CrcTable [index] = CrcCode;
	}; /* End for */
};
/**************************************************************************
 *
 * @purpose  This routine computes the CRC of a file in the file system.
 *
 * @param    file_ptr Name of file.
 * @param    file_size Number of bytes expected in the file.
 *
 * @returns  16-bit crc
 *
 * @comments none.
 *
 * @end
 *
 *************************************************************************/
uint16_t file_crc_compute(uint8_t * file_ptr, uint32_t file_size)
{
	uint16_t computed_crc;
	uint8_t c;
	uint32_t num_chars;
	uint8_t * ptr;

	GenCrcTable ();
	ptr = file_ptr;
	/* Compute CRC. */
	/* Skip the first two bytes of the image. The two bytes contain
	 ** CRC. */
	computed_crc = 0;
	num_chars = 2;
	ptr = ptr + 2;
	do
	{
		c = *(unsigned char *)ptr;
		ptr++;
		num_chars++;
		UpdateCRC (c, &computed_crc);
	}while( num_chars < file_size);
	return(computed_crc);
}

/*********************************************************************
* @purpose  This function return the initial CRC32 value
*
* @returns  inital CRC32 value
*
* @notes
*
* @end
*********************************************************************/
uint32_t l7_buf_crc32_init (void)
{
	return ~0;
}
/*********************************************************************
* @purpose  This function calculates the CRC32 lookup table
*
* @notes
*
* @end
*********************************************************************/
static void l7_crc32_table_init (void)
{
	static int32_t first = 1;
	uint32_t val = 0;
	int32_t i = 0;
	int32_t j = 0;

	if (first != 0){
		for (i = 0; i < 256; i++){
			val = i;
			for (j = 0; j < 8; j++){
				if ((val & 1) != 0){
					val = 0xedb88320 ^ (val >> 1);
				}
				else{
					val = val >> 1;
				}
			}
			l7_crc32_table_g[i] = val;
		}
		first = 0;
	}
}

/*********************************************************************
* @purpose  This function return the final CRC32 value
*
* @param    crc - current CRC32 value
*
* @returns  final CRC32 value
*
* @notes
*
* @end
*********************************************************************/
uint32_t l7_buf_crc32_finish (uint32_t crc)
{
	return ~crc;
}

/*********************************************************************
* @purpose  This function calculates and updates the CRC32 for a given buffer
*
* @param    buffer - buffer for which CRC32 is calculated
* @param    length - length of the buffer
* @param    crc    - memory to read and store the updated CRC32
*
* @returns  calculated CRC32 value
*
* @notes
*
* @end
*********************************************************************/
uint32_t l7_buf_crc32_update (uint8_t * buffer, int32_t length, uint32_t * crc)
{
	uint32_t result = ~0;
	int32_t i = 0;

	l7_crc32_table_init ();
	if (crc != NULLVAL){
		result = *crc;
	}

	for (i = 0; i < length; i++){
		result = l7_crc32_table_g[(result ^ buffer[i]) & 0xff] ^ (result >> 8);
	}

	if (crc != NULLVAL){
		*crc = (result & 0xffffffff);
	}
	return result & 0xffffffff;
}
/*********************************************************************
* @purpose  Gets platform information structure from platfInfo_g by system description.
*
* @param    sysDesc       @b{(in)}   system description
*
* @returns  NULL - description not found
*
* @comments
*
* @end
*********************************************************************/
static platfInfo_t* nsdpPlatfInfoBySysDescGet(int8_t* sysDesc)
{
	platfInfo_t* retValue = NULL;
	uint32_t i = 0;

	if (sysDesc == NULL){
		return NULL;
	}

	for (i = 0; i < platfInfoCount_g; i++){
		if (strcmp(sysDesc, platfInfo_g[i].sysDesc) == 0){
			retValue = &platfInfo_g[i];
			break;
		}
	}
	return retValue;
}

/*********************************************************************
* @purpose  Check if system name + family name pair is present in platfInfo_g structure.
*
* @param    sysDesc       @b{(in)}   System description
* @param    familyName    @b{(in)}   Proposed family name to check
*
* @returns  TRUE - if pare of system name and family name is supported
*
* @comments
*
* @end
*********************************************************************/
static uint32_t nsdpPlatfInfoFamilyCheck (int8_t* sysDesc, int8_t* familyName)
{
	uint32_t i = NULLVAL;

	if ((sysDesc == NULL) || (familyName == NULL)){
		return FALSE;
	}

	for (i = 0; i < platfInfoCount_g; i++){
		if (strncmp(sysDesc, platfInfo_g[i].sysDesc, L7_CLI_MAX_STRING_LENGTH) == 0){
			if (strncmp(familyName, platfInfo_g[i].deviceFamily, L7_CLI_MAX_STRING_LENGTH) == 0){
				return TRUE;
			}
		}
	}
	return FALSE;
}

/*********************************************************************
* @purpose  Validates firmware family.
*
* @param    buf           @b{(in)}   buffer containing firmware family
* @param    bufSize       @b{(in)}   size of buf 
*
* @returns  TRUE     - family is valid
*           FALSE    - family is not valid
*
* @comments
*
* @end
*********************************************************************/
uint32_t nsdpFirmwareFamilyValidate(uint8_t *buf, uint32_t bufSize)
{
	uint32_t     bufStrLen = NULLVAL;
	int8_t      sysDesc[L7_CLI_MAX_STRING_LENGTH];
	int8_t      famName[L7_CLI_MAX_STRING_LENGTH];

	if (buf == NULL){
		return FALSE;
	}
	memset(sysDesc, NULLVAL, sizeof(sysDesc));
	memset(famName, NULLVAL, sizeof(famName));
 
	/* find real length of buf string */
	for (bufStrLen = 0; bufStrLen < bufSize; bufStrLen++){
		if (buf[bufStrLen] == L7_EOS){
			break;
		}
		famName[bufStrLen] = buf[bufStrLen];
	}
	 /* trim trailing spaces */
	while ((bufStrLen > 0) && (buf[bufStrLen - 1] == ' ')){
		bufStrLen--;
	}
	strncpy(sysDesc, "GS7XXT", L7_CLI_MAX_STRING_LENGTH);

	if (TRUE == nsdpPlatfInfoFamilyCheck(sysDesc, famName)){
		return TRUE;
	}
	return FALSE;
}
/*********************************************************************
* @purpose  Validate the firmware.
*
* @param    none
*
* @returns  L7_SUCCESS         on success
*           L7_NOT_SUPPORTED   if firmware validation fails
*           L7_ERROR           if CRC validation fails
*           L7_FAILURE         if error
*
* @comments
*
* @end
*********************************************************************/
L7_RC_t nsdpFirmwareValidate(void *file_ptr, uint32_t size)
{
	uint8_t *ptr;
	nsdpFileHeader_t nsdpFwHeader;
	uint32_t stk_size;
	uint32_t crc = 0;

	memset(&nsdpFwHeader, 0, sizeof(nsdpFwHeader));
	stk_size = size - sizeof(nsdpFwHeader);
	ptr = file_ptr + stk_size;
	memcpy((uint8_t *)&nsdpFwHeader, ptr, sizeof(nsdpFwHeader));

	/* Verify the STE header's magic */
	if (ntohs(nsdpFwHeader.magic) != NSDP_TAG){
		return L7_FAILURE;
	}

	/* Validate the Device family */
	if (nsdpFirmwareFamilyValidate(nsdpFwHeader.device_family, 
				sizeof(nsdpFwHeader.device_family)) != TRUE){
		return L7_FAILURE;
	}
	ptr = file_ptr;
	crc = ~0;
	l7_buf_crc32_update(ptr, stk_size, &crc);
	crc = ~crc;
	if( ntohl(nsdpFwHeader.crc) != crc){
		printf("NSDP checksum failed. calculated crc %x file crc%x\n ", crc, ntohl(nsdpFwHeader.crc));
		return L7_FAILURE;
	}
	return L7_SUCCESS;
}
/*********************************************************************
* @purpose  Returns the IPL Model Tag for this unit
*
* @returns  IPL_MODEL_TAG
*
* @notes    none
*
* @end
*********************************************************************/
uint32_t bspapiIplModelGet(void)
{
	return IPL_MODEL_TAG;
}

static STK_RC_t stkImageInfoGet(uint8_t *filePtr, uint32_t *crc_status,
                                                      uint32_t *size)
{
	stkFileHeader_t stkHeader;
	uint32_t file_size;
	uint16_t computed_crc;

	/* read first few bytes to determine STK or OPR */
	memcpy((uint8_t *)&stkHeader, filePtr,sizeof(stkHeader));
	if((ntohs(stkHeader.tag1) == STK_TAG1) &&
		(ntohl(stkHeader.tag2) == STK_TAG2)){
		 /* First, validate CRC */
   		 /* check all available opr files to get the correct
		  * opr file for this target */
		if(ntohl(stkHeader.num_components) == 0){
			return STK_STK_EMPTY;
		}
	}
	else /* STK tags not present */
	{
		return STK_INVALID_IMAGE_FORMAT;
	}
	file_size = ntohl(stkHeader.file_size);
	printf("STK file size %d\n",file_size); 
	/*Doing at the end as crc check is time consuming*/
	computed_crc = file_crc_compute(filePtr, file_size);
	if (computed_crc != ntohs(stkHeader.crc)){
		printf("Computed CRC %x header CRC %x \n",computed_crc,ntohs(stkHeader.crc));
		return STK_INVALID_IMAGE;
	}

	if(size != NULL){
		*size = file_size;
	}
	return STK_SUCCESS;
}

/**************************************************************************
 *
 * @purpose  Returns the size of the image
 *
 * @param    fileName     Operational code file.
 * @param    size         variable to return file size
 *
 * @returns  STK_SUCCESS   on successful execution
 *
 * @end
 *
 *************************************************************************/
STK_RC_t stkImageSizeGet(uint8_t *fileName, uint32_t *size)
{
	STK_RC_t rc;

	rc = stkImageInfoGet(fileName, NULL, size);
	return rc;
}

uint32_t nsdpFirmwareFileValidate(uint8_t *filePtr)
{
	uint32_t stkSize;
	uint32_t imageSize;
	uint8_t *ptr;
	STK_RC_t rc;
	uint32_t valid = FAILURE;
	nsdpFileHeader_t nsdpFwHeader;
	
	/* Verify the stk file and get the size */
	rc = stkImageSizeGet(filePtr, &stkSize);
	if (STK_SUCCESS != rc){
		printf(" STK validation failed rc %d\n",rc);
		return valid;
	}
	printf(" STK validation Success\n");
	return SUCCESS;
#if 0
    imageSize = stkSize + sizeof(nsdpFwHeader);
	ptr = filePtr + stkSize;
	memcpy((uint8_t *)&nsdpFwHeader, ptr, sizeof(nsdpFwHeader));
	if (nsdpFirmwareValidate(filePtr, imageSize) != L7_SUCCESS){
		printf(" NSDP header validation failed\n");
		return FAILURE;
	}
	printf(" NSDP validation success\n ");
	return SUCCESS;
#endif
}
/**************************************************************************
 *
 * @purpose  Validates the image
 *
 *
 * @returns  SUCCESS   on successful execution
 *           FAILURE   on fail
 *
 * @end
 *
 *************************************************************************/
int do_validate(cmd_tbl_t *cmdtp,
        int flag, int argc, char * const argv[])
{
	uint32_t imageSize;
	uint32_t rc;
  
	rc = 1;
	printf("Validating image at 0x%08X\n", CONFIG_LOADADDR);
	rc = nsdpFirmwareFileValidate((uint8_t *)CONFIG_LOADADDR);
	return rc;
}

U_BOOT_CMD(
	validate, 2, 0,  do_validate,
	"Validate the image",
	"Validate the image"
);
