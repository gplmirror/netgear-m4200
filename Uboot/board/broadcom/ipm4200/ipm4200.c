/*
 * $Copyright Open Broadcom Corporation$
 */

#include <common.h>
#include <asm/io.h>
#include <asm/mach-types.h>
#include <config.h>
#include <asm/arch/iproc.h>
#include <asm/system.h>
#include "asm/iproc/reg_utils.h"
#include "asm/iproc/iproc_common.h"
#include <iproc_i2c.h>
#include "i2c.h"
#include <spi_flash.h>

#define SYS_CPLD_I2C_ADDR  0x30
#define VPD_LENGTH         256
#define VPD_OFFSET         0x180000
#define VPD_MAC_OFFSET     4

DECLARE_GLOBAL_DATA_PTR;

/*****************************************
 * board_init -early hardware init
 *****************************************/
int board_init (void)
{

#if !defined(CONFIG_SYS_NO_FLASH) && \
    !defined(CONFIG_NOR_IPROC_BOOT) && \
    !defined(CONFIG_NAND_IPROC_BOOT)

  /* If booting from PNOR, PNOR is already selected, no override required.
   * If booting from NAND, we shouldn't switch to PNOR.
   */
  *(volatile int *)ICFG_IPROC_IOPAD_SW_OVERRIDE_CTRL |=
    (1 << ICFG_IPROC_IOPAD_SW_OVERRIDE_CTRL__iproc_pnor_sel) |
    (1 << ICFG_IPROC_IOPAD_SW_OVERRIDE_CTRL__iproc_pnor_sel_sw_ovwr);

  /* Configure controller memory width based on mw strap */
  if (*(volatile int *)ICFG_PNOR_STRAPS &
      (1 << ICFG_PNOR_STRAPS__PNOR_SRAM_MW_R))
  {
    /* 16-bit */
    *(volatile int *)PNOR_set_opmode |= (1 << PNOR_set_opmode__set_mw_R);
  }
  else
  {
    /* 8-bit */
    *(volatile int *)PNOR_set_opmode &= ~(1 << PNOR_set_opmode__set_mw_R);
  }
  *(volatile int *)PNOR_direct_cmd |= (2 << PNOR_direct_cmd__cmd_type_R);

#endif /* !CONFIG_SYS_NO_FLASH && !CONFIG_NOR_IPROC_BOOT */

  gd->bd->bi_arch_number = CONFIG_MACH_TYPE;      /* board id for linux */
  gd->bd->bi_boot_params =
    LINUX_BOOT_PARAM_ADDR; /* adress of boot parameters */

  return 0;
}

#define POLYNOMIAL 0xA097

static unsigned short CrcTable[256]; /* CRC lookup table */

/************************************************************************
** NAME: UpdateCRC
**
** This function updates existing CRC
**
** INPUT:
**  DataByte - Received data byte
**
** INPUT/OUTPUT:
**  crc - Current value of CRC.
************************************************************************/
static void UpdateCRC (unsigned char DataByte, unsigned short *crc)
{
  unsigned long new_crc;

  new_crc = *crc;
  new_crc = (new_crc << 8) ^ (CrcTable[new_crc >> 8] ^ DataByte);
  *crc = (unsigned short) new_crc;
}

/************************************************************************
** NAME: GenCrcTable
**
** This function computes CRC look-up table.
************************************************************************/
static void GenCrcTable (void)
{
  unsigned short CrcCode;
  unsigned char  DataByte;
  unsigned long  index, BitNo;
  unsigned long  CarryBit, DataBit;

  for ( index = 0; index <= 255; index++ )
  {
    CrcCode = 0;
    DataByte = index;

    for ( BitNo = 1; BitNo <= 8; BitNo++ )
    {
      CarryBit = ((CrcCode & 0x8000) != 0);
      DataBit = ((DataByte & 0x80) != 0);
      CrcCode = CrcCode << 1;
      if ( CarryBit ^ DataBit )
      {
        CrcCode = CrcCode ^ POLYNOMIAL;
      }
      DataByte = DataByte << 1;
    }

    CrcTable [index] = CrcCode;
  }
}

/**************************************************************************
*
* @purpose  Calculate the CRC of a given null terminated string of data
*
* @param    fp pointer of buffer to calculate CRC
*
* @returns  calculated CRC
*
* @end
*
*************************************************************************/
static unsigned short calc_crc(char *fp, int len)
{
  unsigned short crc;
  unsigned int   i;

  GenCrcTable();
  crc = 0;

  for ( i = 0; i < len; i++ )
  {
    UpdateCRC (fp[i], &crc);
  }

  return (crc);
}

/* increment vpd base MAC address by one to get the OOB MAC address*/
static void getOobMacAddr(unsigned char *vpdMAC, unsigned char *oobMAC)
{
  uint32_t portMacLs4b, sysMacLs4b;
  uint16_t portMacMs2b, sysMacMs2b;

  sysMacMs2b = (vpdMAC[0] << 8) | vpdMAC[1];
  sysMacLs4b = (vpdMAC[2] << 24) | (vpdMAC[3] << 16) |
               (vpdMAC[4] << 8) | (vpdMAC[5]);

  portMacLs4b = sysMacLs4b + 1;

  /* if there was a carry */
  if (portMacLs4b < sysMacLs4b)
  {
    portMacMs2b = sysMacMs2b + 1;
  }
  else
  {
    portMacMs2b = sysMacMs2b;
  }

  oobMAC[0] = portMacMs2b >> 8;
  oobMAC[1] = portMacMs2b & 0x00ff;

  oobMAC[2] = (portMacLs4b >> 24) & 0x000000ff;
  oobMAC[3] = (portMacLs4b >> 16) & 0x000000ff;
  oobMAC[4] = (portMacLs4b >> 8)  & 0x000000ff;
  oobMAC[5] = (portMacLs4b )      & 0x000000ff;
}

/*****************************************************************
 * misc_init_r - miscellaneous platform dependent initializations
 ******************************************************************/
int misc_init_r (void)
{
  extern const uchar default_environment[]; /* env_common.c */
  uchar buf[16];
  uchar enetaddr[6];
  uchar enetaddr_mfg[6] = { 0x00, 0xe0, 0x95, 0x83, 0x01, 0x43 };
  uchar vpd[VPD_LENGTH];
  u16 crc16;
  int i, do_apply = 0;

  /*
   * There is no default value to 'bootcmd' in fw_setenv for IPM4200,
   * so we could restore all the default values to NULL keys upon a NULL
   * pointer returned from 'getenv("bootcmd")'.
   */
  if (!getenv("bootcmd"))
  {
    uchar *env = default_environment;
    uchar *key, *val;

    while (strlen(env) > 0)
    {
      key = env;
      env = env + strlen(env) + 1;
      val = strchr(key, '=');
      if (!val)
      {
        break;
      }
      *(val++) = 0;

      /* DO NOT overwrite! */
      if (!getenv(key))
      {
        setenv(key, val);
        do_apply = 1;
      }
    }
  }

  /* If ethaddr is present in VPD, read it. Set the factory default otherwise. */
  {
#ifdef CONFIG_ENV_IS_IN_SPI_FLASH
    struct spi_flash *sf;

    sf = spi_flash_probe(CONFIG_ENV_SPI_BUS, CONFIG_ENV_SPI_CS,
                         CONFIG_ENV_SPI_MAX_HZ, CONFIG_ENV_SPI_MODE);
    if (!sf)
    {
      printf("%s: %d: unable to initialize SPI flash\n",
             __FILE__, __LINE__);
      return -1;
    }
    if (spi_flash_read(sf, VPD_OFFSET, VPD_LENGTH, vpd) < 0)
    {
      printf("%s: %d: unable to retrieve VPD from SPI flash\n",
             __FILE__, __LINE__);
      return -1;
    }
#elif defined(CONFIG_ENV_IS_IN_NAND)
    size_t vpdSize = VPD_LENGTH;

    nand_read_skip_bad(&nand_info[0], VPD_OFFSET, &vpdSize, vpd);
#endif
    crc16 = calc_crc(&vpd[2], VPD_LENGTH - 2);
    if (crc16 == htons(*(u16 *)&vpd[0]))
    {
      getOobMacAddr(&vpd[VPD_MAC_OFFSET], enetaddr);
      eth_setenv_enetaddr("ethaddr", enetaddr);
      do_apply = 1;
    }
    else
    {
      if (!eth_getenv_enetaddr("ethaddr", enetaddr)) 
      {
         eth_setenv_enetaddr("ethaddr", enetaddr_mfg);

         /* VPD invalid, run mfg diags to program it */
#ifndef CONFIG_SILENT_CONSOLE
         printf("VPD: invalid crc! Activating manufacturing mode...\n");
#endif
         setenv("active", "mfg");
      }
    }
  }

  /* Set the 'memsize' env variable to set the memory size for Linux */
  sprintf(buf, "%dM", (gd->ram_size >> 20) - 16);
  setenv("memsize", buf);

  if (do_apply)
  {
    saveenv();
  }

  return 0;
}

/**********************************************
 * dram_init - sets uboots idea of sdram size
 **********************************************/
int dram_init (void)
{
  uint8_t brdid;

  /* Initialize the I2C controller for CPLD to perform board operations */
  i2c_init(I2C_SPEED_100KHz,0xff);
  mdelay(1000);

#if !defined(CONFIG_IPROC_NO_DDR) && !defined(CONFIG_NO_CODE_RELOC)
  ddr_init2();
#endif

  gd->ram_size = CONFIG_PHYS_SDRAM_1_SIZE - CONFIG_PHYS_SDRAM_RSVD_SIZE;

  /* Find board_rev from CPLD */
  i2c_read(SYS_CPLD_I2C_ADDR, 0x05, 1, &brdid, 1);

  if ((brdid & 0x07) < 3) {
    gd->ram_size = 512 << 20;
  } else {
    gd->ram_size = 1024 << 20;
  }

#ifndef CONFIG_SILENT_CONSOLE
  printf("Board: M4200, HwRev: %d, DRAM: %dMB\n", (brdid & 0x07), (gd->ram_size >> 20));
#endif

  return 0;
}

int board_early_init_f (void)
{
  uint32_t sku;
  sku = reg32_read((volatile uint32_t *)ICFG_CHIP_ID_REG);
  if ((sku & 0xfff0) == 0xb060)
  {
    /* Ranger2 - 5606x: CPU 1250MHz, AXI 400MHz, DDR 800MHz */
    iproc_config_armpll(1250);
    iproc_config_genpll(0);
  }
  else
  {
    /* Greyhound - 5341x: CPU 600MHz, AXI 200MHz, DDR 667MHz */
    iproc_config_armpll(600);
    iproc_config_genpll(1);
  }

#ifndef CONFIG_IPROC_NO_DDR
  /* Initialize ATCM and BTCM for SHMOO (clear to avoid ECC error) */
  {
    int i;
    volatile int *p;
    p = (volatile int *)0x01000000;
    for (i = 0; i < 64 * 1024 / 4; i++, p++)
    {
      *p = 0;
    }
    p = (volatile int *)0x01080000;
    for (i = 0; i < 128 * 1024 / 4; i++, p++)
    {
      *p = 0;
    }
  }
#endif

  return 0;
}

int board_late_init (void)
{
  int status = 0;

#if defined(CONFIG_L2C_AS_RAM) && !defined(CONFIG_NO_CODE_RELOC)
  extern ulong mmu_table_addr;    /* MMU on flash fix: 16KB aligned */

#ifndef CONFIG_SILENT_CONSOLE
  printf("Unlocking L2 Cache ...");
#endif
  l2cc_unlock();
#ifdef CONFIG_SILENT_CONSOLE
  mdelay(10);
#else
  printf("Done\n");
#endif

  /*
   * Relocate MMU table from flash to DDR since flash may not be always accessable.
   * eg. When QSPI controller is in MSPI mode.
   */
  asm volatile ("mcr p15, 0, %0, c2, c0, 0"::"r"(mmu_table_addr)); /* Update TTBR0 */
  asm volatile ("mcr p15, 0, r1, c8, c7, 0");  /* Invalidate TLB*/
  asm volatile ("mcr p15, 0, r1, c7, c10, 4"); /* DSB */
  asm volatile ("mcr p15, 0, r0, c7, c5, 4"); /* ISB */

  l2cc_disable();
#endif

  disable_interrupts();
  iproc_clk_enum();

#ifdef STDK_BUILD
  enable_interrupts();
#endif /* STDK_BUILD */

#if !defined(CONFIG_IPROC_NO_DDR) && defined(CONFIG_SHMOO_AND28_REUSE)
  save_shmoo_to_flash();
#endif

  return status;
}

/* override enable_caches() in arch/arm/lib/cache.c */
void enable_caches(void)
{
  icache_enable();
  dcache_enable();
}

int board_eth_init(bd_t *bis)
{
  int rc = -1;

#ifdef CONFIG_BCMIPROC_ETH
#ifndef CONFIG_SILENT_CONSOLE
  printf("Registering eth\n");
#endif
  rc = bcmiproc_eth_register(0);
#endif

  return rc;
}

/**********************************************
 * Board Specific Reset Handler
 **********************************************/
int do_reset(cmd_tbl_t *cmdtp, int flag, int argc, char * const argv[])
{
   u8 hwreset = 0x01;
   int ret;

   printf("resetting M4200 board...\n");

   udelay (50000);                         /* wait 50 ms */
   disable_interrupts();

   /** Trigger the hardware reset through CPLD */
   ret = i2c_write(SYS_CPLD_I2C_ADDR, 0x1, 1, &hwreset, 1);
   if (ret < 0) {
      udelay (50000);                         /* wait 50 ms */
      ret = i2c_write(SYS_CPLD_I2C_ADDR, 0x1, 1, &hwreset, 1);
   }
   mdelay (5000);   /* on FXCN boards, CPLD reset demands more wait cycles. wait for more time.  */

   printf("Hardware reset failed. Triggering CPU reset...\n");
   reset_cpu(0);
   mdelay(10);

   printf("Hardware reset failed. Power cycle recommended...\n");
   mdelay(10);
   while(1);

   /*NOTREACHED*/
   return 0;
}
