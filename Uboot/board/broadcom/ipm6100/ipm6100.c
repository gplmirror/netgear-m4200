#include <common.h>
#include <asm/io.h>
#include <asm/mach-types.h>
#include <config.h>
#include <iproc_i2c.h>
#include <asm/arch/iproc.h>
#include <asm/system.h>
#include "asm/iproc/reg_utils.h"
#include "asm/iproc/iproc_common.h"
#include <nand.h>
#include "i2c.h"

DECLARE_GLOBAL_DATA_PTR;

extern int bcmiproc_eth_register(u8 dev_num);
extern void iproc_save_shmoo_values(void);
extern int iproc_dump_i2c_regs(void);
unsigned char cpldI2cAddress = 0x0;

#define IO_CONTROLLER_I2C_ADDR  0x30
#define CPLD_MAC_OFFSET_ADDR    0x40

/*****************************************
 * board_init -early hardware init
 *****************************************/
int board_init (void)
{
    gd->bd->bi_arch_number = CONFIG_MACH_TYPE;      /* board id for linux */
    gd->bd->bi_boot_params = LINUX_BOOT_PARAM_ADDR; /* adress of boot parameters */
    return 0;
}

#define POLYNOMIAL 0xA097

static unsigned short CrcTable[256]; /* CRC lookup table */

/************************************************************************
** NAME: UpdateCRC
**
** This function updates existing CRC
**
** INPUT:
**      DataByte - Received data byte
**
** INPUT/OUTPUT:
**      crc - Current value of CRC.
************************************************************************/
static void UpdateCRC (unsigned char DataByte, unsigned short *crc)
{
  unsigned long new_crc;

  new_crc = *crc;
  new_crc = (new_crc << 8) ^ (CrcTable[new_crc >> 8] ^ DataByte);
  *crc = (unsigned short) new_crc;
}

/************************************************************************
** NAME: GenCrcTable
**
** This function computes CRC look-up table.
************************************************************************/
static void GenCrcTable (void)
{
  unsigned short CrcCode;
  unsigned char  DataByte;
  unsigned long  index, BitNo;
  unsigned long  CarryBit, DataBit;

  for( index = 0; index <= 255; index++ )
  {
    CrcCode = 0;
    DataByte = index;

    for( BitNo = 1; BitNo <= 8; BitNo++ )
    {
      CarryBit = ((CrcCode & 0x8000) != 0);
      DataBit = ((DataByte & 0x80) != 0);
      CrcCode = CrcCode << 1;
      if( CarryBit ^ DataBit )
        CrcCode = CrcCode ^ POLYNOMIAL;
      DataByte = DataByte << 1;
    }

    CrcTable [index] = CrcCode;
  }
}

/**************************************************************************
*
* @purpose  Calculate the CRC of a given null terminated string of data
*
* @param    fp pointer of buffer to calculate CRC
*
* @returns  calculated CRC
*
* @end
*
*************************************************************************/
static unsigned short calc_crc(char *fp, int len)
{
  unsigned short crc;
  unsigned int   i;

  GenCrcTable ();
  crc = 0;

  for( i=0; i<len; i++ )
  {
    UpdateCRC (fp[i], &crc);
  }

  return(crc);
}

/*
 *  CPLD i2c address determine
*/
#define CPLD_I2C_MIN_ADDR 0x30
#define CPLD_I2C_MAX_ADDR 0x3F

void cpldI2cAddressDetermine (void)
{
  unsigned char byte; int rc = 0;
  if( cpldI2cAddress > CPLD_I2C_MIN_ADDR ) 
  {
    return;
  }
  cpldI2cAddress = CPLD_I2C_MIN_ADDR | 1;
  if( (rc = i2c_read(cpldI2cAddress, 0, 1, &byte, 1)) != 0 )
  {
    cpldI2cAddress = CPLD_I2C_MIN_ADDR | 2;
    if( (rc = i2c_read(cpldI2cAddress, 0, 1, &byte, 1)) != 0 )
    {
      cpldI2cAddress = CPLD_I2C_MIN_ADDR | 3;
      if( (rc = i2c_read(cpldI2cAddress, 0, 1, &byte, 1)) != 0 )
      {
        cpldI2cAddress = CPLD_I2C_MIN_ADDR | 4;
        if( (rc = i2c_read(cpldI2cAddress, 0, 1, &byte, 1)) != 0 )
        {
          cpldI2cAddress = CPLD_I2C_MIN_ADDR | 0xf;
          if( (rc = i2c_read(cpldI2cAddress, 0, 1, &byte, 1)) != 0 )
          {
            printf("cpldI2cAddress: unable to determine the address \n");
            return; 
          }
        }
      }
    }
  }
}
/*****************************************************************
 * misc_init_r - miscellaneous platform dependent initializations
 ******************************************************************/
#define VPD_LENGTH              256
#define VPD_OFFSET              0x180000
#define VPD_MAC_OFFSET          4
#define SLOT_1_MAC_ADDR_OFFSET  1
#define SLOT_2_MAC_ADDR_OFFSET  4
#define SLOT_3_MAC_ADDR_OFFSET  6
#define SLOT_4_MAC_ADDR_OFFSET  8
#define NO_SLOT_MAC_ADDR_OFFSET 10
#define MAC_ADDR_LEN            17

int misc_init_r (void)
{
    uchar enetaddr[6];
#ifdef VPD_THROUGH_FLASH
    uchar vpd[VPD_LENGTH];
    size_t vpdSize = VPD_LENGTH;
    u16 crc16;
#endif
    int rc = 0, i = 0;

    /* If ethaddr is not set, attempt to read if from VPD */
#ifdef VPD_THROUGH_FLASH
        nand_read_skip_bad(&nand_info[0], VPD_OFFSET, &vpdSize, vpd);
        crc16 = calc_crc(&vpd[2], VPD_LENGTH-2);
        if (crc16 == htons(*(u16 *)&vpd[0])) {
            memcpy(enetaddr, &vpd[VPD_MAC_OFFSET], 6);
            eth_setenv_enetaddr("ethaddr", enetaddr);
        }
#else
       /* establish access to the I2C attached CPLD and blink the status LED*/
       i2c_init(I2C_SPEED_100KHz,0xff);
       mdelay(1000);
       cpldI2cAddressDetermine();

        for(i = 0; i < 6; i++)
        {
           rc = i2c_read(IO_CONTROLLER_I2C_ADDR, (CPLD_MAC_OFFSET_ADDR+i), 1, &enetaddr[i], 1);
           mdelay(10);
        }
        if(rc == 0)
        {
            /* If the current ethaddr and environment ethaddr variable ver */
            /* differ, ensure the environment variable ver is            */
            /* updated. If not FastPath will show the incorrect value    */
            char *version = 0; char prevMac[MAC_ADDR_LEN];
            int save = 0;
#ifdef SEPERATE_MAC_ADDR_FOR_SLOT
            /* MAC addresss macaddr[0] ==> network port, macaddr[1/2/3] ==> For serviceport */
            cpldI2cAddressDetermine();
            if( (cpldI2cAddress - CPLD_I2C_MIN_ADDR) == 1)
                enetaddr[5] += SLOT_1_MAC_ADDR_OFFSET;
            else if( (cpldI2cAddress - CPLD_I2C_MIN_ADDR) == 2)
                enetaddr[5] += SLOT_2_MAC_ADDR_OFFSET;
            else if( (cpldI2cAddress - CPLD_I2C_MIN_ADDR) == 3)
                enetaddr[5] += SLOT_3_MAC_ADDR_OFFSET;
            else if( (cpldI2cAddress - CPLD_I2C_MIN_ADDR) == 4)
                enetaddr[5] += SLOT_4_MAC_ADDR_OFFSET;
            else
                enetaddr[5] += NO_SLOT_MAC_ADDR_OFFSET;
#else
                enetaddr[5] += 1;
#endif

            version = getenv("ethaddr");
            memcpy(prevMac, version, strlen(version));

            eth_setenv_enetaddr("ethaddr", enetaddr);
            version = getenv("ethaddr");
            
            if(version == 0)
               save = 1;

            if (strncmp(version, prevMac, MAC_ADDR_LEN) != 0){
               save = 1;              
            }
            if (save != 0){
                  saveenv();
            }
        }  
#endif
        else
        {
            enetaddr[0]= 0x4;enetaddr[1]= 0x5;enetaddr[2]= 0x6;
            enetaddr[3]= 0x7;enetaddr[4]= 0x8;enetaddr[5]= 0x89; 
            eth_setenv_enetaddr("ethaddr", enetaddr);
        }
    return(0);
}

/**********************************************
 * dram_init - sets uboots idea of sdram size
 **********************************************/
int dram_init (void)
{
	uint32_t sku_id;

#if !defined(CONFIG_SPL) || defined(CONFIG_SPL_BUILD)
	bench_screen_test1();
//#ifdef CONFIG_RUN_DDR_SHMOO
#if 1
	ddr_init2();
#else
	ddr_init();
#endif
#endif

	sku_id = (reg32_read((volatile uint32_t *)CMIC_DEV_REV_ID)) & 0x0000ffff;
	if(sku_id == 0xb040) {
		/* Ranger SVK board */
		gd->ram_size = RANGER_SVK_SDRAM_SIZE - CONFIG_PHYS_SDRAM_RSVD_SIZE;
	}
	else {
		/* default Helix4 */
		gd->ram_size = CONFIG_PHYS_SDRAM_1_SIZE - CONFIG_PHYS_SDRAM_RSVD_SIZE;
	}

    return 0;
}

int board_early_init_f (void)
{
	int status = 0;
	//ihost_pwrdown_cpu(1);
	//ihost_pwrdown_cpu(0);
	return(status);
}

int board_late_init (void) 
{
	int status = 0;
        unsigned char buffer = 0;
	/* Systick initialization(private timer)*/
	iproc_clk_enum();
#ifndef STDK_BUILD
	disable_interrupts();
#else
	gic_disable_interrupt(29);
	irq_install_handler(29, systick_isr, NULL);
	gic_config_interrupt(29, 1, IPROC_INTR_LEVEL_SENSITIVE, 0, IPROC_GIC_DIST_IPTR_CPU0);
	iproc_systick_init(10000);

	/* MMU and cache setup */
	disable_interrupts();
	//printf("Enabling SCU\n");
	scu_enable();

	printf("Enabling icache and dcache\n");
	dcache_enable();
	icache_enable();

	printf("Enabling l2cache\n");
	status = l2cc_enable();
	//printf("Enabling done, status = %d\n", status);

	enable_interrupts();
#endif

#if defined(CONFIG_RUN_DDR_SHMOO2) && defined(CONFIG_SHMOO_REUSE)
	/* Save DDR PHY parameters into flash if Shmoo was performed */
	iproc_save_shmoo_values();
#endif
       return status;
}

int board_eth_init(bd_t *bis)
{
	int rc = -1;
#ifdef CONFIG_BCMIPROC_ETH
#ifndef CONFIG_SILENT_CONSOLE
	printf("Registering eth\n");
#endif
	rc = bcmiproc_eth_register(0);
#endif
	return rc;
}
