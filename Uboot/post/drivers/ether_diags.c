/*
 * (C) Copyright 2002
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>

/*
 * Ethernet test
 *
 * The Serial Communication Controllers (SCC) listed in ctlr_list array below
 * are tested in the loopback ethernet mode.
 * The controllers are configured accordingly and several packets
 * are transmitted. The configurable test parameters are:
 *   MIN_PACKET_LENGTH - minimum size of packet to transmit
 *   MAX_PACKET_LENGTH - maximum size of packet to transmit
 *   TEST_NUM - number of tests
 */

#include <post.h>
#if CONFIG_POST & CONFIG_SYS_POST_ETHER

#include <command.h>
#include <net.h>
#include <serial.h>
#include "../../drivers/net/ethHw_data.h"

DECLARE_GLOBAL_DATA_PTR;

#define MIN_PACKET_LENGTH	64
#define MAX_PACKET_LENGTH	256
#define TEST_NUM		1

#define CTLR_SCC 0

#if defined(CONFIG_SPI)
extern void spi_init_f (void);
extern void spi_init_r (void);
#endif

extern bcm_eth_t g_eth_data;
extern void gmac_loopback(bcm_eth_t *eth_data, bool on);
/* extern void bcmiproc_eth_close(struct eth_device *dev);
extern int bcmiproc_eth_open(struct eth_device *dev, bd_t *bt);
extern int bcmiproc_eth_rcv(struct eth_device *dev);
extern int bcmiproc_eth_send(struct eth_device *dev, volatile void *packet, int length); */

/* The list of controllers to test */
/* #if defined(CONFIG_MPC823)
static int ctlr_list[][2] = { {CTLR_SCC, 1} };
#else
static int ctlr_list[][2] = { };
#endif */

static int ctlr_list[][2] = { {CTLR_SCC, 1} };

/* static struct {
	int (*init) (struct eth_device *dev, bd_t *bt);
	void (*halt) (struct eth_device *dev);
	int (*send) (struct eth_device *dev, volatile void *packet, int length);
	int (*recv) (struct eth_device *dev);
} ctlr_proc[1]; */

static char *ctlr_name[1] = { "SCC" };

/* Ethernet Transmit and Receive Buffers */
#define DBUF_LENGTH  1520

#define TX_BUF_CNT 2

#define TOUT_LOOP 100

static char txbuf[DBUF_LENGTH];

static uint rxIdx;		/* index of the current RX buffer */
static uint txIdx;		/* index of the current TX buffer */



static void packet_fill (char *packet, int length)
{
	char c = (char) length;
	int i;

	packet[0] = 0xFF;
	packet[1] = 0xFF;
	packet[2] = 0xFF;
	packet[3] = 0xFF;
	packet[4] = 0xFF;
	packet[5] = 0xFF;

	for (i = 6; i < length; i++) {
		packet[i] = c++;
	}
}

static int packet_check (char *packet, int length)
{
	char c = (char) length;
	int i;

	for (i = 6; i < length; i++) {
		if (packet[i] != c++)
			return -1;
	}

	return 0;
}

static int test_ctlr (int ctlr, int index)
{
	bd_t *bd = gd->bd;
	int res = -1;
	char packet_send[MAX_PACKET_LENGTH];
	char packet_recv[MAX_PACKET_LENGTH];
	int length;
	int i;
	int l;
	bcm_eth_t *eth_data = &g_eth_data;
	gmacregs_t *regs = eth_data->regs;

  post_log("\nSet LOOPBACK in eth-%d...\n\n", ctlr);
	gmac_loopback(eth_data, true);
	
  /* eth_halt();
	eth_set_current();
	if (eth_init(bd) < 0) {
	  eth_halt();
	  return -1;
	} */
	/* ctlr_proc[ctlr].init (index); */

	for (i = 0; i < TEST_NUM; i++) {
		for (l = MIN_PACKET_LENGTH; l <= MAX_PACKET_LENGTH; l++) {
			post_log("Fill packet with length = %d...\n", l);
			packet_fill(packet_send, l);
			
			post_log("Send this packet to eth-%d...\n", ctlr);
			eth_send(packet_send, l);
			
			NetReceive(packet_recv, &length);
			/* eth_receive(packet_recv, &length); */
			/* length = ctlr_proc[ctlr].recv (index, packet_recv, MAX_PACKET_LENGTH); */
      
			if (length != l || packet_check (packet_recv, length) < 0) {
				goto Done;
			} else {
				post_log("Receive a packet (length = %d) and compare content PASS !!\n", length);
		  }
		}
	}

	res = 0;

Done:
	post_log("\nRelease LOOPBACK in eth-%d...\n", ctlr);
	gmac_loopback(eth_data, false);
	/* ctlr_proc[ctlr].halt (index); */

	/*
	 * SCC2 Ethernet parameter RAM space overlaps
	 * the SPI parameter RAM space. So we need to restore
	 * the SPI configuration after SCC2 ethernet test.
	 */
#if defined(CONFIG_SPI)
	if (ctlr == CTLR_SCC && index == 1) {
		spi_init_f ();
		spi_init_r ();
	}
#endif

	if (res != 0) {
		post_log("ethernet %s%d test failed\n", ctlr_name[ctlr],
				  index + 1);
	}

	return res;
}

/* IPROC_GMAC0_REG_BASE (0x18042000) //GMAC0_DEVCONTROL  */
/* IPROC_GMAC1_REG_BASE	(0x1804a000) //GMAC1_DEVCONTROL  */
/*  cmd_cfg (BASE + 0x808) bit 15                        */

int ether_post_test (int flags)
{
	int res = 0;
	int i;

	/* ctlr_proc[CTLR_SCC].init = bcmiproc_eth_open;
	ctlr_proc[CTLR_SCC].halt = bcmiproc_eth_close;
	ctlr_proc[CTLR_SCC].send = bcmiproc_eth_send;
	ctlr_proc[CTLR_SCC].recv = bcmiproc_eth_rcv; */

	for (i = 0; i < ARRAY_SIZE(ctlr_list); i++) {
		if (test_ctlr (ctlr_list[i][0], ctlr_list[i][1]) != 0) {
			res = -1;
		}
	}

	return res;
}

#endif /* CONFIG_POST & CONFIG_SYS_POST_ETHER */
