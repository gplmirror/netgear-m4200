/*
 * Copyright (C) 2013, Broadcom Corporation. All Rights Reserved.
 * 
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <common.h>
#include <post.h>

#if defined(CONFIG_CYGNUS)

#if CONFIG_POST & CONFIG_SYS_POST_SCI

#include "asm/arch/socregs.h"
#include "../../halapis/include/reg_access.h"


int pass_flag = 0;
int SCI_post_test(int flags);

extern void lcd_write_reg(uint32_t a, uint32_t d);
void sci_reset(void)
{
        uint32_t sw_rst = CPU_READ_32(ASIU_TOP_SW_RESET_CTRL);
        post_log("rd ASIU_TOP_SW_RESET_CTRL = 0x%X\n",sw_rst);
        sw_rst = (sw_rst & ~(1 << ASIU_TOP_SW_RESET_CTRL__SMARTCARD_SW_RESET_N));
        CPU_WRITE_32(ASIU_TOP_SW_RESET_CTRL,sw_rst);
        post_log("wr ASIU_TOP_SW_RESET_CTRL = 0x%X\n",sw_rst);
        post_log("Delay...\n");
        {
                int i = 0;
                for(i = 0 ; i < 100 ; i++);
        }

        sw_rst = (sw_rst | (1 << ASIU_TOP_SW_RESET_CTRL__SMARTCARD_SW_RESET_N));
        post_log("wr ASIU_TOP_SW_RESET_CTRL = 0x%X\n",sw_rst);
        lcd_write_reg(ASIU_TOP_SW_RESET_CTRL,sw_rst);
}
void setgpio3_3p3(int data)
{
	int read_val = CPU_READ_32(ASIU_GP_OUT_EN_3);
	//GPIO 144
	read_val = read_val | 0x1000000;
	CPU_WRITE_32(ASIU_GP_OUT_EN_3,read_val);

	read_val = CPU_READ_32(ASIU_GP_DATA_OUT_3);
	if(data == 1)
		read_val = read_val | 0x1000000;
	else if(data == 0)
		read_val = read_val & 0xFEFFFFFF;

	CPU_WRITE_32(ASIU_GP_DATA_OUT_3,read_val);

}

int scino=0;
#define SCI_OFFSET (SCB_SC_UART_CMD_1 - SCA_SC_UART_CMD_1)

static void cpu_reg_write32(uint32_t addr, uint32_t val)
{
	volatile uint32_t *address =  (volatile uint32_t *)(addr + scino*SCI_OFFSET);

	post_log("WR 0x%08x with 0x%08x\n", address, val);

	CPU_WRITE_32(address, val);
}

static uint32_t cpu_reg_read32(uint32_t addr)
{
	volatile uint32_t *address =  (volatile uint32_t *)(addr + scino*SCI_OFFSET);

	return CPU_READ_32(address);
}

int SCI_post_test(int flags)
{
    uint32_t i;

    int card_detected = 0;
    uint32_t rx_status, rx,pass_flag;
	uint32_t atr_async[20];
	uint32_t cnt=0;
	uint32_t rdata;
	uint32_t atr_sync_byte_count = 0;


	uint32_t atr_async0_rcv[] = {
	                        0x3B,
	                        0xBE,
	                        0x11,
	                        0x00,
	                        0x00,
	                        0x41,
	                        0x01,
	                        0x38,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x02,
	                        0x90,
	                        0x00
	                      };

	uint32_t atr_async1_rcv[] = {
	                        0x3B,
	                        0xBE,
	                        0x11,
	                        0x00,
	                        0x00,
	                        0x41,
	                        0x01,
	                        0x38,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x00,
	                        0x01,
	                        0x90,
	                        0x00
	};

	post_log("\n");
	pass_flag = 0;

	post_log("\nSelect SCI interface: 0 - SCI0; 1 - SCI1\n");
		scino = post_getUserInput("SCI i/f? (0/1) : ");

		CPU_WRITE_32(CRMU_CHIP_IO_PAD_CONTROL, 0x00);
	sci_reset();
	CPU_WRITE_32(ASIU_TOP_CLK_GATING_CTRL, CPU_READ_32(ASIU_TOP_CLK_GATING_CTRL) | 0x100);




	post_log("sci num=%d\n", scino);

	if (!scino) {
	        rdata = CPU_READ_32(GP_OUT_EN);
	        rdata |= 0x3;
	        CPU_WRITE_32(GP_OUT_EN , rdata);

	        rdata = CPU_READ_32(GP_DATA_OUT);
	        rdata |= 0x2;
	        CPU_WRITE_32(GP_DATA_OUT, rdata);
	    }
	    else
	    {
	    	rdata = CPU_READ_32(GP_OUT_EN);
	    	        rdata |= 0x3;
	    	        CPU_WRITE_32(GP_OUT_EN , rdata);

	    	        rdata = CPU_READ_32(GP_DATA_OUT);
	    	        rdata |= 0x1;
	    	        CPU_WRITE_32(GP_DATA_OUT, rdata);
	    }




	cpu_reg_write32(SCA_SC_TIMER_CMD, 0x00);
	cpu_reg_write32(SCA_SC_INTR_EN_1, 0x00);
	cpu_reg_write32(SCA_SC_INTR_EN_2, 0x00);
	cpu_reg_write32(SCA_SC_UART_CMD_1, 0x00);
	cpu_reg_write32(SCA_SC_UART_CMD_2, 0x00);
	cpu_reg_write32(SCA_SC_IF_CMD_1, 0x05);//cpu_reg_write32(SCA_SC_IF_CMD_1, 0x05);

	cpu_reg_write32(SCA_SC_CLK_CMD_1, 0x8b);
	cpu_reg_write32(SCA_SC_PRESCALE, 0x0b);

	cpu_reg_write32(SCA_SC_IF_CMD_1, 0x07);//cpu_reg_write32(SCA_SC_IF_CMD_1, 0x07);
	cpu_reg_write32(SCA_SC_UART_CMD_1, 0x00);

	cpu_reg_write32(SCA_SC_INTR_EN_1, 0x20);
	cpu_reg_read32(SCA_SC_INTR_STAT_1); //read clear present intr

	cpu_reg_write32(SCA_SC_UART_CMD_1, 0x01);
	__udelay(100);
	if (cpu_reg_read32(SCA_SC_UART_CMD_1)==0)
		post_log("Reset UART success!\n");

	cpu_reg_write32(SCA_SC_PROTO_CMD, 0x30);//reset tx/rx buf

	while (!card_detected) {
		card_detected = cpu_reg_read32(SCA_SC_STATUS_1) & (1<<SCA_SC_STATUS_1__card_pres);
	}
	post_log("Card present!\n");

	cpu_reg_write32(SCA_SC_IF_CMD_1, 0x06);
	cpu_reg_write32(SCA_SC_IF_CMD_1, 0x04);
	cpu_reg_write32(SCA_SC_UART_CMD_2, 0x00);
	cpu_reg_write32(SCA_SC_INTR_EN_2, 0x42);
	cpu_reg_write32(SCA_SC_UART_CMD_1, 0x60);
	cpu_reg_write32(SCA_SC_IF_CMD_1, 0x06);

    __udelay(10);

 	for(i=0; i<19; i++)
    {
		do{
			rx_status = cpu_reg_read32(SCA_SC_STATUS_2);
			__udelay(100);
			cnt++;
		}while(rx_status&(1<<SCA_SC_STATUS_2__rempty) && cnt<1000);

		if(cnt==1000)
			break;

		rx = cpu_reg_read32(SCA_SC_RECEIVE) & 0xff;
		atr_async[i] = rx;
		post_log("ATR[%d]=0x%02X \n", i, rx);
    }

 	/*atr_sync_byte_count = 0;
 	for(i=0;i<19;i++)
 	{
 		if (atr_async[i] == atr_async0_rcv[i])
 		{
 			if (atr_sync_byte_count == 19) {
 			post_log("ATR SYNC BYTES MATCHING WITH THE STANDARD VALUES \n");
 			pass_flag = 1;
 			}
 		}
 		else
 		{
 		    post_log("ATR SYNC BYTE NOT MATCHING FOR POSITION %d ACTUAL VALUE : %d EXPECTED VALUE : %d \n",i,atr_async[i],atr_async0_rcv);
 		}

 		atr_sync_byte_count++;


 	}*/

 	atr_sync_byte_count = 0;
 			 	for(i=0;i<19;i++)
 			 	{
 			 		if (atr_async[i] == atr_async0_rcv[i])
 			 		{
 			 			atr_sync_byte_count++;
 			 			if (atr_sync_byte_count == 19) {
 			 		    post_log("===========================================================================================\n");
 			 			post_log("ATR SYNC BYTES MATCHING WITH THE STANDARD VALUES FOR ATR ASYNC VERSION 0\n");
 			 			 post_log("===========================================================================================\n");
 			 			pass_flag = 1;
 			 			break;
 			 			}
 			 		}
 			 		else
 			 		{
 			 			 post_log("===========================================================================================\n");
 			 			post_log("ATR ASYNC VERSION 0 SMART CARDS NOT BEING USED - MAY BE ASYNC VERSION 1 in USE\n");
 			 		    post_log("ATR ASYNC BYTE NOT MATCHING FOR POSITION %d ACTUAL VALUE : %02X EXPECTED VALUE : %02X \n",i,atr_async[i],atr_async0_rcv[i]);

 			 		  post_log("THIS IS FOR ASYNC VERSION 0 ...PROCEEDING TO TRY WITH ASYNC VERSION 1 \n");
 			 		 post_log("===========================================================================================\n");
 			 		    break;
 			 		}


 			 	}

 			 	if (pass_flag == 0)
 			 	{
 			 		atr_sync_byte_count = 0;
 			 	for(i=0;i<19;i++)
 			 			 	{

 			 		if (atr_async[i] == atr_async1_rcv[i])
 			 			 		{
 			 			atr_sync_byte_count++;
 			 					 			if (atr_sync_byte_count == 19) {
 			 					 			 post_log("===========================================================================================\n");
 			 					 			post_log("ATR SYNC BYTES MATCHING WITH THE STANDARD VALUES FOR ATR ASYNC VERSION 1\n");
 			 					 			pass_flag = 1;
 			 					 		 post_log("===========================================================================================\n");
 			 					 			break;
 			 					 			}

 			 			 		}
 			 			 		else
 			 			 		{
 			 			 		 post_log("===========================================================================================\n");
 			 			 			post_log("ATR ASYNC VERSION 1 SMART CARDS NOT BEING USED \n");
 			 			 		    post_log("ATR SYNC BYTE NOT MATCHING FOR POSITION %d ACTUAL VALUE : %02X EXPECTED VALUE : %02X \n",i,atr_async[i],atr_async1_rcv[i]);
 			 			 		 post_log("THIS IS FOR ATR ASYNC VERSION 1 \n");
 			 			 		 		 			 		    post_log("BOTH ATR ASYNC SEQUENCES NOT WORKING FINE \n");
 			 			 		 		 			 		 post_log("===========================================================================================\n");
 			 			 		    break;
 			 			 		}


 			 			 	}
 			 	}



	if (pass_flag == 1)
	{
		 post_log("===========================================================================================\n");
	post_log("Sathishs: TEST_PASSED \n");
	post_log("Sathishs: Please Ignore the below PASSED message \n ");
	 post_log("===========================================================================================\n");
	}
	else
	{
		 post_log("===========================================================================================\n");
	post_log("Sathishs : TEST FAILED \n");
	post_log("Sathishs : Please Ignore the below PASSED message \n");
	 post_log("===========================================================================================\n");
	}

	return 0;
}




#endif /* CONFIG_POST & CONFIG_SYS_POST_SCI */
#endif /* CONFIG_CYGNUS*/        
