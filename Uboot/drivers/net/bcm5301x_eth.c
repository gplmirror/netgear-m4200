/******************************************************************************/
/*                                                                            */
/*  Copyright 2011  Broadcom Corporation                                      */
/*                                                                            */
/*     Unless you and Broadcom execute a separate written software license    */
/*     agreement governing  use of this software, this software is licensed   */
/*     to you under the terms of the GNU General Public License version 2     */
/*     (the GPL), available at                                                */
/*                                                                            */
/*          http://www.broadcom.com/licenses/GPLv2.php                        */
/*                                                                            */
/*     with the following added to such license:                              */
/*                                                                            */
/*     As a special exception, the copyright holders of this software give    */
/*     you permission to link this software with independent modules, and to  */
/*     copy and distribute the resulting executable under terms of your       */
/*     choice, provided that you also meet, for each linked independent       */
/*     module, the terms and conditions of the license of that module.        */
/*     An independent module is a module which is not derived from this       */
/*     software.  The special exception does not apply to any modifications   */
/*     of the software.                                                       */
/*     Notwithstanding the above, under no circumstances may you combine      */
/*     this software in any way with any other Broadcom software provided     */
/*     under a license other than the GPL, without Broadcom's express prior   */
/*     written consent.                                                       */
/*                                                                            */
/******************************************************************************/

/* debug/trace */
//#define BCMDBG
#define BCMDBG_ERR
#ifdef BCMDBG
#define	ET_ERROR(args) printf args
#define	ET_TRACE(args) printf args
#define BCM5301X_ETH_DEBUG
#elif defined(BCMDBG_ERR)
#define	ET_ERROR(args) printf args
#define ET_TRACE(args)
#undef BCM5301X_ETH_DEBUG
#else
#define	ET_ERROR(args)
#define	ET_TRACE(args)
#undef BCM5301X_ETH_DEBUG
#endif /* BCMDBG */

#include <common.h>
#include <malloc.h>
#include <net.h>
#include <config.h>
#include <asm/arch/ethHw.h>
#include <asm/arch/ethHw_dma.h>

#define BCM5301X_ETH_DEV_NAME          "bcm53010_eth"

#define BCM_NET_MODULE_DESCRIPTION    "Broadcom BCM5301X Ethernet driver"
#define BCM_NET_MODULE_VERSION        "0.1"

static const char banner[] = BCM_NET_MODULE_DESCRIPTION " " BCM_NET_MODULE_VERSION "\n";

/******************************************************************
 * u-boot net functions
 */
static int
bcm5301x_eth_send(struct eth_device *dev, volatile void *packet, int length)
{
	uint8_t *buf = (uint8_t *) packet;
	int rc;

	ET_TRACE(("%s enter\n", __FUNCTION__));

	/* load buf and start transmit */
	rc = ethHw_dmaTx( length, buf );
	if (rc) {
		ET_ERROR(("ERROR - Tx failed\n"));
		return rc;
	}

	rc = ethHw_dmaTxWait();
	if (rc) {
		ET_ERROR(("ERROR - no Tx notice\n"));
		return rc;
	}

	ET_TRACE(("%s exit rc(0x%x)\n", __FUNCTION__, rc));
	return rc;
}

static int
bcm5301x_eth_rcv(struct eth_device *dev)
{
	int rc;

	ET_TRACE(("%s enter\n", __FUNCTION__));

	rc = ethHw_dmaRx();

	ET_TRACE(("%s exit rc(0x%x)\n", __FUNCTION__, rc));
	return rc;
}


static int
bcm5301x_eth_write_hwaddr(struct eth_device* dev)
{
	int rc=0;

	ET_TRACE(("%s enter\n", __FUNCTION__));

	ET_TRACE(("%s Not going to change MAC address\n", __FUNCTION__));

	ET_TRACE(("%s exit rc(0x%x)\n", __FUNCTION__, rc));
	return rc;
}


static int
bcm5301x_eth_open( struct eth_device *dev, bd_t * bt )
{
	int rc=0;
	ET_TRACE(("%s enter\n", __FUNCTION__));

	/* Enable forwarding to internal port */
	ethHw_macEnableSet(ETHHW_PORT_INT, 1);

	/* enable tx and rx DMA */
	ethHw_dmaEnable(DMA_RX);
	ethHw_dmaEnable(DMA_TX);

	ET_TRACE(("%s exit rc(0x%x)\n", __FUNCTION__, rc));
	return rc;
}


static void
bcm5301x_eth_close(struct eth_device *dev)
{
	ET_TRACE(("%s enter\n", __FUNCTION__));

	/* disable DMA */
	ethHw_dmaDisable(DMA_RX);
	ethHw_dmaDisable(DMA_TX);

	/* Disable forwarding to internal port */
	ethHw_macEnableSet(ETHHW_PORT_INT, 0);

	ET_TRACE(("%s exit\n", __FUNCTION__));
}


int
bcm5301x_eth_register(u8 dev_num)
{
	struct eth_device *dev;
	int rc;

	ET_TRACE(("%s enter\n", __FUNCTION__));

	dev = (struct eth_device *) malloc(sizeof(struct eth_device));
	if (dev == NULL) {
		return -1;
	}
	memset(dev, 0, sizeof(*dev));
	sprintf(dev->name, "%s-%hu", BCM5301X_ETH_DEV_NAME, dev_num);

	printf(banner);

	/* Initialization */
	ET_TRACE(("Ethernet initialization...\n"));
	rc = ethHw_Init();
	ET_TRACE(("Ethernet initialization %s (rc=%i)\n",
			(rc>=0) ? "successful" : "failed", rc));

	dev->iobase = 0;

	dev->init = bcm5301x_eth_open;
	dev->halt = bcm5301x_eth_close;
	dev->send = bcm5301x_eth_send;
	dev->recv = bcm5301x_eth_rcv;
	dev->write_hwaddr = bcm5301x_eth_write_hwaddr;

	eth_register(dev);

	ET_TRACE(("Ethernet Driver registered...\n"));

	ET_TRACE(("%s exit\n", __FUNCTION__));
	return 1;
}
