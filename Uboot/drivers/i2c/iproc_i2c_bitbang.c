/*
 * Copyright (C) 2014, Broadcom Corporation. All Rights Reserved.
 * Dante Su <dantesu@broadcom.com>
 *
 * Unfortunately the PMBus is not compatible with SMBus, so this driver
 * will use only bit-bang registers to provide full function I2C access.
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <common.h>
#include <asm/io.h>
#include <errno.h>

#if !defined(IPROC_SMBUS_BASE_ADDR)
#if defined(CONFIG_NS_PLUS) || defined(CONFIG_HELIX4)
#define IPROC_SMBUS_BASE_ADDR  0x18038000
#elif defined(CONFIG_CYGNUS) || defined(CONFIG_GREYHOUND)
#define IPROC_SMBUS_BASE_ADDR  0x18008000
#else
#define IPROC_SMBUS_BASE_ADDR  0x18009000
#endif
#endif /* !IPROC_SMBUS_BASE_ADDR */

#ifndef IPROC_SMBUS_BASE_LIST
#define IPROC_SMBUS_BASE_LIST  { IPROC_SMBUS_BASE_ADDR }
#endif

#ifndef CONFIG_SYS_MAX_I2C_BUS
#define CONFIG_SYS_MAX_I2C_BUS 1
#endif

struct iproc_i2c_regs {
    uint32_t cfg;     /* configuration register */
    uint32_t tcfg;    /* timing configuration register */
    uint32_t sacr;    /* slave address control register */
    uint32_t mfcr;    /* master fifo control register */

    uint32_t sfcr;    /* slave fifo control register */
    uint32_t bbcr;    /* bit-bang control register */
    uint32_t rsvd[6];

    uint32_t mcr;     /* master command register */
    uint32_t scr;     /* slave command register */
    uint32_t ier;     /* interrupt enable register */
    uint32_t isr;     /* interrupt status register */

    uint32_t mdwr;    /* master data write register */
    uint32_t mdrr;    /* master data read register */
    uint32_t sdwr;    /* slave data write register */
    uint32_t sdrr;    /* slave data read register */
};

/* Configuration register */
#define CFG_RESET               0x80000000
#define CFG_ENABLE              0x40000000
#define CFG_BITBANG             0x20000000

/* Bit-Bang control register */
#define BBCR_SCLIN              0x80000000
#define BBCR_SCLOUT             0x40000000
#define BBCR_SDAIN              0x20000000
#define BBCR_SDAOUT             0x10000000

#define I2C_INIT                do { } while (0)
#define I2C_ACTIVE              do { } while (0)
#define I2C_TRISTATE            do { } while (0)
#define I2C_DELAY               udelay(5) /* 1/4 I2C clock duration */

#ifdef DEBUG_I2C
#define PRINTD(fmt,args...)	do {	\
		printf (fmt ,##args);	\
	} while (0)
#else
#define PRINTD(fmt,args...)
#endif

#if defined(CONFIG_SPL_BUILD) || !defined(CONFIG_I2C_MULTI_BUS)
static const unsigned int i2c_bus = 0; /* .rodata */
#else
static unsigned int i2c_bus = 0;       /* .data */
#endif

static void *iproc_i2c_chip_regs(unsigned int bus)
{
	unsigned long base[] = IPROC_SMBUS_BASE_LIST;

	return (void *)base[bus];
}

static void iproc_i2c_chip_reset(unsigned int bus)
{
	struct iproc_i2c_regs *regs = iproc_i2c_chip_regs(bus);
	unsigned long now;

	/* chip reset */
	writel(CFG_RESET, &regs->cfg);
	for (now = get_timer(0); get_timer(now) < 500; ) {
		if (!(readl(&regs->cfg) & CFG_RESET))
			break;
	}
	if (readl(&regs->cfg) & CFG_RESET) {
		PRINTD("iproc_i2c: reset timed out\n");
	}

	/* chip enable + bit-bang mode */
	writel(CFG_ENABLE | CFG_BITBANG, &regs->cfg);
	udelay(50);
}

static int iproc_i2c_getsda(void)
{
	struct iproc_i2c_regs *regs = iproc_i2c_chip_regs(i2c_bus);

	return (readl(&regs->bbcr) & BBCR_SDAIN) ? 1 : 0;
}
#define I2C_READ  iproc_i2c_getsda()

static void iproc_i2c_setsda(int level)
{
	struct iproc_i2c_regs *regs = iproc_i2c_chip_regs(i2c_bus);

	if (level)
		setbits_le32(&regs->bbcr, BBCR_SDAOUT);
	else
		clrbits_le32(&regs->bbcr, BBCR_SDAOUT);
}
#define I2C_SDA(level)  iproc_i2c_setsda(level)

static void iproc_i2c_setscl(int level)
{
	struct iproc_i2c_regs *regs = iproc_i2c_chip_regs(i2c_bus);

	if (level)
		setbits_le32(&regs->bbcr, BBCR_SCLOUT);
	else
		clrbits_le32(&regs->bbcr, BBCR_SCLOUT);
}
#define I2C_SCL(level)  iproc_i2c_setscl(level)

/*-----------------------------------------------------------------------
 * Local functions
 */
static void  send_reset(void);
static void  send_start(void);
static void  send_stop(void);
static void  send_ack(int);
static int   write_byte(uchar byte);
static uchar read_byte(int);

/*-----------------------------------------------------------------------
 * Send a reset sequence consisting of 9 clocks with the data signal high
 * to clock any confused device back into an idle state.  Also send a
 * <stop> at the end of the sequence for belts & suspenders.
 */
static void send_reset(void)
{
	int j;

	I2C_SCL(1);
	I2C_SDA(1);
#ifdef	I2C_INIT
	I2C_INIT;
#endif
	I2C_TRISTATE;
	for(j = 0; j < 9; j++) {
		I2C_SCL(0);
		I2C_DELAY;
		I2C_DELAY;
		I2C_SCL(1);
		I2C_DELAY;
		I2C_DELAY;
	}
	send_stop();
	I2C_TRISTATE;
}

/*-----------------------------------------------------------------------
 * START: High -> Low on SDA while SCL is High
 */
static void send_start(void)
{
	I2C_DELAY;
	I2C_SDA(1);
	I2C_ACTIVE;
	I2C_DELAY;
	I2C_SCL(1);
	I2C_DELAY;
	I2C_SDA(0);
	I2C_DELAY;
}

/*-----------------------------------------------------------------------
 * STOP: Low -> High on SDA while SCL is High
 */
static void send_stop(void)
{
	I2C_SCL(0);
	I2C_DELAY;
	I2C_SDA(0);
	I2C_ACTIVE;
	I2C_DELAY;
	I2C_SCL(1);
	I2C_DELAY;
	I2C_SDA(1);
	I2C_DELAY;
	I2C_TRISTATE;
}

/*-----------------------------------------------------------------------
 * ack should be I2C_ACK or I2C_NOACK
 */
static void send_ack(int ack)
{
	I2C_SCL(0);
	I2C_DELAY;
	I2C_ACTIVE;
	I2C_SDA(ack);
	I2C_DELAY;
	I2C_SCL(1);
	I2C_DELAY;
	I2C_DELAY;
	I2C_SCL(0);
	I2C_DELAY;
}

/*-----------------------------------------------------------------------
 * Send 8 bits and look for an acknowledgement.
 */
static int write_byte(uchar data)
{
	int j;
	int nack;

	I2C_ACTIVE;
	for(j = 0; j < 8; j++) {
		I2C_SCL(0);
		I2C_DELAY;
		I2C_SDA(data & 0x80);
		I2C_DELAY;
		I2C_SCL(1);
		I2C_DELAY;
		I2C_DELAY;

		data <<= 1;
	}

	/*
	 * Look for an <ACK>(negative logic) and return it.
	 */
	I2C_SCL(0);
	I2C_DELAY;
	I2C_SDA(1);
	I2C_TRISTATE;
	I2C_DELAY;
	I2C_SCL(1);
	I2C_DELAY;
	I2C_DELAY;
	nack = I2C_READ;
	I2C_SCL(0);
	I2C_DELAY;
	I2C_ACTIVE;

	return(nack);	/* not a nack is an ack */
}

/*-----------------------------------------------------------------------
 * if ack == I2C_ACK, ACK the byte so can continue reading, else
 * send I2C_NOACK to end the read.
 */
static uchar read_byte(int ack)
{
	int  data;
	int  j;

	/*
	 * Read 8 bits, MSB first.
	 */
	I2C_TRISTATE;
	I2C_SDA(1);
	data = 0;
	for(j = 0; j < 8; j++) {
		I2C_SCL(0);
		I2C_DELAY;
		I2C_SCL(1);
		I2C_DELAY;
		data <<= 1;
		data |= I2C_READ;
		I2C_DELAY;
	}
	send_ack(ack);

	return(data);
}

/*=====================================================================*/
/*                         Public Functions                            */
/*=====================================================================*/

#ifdef CONFIG_I2C_MULTI_BUS

/**
 * i2c_set_bus_num - change active I2C bus
 *	@bus: bus index, zero based
 *	@returns: 0 on success, non-0 on failure
 */
int i2c_set_bus_num(unsigned int bus)
{
#ifdef CONFIG_SPL_BUILD
	if (bus)
		return -1;
#else
	if (bus >= CONFIG_SYS_MAX_I2C_BUS)
		return -1;
	i2c_bus = bus;
#endif
	send_reset();
	return 0;
}

/**
 * i2c_get_bus_num - returns index of active I2C bus
 */
unsigned int i2c_get_bus_num(void)
{
	return i2c_bus;
}
#endif

void i2c_init(int speed, int slaveaddr)
{
	int i;

	for (i = 0; i < CONFIG_SYS_MAX_I2C_BUS; ++i)
		iproc_i2c_chip_reset(i);

	send_reset();
}

/*
 * i2c_read: - Read multiple bytes from an i2c device
 *
 * The higher level routines take into account that this function is only
 * called with len < page length of the device (see configuration file)
 *
 * @chip:   address of the chip which is to be read
 * @addr:   i2c data address within the chip
 * @alen:   length of the i2c data address (1..2 bytes)
 * @buf:    where to write the data
 * @len:    how much byte do we want to read
 * @return: 0 in case of success
 */
int i2c_read(u8 chip, u32 addr, int alen, u8 *buf, int len)
{
	int shift;

	PRINTD("i2c_read: chip %02X addr %02X alen %d buffer %p len %d\n",
		chip, addr, alen, buf, len);

#ifdef CONFIG_SYS_I2C_EEPROM_ADDR_OVERFLOW
	/*
	 * EEPROM chips that implement "address overflow" are ones
	 * like Catalyst 24WC04/08/16 which has 9/10/11 bits of
	 * address and the extra bits end up in the "chip address"
	 * bit slots. This makes a 24WC08 (1Kbyte) chip look like
	 * four 256 byte chips.
	 *
	 * Note that we consider the length of the address field to
	 * still be one byte because the extra address bits are
	 * hidden in the chip address.
	 */
	chip |= ((addr >> (alen * 8)) & CONFIG_SYS_I2C_EEPROM_ADDR_OVERFLOW);

	PRINTD("i2c_read: fix addr_overflow: chip %02X addr %02X\n",
		chip, addr);
#endif

	/*
	 * Do the addressing portion of a write cycle to set the
	 * chip's address pointer.  If the address length is zero,
	 * don't do the normal write cycle to set the address pointer,
	 * there is no address pointer in this chip.
	 */
	send_start();
	if (alen > 0) {
		if (write_byte(chip << 1)) {	/* write cycle */
			send_stop();
			PRINTD("i2c_read, no chip responded %02X\n", chip);
			return -1;
		}
		shift = (alen - 1) * 8;
		while (alen-- > 0) {
			if (write_byte(addr >> shift)) {
				PRINTD("i2c_read, address not <ACK>ed\n");
				return -1;
			}
			shift -= 8;
		}

		/* Some I2C chips need a stop/start sequence here,
		 * other chips don't work with a full stop and need
		 * only a start.  Default behaviour is to send the
		 * stop/start sequence.
		 */
#ifdef CONFIG_SOFT_I2C_READ_REPEATED_START
		send_start();
#else
		send_stop();
		send_start();
#endif
	}
	/*
	 * Send the chip address again, this time for a read cycle.
	 * Then read the data.  On the last byte, we do a NACK instead
	 * of an ACK(len == 0) to terminate the read.
	 */
	write_byte((chip << 1) | 1);	/* read cycle */
	while (len-- > 0)
		*buf++ = read_byte(len == 0);
	send_stop();

	return 0;
}

/*
 * i2c_write: -  Write multiple bytes to an i2c device
 *
 * The higher level routines take into account that this function is only
 * called with len < page length of the device (see configuration file)
 *
 * @chip:   address of the chip which is to be written
 * @addr:   i2c data address within the chip
 * @alen:   length of the i2c data address (1..2 bytes)
 * @buf:    where to find the data to be written
 * @len:    how much byte do we want to read
 * @return: 0 in case of success
 */
int i2c_write(u8 chip, u32 addr, int alen, u8 *buf, int len)
{
	int shift, failures = 0;

	PRINTD("i2c_write: chip %02X addr %02X alen %d buffer %p len %d\n",
		chip, addr, alen, buf, len);

	send_start();
	if (write_byte(chip << 1)) {	/* write cycle */
		send_stop();
		PRINTD("i2c_write, no chip responded %02X\n", chip);
		return -1;
	}
	shift = (alen - 1) * 8;
	while (alen-- > 0) {
		if (write_byte(addr >> shift)) {
			PRINTD("i2c_write, address not <ACK>ed\n");
			return -1;
		}
		shift -= 8;
	}

	while (len-- > 0) {
		if (write_byte(*buf++))
			failures++;
	}
	send_stop();

	return -failures;
}

/*
 * i2c_probe: - Test if a chip answers for a given i2c address
 *
 * @chip:   address of the chip which is searched for
 * @return: 0 if a chip was found, -1 otherwhise
 */
int i2c_probe(u8 chip)
{
	int rc;

	/*
	 * perform 1 byte write transaction with just address byte
	 * (fake write)
	 */
	send_start();
	rc = write_byte((chip << 1) | 0);
	send_stop();

	return (rc ? -1 : 0);
}
