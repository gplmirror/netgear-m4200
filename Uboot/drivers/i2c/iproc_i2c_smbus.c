/*
 * Copyright (C) 2014, Broadcom Corporation. All Rights Reserved.
 * Dante Su <dantesu@broadcom.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <common.h>
#include <asm/io.h>
#include <errno.h>

#if !defined(IPROC_SMBUS_BASE_ADDR)
#if defined(CONFIG_NS_PLUS) || defined(CONFIG_HELIX4)
#define IPROC_SMBUS_BASE_ADDR  0x18038000
#elif defined(CONFIG_CYGNUS) || defined(CONFIG_GREYHOUND)
#define IPROC_SMBUS_BASE_ADDR  0x18008000
#else
#define IPROC_SMBUS_BASE_ADDR  0x18009000
#endif
#endif /* !IPROC_SMBUS_BASE_ADDR */

#ifndef IPROC_SMBUS_BASE_LIST
#define IPROC_SMBUS_BASE_LIST  { IPROC_SMBUS_BASE_ADDR }
#endif

#ifndef CONFIG_SYS_MAX_I2C_BUS
#define CONFIG_SYS_MAX_I2C_BUS 1
#endif

struct iproc_i2c_regs {
    uint32_t cfg;     /* configuration register */
    uint32_t tcfg;    /* timing configuration register */
    uint32_t sacr;    /* slave address control register */
    uint32_t mfcr;    /* master fifo control register */

    uint32_t sfcr;    /* slave fifo control register */
    uint32_t bbcr;    /* bit-bang control register */
    uint32_t rsvd[6];

    uint32_t mcr;     /* master command register */
    uint32_t scr;     /* slave command register */
    uint32_t ier;     /* interrupt enable register */
    uint32_t isr;     /* interrupt status register */

    uint32_t mdwr;    /* master data write register */
    uint32_t mdrr;    /* master data read register */
    uint32_t sdwr;    /* slave data write register */
    uint32_t sdrr;    /* slave data read register */
};

/* Configuration register */
#define CFG_RESET               0x80000000
#define CFG_ENABLE              0x40000000
#define CFG_BITBANG             0x20000000

/* Timing configuration register */
#define TCFG_400KHZ             0x80000000
#define TCFG_IDLE(x)            (((x) & 0xff) << 8)

/* Master fifo control register */
#define MFCR_RXCLR              0x80000000
#define MFCR_TXCLR              0x40000000
#define MFCR_RXPKT(x)           (((x) >> 16) & 0x7f)
#define MFCR_RXTHR(x)           (((x) & 0x3f) << 8)

/* Bit-Bang control register */
#define BBCR_SCLIN              0x80000000
#define BBCR_SCLOUT             0x40000000
#define BBCR_SDAIN              0x20000000
#define BBCR_SDAOUT             0x10000000

/* Master command register */
#define MCR_START               0x80000000 /* Send START (busy) */
#define MCR_ABORT               0x40000000
#define MCR_STATUS(x)           (((x) >> 25) & 7)
#define MCR_PROT(x)             (((x) & 0x0f) << 9) /* SMBus protocol */
#define MCR_PEC                 0x00000100 /* Packet Error Check */
#define MCR_RXLEN(x)            ((x) & 0xff)

/* Master command status */
#define MCR_STATUS_SUCCESS      (0 << 25)
#define MCR_STATUS_LOST_ARB     (1 << 25)
#define MCR_STATUS_NACK_FB      (2 << 25) /* NACK on the 1st byte */
#define MCR_STATUS_NACK_NFB     (3 << 25) /* NACK on the non-1st byte */
#define MCR_STATUS_TIMEOUT      (4 << 25)
#define MCR_STATUS_TX_TLOW_MEXT (5 << 25)
#define MCR_STATUS_RX_TLOW_MEXT (6 << 25)

/* Master SMBus protocol type */
#define MCR_PROT_QUICK_CMD               0
#define MCR_PROT_SEND_BYTE               1
#define MCR_PROT_RECV_BYTE               2
#define MCR_PROT_WR_BYTE                 3
#define MCR_PROT_RD_BYTE                 4
#define MCR_PROT_WR_WORD                 5
#define MCR_PROT_RD_WORD                 6
#define MCR_PROT_BLK_WR                  7
#define MCR_PROT_BLK_RD                  8
#define MCR_PROT_PROC_CALL               9
#define MCR_PROT_BLK_WR_BLK_RD_PROC_CALL 10

/* Tx/Rx data control */
#define MDWR_END      0x80000000 /* end of transfer */
#define MDRR_END      0x80000000 /* end of transfer */
#define MDRR_NACK     0x40000000

#if defined(CONFIG_SPL_BUILD) || !defined(CONFIG_I2C_MULTI_BUS)
static const unsigned int i2c_bus = 0; /* .rodata */
#else
static unsigned int i2c_bus = 0;       /* .data */
#endif

static void *iproc_i2c_chip_regs(unsigned int bus)
{
	unsigned long base[] = IPROC_SMBUS_BASE_LIST;

	return (void *)base[bus];
}

static void iproc_i2c_chip_reset(struct iproc_i2c_regs *regs)
{
	unsigned long now, val;

	/* chip reset */
	writel(CFG_RESET, &regs->cfg);
	for (now = get_timer(0); get_timer(now) < 500; ) {
		if (!(readl(&regs->cfg) & CFG_RESET))
			break;
	}
	if (readl(&regs->cfg) & CFG_RESET) {
		printf("iproc_i2c: reset timed out\n");
	}

	/* chip clock speed = 100K */
	val = readl(&regs->tcfg) & (~TCFG_400KHZ);
	writel(val, &regs->tcfg);

	/* chip enable */
	writel(CFG_ENABLE, &regs->cfg);

	/* 50 usec delay as per HW spec. */
	udelay(50);
}

static int iproc_i2c_bus_reset(struct iproc_i2c_regs *regs)
{
	int ret, i;

	/* enable bit-bang */
	setbits_le32(&regs->cfg, CFG_BITBANG);
	udelay(60);

	/* set SDA=H, SCL=H */
	setbits_le32(&regs->bbcr, BBCR_SCLOUT | BBCR_SDAOUT);
	udelay(5);

	/* generate 9 dummy cycles (8-bits data + 1-bit ACK/NACK) */
	for (i = 0; i < 18; ++i) {
		if (i & 1)
			setbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: high */
		else
			clrbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: low */
		udelay(5);
	}

	/* generate a STOP signal */
	clrbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: low */
	udelay(2);
	clrbits_le32(&regs->bbcr, BBCR_SDAOUT); /* SDA: low */
	udelay(3);
	setbits_le32(&regs->bbcr, BBCR_SCLOUT); /* SCL: high */
	udelay(2);
	setbits_le32(&regs->bbcr, BBCR_SDAOUT); /* SDA: high */
	udelay(5);

	/* disable bit-bang control */
	clrbits_le32(&regs->cfg, CFG_BITBANG);
	udelay(60);

	/* Now SDA should be at level high */
	if (readl(&regs->bbcr) & BBCR_SDAIN) {
		ret = 0;
	} else {
		ret = -1;
		printf("iproc_i2c: bus reset failed\n");
	}

	return ret;
}

static int iproc_i2c_poll(struct iproc_i2c_regs *regs)
{
	int ret = 0;
	uint32_t mcr = readl(&regs->mcr);

	if (mcr & MCR_START) {
		ret = -EAGAIN;
	} else if (MCR_STATUS(mcr) != MCR_STATUS_SUCCESS) {
		writel(0, &regs->mcr);
		ret = -EREMOTEIO;
	}

	return ret;
}

static int iproc_i2c_wait(struct iproc_i2c_regs *regs)
{
	int ret = -ETIMEDOUT;
	unsigned long now;

	for (now = get_timer(0); get_timer(now) < 500; ) {
		ret = iproc_i2c_poll(regs);
		if (ret != -EAGAIN)
			break;
	}

	if (ret) {
		/* only activate bus reset upon SDA low */
		if (!(readl(&regs->bbcr) & BBCR_SDAIN))
			iproc_i2c_bus_reset(regs);
		/* only activate chip reset upon BUSY/START=1 */
		if (readl(&regs->mcr) & MCR_START)
			iproc_i2c_chip_reset(regs);
	}

	return ret;
}

static int iproc_i2c_xfer(struct iproc_i2c_regs *regs,
		uint16_t addr, int prot, uint8_t cmd, uint8_t *buf)
{
	int ret = 0;
	int len = 0;
	int retry = 10;

	addr = addr << 1; /* BIT0 is R/W */

xfer_retry:

	/* fifo clear */
	writel(MFCR_TXCLR | MFCR_RXCLR, &regs->mfcr);
	while (readl(&regs->mfcr) & (MFCR_TXCLR | MFCR_RXCLR))
		;

	switch (prot) {
	case MCR_PROT_QUICK_CMD:
		retry = 0;
		writel(addr | MDWR_END, &regs->mdwr);
		break;

	case MCR_PROT_WR_BYTE:
		writel(addr, &regs->mdwr);
		writel(cmd, &regs->mdwr);
		writel(buf[0] | MDWR_END, &regs->mdwr);
		break;

	case MCR_PROT_RD_BYTE:
		writel(addr, &regs->mdwr);
		writel(cmd, &regs->mdwr);
		writel(addr | 1, &regs->mdwr);
		len = 1;
		break;

	case MCR_PROT_WR_WORD:
		writel(addr, &regs->mdwr);
		writel(cmd, &regs->mdwr);
		writel(buf[0], &regs->mdwr);
		writel(buf[1] | MDWR_END, &regs->mdwr);
		break;

	case MCR_PROT_RD_WORD:
		writel(addr, &regs->mdwr);
		writel(cmd, &regs->mdwr);
		writel(addr | 1, &regs->mdwr);
		len = 2;
		break;

	default:
		printf("iproc_i2c: protocol %d is not yet implemented\n", prot);
		return -1;
	}

	writel(MCR_START | MCR_PROT(prot) | MCR_RXLEN(len), &regs->mcr);
	ret = iproc_i2c_wait(regs);

	if (ret) {
		if (retry--)
			goto xfer_retry;

		if (prot == MCR_PROT_QUICK_CMD)
			return -1;

		switch (ret) {
		case -EAGAIN:
			printf("iproc_i2c: timed out\n");
			break;
		case -EREMOTEIO:
			printf("iproc_i2c: bus error\n");
			break;
		default:
			break;
		}
		return -1;
	}

	while (len-- > 0) {
		*buf = (uint8_t)(readl(&regs->mdrr) & 0xff);
		++buf;
	}

	return ret;
}

#ifdef CONFIG_I2C_MULTI_BUS

/**
 * i2c_set_bus_num - change active I2C bus
 *	@bus: bus index, zero based
 *	@returns: 0 on success, non-0 on failure
 */
int i2c_set_bus_num(unsigned int bus)
{
#ifdef CONFIG_SPL_BUILD
	if (bus)
		return -1;
#else
	if (bus >= CONFIG_SYS_MAX_I2C_BUS)
		return -1;
	i2c_bus = bus;
#endif
	return 0;
}

/**
 * i2c_get_bus_num - returns index of active I2C bus
 */
unsigned int i2c_get_bus_num(void)
{
	return i2c_bus;
}
#endif

void i2c_init(int speed, int slaveaddr)
{
	int i;

	for (i = 0; i < CONFIG_SYS_MAX_I2C_BUS; ++i)
		iproc_i2c_chip_reset(iproc_i2c_chip_regs(i));
}

/*
 * i2c_read: - Read multiple bytes from an i2c device
 *
 * The higher level routines take into account that this function is only
 * called with len < page length of the device (see configuration file)
 *
 * @chip:   address of the chip which is to be read
 * @addr:   i2c data address within the chip
 * @alen:   length of the i2c data address (1..2 bytes)
 * @buf:    where to write the data
 * @len:    how much byte do we want to read
 * @return: 0 in case of success
 */
int i2c_read(u8 chip, u32 addr, int alen, u8 *buf, int len)
{
	struct iproc_i2c_regs *regs = iproc_i2c_chip_regs(i2c_bus);
	int idx, ret = -1;

	if (alen != 1) {
		printf("iproc_i2c: bad alen=%d\n", alen);
		return ret;
	}

	for (idx = 0; idx < len; ++idx) {
		ret = iproc_i2c_xfer(regs, chip, MCR_PROT_RD_BYTE,
				addr + idx, buf + idx);
		if (ret)
			break;
	}
	return ret;
}

/*
 * i2c_write: -  Write multiple bytes to an i2c device
 *
 * The higher level routines take into account that this function is only
 * called with len < page length of the device (see configuration file)
 *
 * @chip:   address of the chip which is to be written
 * @addr:   i2c data address within the chip
 * @alen:   length of the i2c data address (1..2 bytes)
 * @buf:    where to find the data to be written
 * @len:    how much byte do we want to read
 * @return: 0 in case of success
 */
int i2c_write(u8 chip, u32 addr, int alen, u8 *buf, int len)
{
	struct iproc_i2c_regs *regs = iproc_i2c_chip_regs(i2c_bus);
	int idx, ret = -1;

	if (alen != 1) {
		printf("iproc_i2c: bad alen=%d\n", alen);
		return ret;
	}

	for (idx = 0; idx < len; ++idx) {
		ret = iproc_i2c_xfer(regs, chip, MCR_PROT_WR_BYTE,
				addr + idx, buf + idx);
		if (ret)
			break;
	}
	return ret;
}

/*
 * i2c_probe: - Test if a chip answers for a given i2c address
 *
 * @chip:   address of the chip which is searched for
 * @return: 0 if a chip was found, -1 otherwhise
 */
int i2c_probe(u8 chip)
{
	struct iproc_i2c_regs *regs = iproc_i2c_chip_regs(i2c_bus);

	return iproc_i2c_xfer(regs, chip, MCR_PROT_QUICK_CMD, 0, NULL);
}
