/*
 * Interface to SPI flash
 *
 * Copyright (C) 2008 Atmel Corporation
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */
#ifndef _SPI_FLASH_H_
#define _SPI_FLASH_H_

#include <spi.h>
#include <linux/types.h>
#include <linux/compiler.h>

struct spi_flash {
	struct spi_slave *spi;

	const char	*name;

	/* Total flash size */
	u32		size;
	/* Write (page) size */
	u32		page_size;
	/* Erase (sector) size */
	u32		sector_size;

	int		(*read)(struct spi_flash *flash, u32 offset,
				size_t len, void *buf);
	int		(*write)(struct spi_flash *flash, u32 offset,
				size_t len, const void *buf);
#ifdef CONFIG_SPI_FLASH_SPANSION
	int		(*quad_write)(struct spi_flash *flash, u32 offset,
				size_t len, const void *buf);
#endif
	int		(*erase)(struct spi_flash *flash, u32 offset,
				size_t len);
#ifdef CONFIG_SPI_FLASH_SPANSION
  int 	(*otp_read)(struct spi_flash *flash, u32 offset,
				size_t len, void *buf);
	int		(*otp_write)(struct spi_flash *flash, u32 offset,
				size_t len, const void *buf);
	int		(*otp_lock)(struct spi_flash *flash, u32 offset,
				size_t len);
	int   (*set_config_reg)(struct spi_flash *flash, u8 conf);
	int   (*set_status_reg)(struct spi_flash *flash, u8 stat);
	int   (*get_config_reg)(struct spi_flash *flash, const void *buf);
	int   (*get_status_reg)(struct spi_flash *flash, const void *buf);
	int   (*set_bank_reg)(struct spi_flash *flash, u8 bar, u8 claim_bus);
	int		(*dual_read)(struct spi_flash *flash, u32 offset,
				size_t len, void *buf);
	int		(*quad_read)(struct spi_flash *flash, u32 offset,
				size_t len, void *buf);
	int		(*dual_hp_read)(struct spi_flash *flash, u32 offset,
				size_t len, void *buf);
	int		(*quad_hp_read)(struct spi_flash *flash, u32 offset,
				size_t len, void *buf);
#endif
};

struct spi_flash *spi_flash_probe(unsigned int bus, unsigned int cs,
		unsigned int max_hz, unsigned int spi_mode);
void spi_flash_free(struct spi_flash *flash);

static inline int spi_flash_read(struct spi_flash *flash, u32 offset,
		size_t len, void *buf)
{
	return flash->read(flash, offset, len, buf);
}

static inline int spi_flash_write(struct spi_flash *flash, u32 offset,
		size_t len, const void *buf)
{
	return flash->write(flash, offset, len, buf);
}

#ifdef CONFIG_SPI_FLASH_SPANSION
static inline int spi_flash_quad_write(struct spi_flash *flash, u32 offset,
		size_t len, const void *buf)
{
	return flash->quad_write(flash, offset, len, buf);
}
#endif
static inline int spi_flash_erase(struct spi_flash *flash, u32 offset,
		size_t len)
{
	return flash->erase(flash, offset, len);
}

#ifdef CONFIG_SPI_FLASH_SPANSION
  static inline int spi_flash_otp_read(struct spi_flash *flash, u32 offset,
				size_t len, void *buf)
	{
	  return flash->otp_read(flash, offset, len, buf);
  }
  static inline int	spi_flash_otp_write(struct spi_flash *flash, u32 offset,
				size_t len, const void *buf)
	{
    return flash->otp_write(flash, offset, len, buf);
  }
  static inline int	spi_flash_otp_lock(struct spi_flash *flash, u32 offset,
				size_t len)
	{
    return flash->otp_lock(flash, offset, len);
  }

  static inline int	spi_flash_set_config_reg(struct spi_flash *flash, u8 conf)
	{
    return flash->set_config_reg(flash, conf);
  }

  static inline int	spi_flash_set_status_reg(struct spi_flash *flash, u8 stat)
	{
    return flash->set_status_reg(flash, stat);
  }

  static inline int     spi_flash_set_bank_reg(struct spi_flash *flash, u8 bar)
        {
    return flash->set_bank_reg(flash, bar, 1);
  }

  static inline int	spi_flash_get_config_reg(struct spi_flash *flash, const void *buf)
	{
    return flash->get_config_reg(flash, buf);
  }

  static inline int	spi_flash_get_status_reg(struct spi_flash *flash, const void *buf)
	{
    return flash->get_status_reg(flash, buf);
  }

  static inline int spi_flash_dual_read(struct spi_flash *flash, u32 offset,
  		size_t len, void *buf)
  {
  	return flash->dual_read(flash, offset, len, buf);
  }

  static inline int spi_flash_quad_read(struct spi_flash *flash, u32 offset,
  		size_t len, void *buf)
  {
  	return flash->quad_read(flash, offset, len, buf);
  }
  
  static inline int spi_flash_dual_hp_read(struct spi_flash *flash, u32 offset,
  		size_t len, void *buf)
  {
  	return flash->dual_hp_read(flash, offset, len, buf);
  }
  
  static inline int spi_flash_quad_hp_read(struct spi_flash *flash, u32 offset,
  		size_t len, void *buf)
  {
  	return flash->quad_hp_read(flash, offset, len, buf);
  }
#endif
void spi_boot(void) __noreturn;

#endif /* _SPI_FLASH_H_ */
