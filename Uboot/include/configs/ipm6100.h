#ifndef __IPM6100_H
#define __IPM6100_H

/* new uImage format support */
#define CONFIG_FIT    1
#define CONFIG_OF_LIBFDT  1
#define CONFIG_FIT_VERBOSE  1 /* enable fit_format_{error,warning}() */

#define CONFIG_BOARD_LATE_INIT
#define CONFIG_BOARD_EARLY_INIT_F (1)
#define CONFIG_PHYS_SDRAM_1_SIZE			0x40000000 /* 1GB */
#define RANGER_SVK_SDRAM_SIZE	0x20000000 /* 512MB */
#define IPROC_ETH_MALLOC_BASE 0xD00000

/* Architecture, CPU, etc */
#define CONFIG_ARMV7
#define CONFIG_IPROC (1)
#define CONFIG_HELIX4 (1)
#define CONFIG_IPM6100 (1)
/* SPL Features */
#ifdef CONFIG_IPROC_SPL
#define CONFIG_IPROC_NAND_SPL_MAX_SIZE 4096

#define CONFIG_SPL
#define CONFIG_SPL_FRAMEWORK
#define CONFIG_SPL_TEXT_BASE            0x1b020000
#define CONFIG_SPL_MAX_SIZE             126976       /* 1 Sector Size - NAND_SPL Max Size = (128KB - 4KB) */
#define CONFIG_SPL_STACK                CONFIG_SYS_INIT_SP_ADDR

/*
 * SPL resides in sram
 */
#define CONFIG_SPL_BSS_START_ADDR       0x1b071000
#define CONFIG_SPL_BSS_MAX_SIZE         0xEFFF       /* 0x1b071000 - 0x1b07FFFF */
#define CONFIG_SYS_SPL_MALLOC_START     0x64100000
#define CONFIG_SYS_SPL_MALLOC_SIZE      0x100000        /* 1 MB */


#define CONFIG_SPL_LIBCOMMON_SUPPORT

#ifdef CONFIG_NAND_IPROC_BOOT
#define CONFIG_IPROC_NAND_SPL_SYS_TEXT_BASE 0x1C000000
#define CONFIG_SPL_NAND_SUPPORT
#define CONFIG_SYS_NAND_U_BOOT_OFFS     0x20000
#define CONFIG_SYS_NAND_PAGE_SIZE       2048
#define CONFIG_SYS_NAND_BLOCK_SIZE      0x20000
#define CONFIG_SYS_NAND_PAGE_COUNT      (CONFIG_SYS_NAND_BLOCK_SIZE / CONFIG_SYS_NAND_PAGE_SIZE)
#define CONFIG_SPL_UBOOT_START          (0x1C000000 + CONFIG_IPROC_NAND_SPL_MAX_SIZE) /* First 4K has NAND_SPL */
#else
#define CONFIG_IPROC_NAND_SPL_SYS_TEXT_BASE 0x1E000000
#define CONFIG_SPL_SPI_SUPPORT
#define CONFIG_SPL_SPI_FLASH_SUPPORT
#define CONFIG_SPL_SPI_LOAD
#define CONFIG_SPL_SPI_BUS 0
#define CONFIG_SPL_SPI_CS 0
#define CONFIG_SYS_SPI_U_BOOT_OFFS       0x20000
#define CONFIG_SYS_SPI_U_BOOT_SIZE       0xD0000
#define CONFIG_SPL_UBOOT_START           (0x1E000000 + CONFIG_IPROC_NAND_SPL_MAX_SIZE)
#endif /* CONFIG_NAND_IPROC_BOOT */

#define CONFIG_SPL_UBOOT_END            (CONFIG_SPL_UBOOT_START + CONFIG_SPL_MAX_SIZE)

#define CONFIG_SPL_LIBGENERIC_SUPPORT
#define CONFIG_SPL_SERIAL_SUPPORT
#define CONFIG_SPL_LDSCRIPT "$(CPUDIR)/iproc/u-boot-spl.lds"
#endif /* CONFIG_IPROC_SPL */

/*
Fix me

When DEBUG is enabled, need to disable both CACHE to make u-boot running
#define CONFIG_SYS_NO_ICACHE
#define CONFIG_SYS_NO_DCACHE
#define DEBUG

*/
#define CONFIG_IPROC_MMU	(1)
#define CONFIG_L2_OFF				/* Disable L2 cache */
#define CONFIG_SYS_ARM_CACHE_WRITETHROUGH (1)
#define CONFIG_MACH_TYPE			0xbb8

#define CONFIG_MISC_INIT_R			/* Call board's misc_init_r function */

#define CONFIG_IPROC_FORCE_HWECCLEVEL4 /* Override Strapping */

/* Interrupt configuration */
#define CONFIG_USE_IRQ          1	/* we need IRQ stuff for timer	*/

/* Memory Info */
#if (defined(CONFIG_L2C_AS_RAM ) && defined(CONFIG_NO_CODE_RELOC))
#define CONFIG_SYS_MALLOC_LEN 			0x8000  	/* see armv7/start.S. */
#else
#define CONFIG_SYS_MALLOC_LEN 			0x2000000  	/* see armv7/start.S. */
#endif
#define CONFIG_STACKSIZE				(0x10000) //64K
#define CONFIG_STACKSIZE_IRQ			(4096)
#define CONFIG_STACKSIZE_FIQ			(4096)

#define CONFIG_PHYS_SDRAM_0				0x1b000000  /* SRAM */
#define CONFIG_L2_CACHE_SIZE			0x80000  
#define CONFIG_PHYS_SDRAM_1				0x60000000
#define CONFIG_LOADADDR					0x70000000 /* default destination location for tftp file (tftpboot cmd) */
#define CONFIG_PHYS_SDRAM_RSVD_SIZE		0x0 /* bytes reserved from CONFIG_PHYS_SDRAM_1 for custom use */

/* Where kernel is loaded to in memory */
#define CONFIG_SYS_LOAD_ADDR				0x70000000
#define LINUX_BOOT_PARAM_ADDR				0x60200000 /* default mapped location to store the atags pointer */

#define CONFIG_SYS_MEMTEST_START			CONFIG_PHYS_SDRAM_1
#define CONFIG_SYS_MEMTEST_END			(CONFIG_PHYS_SDRAM_1+CONFIG_PHYS_SDRAM_1_SIZE)
#define CONFIG_NR_DRAM_BANKS				1

#define CONFIG_SYS_SDRAM_BASE		(CONFIG_PHYS_SDRAM_1 + CONFIG_PHYS_SDRAM_RSVD_SIZE)
/* CONFIG_SYS_TEXT_BASE is where u-boot is loaded by boot1 */
#define CONFIG_SYS_STACK_SIZE		(0x00010000) /* 64K */      
#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_PHYS_SDRAM_0 + CONFIG_SYS_STACK_SIZE - 16)

/* Clocks */
#define CONFIG_SYS_REF_CLK			(25000000) /*Reference clock = 25MHz */
#define CONFIG_SYS_REF2_CLK			(200000000) /*Reference clock = 25MHz */
#define IPROC_ARM_CLK		(1000000000) /* 1GHz */
#define IPROC_AXI_CLK		(500000000)  /* 500 MHz */
#define IPROC_APB_CLK		(125000000)  /* 125 MHz */

#define CONFIG_ENV_OVERWRITE	/* Allow serial# and ethernet mac address to be overwritten in nv storage */
#define CONFIG_ENV_SIZE			0x10000 /* 0xA0000-0xAFFFF */
#define CONFIG_ENV_RANGE                0x80000

/* NO flash */
#define CONFIG_SYS_NO_FLASH		/* Not using NAND/NOR unmanaged flash */

/* Ethernet configuration */
#define CONFIG_BCMIPROC_ETH
#define CONFIG_NET_MULTI
#define CONFIG_CMD_MII
#define CONFIG_ETHADDR          04:05:06:07:08:89

/* DMA configuration */
/*#define CONFIG_BCM5301X_DMA */

/* General U-Boot configuration */
#define CONFIG_SYS_CBSIZE			1024	/* Console buffer size */
#define CONFIG_SYS_PBSIZE			(CONFIG_SYS_CBSIZE +	sizeof(CONFIG_SYS_PROMPT) + 16) /* Printbuffer size */
#define CONFIG_SYS_MAXARGS			64
#define CONFIG_SYS_BARGSIZE		CONFIG_SYS_CBSIZE

#define CONFIG_VERSION_VARIABLE	/* Enabled UBOOT build date/time id string */
#define CONFIG_AUTO_COMPLETE
#define CONFIG_SYS_HUSH_PARSER
#define CONFIG_SYS_PROMPT_HUSH_PS2	"> "
#define CONFIG_CMDLINE_EDITING
#define CONFIG_SYS_LONGHELP

#define CONFIG_CRC32_VERIFY		/* Add crc32 to memory verify commands */
#define CONFIG_MX_CYCLIC			/* Memory display cyclic */

#define CONFIG_CMDLINE_TAG				/* ATAG_CMDLINE setup */
#define CONFIG_SETUP_MEMORY_TAGS		/* ATAG_MEM setup */


/*#include <config_cmd_default.h>*/
#include "iproc_common_configs.h"
#define CONFIG_ENV_IS_NOWHERE
#undef CONFIG_CMD_NFS
#undef CONFIG_GENERIC_MMC
/*#define CONFIG_CMD_MISC*/
#define CONFIG_NETMASK		255.255.255.0
#define CONFIG_IPADDR		10.130.87.66
#define CONFIG_GATEWAYIP	10.130.87.1
#define CONFIG_SERVERIP		10.130.135.120

#define CONFIG_GMAC_NUM		0 

#define CONFIG_SYS_HZ       1000

/* Enable generic u-boot SPI flash drivers and commands */
#define CONFIG_CMD_SF
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_STMICRO_NS
#define CONFIG_SPI_FLASH_MACRONIX_NS
#define CONFIG_SPI_FLASH_SPANSION
#define CONFIG_SPI_FLASH_SST
#define CONFIG_SPI_FLASH_WINBOND
#define CONFIG_SPI_FLASH_ATMEL

/* SPI flash configurations */
#define CONFIG_IPROC_QSPI
#define CONFIG_IPROC_QSPI_BUS                   0
#define CONFIG_IPROC_QSPI_CS                    0

/* SPI flash configuration - flash specific */
#define CONFIG_IPROC_BSPI_DATA_LANES            1
#define CONFIG_IPROC_BSPI_ADDR_LANES            1
#define CONFIG_IPROC_BSPI_READ_CMD              0x0b
#define CONFIG_IPROC_BSPI_READ_DUMMY_CYCLES     8
#define CONFIG_SF_DEFAULT_SPEED                 50000000
#define CONFIG_SF_DEFAULT_MODE                  SPI_MODE_3

/* Environment variables */
#undef CONFIG_ENV_IS_NOWHERE
#ifdef CONFIG_NAND_IPROC_BOOT
#define CONFIG_ENV_IS_IN_NAND                   1
#define CONFIG_ENV_OFFSET                       0x100000
#define CONFIG_ENV_RANGE                        0x80000
#else
#ifdef CONFIG_SPI_FLASH
#define CONFIG_ENV_IS_IN_SPI_FLASH              1
#define CONFIG_ENV_OFFSET                       0x100000
#define CONFIG_ENV_SPI_MAX_HZ                   10000000
#define CONFIG_ENV_SPI_MODE                     SPI_MODE_3
#define CONFIG_ENV_SPI_BUS                      CONFIG_IPROC_QSPI_BUS
#define CONFIG_ENV_SPI_CS                       CONFIG_IPROC_QSPI_CS
#define CONFIG_ENV_SECT_SIZE                    0x10000     /* 64KB */
#endif /* CONFIG_SPI_FLASH */
#endif /* CONFIG_NAND_BOOT */

/* Environment variables for NAND flash */
#define CONFIG_CMD_NAND 
#define CONFIG_IPROC_NAND 
#define CONFIG_SYS_MAX_NAND_DEVICE			1
#define CONFIG_SYS_NAND_BASE		        0xdeadbeef
#define CONFIG_SYS_NAND_ONFI_DETECTION

#define CONFIG_YAFFS2
#define CONFIG_YAFFS2_INBAND_TAGS

#define CONFIG_INITRD_TAG        1       /*  send initrd params           */
#define CONFIG_CMD_MTDPARTS
#define CONFIG_MTD_DEVICE

#ifdef CONFIG_RUN_DDR_SHMOO2
/* Shmoo reuse: skip Shmoo process by reusing values saved in flash. */
#define CONFIG_SHMOO_REUSE
/* Define it for 32bit DDR */
#define CONFIG_SHMOO_REUSE_DDR_32BIT            1
/* Offset of spi flash to save Shmoo values */
#define CONFIG_SHMOO_REUSE_QSPI_OFFSET          0x000E0000
/* Offset of NAND flash to save Shmoo values */
#define CONFIG_SHMOO_REUSE_NAND_OFFSET          0x001c0000
/* Range for the partition to support NAND bad blocks replacement */
#define CONFIG_SHMOO_REUSE_NAND_RANGE           0x00040000
/* Delay to wait for the magic character to force Shmoo; 0 to disable delay */
#define CONFIG_SHMOO_REUSE_DELAY_MSECS          50
/* Length of memory test after restored; 0 to disable memory test */
#define CONFIG_SHMOO_REUSE_MEMTEST_LENGTH       (0x40000)
/* Starting address of memory test after restored */
#define CONFIG_SHMOO_REUSE_MEMTEST_START        IPROC_DDR_MEM_BASE2
#endif /* CONFIG_RUN_DDR_SHMOO2 */

#undef  CONFIG_BOOTDELAY
#define CONFIG_BOOTDELAY			1	/* User can hit a key to abort kernel boot and stay in uboot cmdline */

#define LVL7_FIXUP 1
#ifdef LVL7_FIXUP
#include <asm/sizes.h>

#define CONFIG_SILENT_CONSOLE
#undef  CONFIG_ENV_OFFSET
#define CONFIG_ENV_OFFSET                       0x100000

#undef  CONFIG_ENV_SIZE
#define CONFIG_ENV_SIZE                         0x10000 /* 64K */

#undef  CONFIG_SHMOO_REUSE_QSPI_OFFSET
#define CONFIG_SHMOO_REUSE_QSPI_OFFSET          0x140000/*  256k */

#define CONFIG_CMD_UBI
#define CONFIG_CMD_UBIFS
#define CONFIG_UBI_NO_MSGS
#define CONFIG_RBTREE
#define CONFIG_LZO
#define CONFIG_MTD_DEVICE
#define CONFIG_MTD_PARTITIONS
#define CONFIG_CMD_MTDPARTS

#undef CONFIG_SYS_MALLOC_LEN
#define CONFIG_SYS_MALLOC_LEN                   SZ_32M

#undef CONFIG_BOOTFILE
#define CONFIG_BOOTFILE      "image1"

#undef MTDIDS_DEFAULT
#define MTDIDS_DEFAULT      "nand0=nand_iproc.0"

#undef MTDPARTS_DEFAULT
#define MTDPARTS_DEFAULT    "mtdparts=nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs)"

#undef CONFIG_BOOTCOMMAND
#define CONFIG_BOOTCOMMAND      "run bootargs_set;run fs_mount;run boot_active;run boot_image1;run boot_image2"

/* below bootargs_set variable will overwrite the bootargs */
#undef CONFIG_BOOTARGS
#define CONFIG_BOOTARGS "console=ttyS0,115200n8 maxcpus=2 mem=1024M mtdparts=spi1.0:1024k(u-boot),64k(env),256k(shmoo);nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs) ubi.mtd=fs config=helix loglevel=3 envaddr=0x1E100000"

#define CONFIG_VERSION_VARIABLE /* Enabled UBOOT build date/time id string */
#define CONFIG_LVL7_VERSION_UPDATE /* Ensure UBOOT build date/time is updated */
#define CONFIG_IDENT_STRING "VerNo=1.0.0.9"

#define CONFIG_EXTRA_ENV_SETTINGS      \
  "active=image1\0" \
  "backup=image2\0" \
  "brd_type=helix\0" \
  "loglevelval=3\0"     \
  "fs_mount=ubi part nand0,4 0x0;ubifsmount fs\0" \
  "erase_env=sf probe 0;sf erase 0x100000 0x10000;reset\0"   \
  "boot_image1=setenv active image1;saveenv;ubifsload ${loadaddr} /image1;bootm ${bootaddr}#helix\0"   \
  "boot_image2=setenv active image2;saveenv;ubifsload ${loadaddr} /image2;bootm ${bootaddr}#helix\0"   \
  "boot_active=ubifsload ${loadaddr} /${active};bootm ${bootaddr}#helix\0" \
  "mtdparts=mtdparts=nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs)\0" \
  "bootargs_set=setenv bootargs console=ttyS0,${baudrate}n8 maxcpus=2 mem=1024M ubi.mtd=fs config=${brd_type} loglevel=${loglevelval} envaddr=0x1E100000 mtdparts=spi1.0:1024k(u-boot),64k(env),256k(shmoo)\\\\;nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs)\0" \
  "loadaddr=0x70000000\0" \
  "bootaddr=0x70000074\0" 


/* SPL Features */
#ifdef CONFIG_IPROC_SPL
#define CONFIG_IPROC_NAND_SPL_MAX_SIZE 4096

#define CONFIG_SPL
#define CONFIG_SPL_FRAMEWORK
#define CONFIG_SPL_TEXT_BASE            0x1b020000
#define CONFIG_SPL_MAX_SIZE             126976       /* 1 Sector Size - NAND_SPL Max Size = (128KB - 4KB) */
#define CONFIG_SPL_STACK                CONFIG_SYS_INIT_SP_ADDR

/*
 * SPL resides in sram
 */
#define CONFIG_SPL_BSS_START_ADDR       0x1b071000
#define CONFIG_SPL_BSS_MAX_SIZE           0xEFFF       /* 0x1b071000 - 0x1b07FFFF */
#define CONFIG_SYS_SPL_MALLOC_START     0x64100000
#define CONFIG_SYS_SPL_MALLOC_SIZE        0x100000        /* 1 MB */


#define CONFIG_SPL_LIBCOMMON_SUPPORT

#ifdef CONFIG_NAND_IPROC_BOOT
#define CONFIG_IPROC_NAND_SPL_SYS_TEXT_BASE 0x1C000000
#define CONFIG_SPL_NAND_SUPPORT
#define CONFIG_SPL_NAND_SIMPLE
#define CONFIG_SYS_NAND_U_BOOT_OFFS     0x20000
#define CONFIG_SYS_NAND_PAGE_SIZE       2048
#define CONFIG_SPL_NAND_SIMPLE_BBT       

#define CONFIG_SPL_UBOOT_START        (0x1C000000 + CONFIG_IPROC_NAND_SPL_MAX_SIZE) /* First 4K has NAND_SPL */
#else
#define CONFIG_IPROC_NAND_SPL_SYS_TEXT_BASE 0x1E000000
#define CONFIG_SPL_SPI_SUPPORT
#define CONFIG_SPL_SPI_FLASH_SUPPORT
#define CONFIG_SPL_SPI_LOAD
#define CONFIG_SPL_SPI_BUS 0
#define CONFIG_SPL_SPI_CS 0
#define CONFIG_SYS_SPI_U_BOOT_OFFS    0x20000
#define CONFIG_SYS_SPI_U_BOOT_SIZE    0xD0000
#define CONFIG_SPL_UBOOT_START        (0x1E000000 + CONFIG_IPROC_NAND_SPL_MAX_SIZE)
#endif /* CONFIG_NAND_IPROC_BOOT */

#define CONFIG_SPL_UBOOT_END            (CONFIG_SPL_UBOOT_START + CONFIG_SPL_MAX_SIZE)

#define CONFIG_SPL_LIBGENERIC_SUPPORT
#define CONFIG_SPL_SERIAL_SUPPORT
#define CONFIG_SPL_LDSCRIPT "$(CPUDIR)/iproc/u-boot-spl.lds"
#endif /* CONFIG_IPROC_SPL */
#endif /* LVL7_FIXUP */
/* I2C */
#define CONFIG_CMD_I2C
#define CONFIG_IPROC_I2C
#define CONFIG_SYS_I2C_SPEED    0    	/* Default on 100KHz */
#define CONFIG_SYS_I2C_SLAVE	0xff	/* No slave address */

#endif /* __IPM6100_H */
