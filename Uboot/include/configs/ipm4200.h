#ifndef __IPM4200_H
#define __IPM4200_H

#include <asm/sizes.h>

#define BOARD_LATE_INIT
#define CONFIG_BOARD_EARLY_INIT_F           1

#define CONFIG_USE_BOARD_RESET              1     /* Enable board specific reset API */

/* Architecture, CPU, etc */
#define CONFIG_ARMV7
#define CONFIG_IPROC                        0
#define CONFIG_IPROC_P7                     1
#define CONFIG_GREYHOUND                    1
#define CONFIG_IPM4200                      1

#define CONFIG_IPROC_MMU                    1
#define CONFIG_SYS_ARM_CACHE_WRITETHROUGH   1
#define CONFIG_MACH_TYPE                    4735
#define CONFIG_BOARD_LATE_INIT              1

/* SRAM configuration */
#ifdef CONFIG_L2C_AS_RAM
#define CONFIG_IPROC_SRAM_BASE              (0x50000000)
#define CONFIG_IPROC_SRAM_SIZE              (0x20000)
#else /* !CONFIG_L2C_AS_RAM */
#define CONFIG_IPROC_SRAM_BASE              (0x02000000)
#define CONFIG_IPROC_SRAM_SIZE              (0x8000)
#endif /* !CONFIG_L2C_AS_RAM */

/* Memory Info */
#ifdef CONFIG_REF_BOARD
#define CONFIG_DDR_ROW_BITS                 13
#define CONFIG_DDR_COL_BITS                 10
#define CONFIG_DDR_BANK_BITS                3
#else /* !CONFIG_REF_BOARD: SVK board */
#define CONFIG_DDR_ROW_BITS                 15
#define CONFIG_DDR_COL_BITS                 10
#define CONFIG_DDR_BANK_BITS                3
#endif /* !CONFIG_REF_BOARD */
#define CONFIG_DDR_TOTAL_COLS               ((1 << CONFIG_DDR_ROW_BITS) * (1 << CONFIG_DDR_COL_BITS) * (1 << CONFIG_DDR_BANK_BITS))
#ifdef CONFIG_DDR32
#define CONFIG_DDR_BYTES_PER_COL            4
#else
#define CONFIG_DDR_BYTES_PER_COL            2
#endif
#define CONFIG_SYS_MALLOC_LEN               SZ_8M
#define CONFIG_PHYS_SDRAM_0                 CONFIG_IPROC_SRAM_BASE
#define CONFIG_PHYS_SDRAM_1                 0x60000000
#define CONFIG_PHYS_SDRAM_1_SIZE            (CONFIG_DDR_TOTAL_COLS * CONFIG_DDR_BYTES_PER_COL)
#define CONFIG_PHYS_SDRAM_RSVD_SIZE         0x0 /* bytes reserved from CONFIG_PHYS_SDRAM_1 for custom use */
#define CONFIG_SYS_STACK_SIZE               0x00002000
#define CONFIG_SYS_INIT_SP_ADDR             (CONFIG_PHYS_SDRAM_0 + CONFIG_SYS_STACK_SIZE - 16)

#define CONFIG_SYS_MEMTEST_START            CONFIG_PHYS_SDRAM_1
#define CONFIG_SYS_MEMTEST_END              (CONFIG_PHYS_SDRAM_1 + CONFIG_PHYS_SDRAM_1_SIZE)
#define CONFIG_NR_DRAM_BANKS                1
#define CONFIG_SYS_SDRAM_BASE               (CONFIG_PHYS_SDRAM_1 + CONFIG_PHYS_SDRAM_RSVD_SIZE)
#define CONFIG_SYS_LOAD_ADDR                0x68000000
#define CONFIG_LOADADDR                     CONFIG_SYS_LOAD_ADDR /* default destination location for tftp file (tftpboot cmd) */
#define LINUX_BOOT_PARAM_ADDR               0x60200000

/* Clocks */
#define CONFIG_SYS_REF_CLK                  (25000000)
#define CONFIG_SYS_REF2_CLK                 (200000000)
#define IPROC_ARM_CLK                       (1250000000)
#define IPROC_AXI_CLK                       (400000000)
#define IPROC_APB_CLK                       (100000000)

/* Serial Info */
#define CONFIG_BAUDRATE                     115200
#define CONFIG_SYS_NS16550
#define CONFIG_SYS_NS16550_SERIAL
#define CONFIG_CONS_INDEX                   1
#define CONFIG_SYS_NS16550_COM1             (0x18020000)
#define CONFIG_SYS_NS16550_REG_SIZE         (-4)    /* Post pad 3 bytes after each reg addr */
#define CONFIG_SYS_NS16550_CLK              iproc_get_uart_clk(0)
#define CONFIG_SYS_BAUDRATE_TABLE           {9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600} 

/* No flash by default */
#define CONFIG_SYS_NO_FLASH                 /* Not using NAND/NOR unmanaged flash */

/* Enable generic u-boot SPI flash drivers and commands */
#define CONFIG_CMD_SF
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_STMICRO_NS
#define CONFIG_SPI_FLASH_MACRONIX_NS
#define CONFIG_SPI_FLASH_SPANSION
#define CONFIG_SPI_FLASH_SST
#define CONFIG_SPI_FLASH_WINBOND
#define CONFIG_SPI_FLASH_ATMEL

/* SPI flash configurations */
#define CONFIG_IPROC_QSPI
#define CONFIG_IPROC_QSPI_BUS               0
#define CONFIG_IPROC_QSPI_CS                0

/* SPI flash configuration - flash specific */
#define CONFIG_IPROC_BSPI_DATA_LANES        1
#define CONFIG_IPROC_BSPI_ADDR_LANES        1
#define CONFIG_IPROC_BSPI_READ_CMD          0x0b
#define CONFIG_IPROC_BSPI_READ_DUMMY_CYCLES 8
#define CONFIG_SF_DEFAULT_SPEED             50000000
#define CONFIG_SF_DEFAULT_MODE              SPI_MODE_3

/* NAND flash configurations */
#define CONFIG_IPROC_NAND
#define CONFIG_SYS_MAX_NAND_DEVICE          1
#define CONFIG_SYS_NAND_BASE                0xdeadbeef
#define CONFIG_SYS_NAND_ONFI_DETECTION
#define CONFIG_CMD_NAND

/* Environment variables */
#ifdef CONFIG_NAND_IPROC_BOOT
#define CONFIG_ENV_IS_IN_NAND               1
#define CONFIG_ENV_OFFSET                   0x100000
#define CONFIG_ENV_RANGE                    SZ_512K
#elif defined(CONFIG_NOR_IPROC_BOOT)
#undef CONFIG_SYS_NO_FLASH
#define CONFIG_ENV_IS_IN_FLASH
#define CONFIG_ENV_OFFSET                   0x100000
#define CONFIG_ENV_ADDR                     (CONFIG_SYS_FLASH_BASE + 0xc0000)
#define CONFIG_ENV_SECT_SIZE                0x20000 /* size of one complete sector */
#elif defined(CONFIG_SPI_FLASH)
#define CONFIG_ENV_IS_IN_SPI_FLASH          1
#define CONFIG_ENV_OFFSET                   0x100000
#define CONFIG_ENV_SPI_MAX_HZ               10000000
#define CONFIG_ENV_SPI_MODE                 SPI_MODE_3
#define CONFIG_ENV_SPI_BUS                  CONFIG_IPROC_QSPI_BUS
#define CONFIG_ENV_SPI_CS                   CONFIG_IPROC_QSPI_CS
#define CONFIG_ENV_SECT_SIZE                0x10000     /* 64KB */
#else /* No flash defined */
#define CONFIG_ENV_IS_NOWHERE
#endif

#define CONFIG_ENV_SIZE                     0x10000     /* 64KB */

#ifndef CONFIG_ENV_IS_NOWHERE
#define CONFIG_CMD_SAVEENV 
#endif

/* Ethernet configuration */
#define CONFIG_CMD_NET
#define CONFIG_CMD_PING
#define CONFIG_CMD_DHCP
#define CONFIG_BCMIPROC_ETH
#define CONFIG_ENV_OVERWRITE /* For overwriting 'ethaddr' */

/* AMAC configuration */
#define CONFIG_GMAC_NUM                     0
#define IPROC_ETH_MALLOC_BASE               0x60d00000
#define CONFIG_EXTERNAL_PHY_BUS_ID          0x02
#define CONFIG_EXTERNAL_PHY_DEV_ID          0x00

/* u-boot rich features */
#define CONFIG_CMD_RUN
#define CONFIG_CMD_BOOT
#define CONFIG_CMD_LICENSE
#define CONFIG_CMD_ECHO
#define CONFIG_CMD_ITEST
#define CONFIG_CMD_MISC
#define CONFIG_CMD_SOURCE
#define CONFIG_SYS_HUSH_PARSER
#define CONFIG_MISC_INIT_R /* Call board's misc_init_r function */

/* 
 * SHMOO and28 needs about 64K storage for calibration process 
 * The buffer will be released after trace calibration is done
 * and final config saved into flash 
 * For Greyhound, we use the second BTCM as this buffer
 */
#define CONFIG_SHMOO_SRAM_BUF               0x01080000

/* SHMOO values reuse */
#define CONFIG_SHMOO_AND28_REUSE            1
/* Memory test address to verify stored SHMOO values */
#define CONFIG_SHMOO_REUSE_MEMTEST_START    0x61000000
/* Memory test address to verify stored SHMOO values */
#define CONFIG_SHMOO_REUSE_MEMTEST_LENGTH   0x1000
/* Offset of SPI flash to save Shmoo values */
#define CONFIG_SHMOO_REUSE_QSPI_OFFSET      0x00140000
/* Offset of NOR flash to save Shmoo values */
#define CONFIG_SHMOO_REUSE_NOR_OFFSET       0x00140000
/* Offset of NAND flash to save Shmoo values */
#define CONFIG_SHMOO_REUSE_NAND_OFFSET      0x00140000
/* Range for the partition to support NAND bad blocks replacement */
#define CONFIG_SHMOO_REUSE_NAND_RANGE       SZ_256K
/* Delay to wait for the magic character to force Shmoo; 0 to disable delay */
#define CONFIG_SHMOO_REUSE_DELAY_MSECS      50

/* General U-Boot configuration */
#define CONFIG_SYS_HZ                  1000
#define CONFIG_SYS_CBSIZE              1024    /* Console buffer size */
#define CONFIG_SYS_PBSIZE              (CONFIG_SYS_CBSIZE + sizeof(CONFIG_SYS_PROMPT) + 16) /* Printbuffer size */
#define CONFIG_SYS_MAXARGS             16
#define CONFIG_SYS_BARGSIZE            CONFIG_SYS_CBSIZE
#define CONFIG_SYS_PROMPT              "u-boot> "
#define CONFIG_CMD_CONSOLE
#define CONFIG_CMD_MEMORY
#define CONFIG_CMDLINE_EDITING
#define CONFIG_AUTO_COMPLETE
#define CONFIG_SYS_LONGHELP

#define CONFIG_VERSION_VARIABLE     /* Enabled UBOOT build date/time id string */
#define CONFIG_LVL7_VERSION_UPDATE  /* Ensure UBOOT build date/time is updated */
#define CONFIG_IDENT_STRING         "VerNo=1.0.0.6"

/* For booting Linux */
#define CONFIG_INITRD_TAG              /* ATAG_INITRD2 setup */
#define CONFIG_CMDLINE_TAG             /* ATAG_CMDLINE setup */
#define CONFIG_SETUP_MEMORY_TAGS       /* ATAG_MEM setup */

/* I2C */
#define CONFIG_IPROC_I2C
#define CONFIG_CMD_I2C
#define CONFIG_SYS_I2C_SPEED           0     /* Default on 100KHz */
#define CONFIG_SYS_I2C_SLAVE           0xff  /* No slave address */
#define IPROC_SMBUS_BASE_ADDR          0x18008000

/* UBIFS */
#define CONFIG_CMD_UBI
#define CONFIG_CMD_UBIFS
#define CONFIG_UBI_NO_MSGS
#define CONFIG_RBTREE
#define CONFIG_LZO
#define CONFIG_MTD_DEVICE
#define CONFIG_MTD_PARTITIONS
#define CONFIG_CMD_MTDPARTS

#define CONFIG_NETMASK                 255.255.255.0
#define CONFIG_IPADDR                  10.130.184.31
#define CONFIG_GATEWAYIP               10.130.184.1
#define CONFIG_SERVERIP                10.130.135.120

#define MTDIDS_DEFAULT                 "nand0=nand_iproc.0"
#undef  MTDPARTS_DEFAULT
#undef  CONFIG_BOOTARGS
#define CONFIG_EXTRA_ENV_SETTINGS      \
  "boardname=m4200\0"                  \
  "ethact=bcmiproc_eth-0\0"            \
  "et0phyaddr=0\0"                     \
  "loadaddr=0x68000000\0"              \
  "bootaddr=0x68000074\0"              \
  "dummyaddr=0x72000000\0"             \
  "envaddr=0xF0100000\0"               \
  "uboot=rakoduri/m4200/u-boot.bin\0"  \
  "tftpblocksize=512\0" \
  "nuke_env=sf probe;sf erase 0x100000 +0x40000\0" \
  "nuke_ddr=sf probe;sf erase 0x140000 +0x40000\0" \
  "factory_reset=sf probe;sf erase 0x100000 +0x80000;reset\0" \
  "spiparts=spi1.0:1024k(uboot),256k(uboot-env),256k(shmoo),512k(vpd)\0" \
  "nandparts=nand_iproc.0:16m(diags),-(fs)\0" \
  "active=image1\0" \
  "backup=image2\0" \
  "boot_image1=ubifsload ${loadaddr} /image1;bootm ${bootaddr}\0" \
  "boot_image2=ubifsload ${loadaddr} /image2;bootm ${bootaddr}\0" \
  "ubifscfg=setenv mtdparts \"mtdparts=${nandparts}\"; " \
           "ubi part nand0,1 0; " \
           "ubifsmount fs\0" \
  "loglevel=4\0" \
  "memsize=1008M\0" \
  "fpboot=setenv bootargs \"console=ttyS0,${baudrate}n8 maxcpus=1 mem=${memsize} envaddr=${envaddr} root=/dev/ram mtdparts=${spiparts};${nandparts} ubi.mtd=fs loglevel=${loglevel} \"\0" \
  "flash_uboot=tftpboot $loadaddr $uboot; sf probe 0; sf erase 0x0 0x100000; sf write $loadaddr 0x0 $filesize; sf read $dummyaddr 0x0 $filesize; cmp.b $loadaddr $dummyaddr $filesize\0" \
  "mfgdiags=echo running diags to program MAC address; " \
           "nand read ${loadaddr} 0x00000000 0x1000000; " \
           "bootm ${loadaddr}; "       \
           "setenv active image1\0"

#define CONFIG_BOOTDELAY               3
#define CONFIG_BOOTCOMMAND                              \
  "run fpboot ubifscfg; "                               \
  "setenv i1 image1; "                                  \
  "setenv i2 image2; "                                  \
  "setenv mfg mfg; "                                    \
  "if test $active = $i1; then "                        \
    "run boot_image1;"                                  \
    "setenv active image2; saveenv; run boot_image2;"   \
  "else; "                                              \
    "if test $active = $i2; then "                      \
      "run boot_image2;"                                \
      "setenv active image1; saveenv; run boot_image1;" \
    "fi; "                                              \
  "fi; "                                                \
  "if test $active = $mfg; then "                       \
    "run mfgdiags;"                                     \
  "fi; "

#define CONFIG_SILENT_CONSOLE                   1

#endif /* __IPM4200_H */
