#ifndef __M4300_R2__
#define __M4300_R2__

#include <asm/sizes.h>

/* FIT and uImage format support */
#define CONFIG_FIT                          1
//#define CONFIG_OF_LIBFDT                    1
//#define CONFIG_FIT_VERBOSE                  1 /* enable fit_format_{error,warning}() */

/* U-boot init definitions */
#define BOARD_LATE_INIT
#define CONFIG_BOARD_LATE_INIT             (1)
#define CONFIG_BOARD_EARLY_INIT_F          (1)
#define CONFIG_MISC_INIT_R                 /* Call board's misc_init_r function */

/* Architecture, CPU, etc */
#define CONFIG_ARMV7
#define CONFIG_IPROC                       (0)
#define CONFIG_IPROC_P7                    (1)
#define CONFIG_GREYHOUND                   (1)
#define CONFIG_M4300_R2                    (1)

#define CONFIG_IPROC_MMU                    1
//#define CONFIG_L2_OFF                       /* Disable L2 cache */
#define CONFIG_SYS_ARM_CACHE_WRITETHROUGH   1
#define CONFIG_MACH_TYPE                    0xb064

/* Interrupt configuration */
#ifdef CONFIG_STDK_BUILD
#define STDK_BUILD
#define CONFIG_USE_IRQ                      1
#define CONFIG_STACKSIZE_IRQ                (1024)
#define CONFIG_STACKSIZE_FIQ                (1024)
#endif

/* SRAM configuration */
#ifdef CONFIG_L2C_AS_RAM
#define CONFIG_IPROC_SRAM_BASE              (0x50000000)
#define CONFIG_IPROC_SRAM_SIZE              (0x20000)
#else /* !CONFIG_L2C_AS_RAM */
#define CONFIG_IPROC_SRAM_BASE              (0x02000000)
#define CONFIG_IPROC_SRAM_SIZE              (0x8000)
#endif /* !CONFIG_L2C_AS_RAM */

/* Memory Info */
#if (defined(CONFIG_DDR32) && defined(CONFIG_IPROC_DDR_ECC))
#error "M4300 Ranger2 doesn't support 32-bit DDR with ECC."
#endif

#define CONFIG_DDR_ROW_BITS                 15
#define CONFIG_DDR_COL_BITS                 10
#define CONFIG_DDR_BANK_BITS                3
#define CONFIG_DDR_TOTAL_COLS               ((1 << CONFIG_DDR_ROW_BITS) * (1 << CONFIG_DDR_COL_BITS) * (1 << CONFIG_DDR_BANK_BITS))

#ifdef CONFIG_DDR32
#define CONFIG_DDR_BYTES_PER_COL            4
#else
#define CONFIG_DDR_BYTES_PER_COL            2
#endif

/* Memory Info */
#define CONFIG_PHYS_SDRAM_0                 CONFIG_IPROC_SRAM_BASE
#define CONFIG_L2_CACHE_SIZE                0x40000
#define CONFIG_PHYS_SDRAM_1                 0x60000000
//#define CONFIG_PHYS_SDRAM_1_SIZE            (CONFIG_DDR_TOTAL_COLS * CONFIG_DDR_BYTES_PER_COL)
#define CONFIG_PHYS_SDRAM_1_SIZE            0x40000000 /* 1GB */

#define CONFIG_PHYS_SDRAM_RSVD_SIZE         0x0 /* bytes reserved from CONFIG_PHYS_SDRAM_1 for custom use */

#if (defined(CONFIG_L2C_AS_RAM ) && defined(CONFIG_NO_CODE_RELOC))
#define CONFIG_SYS_MALLOC_LEN               0x8000          /* see armv7/start.S. */
#else
//#define CONFIG_SYS_MALLOC_LEN               0x40000
#define CONFIG_SYS_MALLOC_LEN               0x2000000       /* see armv7/start.S. */
#endif

#define CONFIG_SYS_STACK_SIZE               0x00002000
#define CONFIG_SYS_INIT_SP_ADDR             (CONFIG_PHYS_SDRAM_0 + CONFIG_SYS_STACK_SIZE - 16)

#define CONFIG_STACKSIZE                    (0x10000) //64K
#define CONFIG_STACKSIZE_IRQ                (4096)
#define CONFIG_STACKSIZE_FIQ                (4096)

#define CONFIG_SYS_MEMTEST_START            CONFIG_PHYS_SDRAM_1
#define CONFIG_SYS_MEMTEST_END              (CONFIG_PHYS_SDRAM_1 + CONFIG_PHYS_SDRAM_1_SIZE)
#define CONFIG_NR_DRAM_BANKS                1
#define CONFIG_SYS_SDRAM_BASE               (CONFIG_PHYS_SDRAM_1 + CONFIG_PHYS_SDRAM_RSVD_SIZE)

/* Where kernel is loaded to in memory */
#define CONFIG_SYS_LOAD_ADDR                0x62000000
#define CONFIG_LOADADDR                     CONFIG_SYS_LOAD_ADDR /* default destination location for tftp file (tftpboot cmd) */
#define LINUX_BOOT_PARAM_ADDR               0x60200000

/* Clocks */
#define CONFIG_SYS_REF_CLK                  (25000000)     /* Reference clock (XTAL) = 25MHz   */
#define CONFIG_SYS_REF2_CLK                 (200000000)    /* Reference clock        = 25MHz   */
#define IPROC_ARM_CLK                       (1250000000)   /* Core clock             = 1.25GHz */
#define IPROC_AXI_CLK                       (400000000)    /* AXI clock              = 400MHz  */
#define IPROC_APB_CLK                       (100000000)    /* APB clock              = 100MHz  */

/* Serial Info */
#define CONFIG_SYS_NS16550
#define CONFIG_SYS_NS16550_SERIAL
#define CONFIG_CONS_INDEX                   1
#define CONFIG_SYS_NS16550_COM1             (0x18020000)
#define CONFIG_SYS_NS16550_REG_SIZE         (-4)    /* Post pad 3 bytes after each reg addr */
#define CONFIG_SYS_NS16550_CLK              iproc_get_uart_clk(0)
#define CONFIG_BAUDRATE                     115200
#define CONFIG_SYS_BAUDRATE_TABLE           {9600, 19200, 38400, 57600, 115200, 230400, 460800, 921600} 

/* No flash by default */
#define CONFIG_SYS_NO_FLASH                 /* Not using NAND/NOR unmanaged flash */

#define CONFIG_ENV_IS_NOWHERE
#undef CONFIG_CMD_NFS
#undef CONFIG_GENERIC_MMC

/* I2C */
#define CONFIG_CMD_I2C
#define CONFIG_IPROC_I2C
//#define CONFIG_HARD_I2C
#define CONFIG_SYS_I2C_SLAVE	0xff	/* No slave address */
#define CONFIG_SYS_I2C_SPEED    0    	/* Default on 100KHz */

/* Enable generic u-boot SPI flash drivers and commands */
#define CONFIG_CMD_SF
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_STMICRO_NS
#define CONFIG_SPI_FLASH_MACRONIX_NS
#define CONFIG_SPI_FLASH_SPANSION
#define CONFIG_SPI_FLASH_SST
#define CONFIG_SPI_FLASH_WINBOND
#define CONFIG_SPI_FLASH_ATMEL

/* SPI flash configurations */
#define CONFIG_IPROC_QSPI
#define CONFIG_IPROC_QSPI_BUS                   0
#define CONFIG_IPROC_QSPI_CS                    0

/* SPI flash configuration - flash specific */
#define CONFIG_IPROC_BSPI_DATA_LANES            1
#define CONFIG_IPROC_BSPI_ADDR_LANES            1
#define CONFIG_IPROC_BSPI_READ_CMD              0x0b
#define CONFIG_IPROC_BSPI_READ_DUMMY_CYCLES     8
#define CONFIG_SF_DEFAULT_SPEED                 50000000
#define CONFIG_SF_DEFAULT_MODE                  SPI_MODE_3

/* Environment variables for NAND flash */
#define CONFIG_CMD_NAND
#define CONFIG_IPROC_NAND
#define CONFIG_SYS_MAX_NAND_DEVICE                      1
#define CONFIG_SYS_NAND_BASE                    0xdeadbeef
#define CONFIG_SYS_NAND_ONFI_DETECTION

/** SPI Flash Map -- Pre-Alpha board layout from DNI
 *  768k - u-boot.bin (boot)
 *  256k - u-boot env (env)
 *  10M  - kernel     (sys)
 *  21M  - file system(fs)
 */

/* Environment variables */
#undef CONFIG_ENV_IS_NOWHERE
#ifdef CONFIG_NAND_IPROC_BOOT

#define CONFIG_ENV_IS_IN_NAND                   1
#define CONFIG_ENV_OFFSET                       0x200000
#define CONFIG_ENV_RANGE                        0x200000

#elif defined(CONFIG_SPI_FLASH)

#define CONFIG_ENV_IS_IN_SPI_FLASH              1
#define CONFIG_ENV_OFFSET                       0x100000   /* FastPath needs this to be 1MB */
#define CONFIG_ENV_SPI_MAX_HZ                   10000000
#define CONFIG_ENV_SPI_MODE                     SPI_MODE_3
#define CONFIG_ENV_SPI_BUS                      CONFIG_IPROC_QSPI_BUS
#define CONFIG_ENV_SPI_CS                       CONFIG_IPROC_QSPI_CS
#define CONFIG_ENV_SECT_SIZE                    0x10000     /* 64KB */

#endif

#define CONFIG_ENV_OVERWRITE            	/* Allow serial# and ethernet mac address to be overwritten in nv storage */
#define CONFIG_ENV_SIZE                 	0x20000  /* 128kB for now. FastPath can override it to 64KB */
#define CONFIG_ENV_RANGE                	0x80000


#ifndef CONFIG_ENV_IS_NOWHERE
#define CONFIG_CMD_SAVEENV 
#endif

/* Ethernet configuration */
#define CONFIG_CMD_NET
#define CONFIG_CMD_PING
#define CONFIG_CMD_DHCP
#define CONFIG_BCMIPROC_ETH
#define CONFIG_NET_MULTI
#define CONFIG_CMD_MII

/* AMAC configuration */
#define CONFIG_GMAC_NUM                 0
#define IPROC_ETH_MALLOC_BASE           0x60d00000
#define CONFIG_EXTERNAL_PHY_BUS_ID      0x2
#define CONFIG_EXTERNAL_PHY_DEV_ID      0x0

#define CONFIG_VERSION_VARIABLE         /* Enabled UBOOT build date/time id string */
#define CONFIG_SYS_HUSH_PARSER
#define CONFIG_SYS_PROMPT_HUSH_PS2      "> "
#define CONFIG_CMDLINE_EDITING
#define CONFIG_AUTO_COMPLETE
#define CONFIG_SYS_LONGHELP

#define CONFIG_CRC32_VERIFY             /* Add crc32 to memory verify commands */
#define CONFIG_MX_CYCLIC                /* Memory display cyclic */

/* u-boot rich features */
#undef CONFIG_CMD_NFS
#undef CONFIG_GENERIC_MMC

#define CONFIG_CMD_RUN
#define CONFIG_CMD_BOOT
#define CONFIG_CMD_LICENSE
#define CONFIG_CMD_ECHO
#define CONFIG_CMD_ITEST
#define CONFIG_CMD_MISC
#define CONFIG_CMD_SOURCE
#define CONFIG_SYS_HUSH_PARSER

#if !defined(CONFIG_IPROC_NO_DDR)
/* 
 * SHMOO and28 needs about 64K storage for calibration process 
 * The buffter will be released after trace calibration is done
 * and final config saved into flash 
 * For Greyhound, we use the second BTCM as this buffer
 */
#define CONFIG_SHMOO_SRAM_BUF                   0x01080000

/* SHMOO values reuse */
#define CONFIG_SHMOO_AND28_REUSE                1
/* Memory test address to verify stored SHMOO values */
#define CONFIG_SHMOO_REUSE_MEMTEST_START        0x61000000
/* Memory test address to verify stored SHMOO values */
#define CONFIG_SHMOO_REUSE_MEMTEST_LENGTH       0x1000
/* Offset of SPI flash to save Shmoo values */
#define CONFIG_SHMOO_REUSE_QSPI_OFFSET          0x00140000
/* Offset of NOR flash to save Shmoo values */
#define CONFIG_SHMOO_REUSE_NOR_OFFSET           0x00140000
/* Offset of NAND flash to save Shmoo values */
#define CONFIG_SHMOO_REUSE_NAND_OFFSET          0x00140000
/* Range for the partition to support NAND bad blocks replacement */
#define CONFIG_SHMOO_REUSE_NAND_RANGE           SZ_256K
/* Delay to wait for the magic character to force Shmoo; 0 to disable delay */
#define CONFIG_SHMOO_REUSE_DELAY_MSECS          50
#endif /* !CONFIG_IPROC_NO_DDR */

/* General U-Boot configuration */
#define CONFIG_SYS_HZ                       1000
#define CONFIG_SYS_CBSIZE                   1024    /* Console buffer size */
#define CONFIG_SYS_PBSIZE                   (CONFIG_SYS_CBSIZE + sizeof(CONFIG_SYS_PROMPT) + 16) /* Printbuffer size */
#define CONFIG_SYS_MAXARGS                  64
#define CONFIG_SYS_BARGSIZE                 CONFIG_SYS_CBSIZE
#define CONFIG_SYS_PROMPT                   "m4300r2> "
#define CONFIG_CMD_CONSOLE
#define CONFIG_CMD_MEMORY
#define CONFIG_CMDLINE_EDITING
#define CONFIG_BOOTDELAY                    3
#define CONFIG_BOOTCOMMAND                  ""

/* For booting Linux */
#define CONFIG_CMDLINE_TAG                  1 /* ATAG_CMDLINE setup */
#define CONFIG_SETUP_MEMORY_TAGS            1 /* ATAG_MEM setup     */
#define CONFIG_INITRD_TAG                   1 /* Send INITRD params */

/* debug purpose */
#define CONFIG_CMD_IMI                      1
#define CONFIG_CMD_BOOTZ                    1

/* Undefine config_defaults.h */
#undef CONFIG_CMD_NFS
#undef CONFIG_GENERIC_MMC
#undef CONFIG_BOOTM_NETBSD
#undef CONFIG_BOOTM_OSE
#undef CONFIG_BOOTM_RTEMS

/* FastPath */
#define CONFIG_NETMASK          255.255.255.0
#define CONFIG_IPADDR           10.130.184.162
#define CONFIG_GATEWAYIP        10.130.184.1
#define CONFIG_SERVERIP         10.130.135.120
#define CONFIG_ETHADDR          03:04:05:06:07:90

#define CONFIG_CMD_JFFS2
#define CONFIG_JFFS2_CMDLINE
#define CONFIG_CMD_LOADB

/* below bootargs_set variable will overwrite the bootargs */
#undef CONFIG_BOOTARGS

#define CONFIG_VERSION_VARIABLE     /* Enabled UBOOT build date/time id string */
#define CONFIG_LVL7_VERSION_UPDATE  /* Ensure UBOOT build date/time is updated */
#define CONFIG_IDENT_STRING         "VerNo=1.0.0.8"

#define MTDPARTS_DEFAULT            "mtdparts=nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs)"

#define CONFIG_EXTRA_ENV_SETTINGS      \
  "ethact=bcmiproc_eth-0\0" \
  "et0phyaddr=0\0" \
  "boardtype=ranger2\0" \
  "boardid=0\0" \
  "baudrate=115200\0" \
  "bootdelay=3\0" \
  "consoledev=ttyS0\0" \
  "ipaddr=10.130.184.162\0" \
  "serverip=10.130.135.120\0" \
  "netmask=255.255.255.0\0" \
  "uboot=rakoduri/m4300_r2/u-boot.bin\0" \
  "stk_img=rakoduri/m4300.stk\0" \
  "tftpblocksize=512\0" \
  "nuke_env=sf probe 0; sf erase 0x100000 0x20000\0" \
  "nuke_ddr=sf probe 0; sf erase 0x140000 0x40000\0" \
  "factory_reset=run nuke_env; run nuke_ddr; reset\0" \
  "flash_uboot=tftpboot $loadaddr $uboot; sf probe 0; sf erase 0x0 0x100000; sf write $loadaddr 0x0 $filesize; sf read $dummyaddr 0x0 $filesize; cmp.b $loadaddr $dummyaddr $filesize\0" \
  "active=image1\0" \
  "backup=image2\0" \
  "loglevel=0\0" \
  "fs_mount=ubi part nand0,4 0x0;ubifsmount fs\0" \
  "boot_image1=setenv active image1;saveenv;ubifsload ${loadaddr} /image1;bootm ${bootaddr}#ranger2\0"   \
  "boot_image2=setenv active image2;saveenv;ubifsload ${loadaddr} /image2;bootm ${bootaddr}#ranger2\0"   \
  "boot_active=ubifsload ${loadaddr} /${active};bootm ${bootaddr}#ranger2\0" \
  "mtdparts=mtdparts=nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs)\0" \
  "bootargs_set=setenv bootargs console=${consoledev},${baudrate}n8 maxcpus=1 mem=1008M ubi.mtd=fs config=${boardtype} loglevel=${loglevel} envaddr=0xF0100000 mtdparts=spi1.0:1024k(u-boot),128k(env),128k(vpd),256k(shmoo),512k(res)\\\\;nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs)\0" \
  "tftp_boot=run bootargs_set; dhcp ${loadaddr} ${stk_img}; bootm ${bootaddr}#ranger2\0" \
  "nand_boot=run bootargs_set;run fs_mount;run boot_active;run boot_image1;run boot_image2\0" \
  "loadaddr=0x62000000\0" \
  "bootaddr=0x62000074\0" \
  "dummyaddr=0x71000000\0"

#define LVL7_FIXUP 1

#ifdef LVL7_FIXUP

#define CONFIG_SILENT_CONSOLE                   1

#undef  CONFIG_ENV_OFFSET
#define CONFIG_ENV_OFFSET                       0x100000  /* 1MB offset */

#undef  CONFIG_ENV_SIZE
#define CONFIG_ENV_SIZE                         0x20000   /* 128k */

#undef  CONFIG_SHMOO_REUSE_QSPI_OFFSET
#define CONFIG_SHMOO_REUSE_QSPI_OFFSET          0x140000  /* 1024k + 128 + 128k offset */

#define CONFIG_CMD_UBI
#define CONFIG_CMD_UBIFS
#define CONFIG_UBI_NO_MSGS
#define CONFIG_RBTREE
#define CONFIG_LZMA
#define CONFIG_LZO
#define CONFIG_MTD_DEVICE
#define CONFIG_MTD_PARTITIONS
#define CONFIG_CMD_MTDPARTS

#define CONFIG_USE_BOARD_RESET     1     /* Enable board specific reset API */

/* FASTPATH overrides */
#undef CONFIG_BOOTFILE
#define CONFIG_BOOTFILE      "image1"

#undef MTDIDS_DEFAULT
#define MTDIDS_DEFAULT       "nand0=nand_iproc.0"

#undef MTDPARTS_DEFAULT
#define MTDPARTS_DEFAULT     "mtdparts=nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs)"

#undef CONFIG_BOOTCOMMAND
#define CONFIG_BOOTCOMMAND   "run bootargs_set; run nand_boot; run tftp_boot"

#endif

#endif /* __M4300_R2__ */
