#ifndef __IPM6100_VEGA_H
#define __IPM6100_VEGA_H

#include <asm/sizes.h>

/* Architecture, CPU, etc */
#define CONFIG_ARMV7
#define CONFIG_IPROC (1)
#define CONFIG_NORTHSTAR (1)
#define CONFIG_IPM6100_VEGA (1)

#define CONFIG_IPROC_MMU	(1)
#define CONFIG_L2_OFF				/* Disable L2 cache */
#define CONFIG_SYS_ARM_CACHE_WRITETHROUGH (1)
#define CONFIG_MACH_TYPE			0xbb8

#define CONFIG_MISC_INIT_R			/* Call board's misc_init_r function */

/* Interrupt configuration */
#define CONFIG_USE_IRQ          1	/* we need IRQ stuff for timer	*/

/* Memory Info */
#if (defined(CONFIG_L2C_AS_RAM ) && defined(CONFIG_NO_CODE_RELOC))
#define CONFIG_SYS_MALLOC_LEN 			0x8000  	/* see armv7/start.S. */
#else
/* #define CONFIG_SYS_MALLOC_LEN 			SZ_256K */ 	/* see armv7/start.S. */ 
#define CONFIG_SYS_MALLOC_LEN 			SZ_32M  	/* see armv7/start.S. */
#endif

#define CONFIG_STACKSIZE_IRQ			(4096)
#define CONFIG_STACKSIZE_FIQ			(4096)

#ifdef CONFIG_L2C_AS_RAM
#define CONFIG_PHYS_SDRAM_0				0xc0000000
#define CONFIG_L2_CACHE_SIZE			0x40000  
#define CONFIG_PHYS_SDRAM_1				0x80000000
#define CONFIG_LOADADDR					0x90000000 /* default destination location for tftp file (tftpboot cmd) */
#define CONFIG_PHYS_SDRAM_RSVD_SIZE		0x0 /* bytes reserved from CONFIG_PHYS_SDRAM_1 for custom use */
#else
#define CONFIG_PHYS_SDRAM_0				0x00000000
#define CONFIG_PHYS_SDRAM_1				0x00000000
#define CONFIG_LOADADDR					0x90000000 /* default destination location for tftp file (tftpboot cmd) */
#define CONFIG_PHYS_SDRAM_RSVD_SIZE		0x00100000 /* bytes reserved from CONFIG_PHYS_SDRAM_1 for custom use */
#endif

/* Where kernel is loaded to in memory */
#define CONFIG_SYS_LOAD_ADDR				0x90000000
#define LINUX_BOOT_PARAM_ADDR				0x80200000 /* default mapped location to store the atags pointer */

#define CONFIG_SYS_MEMTEST_START			CONFIG_PHYS_SDRAM_1
#define CONFIG_SYS_MEMTEST_END			(CONFIG_PHYS_SDRAM_1+CONFIG_PHYS_SDRAM_1_SIZE)
#define CONFIG_NR_DRAM_BANKS				1

#define CONFIG_SYS_SDRAM_BASE		(CONFIG_PHYS_SDRAM_1 + CONFIG_PHYS_SDRAM_RSVD_SIZE)
/* CONFIG_SYS_TEXT_BASE is where u-boot is loaded by boot1 */
#ifdef CONFIG_L2C_AS_RAM
#define CONFIG_SYS_STACK_SIZE		(0x00010000) /* 64K */       
#else
#define CONFIG_SYS_STACK_SIZE		(0x00010000) /* 64K */       
#endif

#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_PHYS_SDRAM_0 + CONFIG_SYS_STACK_SIZE - 16)
//#define CONFIG_SYS_INIT_SP_ADDR		(CONFIG_PHYS_SDRAM_1 + CONFIG_SYS_STACK_SIZE - 16)

/* Clocks */
#define CONFIG_SYS_REF_CLK			(25000000) /*Reference clock = 25MHz */
#define CONFIG_SYS_REF2_CLK			(200000000) /*Reference clock = 25MHz */

#define CONFIG_ENV_OVERWRITE	/* Allow serial# and ethernet mac address to be overwritten in nv storage */
#ifdef CONFIG_L2C_AS_RAM
#define CONFIG_ENV_SIZE			0x10000 /* 64K */
#else
#define CONFIG_ENV_SIZE			0x10000 /* 64K */
#endif
/* NO flash */
#define CONFIG_SYS_NO_FLASH		/* Not using NAND/NOR unmanaged flash */

/* Ethernet configuration */
#ifndef CONFIG_NO_CODE_RELOC
#define CONFIG_BCMIPROC_ETH
#endif
//#define CONFIG_BCMHANA_ETH
#define CONFIG_NET_MULTI

/* DMA configuration */
//#define CONFIG_BCM5301X_DMA

/* General U-Boot configuration */
#define CONFIG_SYS_CBSIZE			1024	/* Console buffer size */
#define CONFIG_SYS_PBSIZE			(CONFIG_SYS_CBSIZE +	sizeof(CONFIG_SYS_PROMPT) + 16) /* Printbuffer size */
#define CONFIG_SYS_MAXARGS			64
#define CONFIG_SYS_BARGSIZE		CONFIG_SYS_CBSIZE

//#define CONFIG_VERSION_VARIABLE	/* Enabled UBOOT build date/time id string */
#define CONFIG_AUTO_COMPLETE
#define CONFIG_SYS_HUSH_PARSER
#define CONFIG_SYS_PROMPT_HUSH_PS2	"> "
#define CONFIG_CMDLINE_EDITING
#define CONFIG_SYS_LONGHELP

#define CONFIG_CRC32_VERIFY		/* Add crc32 to memory verify commands */
#define CONFIG_MX_CYCLIC			/* Memory display cyclic */

#define CONFIG_CMDLINE_TAG				/* ATAG_CMDLINE setup */
#define CONFIG_SETUP_MEMORY_TAGS		/* ATAG_MEM setup */

#define CONFIG_SYS_PROMPT					"u-boot> "  

//#include <config_cmd_default.h>
#define CONFIG_CMD_CONSOLE
#define CONFIG_ENV_IS_NOWHERE
#define CONFIG_CMD_NFS
#undef CONFIG_GENERIC_MMC
#define CONFIG_CMD_NET
#define CONFIG_CMD_PING
#define CONFIG_CMD_MEMORY
//#define CONFIG_CMD_MISC
#define CONFIG_CMD_LICENSE

#define CONFIG_CMD_NAND 
#define CONFIG_IPROC_NAND 

#define CONFIG_ETHADDR		d4:ae:52:bc:a5:08
#define CONFIG_NETMASK		255.255.255.0
#define CONFIG_IPADDR		10.130.87.67
#define CONFIG_GATEWAYIP	10.130.87.1
#define CONFIG_SERVERIP		10.130.135.120


#ifdef UBOOT_MDK
#define CONFIG_GMAC_NUM		2 
#else
#define CONFIG_GMAC_NUM		1 
#endif

#ifdef CONFIG_IPROC_BOARD_DIAGS
#define IPROC_BOARD_DIAGS 	/* Enabling this fails booting on HR board */
#endif

#define IPROC_BOARD_DIAGS
#ifdef IPROC_BOARD_DIAGS
//POST related defines
#define CONFIG_HAS_POST
//define post to support diags available, this is a ORed list
//See bitmask definition in post.h
#define CONFIG_POST      (CONFIG_SYS_POST_PWM | CONFIG_SYS_POST_QSPI|CONFIG_SYS_POST_NAND|\
                          CONFIG_SYS_POST_UART |CONFIG_SYS_POST_MEMORY|CONFIG_SYS_POST_GPIO|\
                          CONFIG_SYS_POST_GSIO_SPI | CONFIG_SYS_POST_USB20 | CONFIG_SYS_POST_PCIE |\
                          CONFIG_SYS_POST_I2C | CONFIG_SYS_POST_MMC | CONFIG_SYS_POST_I2S | CONFIG_SYS_POST_USB30|\
                          CONFIG_SYS_POST_I2S_W8955 | CONFIG_SYS_POST_SPDIF | CONFIG_SYS_POST_RGMII |CONFIG_SYS_POST_VOIP)// | CONFIG_SYS_POST_SATA)

#define CONFIG_CMD_DIAG
//#define CONFIG_SWANG_DEBUG_BUILD

#ifdef CONFIG_SWANG_DEBUG_BUILD
#define CONFIG_SKIP_LOWLEVEL_INIT
#endif
/*
#define CONFIG_DIAGS_MEMTEST_START                (CONFIG_PHYS_SDRAM_1 + 0x1000000 )//offset by 16 MB if not running from L2
#define CONFIG_DIAGS_MEMTEST_END                  0x8000000
*/
#define CONFIG_DIAGS_MEMTEST_START                0x81000000//offset by 16 MB if not running from L2
#define CONFIG_DIAGS_MEMTEST_END                  0xC0000000

//Chip Common A UART 0
//#define CONFIG_SYS_NS16550_COM1                       (0x18000300)
#ifdef CONFIG_NORTHSTAR_COM2
// Enabling COM2 will hang the HR board. 
//Chip Common A UART 1
#define CONFIG_SYS_NS16550_COM2                 (0x18000400)
#endif
//Chip Common B UART 0
#define CONFIG_SYS_NS16550_COM3                 (0x18008000)

#define  CONFIG_UART0_INDEX              1
#define  CONFIG_UART1_INDEX              2
#define  CONFIG_UART2_INDEX              3


#define CONFIG_HAS_PWM
#define CONFIG_IPROC_GPIO
#define CONFIG_IPROC_GSIOSPI


/* USB */
#define CONFIG_CMD_USB
#ifdef CONFIG_CMD_USB
#define CONFIG_USB_EHCI
#define CONFIG_USB_EHCI_IPROC
#define CONFIG_USB_STORAGE
#define CONFIG_CMD_FAT
#define CONFIG_DOS_PARTITION
#define CONFIG_LEGACY_USB_INIT_SEQ
#endif /* CONFIG_CMD_USB */

/* PCIE */
#define CONFIG_CMD_PCI
#define CONFIG_CMD_PCI_ENUM
#define CONFIG_PCI
#define CONFIG_PCI_SCAN_SHOW
#define CONFIG_IPROC_PCIE

/* I2C */
#define CONFIG_CMD_I2C
#define CONFIG_IPROC_I2C
#define CONFIG_SYS_I2C_SPEED    0    	/* Default on 100KHz */
#define CONFIG_SYS_I2C_SLAVE	0xff	/* No slave address */

/* SDIO */
#define CONFIG_CMD_MMC
#define CONFIG_GENERIC_MMC
#define CONFIG_IPROC_MMC

/* Sound */
#define CONFIG_IPROC_I2S
#define CONFIG_IPROC_PCM

/* USB3.0 */
#define CONFIG_USB_XHCI_IPROC

/* QSPI */
#define CONFIG_CMD_SPI
#define CONFIG_IPROC_QSPI


/* MDIO */
#define CONFIG_IPROC_MDIO

/* SATA */
//#define CONFIG_CMD_SATA
//#define CONFIG_SCSI_AHCI
//#define CONFIG_SYS_SATA_MAX_DEVICE    1
//#define CONFIG_SYS_SCSI_MAX_SCSI_ID   0xff

#define CONFIG_NS_DMA
#ifndef CONFIG_NO_SLIC
#define CONFIG_IPROC_SLIC
#endif
#endif

#define CONFIG_BOARD_LATE_INIT
#define CONFIG_PHYS_SDRAM_1_SIZE			0x40000000 /* 1 GB */
#define IPROC_ETH_MALLOC_BASE 0xD00000

#undef CONFIG_ENV_IS_NOWHERE

/* Enable generic u-boot SPI flash drivers and commands */
#define CONFIG_CMD_SF
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_STMICRO_NS
#define CONFIG_SPI_FLASH_MACRONIX_NS
#define CONFIG_SPI_FLASH_SPANSION
#define CONFIG_SPI_FLASH_SST
#define CONFIG_SPI_FLASH_WINBOND
#define CONFIG_SPI_FLASH_ATMEL

/* SPI flash configurations for Northstar */
#define CONFIG_IPROC_QSPI
#define CONFIG_IPROC_QSPI_BUS                   0
#define CONFIG_IPROC_QSPI_CS                    0

/* SPI flash configuration - flash specific */
#define CONFIG_IPROC_BSPI_DATA_LANES            1
#define CONFIG_IPROC_BSPI_ADDR_LANES            1
#define CONFIG_IPROC_BSPI_READ_CMD              0x0b
#define CONFIG_IPROC_BSPI_READ_DUMMY_CYCLES     8
#define CONFIG_SF_DEFAULT_SPEED                 50000000
#define CONFIG_SF_DEFAULT_MODE                  SPI_MODE_3


/* Environment variables */
#ifdef CONFIG_NAND_IPROC_BOOT
#define CONFIG_ENV_IS_IN_NAND                   1
#define CONFIG_ENV_OFFSET                       0x200000
#define CONFIG_ENV_RANGE                        0x200000
#else
#ifdef CONFIG_SPI_FLASH
#define CONFIG_ENV_IS_IN_SPI_FLASH              1
#define CONFIG_ENV_OFFSET                       0x100000
#define CONFIG_ENV_SPI_MAX_HZ                   10000000
#define CONFIG_ENV_SPI_MODE                     SPI_MODE_3
#define CONFIG_ENV_SPI_BUS                      CONFIG_IPROC_QSPI_BUS
#define CONFIG_ENV_SPI_CS                       CONFIG_IPROC_QSPI_CS
#define CONFIG_ENV_SECT_SIZE                    0x10000     /* 64KB */
#endif /* CONFIG_SPI_FLASH */
#endif /* CONFIG_NAND_BOOT */


#ifdef IPROC_BOARD_DIAGS
#define CONFIG_SYS_HZ       1000*1000
#else
#define CONFIG_SYS_HZ       1000
#endif



/* Environment variables for NAND flash */
#define CONFIG_CMD_NAND 
#define CONFIG_IPROC_NAND 
#define CONFIG_SYS_MAX_NAND_DEVICE			1
#define CONFIG_SYS_NAND_BASE		        0xdeadbeef
#define CONFIG_SYS_NAND_ONFI_DETECTION

/* Serial Info */
#define CONFIG_SYS_NS16550
#define CONFIG_SYS_NS16550_SERIAL
#define CONFIG_CONS_INDEX					1
#if 0 /*CCB*/
#define CONFIG_SYS_NS16550_COM1			(0x18037000)
#define CONFIG_SYS_NS16550_REG_SIZE		(-4)	/* Post pad 3 bytes after each reg addr */
#endif
#if 1 /*CCA*/ 
#if defined(CONFIG_HURRICANE2)
#define CONFIG_SYS_NS16550_COM1			(0x18000400) /* CCA UART 1 */
#else
#define CONFIG_SYS_NS16550_COM1			(0x18000300) /* CCA UART 0 */
#endif
#define CONFIG_SYS_NS16550_REG_SIZE		(1)	/* no padding */
#endif
#define CONFIG_SYS_NS16550_CLK			iproc_get_uart_clk(0)
#define CONFIG_BAUDRATE					115200
#define CONFIG_SYS_BAUDRATE_TABLE	{9600, 19200, 38400, 57600, 115200} 


#define CONFIG_BOOTFILE         "image1"


#define CONFIG_BOOTDELAY	1	/* User can hit a key to abort kernel boot and stay in uboot cmdline */

#define CONFIG_CMD_SAVEENV

#define CONFIG_INITRD_TAG        1       /*  send initrd params           */
#define CONFIG_CMD_CONSOLE
#define CONFIG_CMD_NET
#define CONFIG_CMD_PING
#define CONFIG_CMD_MEMORY
#define CONFIG_CMD_RUN
#define CONFIG_CMD_MTDPARTS
#define CONFIG_MTD_DEVICE
#define MTDIDS_DEFAULT      "nand0=nand_iproc.0"
#define MTDPARTS_DEFAULT    "mtdparts=mtdparts=nand_iproc.0:1024k(nboot),512k(nenv),64k(none),256k(shmoo),260096k(fs)"
#define CONFIG_SYS_HUSH_PARSER
#define CONFIG_CMD_DHCP
#define CONFIG_CMD_BOOT
#define CONFIG_CMD_LICENSE
#define CONFIG_CMD_ECHO
#define CONFIG_CMD_ITEST
#define CONFIG_CMD_MISC
#define CONFIG_CMD_SOURCE

#define CONFIG_STANDALONE_LOAD_ADDR 0x81000000

#define CONFIG_CMD_UBI
#define CONFIG_CMD_UBIFS
#define CONFIG_UBI_NO_MSGS
#define CONFIG_RBTREE
#define CONFIG_LZO
#define CONFIG_MTD_PARTITIONS


/* new uImage format support */
#define CONFIG_FIT    1
#define CONFIG_OF_LIBFDT  1
#define CONFIG_FIT_VERBOSE  1 /* enable fit_format_{error,warning}() */


#define CONFIG_SILENT_CONSOLE  /* silent console */
#define CONFIG_BAUDRATE 115200
#undef CONFIG_BOOTCOMMAND
#define CONFIG_BOOTCOMMAND      "run bootargs_set;run fs_mount;run boot_active;run boot_image1;run boot_image2"

#define CONFIG_BOOTARGS "console=ttyS0,115200n8 maxcpus=2 mem=1008M mtdparts=spi1.0:1024k(u-boot),64k(env),256k(shmoo);nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs) ubi.mtd=fs config=vega loglevel=3 envaddr=0x1E100000"

#define CONFIG_VERSION_VARIABLE /* Enabled UBOOT build date/time id string */
#define CONFIG_LVL7_VERSION_UPDATE /* Ensure UBOOT build date/time is updated */
#define CONFIG_IDENT_STRING "VerNo=1.0.0.9"

#define CONFIG_EXTRA_ENV_SETTINGS      \
  "active=image1\0" \
  "backup=image2\0" \
  "brd_type=vega\0" \
  "loglevelval=3\0"     \
  "fs_mount=ubi part nand0,4 0x0;ubifsmount fs\0" \
  "erase_env=sf probe 0;sf erase 0x100000 0x10000;reset\0"   \
  "boot_image1=setenv active image1;saveenv;ubifsload ${loadaddr} /image1;bootm ${bootaddr}#vega\0"   \
  "boot_image2=setenv active image2;saveenv;ubifsload ${loadaddr} /image2;bootm ${bootaddr}#vega\0"   \
  "boot_active=ubifsload ${loadaddr} /${active};bootm ${bootaddr}#vega\0" \
  "mtdparts=mtdparts=nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs)\0" \
  "bootargs_set=setenv bootargs console=ttyS0,${baudrate}n8 maxcpus=2 mem=1008M ubi.mtd=fs config=${brd_type} loglevel=${loglevelval} envaddr=0x1E100000 mtdparts=spi1.0:1024k(u-boot),64k(env),256k(shmoo)\\\\;nand_iproc.0:1024k(nboot),512k(nenv),256k(vpd),256k(shmoo),260096k(fs)\0" \
  "loadaddr=0x90000000\0"                                         \
  "bootaddr=0x90000074\0"     \
  "hostname=northstar\0"     \
  "machid=0xbb8\0"     \
  "tftpblocksize=512\0"     \
  "vlan1ports=0 1 2 3 8u\0"     \
  "brcmtag=0\0"     



#endif /* __IPM6100_VEGA_H */



	
