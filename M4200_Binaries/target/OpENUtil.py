
#
# (C) Copyright Broadcom Corporation 2012-2014
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

import OpEN_py as OpEN

class OpENUtil:
  def get_client(self):
    return self.m_client

  def __init__(self):
    self.m_client = None

  def getCharBuffer(self, size, init=None):
    ret = OpEN.charArray(size)
    if init != None:
      if len(init) >= size:
        count = size
      else:
        count = len(init)
      for x in xrange(0, count):
        ret[x] = init[x]
      ret[count] = '\0'
    else:
      for x in xrange(0, size):
        ret[x] = '\0' 
    return ret

  def getUCharBuffer(self, size, init=None):
    ret = OpEN.ucharArray(size)
    if init != None:
      if len(init) >= size:
        count = size
      else:
        count = len(init)
      for x in xrange(0, count):
        ret[x] = ord(init[x])
      ret[count] = ord('\0')
    else:
      for x in xrange(0, size):
        ret[x] = ord('\0')
    return ret

  def getIntBuffer(self, size):
    ret = OpEN.intArray(size)
    return ret

  def connect(self, appname):
    maxtries = 100
    handle = OpEN.openapiClientHandle_s()
    ret = OpEN.openapiClientRegister(appname, handle)
    self.m_client = handle
    if ret == OpEN.OPEN_E_NONE:
      ret = OpEN.openapiConnectivityCheck(self.m_client)
      count = 0
      while ret != OpEN.OPEN_E_NONE and count < maxtries:
        count += 1 
        sleep(1)
        ret = OpEN.openapiConnectivityCheck(self.m_client)
    return ret

  def getNetworkOSVersion(self):
    size = 100
    switch_os_revision_string = self.getCharBuffer(size)
    switch_os_revision = OpEN.open_buffdesc()
    switch_os_revision.pstart = switch_os_revision_string
    switch_os_revision.size = size 
    ret = OpEN.openapiNetworkOSVersionGet(self.m_client, switch_os_revision)
    if ret != OpEN.OPEN_E_NONE:
      print "Network OS version retrieve error\n"
    else:
      print "ICOS Version %s\n" % switch_os_revision_string.cast()

  def getAPIVersion(self):
    revData = OpEN.open_revision_data_t()
    ret = OpEN.openapiApiVersionGet(self.m_client, revData)
    if ret != OpEN.OPEN_E_NONE:
      print "OpEN version retrieve error\n"
    else:
      print "OpEN version = %u.%u.%u.%u\n" % (revData.release, revData.version, revData.maint_level, revData.build_num)


  def terminate(self):
    pass
