#!/bin/sh

# This is a script which will update u-boot on the switch.

# For any code update changes, this is the only file which needs to 
# be changed (i.e. neither startup code nor operational code change).

APPLICATION_PATH=/mnt/application
IMAGE_PATH=/mnt/fastpath
UBOOT_WILDCARD=u-boot
UBOOT_NAME=u-boot.bin
UBOOT_DEV=/dev/mtd0

VERBOSE=1
REBOOT=0

IMAGE_FILE=$1

validate_board_rev()
{
  BRD_REV=$(/usr/bin/i2cget -y 0 0x30 0x5 b)
  BRD_REV=$((${BRD_REV} & 0x07))

  ### Do not upgrade ALPHA boards with u-boot ver 1.0.0.5 or above
  if [ ${BRD_REV} -lt 3 ]; then
     echo "Note: Please do not upgrade u-boot image on ALPHA boards using this method."
     echo "      Upgrade the u-boot image from u-boot CLI.  Maximum supported u-boot verion for ALPHA boards: 1.0.0.4"
     exit 0
  fi
}

extract_uboot()
{
if [ $VERBOSE = 1 ]
then
	echo "Extracting components.  Please wait..."
fi

cd ${APPLICATION_PATH}

if [ -s ${IMAGE_PATH}/${IMAGE_FILE} ]
then
        # Extract once to get the file names (u-boot.bin and its md5sum)
        # exactly as stored in the tarball. Then extract again to get 
        # just those files. This saves tmpfs space over extracting the 
        # whole tarball.
        FILES=`extimage -n 3 -i ${IMAGE_PATH}/${IMAGE_FILE} -o - | tar tzf - | grep ${UBOOT_WILDCARD}`
	    extimage -n 3 -i ${IMAGE_PATH}/${IMAGE_FILE} -o - | tar xzf - ${FILES}

        # update UBOOT_NAME with name of extracted file
        UBOOT_NAME=$(ls ${UBOOT_WILDCARD}*.bin)  
else
        if [ $VERBOSE = 1]
	then
        echo "Image file ${IMAGE_FILE} not found!"
	fi
	exit 2
fi
}

update_uboot()
{
if [ $VERBOSE = 1 ]
then
	echo "Integrity-checking components.  Please wait..."
fi
   
# Go to the applicaton directory
cd ${APPLICATION_PATH}

# Check for PPCBoot...
if [ -s ${UBOOT_NAME}.md5sum ]
then

	if [ ! -s ${UBOOT_NAME} ]
	then
		if [ $VERBOSE = 1 ]
		then
			echo "$UBOOT_NAME not found!"
		fi
		exit 2
	else
		MD5SUM_RESULTS=`md5sum -c ${UBOOT_NAME}.md5sum`
		if [ "$MD5SUM_RESULTS" != "${UBOOT_NAME}: OK" ]
		then
			if [ $VERBOSE = 1 ]
			then
				echo "${UBOOT_NAME}: md5sum failed!"
			fi
			exit 3
		else
		        if [ $VERBOSE = 1 ]
			then
			        echo "Erasing ${UBOOT_DEV}..."
		        fi
			if ! eraseall $UBOOT_DEV >/dev/null 2>&1
			then
				if [ $VERBOSE = 1 ]
				then
					echo "Erase of ${UBOOT_DEV} failed!"
				fi
				exit 4
			fi
		        if [ $VERBOSE = 1 ]
			then
			        echo "Flashing ${UBOOT_DEV}..."
		        fi
			if ! nandwrite $UBOOT_DEV $UBOOT_NAME -p >/dev/null 2>&1
			then
				if [ $VERBOSE = 1 ]
				then
					echo "Copy of ${UBOOT_NAME} to ${UBOOT_DEV} failed!"
				fi
				exit 5
			fi
			REBOOT=1
		fi
	fi
fi

sync 

# Don't leave the file hanging around tmpfs taking up space.
rm -f ${UBOOT_NAME}
rm -f ${UBOOT_NAME}.md5sum
if [ $VERBOSE = 1 ]
then
	echo "Done."
fi

return
}

##############
# START HERE #
##############

validate_board_rev

# Keep the script quiet when invoked by the CLI
if [ $# = 2 ]
then
	if [ "$2" = "-q" ]
	then
		VERBOSE=0
	else
		echo "Invalid arguments to ${0}!"
		exit 1
	fi
else
	if [ $# -gt 2 ]
	then
		echo "Too many arguments to ${0}!"
		exit 1
	fi
	if [ $# -lt 1 ]
	then
	        echo "Too few agruments to ${0}! Image name must be provided!"
		exit 1
        fi
fi

extract_uboot
update_uboot

exit 0
